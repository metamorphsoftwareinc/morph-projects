# QUAD DIFFERENTIAL COMPARATORS
## LM139DR
### Semiconductors and Actives > Amplifiers/Buffers > Comparators
***

### Summary
IC QUAD DIFF COMPARATOR 14-SOIC

#### General Description
These devices consist of four independent voltage comparators that are designed to operate from a single power supply over a wide range of voltages. Operation from dual supplies also is possible, as long as the difference between the two supplies is 2 V to 36 V, and VCC is at least 1.5 V more positive than the input common-mode voltage. Current drain is independent of the supply voltage. The outputs can be connected to other open-collector outputs to achieve wired-AND relationships.

The LM139 and LM139A are characterized for operation over the full military temperature range of –55°C to 125°C. The LM239 and LM239A are characterized for operation from –25°C to 125°C. The LM339 and LM339A are characterized for operation from 0°C to 70°C. The LM2901, LM2901AV, and LM2901V are characterized for operation from –40°C to 125°C.

### Connectors 
- ***1INp* [StdPin]:** Non-inverting input 1 
- ***2INp* [StdPin]:** Non-inverting input 2 
- ***3INp* [StdPin]:** Non-inverting input 3 
- ***4INp* [StdPin]:** Non-inverting input 4 
- ***1INn* [StdPin]:** Inverting input 1
- ***2INn* [StdPin]:** Inverting input 2 
- ***3INn* [StdPin]:** Inverting input 3
- ***4INn* [StdPin]:** Inverting input 4
- ***1OUT* [StdPin]:** Output 1
- ***2OUT* [StdPin]:** Output 2
- ***3OUT* [StdPin]:** Output 3
- ***4OUT* [StdPin]:** Output 4
- ***VCC* [PwrGnd_TwoPort]:** Supply voltage
- ***GND* [PwrGnd_TwoPort]:** Power supply ground


