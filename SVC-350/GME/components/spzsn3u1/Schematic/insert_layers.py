import os
from os import remove, close
from tempfile import mkstemp
from shutil import move


pinFix = 'direction="pas"'
ratioFix = 'ratio="15"'
visibleFix = 'visible="on"'
fontFix = 'font="vector"'
pTextFix = 'size="1.016"'
sTextFix = 'size="1.778"'
circleRadiusFix = 'radius="0.1542"'
circleWidthFix = 'width="0"'
wireWidthFix = 'width="0.1542"'
silkScreen = 'layer="21"'
numEdits = 0
numFiles = 0

def insertLayers(file_path):
	fh, abs_path = mkstemp()
	new_file = open(abs_path, 'w')
	old_file = open(file_path)
	rangeFlag = False
	n = 0

	layer_file = open("layers.txt")

	for num, line in enumerate(old_file):
		if "drawing" in line:
			new_file.write(line)
			n += 1
			for l in layer_file.readlines():
				new_file.write(l)
		elif "layers" in line:
			continue
		else:
			new_file.write(line)


	

	new_file.close()
	close(fh)
	old_file.close()
	remove(file_path)

	move(abs_path, file_path)
	return n


def LineRemove(file_path, pattern, removeFlag, r = ""):
	fh, abs_path = mkstemp()
	new_file = open(abs_path, 'w')
	old_file = open(file_path)
	rangeFlag = False
	n = 0
	
	for num, line in enumerate(old_file):
		if (len(r) > 0):
			if r in line:
				rangeFlag = not rangeFlag
		else:
			rangeFlag = True

		if (rangeFlag):
			if pattern in line:  
				if removeFlag in line.lower():
					n+=1
				else:
					new_file.write(line)
			else:
				new_file.write(line)
		else:
			new_file.write(line)

	new_file.close()
	close(fh)
	old_file.close()
	remove(file_path)

	move(abs_path, file_path)
	return n


for root, dirs, files in os.walk("."):
	for f in files:
		if f.endswith(".lbr"):	
			
			fpath = os.path.abspath(os.path.join(root, f))

			numEdits += LineRemove(fpath, "", "", "layers>")
			numEdits += insertLayers(fpath)
			print "Added eagle layers to " + fpath
		

#print "Made " + str(numEdits) + " edits across " + str(numFiles) + " file(s)."