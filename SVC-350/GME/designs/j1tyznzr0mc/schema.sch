<?xml version="1.0"?>
<eagle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="6.5.0" xmlns="eagle">
  <compatibility />
  <drawing>
    <settings>
      <setting alwaysvectorfont="no" />
      <setting />
    </settings>
    <grid distance="0.01" unitdist="inch" unit="inch" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch" />
    <layers>
      <layer number="1" name="Top" color="4" fill="1" visible="no" active="no" />
      <layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no" />
      <layer number="17" name="Pads" color="2" fill="1" visible="no" active="no" />
      <layer number="18" name="Vias" color="2" fill="1" visible="no" active="no" />
      <layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no" />
      <layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no" />
      <layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="25" name="tNames" color="7" fill="1" visible="no" active="no" />
      <layer number="26" name="bNames" color="7" fill="1" visible="no" active="no" />
      <layer number="27" name="tValues" color="7" fill="1" visible="no" active="no" />
      <layer number="28" name="bValues" color="7" fill="1" visible="no" active="no" />
      <layer number="29" name="tStop" color="7" fill="3" visible="no" active="no" />
      <layer number="30" name="bStop" color="7" fill="6" visible="no" active="no" />
      <layer number="31" name="tCream" color="7" fill="4" visible="no" active="no" />
      <layer number="32" name="bCream" color="7" fill="5" visible="no" active="no" />
      <layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no" />
      <layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no" />
      <layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no" />
      <layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no" />
      <layer number="37" name="tTest" color="7" fill="1" visible="no" active="no" />
      <layer number="38" name="bTest" color="7" fill="1" visible="no" active="no" />
      <layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no" />
      <layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no" />
      <layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no" />
      <layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no" />
      <layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no" />
      <layer number="44" name="Drills" color="7" fill="1" visible="no" active="no" />
      <layer number="45" name="Holes" color="7" fill="1" visible="no" active="no" />
      <layer number="46" name="Milling" color="3" fill="1" visible="no" active="no" />
      <layer number="47" name="Measures" color="7" fill="1" visible="no" active="no" />
      <layer number="48" name="Document" color="7" fill="1" visible="no" active="no" />
      <layer number="49" name="Reference" color="7" fill="1" visible="no" active="no" />
      <layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="91" name="Nets" color="2" fill="1" />
      <layer number="92" name="Busses" color="1" fill="1" />
      <layer number="93" name="Pins" color="2" fill="1" visible="no" />
      <layer number="94" name="Symbols" color="4" fill="1" />
      <layer number="95" name="Names" color="7" fill="1" />
      <layer number="96" name="Values" color="7" fill="1" />
      <layer number="97" name="Info" color="7" fill="1" />
      <layer number="98" name="Guide" color="6" fill="1" />
    </layers>
    <schematic xrefpart="/%S.%C%R" xreflabel="%F%N/%S.%C%R">
      <description />
      <libraries>
        <library name="General_Will">
          <description />
          <packages>
            <package name="SOT363">
              <description />
              <circle x="-1.505" y="1.12" radius="0.1542" width="0" layer="21" />
              <text x="-1.45" y="1.35" size="1.106" layer="25" font="vector" ratio="15" distance="40">&gt;NAME</text>
              <smd name="P$1" x="-0.75" y="0.75" dx="0.5" dy="0.5" layer="1" />
              <smd name="P$2" x="-0.75" y="0" dx="0.5" dy="0.4" layer="1" />
              <smd name="P$3" x="-0.75" y="-0.75" dx="0.5" dy="0.5" layer="1" />
              <smd name="P$4" x="0.75" y="-0.75" dx="0.5" dy="0.5" layer="1" />
              <smd name="P$5" x="0.75" y="0" dx="0.5" dy="0.4" layer="1" />
              <smd name="P$6" x="0.75" y="0.75" dx="0.5" dy="0.5" layer="1" />
              <wire x1="-0.275" y1="1.175" x2="0.275" y2="1.175" width="0.127" layer="21" />
              <wire x1="-0.275" y1="-1.175" x2="0.275" y2="-1.175" width="0.127" layer="21" />
            </package>
            <package name="TO-92">
              <description>&lt;b&gt;TO 92&lt;/b&gt;</description>
              <wire x1="-1.0403" y1="2.4215" x2="-2.0946" y2="-1.651" width="0.1524" layer="21" curve="111.098962" />
              <wire x1="2.0945" y1="-1.651" x2="1.0403" y2="2.421396875" width="0.1524" layer="21" curve="111.099507" />
              <wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.1524" layer="21" />
              <wire x1="-2.6549" y1="-0.254" x2="-2.3807" y2="-0.254" width="0.1524" layer="21" />
              <wire x1="-0.1593" y1="-0.254" x2="0.1593" y2="-0.254" width="0.1524" layer="21" />
              <wire x1="2.3807" y1="-0.254" x2="2.6549" y2="-0.254" width="0.1524" layer="21" />
              <pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796" />
              <pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796" />
              <pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796" />
              <text x="-2.159" y="-2.794" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="8-SOP">
              <description />
              <smd name="P$1" x="-2.75" y="1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$2" x="-2.75" y="0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$3" x="-2.75" y="-0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$4" x="-2.75" y="-1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$5" x="2.75" y="-1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$6" x="2.75" y="-0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$7" x="2.75" y="0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$8" x="2.75" y="1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <wire x1="-2.65" y1="2.65" x2="2.65" y2="2.65" width="0.127" layer="21" />
              <wire x1="-2.65" y1="-2.65" x2="2.65" y2="-2.65" width="0.127" layer="21" />
              <circle x="-3.99" y="2.31" radius="0.1542" width="0" layer="21" />
              <text x="-3.36" y="2.94" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="R-PDSO-G8">
              <description />
              <smd name="P$1" x="-2.7" y="1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$2" x="-2.7" y="0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$3" x="-2.7" y="-0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$4" x="-2.7" y="-1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$5" x="2.7" y="-1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$6" x="2.7" y="-0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$7" x="2.7" y="0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$8" x="2.7" y="1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21" />
              <wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21" />
              <circle x="-3.8" y="2.4" radius="0.1542" width="0" layer="21" />
              <text x="-3.6" y="2.8" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="TRIMPOT">
              <description />
              <pad name="P$1" x="-2.5" y="0" drill="0.8" />
              <pad name="P$2" x="0" y="2.5" drill="0.8" />
              <pad name="P$3" x="2.5" y="0" drill="0.8" />
              <wire x1="-3.6" y1="3.6" x2="-3.6" y2="-3.6" width="0.127" layer="21" />
              <wire x1="-3.6" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21" />
              <wire x1="3.6" y1="-3.6" x2="3.6" y2="3.6" width="0.127" layer="21" />
              <wire x1="3.6" y1="3.6" x2="-3.6" y2="3.6" width="0.127" layer="21" />
              <circle x="-4.445" y="1.27" radius="0.1542" width="0" layer="21" />
              <text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="SP3T">
              <description />
              <pad name="P$1" x="-4" y="0" drill="0.8" />
              <pad name="P$2" x="0" y="0" drill="0.8" />
              <pad name="P$3" x="2" y="0" drill="0.8" />
              <pad name="P$4" x="4" y="0" drill="0.8" />
              <hole x="-6.1" y="0" drill="1.5" />
              <hole x="6.1" y="0" drill="1.5" />
              <wire x1="-7.6" y1="2.15" x2="7.6" y2="2.15" width="0.127" layer="21" />
              <wire x1="7.6" y1="2.15" x2="7.6" y2="-2.15" width="0.127" layer="21" />
              <wire x1="7.6" y1="-2.15" x2="-7.6" y2="-2.15" width="0.127" layer="21" />
              <wire x1="-7.6" y1="-2.15" x2="-7.6" y2="2.15" width="0.127" layer="21" />
              <circle x="-4.5" y="2.5" radius="0.1542" width="0" layer="21" />
              <text x="-4" y="2.5" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="DP3T">
              <description />
              <pad name="P$1" x="-5.27" y="1.25" drill="0.8" />
              <pad name="P$2" x="-3.27" y="1.25" drill="0.8" />
              <pad name="P$3" x="-1.27" y="1.25" drill="0.8" />
              <pad name="P$4" x="2.73" y="1.25" drill="0.8" />
              <hole x="-7.37" y="0" drill="1.5" />
              <hole x="4.83" y="0" drill="1.5" />
              <wire x1="-8.87" y1="3.42" x2="6.33" y2="3.42" width="0.1524" layer="21" />
              <wire x1="6.33" y1="3.42" x2="6.33" y2="-3.42" width="0.1524" layer="21" />
              <wire x1="6.33" y1="-3.42" x2="-8.87" y2="-3.42" width="0.1524" layer="21" />
              <wire x1="-8.87" y1="-3.42" x2="-8.87" y2="3.42" width="0.1524" layer="21" />
              <circle x="-5.77" y="3.77" radius="0.1524" width="0" layer="21" />
              <text x="-5.27" y="3.77" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <pad name="P$5" x="-5.27" y="-1.25" drill="0.8" />
              <pad name="P$6" x="-3.27" y="-1.25" drill="0.8" />
              <pad name="P$7" x="-1.27" y="-1.25" drill="0.8" />
              <pad name="P$8" x="2.73" y="-1.25" drill="0.8" />
            </package>
            <package name="K0002C">
              <description />
              <pad name="ADJ" x="-5.46" y="-1.815" drill="1.2" diameter="2" />
              <pad name="VOUT" x="5.46" y="-1.815" drill="1.2" diameter="2" />
              <hole x="0" y="15.075" drill="4" />
              <hole x="0" y="-15.075" drill="4" />
              <wire x1="-12.7" y1="0" x2="-12.065" y2="3.81" width="0.127" layer="21" />
              <wire x1="-12.065" y1="3.81" x2="-1.905" y2="17.78" width="0.127" layer="21" curve="-53.130102" />
              <wire x1="-1.905" y1="17.78" x2="1.905" y2="17.78" width="0.127" layer="21" curve="-54.815157" />
              <wire x1="12.7" y1="0" x2="12.065" y2="-3.81" width="0.127" layer="21" />
              <wire x1="12.065" y1="-3.81" x2="1.905" y2="-17.78" width="0.127" layer="21" curve="-53.130102" />
              <wire x1="1.905" y1="-17.78" x2="-1.905" y2="-17.78" width="0.127" layer="21" curve="-54.815157" />
              <wire x1="12.7" y1="0" x2="12.065" y2="3.81" width="0.127" layer="21" />
              <wire x1="12.065" y1="3.81" x2="1.905" y2="17.78" width="0.127" layer="21" curve="53.130102" />
              <wire x1="-12.7" y1="0" x2="-12.065" y2="-3.81" width="0.127" layer="21" />
              <wire x1="-12.065" y1="-3.81" x2="-1.905" y2="-17.78" width="0.127" layer="21" curve="53.130102" />
              <smd name="P$3" x="0" y="5.445" dx="10" dy="10" layer="1" />
              <smd name="P$4" x="0" y="-7.26" dx="10" dy="7" layer="1" />
              <smd name="P$5" x="0" y="-1.815" dx="10" dy="5" layer="1" rot="R90" />
              <text x="-3.175" y="19.05" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="RB-15">
              <description />
              <pad name="P$1" x="-2.55" y="2.55" drill="1" rot="R270" />
              <pad name="P$2" x="2.55" y="2.55" drill="1" rot="R270" />
              <pad name="P$3" x="2.55" y="-2.55" drill="1" rot="R270" />
              <pad name="P$4" x="-2.55" y="-2.55" drill="1" rot="R270" />
              <circle x="-4.455" y="3.185" radius="0.1542" width="0" layer="21" />
              <circle x="0" y="0" radius="5" width="0.127" layer="21" />
              <text x="-3.175" y="5.08" size="1.27" layer="25">&gt;NAME</text>
              <text x="-2.54" y="3.175" size="1.27" layer="51">+</text>
              <text x="3.175" y="-1.905" size="1.27" layer="51">-</text>
              <text x="-2.54" y="-1.905" size="1.27" layer="51">~</text>
              <text x="1.27" y="3.175" size="1.27" layer="51">~</text>
            </package>
            <package name="POWERJACK">
              <description />
              <pad name="P$1" x="0" y="0" drill="2" shape="long" />
              <pad name="P$2" x="0" y="6" drill="2" />
              <pad name="P$3" x="-4.7" y="3" drill="2" />
              <circle x="3.21" y="-1.47" radius="0.1542" width="0" layer="21" />
              <wire x1="-4.7" y1="13.7" x2="-4.7" y2="5" width="0.127" layer="21" />
              <wire x1="4.7" y1="0" x2="4.7" y2="13.7" width="0.127" layer="21" />
              <wire x1="4.7" y1="13.7" x2="-4.7" y2="13.7" width="0.127" layer="21" />
              <wire x1="4.7" y1="0" x2="-4.7" y2="0" width="0.127" layer="51" />
              <wire x1="-4.7" y1="0" x2="-4.7" y2="5" width="0.127" layer="51" />
              <wire x1="-2.5" y1="13.7" x2="-2.5" y2="14.4" width="0.127" layer="51" />
              <wire x1="-2.5" y1="14.4" x2="2.5" y2="14.4" width="0.127" layer="51" />
              <wire x1="2.5" y1="14.4" x2="2.5" y2="13.7" width="0.127" layer="51" />
              <text x="5" y="6.3" size="1.27" layer="25" rot="R270">&gt;NAME</text>
            </package>
            <package name="3TERMINALBLOCK">
              <description />
              <pad name="P$1" x="-2.54" y="0" drill="1.2" />
              <pad name="P$2" x="0" y="0" drill="1.2" />
              <pad name="P$3" x="2.54" y="0" drill="1.2" />
              <wire x1="-4.04" y1="-3.25" x2="4.04" y2="-3.25" width="0.127" layer="21" />
              <wire x1="4.04" y1="-3.25" x2="4.04" y2="3.25" width="0.127" layer="21" />
              <wire x1="4.04" y1="3.25" x2="-4.04" y2="3.25" width="0.127" layer="21" />
              <wire x1="-4.04" y1="3.25" x2="-4.04" y2="-3.25" width="0.127" layer="21" />
              <text x="-3.81" y="3.81" size="1.27" layer="25">&gt;NAME</text>
              <circle x="-4.445" y="2.54" radius="0.1542" width="0" layer="21" />
            </package>
            <package name="STACKING_MONO-JACK">
              <description />
              <pad name="G" x="-1.73" y="5.96" drill="1.5" shape="square" rot="R90" />
              <pad name="SB" x="-4.61" y="7.23" drill="1.5" />
              <pad name="ST" x="-7.78" y="8.5" drill="1.5" />
              <pad name="TB" x="-17.3" y="7.23" drill="1.5" />
              <pad name="TT" x="-20.48" y="8.5" drill="1.5" />
              <pad name="TNT" x="-20.48" y="-4.2" drill="1.5" />
              <pad name="SNT" x="-7.78" y="-4.2" drill="1.5" />
              <pad name="TNB" x="-17.3" y="-5.47" drill="1.5" />
              <pad name="SNB" x="-4.61" y="-5.47" drill="1.5" />
              <wire x1="0" y1="9.8" x2="-24.3" y2="9.8" width="0.15239999999999998" layer="21" />
              <wire x1="-24.3" y1="9.8" x2="-24.3" y2="-7.2" width="0.15239999999999998" layer="21" />
              <wire x1="-24.3" y1="-7.2" x2="0" y2="-7.2" width="0.15239999999999998" layer="21" />
              <wire x1="0" y1="-4" x2="8" y2="-4" width="0.15239999999999998" layer="51" />
              <wire x1="8" y1="-4" x2="8" y2="4" width="0.15239999999999998" layer="51" />
              <wire x1="8" y1="4" x2="0" y2="4" width="0.15239999999999998" layer="51" />
              <text x="-22" y="10" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="0" y1="9.8" x2="0" y2="-7.2" width="0.15239999999999998" layer="21" />
            </package>
            <package name="2X12TERMINAL">
              <description />
              <pad name="1-1" x="0" y="0" drill="1.8" shape="square" />
              <pad name="2-1" x="-1.9" y="11.43" drill="1.8" />
              <wire x1="-1.9" y1="-3.6" x2="47.62" y2="-3.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="-3.6" x2="47.62" y2="3.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="3.6" x2="45.72" y2="3.6" width="0.1524" layer="21" />
              <wire x1="45.72" y1="3.6" x2="45.72" y2="13.5" width="0.1524" layer="21" />
              <wire x1="-3.8" y1="13.5" x2="-3.8" y2="3.6" width="0.1524" layer="21" />
              <wire x1="-3.8" y1="3.6" x2="-1.9" y2="3.6" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="3.6" x2="-1.9" y2="-3.6" width="0.1524" layer="21" />
              <text x="-5.08" y="4.445" size="1.016" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
              <pad name="1-2" x="3.81" y="0" drill="1.8" />
              <pad name="1-3" x="7.62" y="0" drill="1.8" />
              <pad name="1-4" x="11.43" y="0" drill="1.8" />
              <pad name="1-5" x="15.24" y="0" drill="1.8" />
              <pad name="1-6" x="19.05" y="0" drill="1.8" />
              <pad name="1-7" x="22.86" y="0" drill="1.8" />
              <pad name="1-8" x="26.67" y="0" drill="1.8" />
              <pad name="1-9" x="30.48" y="0" drill="1.8" />
              <pad name="1-10" x="34.29" y="0" drill="1.8" />
              <pad name="1-11" x="38.1" y="0" drill="1.8" />
              <pad name="1-12" x="41.91" y="0" drill="1.8" />
              <pad name="2-2" x="1.91" y="11.43" drill="1.8" />
              <pad name="2-3" x="5.72" y="11.43" drill="1.8" />
              <pad name="2-4" x="9.53" y="11.43" drill="1.8" />
              <pad name="2-5" x="13.34" y="11.43" drill="1.8" />
              <pad name="2-6" x="17.15" y="11.43" drill="1.8" />
              <pad name="2-7" x="20.96" y="11.43" drill="1.8" />
              <pad name="2-8" x="24.77" y="11.43" drill="1.8" />
              <pad name="2-9" x="28.58" y="11.43" drill="1.8" />
              <pad name="2-10" x="32.39" y="11.43" drill="1.8" />
              <pad name="2-11" x="36.2" y="11.43" drill="1.8" />
              <pad name="2-12" x="40.01" y="11.43" drill="1.8" />
              <wire x1="45.72" y1="13.5" x2="-3.8" y2="13.5" width="0.1524" layer="21" />
            </package>
            <package name="XLRMONOCOMBO">
              <description />
              <pad name="T" x="0" y="0" drill="1.2" />
              <pad name="2" x="-3.1" y="6.6" drill="1.2" />
              <pad name="3" x="-8.3" y="6.6" drill="1.2" />
              <pad name="1" x="-14.4" y="6.6" drill="1.2" />
              <pad name="S" x="-3.1" y="13.9" drill="1.2" />
              <pad name="G" x="-8.3" y="13.9" drill="1.2" />
              <hole x="-12.3" y="18" drill="2.1" />
              <wire x1="1.7" y1="25" x2="-18.3" y2="25" width="0.15239999999999998" layer="21" />
              <wire x1="-18.3" y1="25" x2="-18.3" y2="-1.6" width="0.15239999999999998" layer="21" />
              <wire x1="-18.3" y1="-1.6" x2="1.7" y2="-1.6" width="0.15239999999999998" layer="21" />
              <wire x1="1.7" y1="-1.6" x2="1.7" y2="25" width="0.15239999999999998" layer="21" />
              <text x="-18.8" y="0" size="1.016" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
              <wire x1="-12" y1="25" x2="-12" y2="27.4" width="0.15239999999999998" layer="51" />
              <wire x1="-12" y1="27.4" x2="0" y2="27.4" width="0.15239999999999998" layer="51" />
              <wire x1="0" y1="27.4" x2="0" y2="25" width="0.15239999999999998" layer="51" />
            </package>
            <package name="JACKMONOSWITCHED">
              <description />
              <pad name="S" x="-4.25" y="7.23" drill="1.5" />
              <pad name="T" x="-16.95" y="7.23" drill="1.5" />
              <pad name="SN" x="-4.25" y="-4.2" drill="1.5" />
              <pad name="TN" x="-16.95" y="-4.2" drill="1.5" />
              <hole x="-7.45" y="7.23" drill="2" />
              <hole x="-7.45" y="-4.2" drill="2" />
              <hole x="-13.8" y="5.1" drill="3" />
              <wire x1="0" y1="-6.43" x2="-21.36" y2="-6.43" width="0.15239999999999998" layer="21" />
              <wire x1="-21.36" y1="-6.43" x2="-21.36" y2="9.32" width="0.15239999999999998" layer="21" />
              <wire x1="-21.36" y1="9.32" x2="0" y2="9.32" width="0.15239999999999998" layer="21" />
              <wire x1="0" y1="9.32" x2="0" y2="-6.43" width="0.15239999999999998" layer="21" />
              <wire x1="0" y1="-5.6" x2="2.94" y2="-5.6" width="0.15239999999999998" layer="51" />
              <wire x1="2.94" y1="-5.6" x2="2.94" y2="5.6" width="0.15239999999999998" layer="51" />
              <wire x1="2.94" y1="5.6" x2="0" y2="5.6" width="0.15239999999999998" layer="51" />
              <text x="-20.955" y="10.16" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="DUALNPN">
              <description />
              <wire x1="-13.97" y1="5.08" x2="-13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-13.97" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-8.89" y2="7.62" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-8.89" y2="-7.62" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
              <pin name="B1" x="19.05" y="0" length="middle" direction="pas" rot="R180" />
              <wire x1="13.97" y1="-5.08" x2="13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="13.97" y2="5.08" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="8.89" y2="-7.62" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="8.89" y2="7.62" width="0.254" layer="94" />
              <pin name="C1" x="3.81" y="-7.62" length="middle" direction="pas" />
              <pin name="E1" x="3.81" y="7.62" length="middle" direction="pas" />
              <wire x1="-8.89" y1="-7.62" x2="-8.89" y2="-6.35" width="0.254" layer="94" />
              <wire x1="-8.89" y1="-6.35" x2="-10.16" y2="-7.62" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-7.62" x2="-8.89" y2="-7.62" width="0.254" layer="94" />
              <wire x1="8.89" y1="7.62" x2="8.89" y2="6.35" width="0.254" layer="94" />
              <wire x1="8.89" y1="6.35" x2="10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="10.16" y1="7.62" x2="8.89" y2="7.62" width="0.254" layer="94" />
              <pin name="E2" x="-3.81" y="-7.62" length="middle" direction="pas" rot="R180" />
              <pin name="C2" x="-3.81" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="B2" x="-19.05" y="0" length="middle" direction="pas" />
            </symbol>
            <symbol name="DUALPNP">
              <description />
              <wire x1="-13.97" y1="5.08" x2="-13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-13.97" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-8.89" y2="7.62" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-8.89" y2="-7.62" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
              <wire x1="-13.97" y1="-2.54" x2="-13.208" y2="-4.572" width="0.254" layer="94" />
              <wire x1="-13.208" y1="-4.572" x2="-11.938" y2="-3.302" width="0.254" layer="94" />
              <wire x1="-11.938" y1="-3.302" x2="-13.97" y2="-2.54" width="0.254" layer="94" />
              <pin name="B1" x="19.05" y="0" length="middle" direction="pas" rot="R180" />
              <wire x1="13.97" y1="-5.08" x2="13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="13.97" y2="5.08" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="8.89" y2="-7.62" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="8.89" y2="7.62" width="0.254" layer="94" />
              <pin name="C1" x="3.81" y="-7.62" length="middle" direction="pas" />
              <pin name="E1" x="3.81" y="7.62" length="middle" direction="pas" />
              <wire x1="13.97" y1="2.54" x2="13.208" y2="4.572" width="0.254" layer="94" />
              <wire x1="13.208" y1="4.572" x2="11.938" y2="3.302" width="0.254" layer="94" />
              <wire x1="11.938" y1="3.302" x2="13.97" y2="2.54" width="0.254" layer="94" />
              <pin name="E2" x="-3.81" y="-7.62" length="middle" direction="pas" rot="R180" />
              <pin name="C2" x="-3.81" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="B2" x="-19.05" y="0" length="middle" direction="pas" />
            </symbol>
            <symbol name="NFET">
              <description />
              <wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" />
              <wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="-2.54" x2="2.54" y2="-7.62" width="0.254" layer="94" />
              <wire x1="2.54" y1="2.54" x2="2.54" y2="7.62" width="0.254" layer="94" />
              <pin name="G" x="-7.62" y="0" length="middle" direction="pas" />
              <pin name="D" x="2.54" y="12.7" length="middle" direction="pas" rot="R270" />
              <pin name="S" x="2.54" y="-12.7" length="middle" direction="pas" rot="R90" />
              <text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
              <text x="-7.62" y="7.62" size="1.778" layer="95">&gt;VALUE</text>
              <wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94" />
            </symbol>
            <symbol name="NPN">
              <description />
              <pin name="B" x="-10.16" y="0" length="middle" direction="pas" />
              <wire x1="-5.08" y1="5.08" x2="-5.08" y2="2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="2.54" x2="0" y2="7.62" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="0" y2="-7.62" width="0.254" layer="94" />
              <wire x1="0" y1="-7.62" x2="-1.016" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-1.016" y1="-5.08" x2="-2.54" y2="-6.604" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-6.604" x2="0" y2="-7.62" width="0.254" layer="94" />
              <pin name="C" x="5.08" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="E" x="5.08" y="-7.62" length="middle" direction="pas" rot="R180" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="PNP">
              <description />
              <pin name="B" x="-10.16" y="2.54" length="middle" direction="pas" />
              <wire x1="-5.08" y1="7.62" x2="-5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="0" y2="10.16" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="0" y2="-5.08" width="0.254" layer="94" />
              <pin name="C" x="5.08" y="10.16" length="middle" direction="pas" rot="R180" />
              <pin name="E" x="5.08" y="-5.08" length="middle" direction="pas" rot="R180" />
              <text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="15.24" size="1.778" layer="96">&gt;VALUE</text>
              <wire x1="-5.08" y1="0" x2="-4.318" y2="-2.032" width="0.254" layer="94" />
              <wire x1="-4.318" y1="-2.032" x2="-3.048" y2="-0.762" width="0.254" layer="94" />
              <wire x1="-3.048" y1="-0.762" x2="-5.08" y2="0" width="0.254" layer="94" />
            </symbol>
            <symbol name="DUALOPAMP">
              <description />
              <pin name="OUT1" x="-15.24" y="7.62" length="middle" />
              <pin name="I1-" x="-15.24" y="2.54" length="middle" />
              <pin name="I1+" x="-15.24" y="-2.54" length="middle" />
              <pin name="V-" x="-15.24" y="-7.62" length="middle" />
              <pin name="I2+" x="15.24" y="-7.62" length="middle" rot="R180" />
              <pin name="I2-" x="15.24" y="-2.54" length="middle" rot="R180" />
              <pin name="OUT2" x="15.24" y="2.54" length="middle" rot="R180" />
              <pin name="V+" x="15.24" y="7.62" length="middle" rot="R180" />
              <wire x1="-10.16" y1="10.16" x2="-10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="-10.16" y1="7.62" x2="-10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="-10.16" y1="2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-2.54" x2="-10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="-10.16" x2="10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="10.16" y1="2.54" x2="10.16" y2="10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94" />
              <wire x1="-7.62" y1="2.54" x2="-5.08" y2="7.62" width="0.254" layer="94" />
              <wire x1="-5.08" y1="7.62" x2="-2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="2.54" x2="-4.318" y2="2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="2.54" x2="-6.096" y2="2.54" width="0.254" layer="94" />
              <wire x1="-6.096" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94" />
              <wire x1="5.08" y1="0" x2="7.62" y2="-5.08" width="0.254" layer="94" />
              <wire x1="7.62" y1="-5.08" x2="6.096" y2="-5.08" width="0.254" layer="94" />
              <text x="-6.35" y="3.048" size="1.27" layer="94">-</text>
              <text x="5.842" y="-4.826" size="1.27" layer="94">-</text>
              <text x="-4.826" y="3.048" size="1.27" layer="94">+</text>
              <text x="3.556" y="-5.08" size="1.27" layer="94">+</text>
              <wire x1="6.096" y1="-5.08" x2="4.064" y2="-5.08" width="0.254" layer="94" />
              <wire x1="4.064" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="7.62" x2="-5.08" y2="8.89" width="0.254" layer="94" />
              <wire x1="-5.08" y1="8.89" x2="-7.62" y2="8.89" width="0.254" layer="94" />
              <wire x1="-7.62" y1="8.89" x2="-7.62" y2="7.62" width="0.254" layer="94" />
              <wire x1="-7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="-6.096" y1="2.54" x2="-6.096" y2="1.524" width="0.254" layer="94" />
              <wire x1="-6.096" y1="1.524" x2="-8.89" y2="1.524" width="0.254" layer="94" />
              <wire x1="-8.89" y1="1.524" x2="-8.89" y2="2.54" width="0.254" layer="94" />
              <wire x1="-8.89" y1="2.54" x2="-10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="2.54" x2="-4.318" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="-2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94" />
              <wire x1="4.064" y1="-5.08" x2="4.064" y2="-7.62" width="0.254" layer="94" />
              <wire x1="4.064" y1="-7.62" x2="9.906" y2="-7.62" width="0.254" layer="94" />
              <wire x1="6.096" y1="-5.08" x2="6.096" y2="-6.35" width="0.254" layer="94" />
              <wire x1="6.096" y1="-6.35" x2="8.89" y2="-6.35" width="0.254" layer="94" />
              <wire x1="8.89" y1="-6.35" x2="8.89" y2="-2.54" width="0.254" layer="94" />
              <wire x1="8.89" y1="-2.54" x2="10.414" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.27" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="TRIMPOT">
              <description />
              <pin name="P$1" x="-12.7" y="0" length="middle" />
              <pin name="P$2" x="12.7" y="0" length="middle" rot="R180" />
              <pin name="W" x="1.27" y="12.7" length="middle" rot="R270" />
              <wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="-3.81" y2="2.54" width="0.254" layer="94" />
              <wire x1="-3.81" y1="2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.254" layer="94" />
              <wire x1="1.27" y1="2.54" x2="3.81" y2="-2.54" width="0.254" layer="94" />
              <wire x1="3.81" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94" />
              <wire x1="6.35" y1="2.54" x2="7.62" y2="0" width="0.254" layer="94" />
              <wire x1="1.27" y1="7.62" x2="1.27" y2="3.302" width="0.254" layer="94" />
              <wire x1="1.27" y1="3.302" x2="0.762" y2="4.064" width="0.254" layer="94" />
              <wire x1="0.762" y1="4.064" x2="1.778" y2="4.064" width="0.254" layer="94" />
              <wire x1="1.778" y1="4.064" x2="1.27" y2="3.302" width="0.254" layer="94" />
              <text x="-10.16" y="7.62" size="1.27" layer="95">&gt;NAME</text>
              <text x="-10.16" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="3POSSWITCH">
              <description />
              <pin name="1" x="-12.7" y="2.54" length="middle" />
              <pin name="2" x="-12.7" y="0" length="middle" />
              <pin name="3" x="-12.7" y="-2.54" length="middle" />
              <pin name="COM" x="12.7" y="0" length="middle" rot="R180" />
              <wire x1="7.62" y1="0" x2="-7.62" y2="2.54" width="0.254" layer="94" />
              <wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94" />
              <wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94" />
              <text x="-10.16" y="10.16" size="1.27" layer="95">&gt;NAME</text>
              <text x="-10.16" y="7.62" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="DUAL3POSSWITCH">
              <description />
              <pin name="1" x="-12.7" y="12.7" length="middle" direction="pas" />
              <pin name="2" x="-12.7" y="10.16" length="middle" direction="pas" />
              <pin name="4" x="-12.7" y="7.62" length="middle" direction="pas" />
              <pin name="3" x="12.7" y="10.16" length="middle" direction="pas" rot="R180" />
              <wire x1="7.62" y1="10.16" x2="-7.62" y2="12.7" width="0.254" layer="94" />
              <wire x1="-10.16" y1="17.78" x2="-10.16" y2="-7.62" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94" />
              <wire x1="10.16" y1="-7.62" x2="10.16" y2="17.78" width="0.254" layer="94" />
              <wire x1="10.16" y1="17.78" x2="-10.16" y2="17.78" width="0.254" layer="94" />
              <text x="-10.16" y="20.32" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="17.78" size="1.778" layer="96">&gt;VALUE</text>
              <pin name="5" x="-12.7" y="2.54" length="middle" direction="pas" />
              <pin name="6" x="-12.7" y="0" length="middle" direction="pas" />
              <pin name="8" x="-12.7" y="-2.54" length="middle" direction="pas" />
              <pin name="7" x="12.7" y="0" length="middle" direction="pas" rot="R180" />
              <wire x1="7.62" y1="0" x2="-7.62" y2="2.54" width="0.254" layer="94" />
            </symbol>
            <symbol name="NCP1117">
              <description />
              <wire x1="-10.16" y1="-2.54" x2="-10.16" y2="5.08" width="0.254" layer="94" />
              <wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94" />
              <wire x1="10.16" y1="5.08" x2="10.16" y2="-2.54" width="0.254" layer="94" />
              <wire x1="10.16" y1="-2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94" />
              <pin name="IN" x="-12.7" y="2.54" length="short" direction="pas" />
              <pin name="ADJ" x="0" y="-5.08" length="short" direction="pas" rot="R90" />
              <pin name="OUT" x="12.7" y="2.54" length="short" direction="pas" rot="R180" />
              <text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
              <text x="-7.62" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="BRIDGERECTIFIER">
              <description />
              <pin name="+" x="0" y="12.7" length="middle" rot="R270" />
              <pin name="-" x="0" y="-12.7" length="middle" rot="R90" />
              <pin name="~1" x="-12.7" y="0" length="middle" />
              <pin name="~2" x="12.7" y="0" length="middle" rot="R180" />
              <wire x1="-7.62" y1="0" x2="-3.81" y2="3.81" width="0.254" layer="94" />
              <wire x1="-3.81" y1="3.81" x2="-5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="2.54" x2="-3.81" y2="3.81" width="0.254" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="0" y2="7.62" width="0.254" layer="94" />
              <wire x1="7.62" y1="0" x2="3.81" y2="3.81" width="0.254" layer="94" />
              <wire x1="3.81" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.254" layer="94" />
              <wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="5.08" y1="5.08" x2="3.81" y2="3.81" width="0.254" layer="94" />
              <wire x1="2.54" y1="5.08" x2="0" y2="7.62" width="0.254" layer="94" />
              <wire x1="0" y1="-7.62" x2="-3.81" y2="-3.81" width="0.254" layer="94" />
              <wire x1="-3.81" y1="-3.81" x2="-5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-3.81" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="-7.62" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="-7.62" x2="3.81" y2="-3.81" width="0.254" layer="94" />
              <wire x1="3.81" y1="-3.81" x2="2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="5.08" y1="-5.08" x2="3.81" y2="-3.81" width="0.254" layer="94" />
              <wire x1="5.08" y1="-2.54" x2="7.62" y2="0" width="0.254" layer="94" />
              <wire x1="-3.81" y1="6.35" x2="-1.27" y2="3.81" width="0.254" layer="94" />
              <wire x1="1.27" y1="3.81" x2="3.81" y2="6.35" width="0.254" layer="94" />
              <wire x1="6.35" y1="-3.81" x2="3.81" y2="-1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="-1.27" x2="-6.35" y2="-3.81" width="0.254" layer="94" />
              <text x="-12.7" y="10.16" size="1.27" layer="95">&gt;NAME</text>
              <text x="-12.7" y="7.62" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="POWERJACK">
              <description />
              <pin name="SLEEVE" x="10.16" y="5.08" length="middle" rot="R180" />
              <pin name="SHUNT" x="10.16" y="0" length="middle" rot="R180" />
              <pin name="TIP" x="10.16" y="-2.54" length="middle" rot="R180" />
              <wire x1="5.08" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="-7.62" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="0" x2="0" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="0" x2="0" y2="-2.286" width="0.254" layer="94" />
              <wire x1="0" y1="-2.286" x2="-0.254" y2="-2.032" width="0.254" layer="94" />
              <wire x1="-0.254" y1="-2.032" x2="0.254" y2="-2.032" width="0.254" layer="94" />
              <wire x1="0.254" y1="-2.032" x2="0" y2="-2.286" width="0.254" layer="94" />
              <wire x1="5.08" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94" />
              <text x="-7.62" y="10.16" size="1.27" layer="95">&gt;NAME</text>
              <text x="-7.62" y="7.62" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="3TERMINALBLOCK">
              <description />
              <pin name="P$1" x="-7.62" y="2.54" length="middle" />
              <pin name="P$2" x="-7.62" y="0" length="middle" />
              <pin name="P$3" x="-7.62" y="-2.54" length="middle" />
              <wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <text x="-5.08" y="7.62" size="1.27" layer="95">&gt;NAME</text>
              <text x="-5.08" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="DUALJACK">
              <description />
              <pin name="TT" x="-12.7" y="12.7" length="middle" direction="pas" />
              <wire x1="-7.62" y1="12.7" x2="5.08" y2="12.7" width="0.254" layer="94" />
              <wire x1="5.08" y1="12.7" x2="7.62" y2="10.16" width="0.254" layer="94" />
              <wire x1="7.62" y1="10.16" x2="10.16" y2="12.7" width="0.254" layer="94" />
              <pin name="TNT" x="-12.7" y="7.62" length="middle" direction="pas" />
              <wire x1="-7.62" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94" />
              <wire x1="2.54" y1="7.62" x2="2.54" y2="12.446" width="0.254" layer="94" />
              <wire x1="2.54" y1="12.446" x2="2.286" y2="12.192" width="0.254" layer="94" />
              <wire x1="2.286" y1="12.192" x2="2.794" y2="12.192" width="0.254" layer="94" />
              <wire x1="2.794" y1="12.192" x2="2.54" y2="12.446" width="0.254" layer="94" />
              <pin name="ST" x="-12.7" y="5.08" length="middle" direction="pas" />
              <wire x1="-7.62" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94" />
              <pin name="G" x="-12.7" y="-15.24" length="middle" direction="pas" />
              <wire x1="-7.62" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94" />
              <wire x1="5.08" y1="-15.24" x2="5.08" y2="-17.78" width="0.254" layer="94" />
              <wire x1="5.08" y1="-17.78" x2="2.54" y2="-17.78" width="0.254" layer="94" />
              <wire x1="5.08" y1="-17.78" x2="7.62" y2="-17.78" width="0.254" layer="94" />
              <wire x1="3.556" y1="-18.542" x2="6.604" y2="-18.542" width="0.254" layer="94" />
              <wire x1="4.318" y1="-19.304" x2="5.842" y2="-19.304" width="0.254" layer="94" />
              <wire x1="10.16" y1="5.08" x2="12.7" y2="2.54" width="0.254" layer="94" />
              <wire x1="12.7" y1="2.54" x2="15.24" y2="5.08" width="0.254" layer="94" />
              <pin name="SNT" x="-12.7" y="2.54" length="middle" direction="pas" />
              <wire x1="-7.62" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94" />
              <wire x1="7.62" y1="2.54" x2="7.62" y2="4.826" width="0.254" layer="94" />
              <wire x1="7.62" y1="4.826" x2="7.366" y2="4.572" width="0.254" layer="94" />
              <wire x1="7.366" y1="4.572" x2="7.874" y2="4.572" width="0.254" layer="94" />
              <wire x1="7.874" y1="4.572" x2="7.62" y2="4.826" width="0.254" layer="94" />
              <pin name="TB" x="-12.7" y="-2.54" length="middle" direction="pas" />
              <wire x1="-7.62" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="-2.54" x2="7.62" y2="-5.08" width="0.254" layer="94" />
              <wire x1="7.62" y1="-5.08" x2="10.16" y2="-2.54" width="0.254" layer="94" />
              <pin name="TNB" x="-12.7" y="-7.62" length="middle" direction="pas" />
              <wire x1="-7.62" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94" />
              <wire x1="2.54" y1="-7.62" x2="2.54" y2="-2.794" width="0.254" layer="94" />
              <wire x1="2.54" y1="-2.794" x2="2.286" y2="-3.048" width="0.254" layer="94" />
              <wire x1="2.286" y1="-3.048" x2="2.794" y2="-3.048" width="0.254" layer="94" />
              <wire x1="2.794" y1="-3.048" x2="2.54" y2="-2.794" width="0.254" layer="94" />
              <pin name="SB" x="-12.7" y="-10.16" length="middle" direction="pas" />
              <wire x1="-7.62" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="-10.16" x2="12.7" y2="-12.7" width="0.254" layer="94" />
              <wire x1="12.7" y1="-12.7" x2="15.24" y2="-10.16" width="0.254" layer="94" />
              <pin name="SNB" x="-12.7" y="-12.7" length="middle" direction="pas" />
              <wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94" />
              <wire x1="7.62" y1="-12.7" x2="7.62" y2="-10.414" width="0.254" layer="94" />
              <wire x1="7.62" y1="-10.414" x2="7.366" y2="-10.668" width="0.254" layer="94" />
              <wire x1="7.366" y1="-10.668" x2="7.874" y2="-10.668" width="0.254" layer="94" />
              <wire x1="7.874" y1="-10.668" x2="7.62" y2="-10.414" width="0.254" layer="94" />
              <wire x1="-7.62" y1="-13.97" x2="-7.62" y2="-1.27" width="0.254" layer="94" />
              <wire x1="-7.62" y1="-1.27" x2="17.78" y2="-1.27" width="0.254" layer="94" />
              <wire x1="17.78" y1="-1.27" x2="17.78" y2="-13.97" width="0.254" layer="94" />
              <wire x1="17.78" y1="-13.97" x2="-7.62" y2="-13.97" width="0.254" layer="94" />
              <wire x1="-7.62" y1="14.224" x2="-7.62" y2="1.27" width="0.254" layer="94" />
              <wire x1="-7.62" y1="1.27" x2="17.78" y2="1.27" width="0.254" layer="94" />
              <wire x1="17.78" y1="1.27" x2="17.78" y2="14.224" width="0.254" layer="94" />
              <wire x1="17.78" y1="14.224" x2="-7.62" y2="14.224" width="0.254" layer="94" />
              <text x="-7.62" y="15.24" size="1.778" layer="94">top</text>
              <text x="-7.62" y="-0.508" size="1.778" layer="94">bottom</text>
              <text x="-2.54" y="17.78" size="1.778" layer="95">&gt;NAME</text>
              <text x="-2.54" y="15.24" size="1.778" layer="95">&gt;VALUE</text>
            </symbol>
            <symbol name="24TERMINALBLOCK">
              <description />
              <pin name="1-1" x="-20.32" y="10.16" length="middle" direction="pas" />
              <pin name="2-1" x="-7.62" y="10.16" length="middle" direction="pas" />
              <pin name="1-2" x="-20.32" y="7.62" length="middle" direction="pas" />
              <pin name="2-2" x="-7.62" y="7.62" length="middle" direction="pas" />
              <pin name="1-3" x="-20.32" y="5.08" length="middle" direction="pas" />
              <pin name="2-3" x="-7.62" y="5.08" length="middle" direction="pas" />
              <pin name="1-4" x="-20.32" y="2.54" length="middle" direction="pas" />
              <pin name="2-4" x="-7.62" y="2.54" length="middle" direction="pas" />
              <pin name="1-5" x="-20.32" y="0" length="middle" direction="pas" />
              <pin name="2-5" x="-7.62" y="0" length="middle" direction="pas" />
              <pin name="1-6" x="-20.32" y="-2.54" length="middle" direction="pas" />
              <pin name="2-6" x="-7.62" y="-2.54" length="middle" direction="pas" />
              <pin name="1-7" x="-20.32" y="-5.08" length="middle" direction="pas" />
              <pin name="2-7" x="-7.62" y="-5.08" length="middle" direction="pas" />
              <pin name="1-8" x="-20.32" y="-7.62" length="middle" direction="pas" />
              <pin name="2-8" x="-7.62" y="-7.62" length="middle" direction="pas" />
              <pin name="1-9" x="-20.32" y="-10.16" length="middle" direction="pas" />
              <pin name="2-9" x="-7.62" y="-10.16" length="middle" direction="pas" />
              <pin name="1-10" x="-20.32" y="-12.7" length="middle" direction="pas" />
              <pin name="2-10" x="-7.62" y="-12.7" length="middle" direction="pas" />
              <pin name="1-11" x="-20.32" y="-15.24" length="middle" direction="pas" />
              <pin name="2-11" x="-7.62" y="-15.24" length="middle" direction="pas" />
              <pin name="1-12" x="-20.32" y="-17.78" length="middle" direction="pas" />
              <pin name="2-12" x="-7.62" y="-17.78" length="middle" direction="pas" />
              <wire x1="-17.78" y1="12.7" x2="-17.78" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-17.78" y1="-20.32" x2="-12.7" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-17.78" y1="12.7" x2="-12.7" y2="12.7" width="0.254" layer="94" />
              <wire x1="-5.08" y1="12.7" x2="-5.08" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-20.32" x2="0" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-5.08" y1="12.7" x2="0" y2="12.7" width="0.254" layer="94" />
              <text x="-17.78" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-17.78" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
            </symbol>
            <symbol name="XLRPHONOCOMBO">
              <description />
              <pin name="T" x="-12.7" y="2.54" length="middle" direction="pas" />
              <pin name="S" x="-12.7" y="0" length="middle" direction="pas" />
              <wire x1="-7.62" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94" />
              <wire x1="0" y1="2.54" x2="1.27" y2="1.524" width="0.254" layer="94" />
              <wire x1="1.27" y1="1.524" x2="2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-7.62" y1="0" x2="2.54" y2="0" width="0.254" layer="94" />
              <pin name="1" x="-12.7" y="12.7" length="middle" direction="pas" />
              <pin name="2" x="-12.7" y="10.16" length="middle" direction="pas" />
              <pin name="3" x="-12.7" y="7.62" length="middle" direction="pas" />
              <pin name="G" x="-12.7" y="5.08" length="middle" direction="pas" />
              <wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94" />
              <wire x1="7.62" y1="5.08" x2="7.62" y2="2.54" width="0.254" layer="94" />
              <wire x1="7.62" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94" />
              <wire x1="7.62" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="5.842" y1="1.778" x2="9.398" y2="1.778" width="0.254" layer="94" />
              <wire x1="6.604" y1="1.016" x2="8.636" y2="1.016" width="0.254" layer="94" />
              <wire x1="-10.16" y1="13.97" x2="-10.16" y2="6.35" width="0.254" layer="94" />
              <wire x1="-10.16" y1="6.35" x2="-5.588" y2="6.35" width="0.254" layer="94" />
              <wire x1="-5.588" y1="6.35" x2="-5.588" y2="13.97" width="0.254" layer="94" />
              <wire x1="-5.588" y1="13.97" x2="-10.16" y2="13.97" width="0.254" layer="94" />
              <text x="-8.636" y="14.478" size="1.778" layer="94">xlr</text>
              <text x="-2.54" y="17.78" size="1.778" layer="95">&gt;NAME</text>
              <text x="-2.54" y="15.24" size="1.778" layer="95">&gt;VALUE</text>
            </symbol>
            <symbol name="MONOSWITCHJACK">
              <description />
              <pin name="T" x="-17.78" y="5.08" length="middle" direction="pas" />
              <wire x1="-12.7" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94" />
              <wire x1="0" y1="5.08" x2="2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="2.54" x2="5.08" y2="5.08" width="0.254" layer="94" />
              <pin name="TN" x="-17.78" y="0" length="middle" direction="pas" />
              <wire x1="-12.7" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" />
              <wire x1="-2.54" y1="0" x2="-2.54" y2="4.826" width="0.254" layer="94" />
              <wire x1="-2.54" y1="4.826" x2="-2.794" y2="4.572" width="0.254" layer="94" />
              <wire x1="-2.794" y1="4.572" x2="-2.286" y2="4.572" width="0.254" layer="94" />
              <wire x1="-2.286" y1="4.572" x2="-2.54" y2="4.826" width="0.254" layer="94" />
              <pin name="S" x="-17.78" y="-2.54" length="middle" direction="pas" />
              <wire x1="-12.7" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="-2.54" x2="7.62" y2="-5.08" width="0.254" layer="94" />
              <wire x1="7.62" y1="-5.08" x2="10.16" y2="-2.54" width="0.254" layer="94" />
              <pin name="SN" x="-17.78" y="-5.08" length="middle" direction="pas" />
              <wire x1="-12.7" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94" />
              <wire x1="2.54" y1="-5.08" x2="2.54" y2="-2.794" width="0.254" layer="94" />
              <wire x1="2.54" y1="-2.794" x2="2.286" y2="-3.048" width="0.254" layer="94" />
              <wire x1="2.286" y1="-3.048" x2="2.794" y2="-3.048" width="0.254" layer="94" />
              <wire x1="2.794" y1="-3.048" x2="2.54" y2="-2.794" width="0.254" layer="94" />
              <text x="-12.7" y="10.16" size="1.778" layer="95">&gt;NAME</text>
              <text x="-12.7" y="7.62" size="1.778" layer="95">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset name="PMP4201Y">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALNPN" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOT363">
                  <connects>
                    <connect gate="G$1" pin="B1" pad="P$1" />
                    <connect gate="G$1" pin="B2" pad="P$2" />
                    <connect gate="G$1" pin="C1" pad="P$6" />
                    <connect gate="G$1" pin="C2" pad="P$3" />
                    <connect gate="G$1" pin="E1" pad="P$5" />
                    <connect gate="G$1" pin="E2" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PMP5201Y">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALPNP" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOT363">
                  <connects>
                    <connect gate="G$1" pin="B1" pad="P$1" />
                    <connect gate="G$1" pin="B2" pad="P$2" />
                    <connect gate="G$1" pin="C1" pad="P$6" />
                    <connect gate="G$1" pin="C2" pad="P$3" />
                    <connect gate="G$1" pin="E1" pad="P$5" />
                    <connect gate="G$1" pin="E2" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="2N3819">
              <description />
              <gates>
                <gate name="G$1" symbol="NFET" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="D" pad="1" />
                    <connect gate="G$1" pin="G" pad="2" />
                    <connect gate="G$1" pin="S" pad="3" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="KSC1008YBU">
              <description />
              <gates>
                <gate name="G$1" symbol="NPN" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="B" pad="2" />
                    <connect gate="G$1" pin="C" pad="3" />
                    <connect gate="G$1" pin="E" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PN2907ABU">
              <description />
              <gates>
                <gate name="G$1" symbol="PNP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="B" pad="2" />
                    <connect gate="G$1" pin="C" pad="3" />
                    <connect gate="G$1" pin="E" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="UPC4558G2">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALOPAMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="8-SOP">
                  <connects>
                    <connect gate="G$1" pin="I1+" pad="P$3" />
                    <connect gate="G$1" pin="I1-" pad="P$2" />
                    <connect gate="G$1" pin="I2+" pad="P$5" />
                    <connect gate="G$1" pin="I2-" pad="P$6" />
                    <connect gate="G$1" pin="OUT1" pad="P$1" />
                    <connect gate="G$1" pin="OUT2" pad="P$7" />
                    <connect gate="G$1" pin="V+" pad="P$8" />
                    <connect gate="G$1" pin="V-" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="TL082CD">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALOPAMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="R-PDSO-G8">
                  <connects>
                    <connect gate="G$1" pin="I1+" pad="P$3" />
                    <connect gate="G$1" pin="I1-" pad="P$2" />
                    <connect gate="G$1" pin="I2+" pad="P$5" />
                    <connect gate="G$1" pin="I2-" pad="P$6" />
                    <connect gate="G$1" pin="OUT1" pad="P$1" />
                    <connect gate="G$1" pin="OUT2" pad="P$7" />
                    <connect gate="G$1" pin="V+" pad="P$8" />
                    <connect gate="G$1" pin="V-" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="CT6EP104">
              <description />
              <gates>
                <gate name="G$1" symbol="TRIMPOT" x="-2.54" y="0" />
              </gates>
              <devices>
                <device package="TRIMPOT">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="P$1" />
                    <connect gate="G$1" pin="P$2" pad="P$3" />
                    <connect gate="G$1" pin="W" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="OS103011MS8QP1">
              <description />
              <gates>
                <gate name="G$1" symbol="3POSSWITCH" x="0" y="0" />
              </gates>
              <devices>
                <device package="SP3T">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$3" />
                    <connect gate="G$1" pin="3" pad="P$4" />
                    <connect gate="G$1" pin="COM" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="OS203011MS1QP1">
              <description />
              <gates>
                <gate name="G$1" symbol="DUAL3POSSWITCH" x="0" y="0" />
              </gates>
              <devices>
                <device package="DP3T">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                    <connect gate="G$1" pin="3" pad="P$3" />
                    <connect gate="G$1" pin="4" pad="P$4" />
                    <connect gate="G$1" pin="5" pad="P$5" />
                    <connect gate="G$1" pin="6" pad="P$6" />
                    <connect gate="G$1" pin="7" pad="P$7" />
                    <connect gate="G$1" pin="8" pad="P$8" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="LM337H">
              <description />
              <gates>
                <gate name="G$1" symbol="NCP1117" x="0" y="-2.54" />
              </gates>
              <devices>
                <device package="K0002C">
                  <connects>
                    <connect gate="G$1" pin="ADJ" pad="ADJ" />
                    <connect gate="G$1" pin="IN" pad="P$3 P$4 P$5" route="any" />
                    <connect gate="G$1" pin="OUT" pad="VOUT" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="W02">
              <description />
              <gates>
                <gate name="G$1" symbol="BRIDGERECTIFIER" x="0" y="0" />
              </gates>
              <devices>
                <device package="RB-15">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$3" />
                    <connect gate="G$1" pin="~1" pad="P$2" />
                    <connect gate="G$1" pin="~2" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PJ-102AH">
              <description />
              <gates>
                <gate name="G$1" symbol="POWERJACK" x="0" y="0" />
              </gates>
              <devices>
                <device package="POWERJACK">
                  <connects>
                    <connect gate="G$1" pin="SHUNT" pad="P$3" />
                    <connect gate="G$1" pin="SLEEVE" pad="P$1" />
                    <connect gate="G$1" pin="TIP" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="282834-3">
              <description />
              <gates>
                <gate name="G$1" symbol="3TERMINALBLOCK" x="0" y="0" />
              </gates>
              <devices>
                <device package="3TERMINALBLOCK">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="P$1" />
                    <connect gate="G$1" pin="P$2" pad="P$2" />
                    <connect gate="G$1" pin="P$3" pad="P$3" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="NSJ8HC">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALJACK" x="-2.54" y="0" />
              </gates>
              <devices>
                <device package="STACKING_MONO-JACK">
                  <connects>
                    <connect gate="G$1" pin="G" pad="G" />
                    <connect gate="G$1" pin="SB" pad="SB" />
                    <connect gate="G$1" pin="SNB" pad="SNB" />
                    <connect gate="G$1" pin="SNT" pad="SNT" />
                    <connect gate="G$1" pin="ST" pad="ST" />
                    <connect gate="G$1" pin="TB" pad="TB" />
                    <connect gate="G$1" pin="TNB" pad="TNB" />
                    <connect gate="G$1" pin="TNT" pad="TNT" />
                    <connect gate="G$1" pin="TT" pad="TT" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="1708136">
              <description />
              <gates>
                <gate name="G$1" symbol="24TERMINALBLOCK" x="0" y="0" />
              </gates>
              <devices>
                <device package="2X12TERMINAL">
                  <connects>
                    <connect gate="G$1" pin="1-1" pad="1-1" />
                    <connect gate="G$1" pin="1-10" pad="1-10" />
                    <connect gate="G$1" pin="1-11" pad="1-11" />
                    <connect gate="G$1" pin="1-12" pad="1-12" />
                    <connect gate="G$1" pin="1-2" pad="1-2" />
                    <connect gate="G$1" pin="1-3" pad="1-3" />
                    <connect gate="G$1" pin="1-4" pad="1-4" />
                    <connect gate="G$1" pin="1-5" pad="1-5" />
                    <connect gate="G$1" pin="1-6" pad="1-6" />
                    <connect gate="G$1" pin="1-7" pad="1-7" />
                    <connect gate="G$1" pin="1-8" pad="1-8" />
                    <connect gate="G$1" pin="1-9" pad="1-9" />
                    <connect gate="G$1" pin="2-1" pad="2-1" />
                    <connect gate="G$1" pin="2-10" pad="2-10" />
                    <connect gate="G$1" pin="2-11" pad="2-11" />
                    <connect gate="G$1" pin="2-12" pad="2-12" />
                    <connect gate="G$1" pin="2-2" pad="2-2" />
                    <connect gate="G$1" pin="2-3" pad="2-3" />
                    <connect gate="G$1" pin="2-4" pad="2-4" />
                    <connect gate="G$1" pin="2-5" pad="2-5" />
                    <connect gate="G$1" pin="2-6" pad="2-6" />
                    <connect gate="G$1" pin="2-7" pad="2-7" />
                    <connect gate="G$1" pin="2-8" pad="2-8" />
                    <connect gate="G$1" pin="2-9" pad="2-9" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="NCJ5FI-H-0">
              <description />
              <gates>
                <gate name="G$1" symbol="XLRPHONOCOMBO" x="0" y="0" />
              </gates>
              <devices>
                <device package="XLRMONOCOMBO">
                  <connects>
                    <connect gate="G$1" pin="1" pad="1" />
                    <connect gate="G$1" pin="2" pad="2" />
                    <connect gate="G$1" pin="3" pad="3" />
                    <connect gate="G$1" pin="G" pad="G" />
                    <connect gate="G$1" pin="S" pad="S" />
                    <connect gate="G$1" pin="T" pad="T" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="NRJ4HF">
              <description />
              <gates>
                <gate name="G$1" symbol="MONOSWITCHJACK" x="0" y="0" />
              </gates>
              <devices>
                <device package="JACKMONOSWITCHED">
                  <connects>
                    <connect gate="G$1" pin="S" pad="S" />
                    <connect gate="G$1" pin="SN" pad="SN" />
                    <connect gate="G$1" pin="T" pad="T" />
                    <connect gate="G$1" pin="TN" pad="TN" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="resistor">
          <description />
          <packages>
            <package name="R_0603">
              <description>&lt;B&gt;
0603
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-0.1" y1="0.3" x2="0.1" y2="0.3" width="0.1524" layer="21" />
              <wire x1="-0.1" y1="-0.3" x2="0.1" y2="-0.3" width="0.1524" layer="21" />
              <smd name="P$1" x="-0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <smd name="P$2" x="0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <text x="-1.3" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
            </package>
            <package name="R_1206">
              <description>&lt;B&gt;
1206
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-0.6" y1="0.8" x2="0.6" y2="0.8" width="0.1524" layer="21" />
              <wire x1="-0.6" y1="-0.8" x2="0.6" y2="-0.8" width="0.1524" layer="21" />
              <smd name="P$1" x="-1.55" y="0" dx="1.3" dy="1.5" layer="1" />
              <smd name="P$2" x="1.55" y="0" dx="1.3" dy="1.5" layer="1" />
              <text x="-2.2" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.0762" layer="51" />
              <wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.0762" layer="51" />
              <wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.0762" layer="51" />
              <wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.0762" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="R">
              <description>&lt;B&gt;Resistor&lt;/B&gt;</description>
              <wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94" />
              <wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94" />
              <wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94" />
              <pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" />
              <pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180" />
              <text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
              <text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="R" name="RESISTOR_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="0" y="0" />
              </gates>
              <devices>
                <device package="R_0603">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="R" name="RESISTOR_1206">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="2.54" y="0" />
              </gates>
              <devices>
                <device package="R_1206">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="keystone">
          <description />
          <packages>
            <package name="TP-040">
              <description>&lt;b&gt;TEST POINT&lt;/b&gt;&lt;p&gt; 
Series 5000-5004, Hole 0.040"</description>
              <circle x="0" y="0" radius="1.27" width="0.1524" layer="51" />
              <pad name="1" x="0" y="0" drill="1.016" diameter="2.159" shape="octagon" />
              <text x="-1.175421875" y="1.385528125" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <rectangle x1="-0.9525" y1="-0.3175" x2="0.9525" y2="0.3175" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="TP">
              <description />
              <wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94" />
              <wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94" />
              <wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94" />
              <text x="-1.524" y="0.508" size="1.778" layer="95">&gt;NAME</text>
              <pin name="1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90" />
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="TP" name="TP">
              <description />
              <gates>
                <gate name="G$1" symbol="TP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TP-040" name="-040">
                  <connects>
                    <connect gate="G$1" pin="1" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="mlcc">
          <description />
          <packages>
            <package name="C_0603">
              <description>&lt;B&gt; 0603&lt;/B&gt; (1608 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="-0.5334" x2="0.1016" y2="-0.5334" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="0.5334" x2="0.1016" y2="0.5334" width="0.1524" layer="21" />
              <smd name="1" x="-0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <smd name="2" x="0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <text x="-1.6" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0402">
              <description>&lt;B&gt; 0402&lt;/B&gt; (1005 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.0762" layer="51" />
              <wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.0762" layer="51" />
              <smd name="1" x="-0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <smd name="2" x="0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <text x="-1" y="0.6" size="0.508" layer="51" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0805">
              <description>&lt;B&gt; 0805&lt;/B&gt; (2012 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.0762" layer="51" />
              <wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.0762" layer="51" />
              <wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.0762" layer="51" />
              <wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="0.7112" x2="0.1016" y2="0.7112" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="-0.7112" x2="0.1016" y2="-0.7112" width="0.1524" layer="21" />
              <smd name="1" x="-1" y="0" dx="1.35" dy="1.55" layer="1" />
              <smd name="2" x="1" y="0" dx="1.35" dy="1.55" layer="1" />
              <text x="-1.8" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="CAP_NP">
              <description>&lt;B&gt;Capacitor&lt;/B&gt; -- non-polarized</description>
              <wire x1="-1.905" y1="-3.175" x2="0" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="0" y1="-3.175" x2="1.905" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="-1.905" y1="-4.445" x2="0" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-4.445" x2="1.905" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-2.54" x2="0" y2="-3.175" width="0.254" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-4.445" width="0.254" layer="94" />
              <pin name="P$1" x="0" y="0" visible="off" length="short" direction="pas" rot="R270" />
              <pin name="P$2" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90" />
              <text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
              <text x="-5.08" y="-7.62" size="1.778" layer="95" rot="R90">&gt;NAME</text>
              <text x="0.508" y="-2.286" size="1.778" layer="95">1</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="C" name="C_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0603">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0402">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0402">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0805">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0805">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="ecad">
          <description />
          <packages>
            <package name="DIODES_DO-35">
              <description>Diode, DO-35 package, 0.2in lead spacing, 0.7mm drill</description>
              <wire x1="-1.605" y1="0.9525" x2="1.605" y2="0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="-1.605" y1="-0.9525" x2="1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="-1.605" y1="0.9525" x2="-1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="1.605" y1="0.9525" x2="1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <pad name="+" x="-2.54" y="0" drill="0.7" />
              <pad name="-" x="2.54" y="0" drill="0.7" />
              <text x="-3.505" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="TANT_6032-METRIC">
              <description />
              <smd name="P$1" x="-2.54" y="0" dx="2.03" dy="2.28" layer="1" />
              <smd name="P$2" x="2.54" y="0" dx="2.03" dy="2.28" layer="1" />
              <circle x="-4.12" y="0.11" radius="0.1542" width="0" layer="21" />
              <text x="-3.45" y="1.65" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.6" y1="0.8" x2="-1.6" y2="0" width="0.127" layer="51" />
              <wire x1="-1.6" y1="0" x2="-1.6" y2="-0.8" width="0.127" layer="51" />
              <wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.127" layer="51" />
              <wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.127" layer="51" />
              <wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0.4" x2="-0.4" y2="0" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0" x2="-0.4" y2="-0.4" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="51" />
              <wire x1="0.6" y1="0.4" x2="0.2" y2="0" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0.2" y1="0" x2="0.6" y2="-0.4" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0.2" y1="0" x2="1.6" y2="0" width="0.127" layer="51" />
              <wire x1="-1.3" y1="1.25" x2="1.3" y2="1.25" width="0.127" layer="21" />
              <wire x1="-1.3" y1="-1.25" x2="1.3" y2="-1.25" width="0.127" layer="21" />
              <text x="-3.175" y="-0.635" size="1.27" layer="51">+</text>
            </package>
            <package name="TANT_2012-METRIC">
              <description />
              <smd name="P$1" x="-0.955" y="0" dx="1.14" dy="1.27" layer="1" />
              <smd name="P$2" x="0.955" y="0" dx="1.14" dy="1.27" layer="1" />
              <circle x="-1.96" y="0.34" radius="0.1542" width="0" layer="21" />
              <wire x1="-0.2" y1="0.8" x2="0.2" y2="0.8" width="0.127" layer="21" />
              <wire x1="-0.2" y1="-0.8" x2="0.2" y2="-0.8" width="0.127" layer="21" />
              <text x="-3.4" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.025" y1="0.65" x2="-1.025" y2="-0.65" width="0.127" layer="51" />
              <wire x1="-1.025" y1="-0.65" x2="1.025" y2="-0.65" width="0.127" layer="51" />
              <wire x1="1.025" y1="-0.65" x2="1.025" y2="0.65" width="0.127" layer="51" />
              <wire x1="1.025" y1="0.65" x2="-1.025" y2="0.65" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0.4" x2="-0.2" y2="0" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0" x2="-0.2" y2="-0.4" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0" x2="-1" y2="0" width="0.127" layer="51" />
              <wire x1="0.4" y1="0.4" x2="0" y2="0" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0" y1="0" x2="0.4" y2="-0.4" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0" y1="0" x2="1" y2="0" width="0.127" layer="51" />
              <text x="-2.54" y="-0.635" size="1.27" layer="51">+</text>
            </package>
            <package name="TANT_3216-METRIC">
              <description />
              <smd name="P$1" x="-1.525" y="0" dx="1.78" dy="1.27" layer="1" />
              <smd name="P$2" x="1.525" y="0" dx="1.78" dy="1.27" layer="1" />
              <circle x="-2.82" y="0.31" radius="0.1542" width="0" layer="21" />
              <text x="-3.4" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.6" y1="0.8" x2="-1.6" y2="0" width="0.127" layer="51" />
              <wire x1="-1.6" y1="0" x2="-1.6" y2="-0.8" width="0.127" layer="51" />
              <wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.127" layer="51" />
              <wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.127" layer="51" />
              <wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0.4" x2="-0.4" y2="0" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0" x2="-0.4" y2="-0.4" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="51" />
              <wire x1="0.6" y1="0.4" x2="0.2" y2="0" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0.2" y1="0" x2="0.6" y2="-0.4" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0.2" y1="0" x2="1.6" y2="0" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0.8" x2="0.4" y2="0.8" width="0.127" layer="21" />
              <wire x1="-0.4" y1="-0.8" x2="0.4" y2="-0.8" width="0.127" layer="21" />
              <text x="-2.54" y="-0.635" size="1.27" layer="51">+</text>
            </package>
            <package name="TANT_7343-METRIC">
              <description />
              <smd name="P$1" x="-3.305" y="0" dx="2.16" dy="2.41" layer="1" />
              <smd name="P$2" x="3.305" y="0" dx="2.16" dy="2.41" layer="1" />
              <circle x="-5.02" y="0.01" radius="0.1542" width="0" layer="21" />
              <text x="-3.45" y="1.65" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.6" y1="0.8" x2="-1.6" y2="0" width="0.127" layer="51" />
              <wire x1="-1.6" y1="0" x2="-1.6" y2="-0.8" width="0.127" layer="51" />
              <wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.127" layer="51" />
              <wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.127" layer="51" />
              <wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0.4" x2="-0.4" y2="0" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0" x2="-0.4" y2="-0.4" width="0.127" layer="51" />
              <wire x1="-0.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="51" />
              <wire x1="0.6" y1="0.4" x2="0.2" y2="0" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0.2" y1="0" x2="0.6" y2="-0.4" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0.2" y1="0" x2="1.6" y2="0" width="0.127" layer="51" />
              <wire x1="-2" y1="1.4" x2="2" y2="1.4" width="0.127" layer="21" />
              <wire x1="-2" y1="-1.4" x2="2" y2="-1.4" width="0.127" layer="21" />
              <text x="-3.81" y="-0.635" size="1.27" layer="51">+</text>
            </package>
          </packages>
          <symbols>
            <symbol name="DIODES_DIODE">
              <description />
              <wire x1="-1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94" />
              <wire x1="1.27" y1="0" x2="-1.27" y2="1.905" width="0.254" layer="94" />
              <wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94" />
              <wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94" />
              <pin name="+" x="-2.54" y="0" visible="off" length="short" direction="pas" />
              <pin name="-" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180" />
              <text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
              <text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="TANT_CAP">
              <description />
              <wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94" />
              <wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat" />
              <wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat" />
              <text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
              <text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
              <rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94" />
              <rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94" />
              <pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270" />
              <pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90" />
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="D" name="1N4148,113">
              <description />
              <gates>
                <gate name="A" symbol="DIODES_DIODE" x="0" y="0" />
              </gates>
              <devices>
                <device package="DIODES_DO-35" name="1N4148">
                  <connects>
                    <connect gate="A" pin="+" pad="+" />
                    <connect gate="A" pin="-" pad="-" />
                  </connects>
                  <technologies>
                    <technology name="">
                      <attribute name="SPICE" value="D{NAME} {+.NET} {-.NET} 1N4148" />
                      <attribute name="SPICEMOD" value=".model 1N4148 D (IS=0.1PA, RS=16 CJO=2PF TT=12N BV=100 IBV=1nA)" />
                    </technology>
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="TANT_CAP">
              <description />
              <gates>
                <gate name="G$1" symbol="TANT_CAP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TANT_6032-METRIC" name="6032-METRIC">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
                <device package="TANT_2012-METRIC" name="2012-METRIC">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
                <device package="TANT_3216-METRIC" name="3216-METRIC">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
                <device package="TANT_7343-METRIC" name="7343-METRIC">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="IC">
          <description />
          <packages>
            <package name="SOT-223">
              <description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;</description>
              <wire x1="3.277" y1="1.778" x2="3.277" y2="-1.778" width="0.2032" layer="21" />
              <wire x1="3.277" y1="-1.778" x2="-3.277" y2="-1.778" width="0.2032" layer="21" />
              <wire x1="-3.277" y1="-1.778" x2="-3.277" y2="1.778" width="0.2032" layer="21" />
              <wire x1="-3.277" y1="1.778" x2="3.277" y2="1.778" width="0.2032" layer="21" />
              <wire x1="-3.473" y1="4.483" x2="3.473" y2="4.483" width="0.0508" layer="39" />
              <wire x1="3.473" y1="-4.483" x2="-3.473" y2="-4.483" width="0.0508" layer="39" />
              <wire x1="-3.473" y1="-4.483" x2="-3.473" y2="4.483" width="0.0508" layer="39" />
              <wire x1="3.473" y1="4.483" x2="3.473" y2="-4.483" width="0.0508" layer="39" />
              <smd name="1" x="-2.286" y="-3.175" dx="1.27" dy="2.286" layer="1" />
              <smd name="2" x="0" y="-3.175" dx="1.27" dy="2.286" layer="1" />
              <smd name="3" x="2.286" y="-3.175" dx="1.27" dy="2.286" layer="1" />
              <smd name="TER" x="0" y="3.175" dx="3.556" dy="2.159" layer="1" />
              <text x="-3.81" y="-2.032" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
              <text x="-2.54" y="-0.635" size="0.889" layer="27" ratio="11">&gt;VALUE</text>
              <rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51" />
              <rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51" />
              <rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51" />
              <rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51" />
              <rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51" />
              <rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51" />
              <rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51" />
              <rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51" />
              <rectangle x1="-1" y1="-1" x2="1" y2="1" layer="35" />
            </package>
          </packages>
          <symbols>
            <symbol name="PMIC-LM317EMP">
              <description />
              <pin name="ADJ" x="0" y="-7.62" length="short" rot="R90" />
              <pin name="VOUT" x="12.7" y="0" length="short" rot="R180" />
              <pin name="VIN" x="-12.7" y="0" length="short" />
              <text x="-8.89" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
              <text x="-1.27" y="3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
              <wire x1="-8.89" y1="-3.81" x2="0" y2="-3.81" width="0.1524" layer="94" />
              <wire x1="0" y1="-3.81" x2="8.89" y2="-3.81" width="0.1524" layer="94" />
              <wire x1="8.89" y1="-3.81" x2="8.89" y2="0" width="0.1524" layer="94" />
              <wire x1="8.89" y1="0" x2="8.89" y2="3.81" width="0.1524" layer="94" />
              <wire x1="8.89" y1="3.81" x2="-8.89" y2="3.81" width="0.1524" layer="94" />
              <wire x1="-8.89" y1="3.81" x2="-8.89" y2="0" width="0.1524" layer="94" />
              <wire x1="-8.89" y1="0" x2="-8.89" y2="-3.81" width="0.1524" layer="94" />
              <wire x1="-10.16" y1="0" x2="-8.89" y2="0" width="0.1524" layer="94" />
              <wire x1="10.16" y1="0" x2="8.89" y2="0" width="0.1524" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94" />
            </symbol>
          </symbols>
          <devicesets>
            <deviceset uservalue="yes" prefix="U" name="LM317EMP">
              <description />
              <gates>
                <gate name="G$1" symbol="PMIC-LM317EMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOT-223" name="-310030006">
                  <connects>
                    <connect gate="G$1" pin="ADJ" pad="1" />
                    <connect gate="G$1" pin="VIN" pad="3" />
                    <connect gate="G$1" pin="VOUT" pad="2 TER" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
      </libraries>
      <attributes />
      <variantdefs />
      <classes>
        <class number="0" name="default" />
      </classes>
      <parts>
        <part device="" value="PMP4201Y,115" name="Q1$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q2$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q3$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q4$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q5$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q6" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q7" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q8" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q9" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="ERJ-PA3J473V" name="R1$1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R2$1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-040" value="5002" name="1" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="2" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="3" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="4" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="5" library="keystone" deviceset="TP" />
        <part device="" value="C1608JB1H105K080AB" name="C1" library="mlcc" deviceset="C_0603" />
        <part device="" value="CL05B332JB5NNNC" name="C10" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0603C473J5RACTU" name="C11" library="mlcc" deviceset="C_0603" />
        <part device="" value="C1005CH1H470J050BA" name="C2" library="mlcc" deviceset="C_0402" />
        <part device="" value="CL10B104JB8NNNC" name="C3" library="mlcc" deviceset="C_0603" />
        <part device="" value="C2012JB1C106K085AC" name="C4" library="mlcc" deviceset="C_0805" />
        <part device="" value="C1005CH1H470J050BA" name="C5" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0603C123J5RACTU" name="C6" library="mlcc" deviceset="C_0603" />
        <part device="" value="GRM155R71H222JA01J" name="C9" library="mlcc" deviceset="C_0402" />
        <part device="1N4148" value="1N4148,113" name="D1" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D2" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D3" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D4" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D5" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D6" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D7" library="ecad" deviceset="1N4148,113" />
        <part device="" value="2N3819" name="Q1" library="General_Will" deviceset="2N3819" />
        <part device="" value="KSC1008YBU" name="Q2" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q3" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="2N3819" name="Q4" library="General_Will" deviceset="2N3819" />
        <part device="" value="PN2907ABU" name="Q5" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="MF-RES-0603-10K" name="R1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R10" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R11" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R12" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R13" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R14" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R15" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R16" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R17" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R18" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R19" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R20" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R21" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R22" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R23" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R24" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J274V" name="R25" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R26" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ESR03EZPJ475" name="R27" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R28" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R29" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R3" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100" name="R30" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R31" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R32" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-15K" name="R33" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R34" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R35" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R36" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-8.2K" name="R37" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R4" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R5" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R6" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-5.6K" name="R7" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R8" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R9" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U2" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="TL082CD" name="U3" library="General_Will" deviceset="TL082CD" />
        <part device="" value="TL082CD" name="U4" library="General_Will" deviceset="TL082CD" />
        <part device="" value="TL082CD" name="U5" library="General_Will" deviceset="TL082CD" />
        <part device="" value="CT6EP104" name="VR1" library="General_Will" deviceset="CT6EP104" />
        <part device="" value="CL10B104JB8NNNC" name="C12" library="mlcc" deviceset="C_0603" />
        <part device="" value="C1608JB1H105K080AB" name="C13" library="mlcc" deviceset="C_0603" />
        <part device="" value="C1005CH1H470J050BA" name="C16" library="mlcc" deviceset="C_0402" />
        <part device="1N4148" value="1N4148,113" name="D10" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D8" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D9" library="ecad" deviceset="1N4148,113" />
        <part device="" value="MF-RES-0603-1M" name="R38" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R39" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R40" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R41" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R42" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R43" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R44" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R45" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R46" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R48" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R49" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R50" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-5.6K" name="R66" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U7" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U8" library="General_Will" deviceset="UPC4558G2" />
        <part device="-040" value="5002" name="8" library="keystone" deviceset="TP" />
        <part device="6032-METRIC" value="F931C476KCC" name="C14" library="ecad" deviceset="TANT_CAP" />
        <part device="6032-METRIC" value="F931C476KCC" name="C15" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="CL10B104JB8NNNC" name="C23" library="mlcc" deviceset="C_0603" />
        <part device="" value="C1608JB1H105K080AB" name="C24" library="mlcc" deviceset="C_0603" />
        <part device="" value="C1005CH1H470J050BA" name="C25" library="mlcc" deviceset="C_0402" />
        <part device="1N4148" value="1N4148,113" name="D12" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D13" library="ecad" deviceset="1N4148,113" />
        <part device="" value="KSC1008YBU" name="Q7$1" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="MF-RES-0603-1M" name="R67" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R68" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R69" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R70" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R71" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R72" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R73" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R74" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R75" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R76" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J823V" name="R801" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-15K" name="R802" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J332V" name="R803" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="OS103011MA7QP1" name="SW1" library="General_Will" deviceset="OS103011MS8QP1" />
        <part device="" value="UPC4558G2" name="U11" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="PMP4201Y,115" name="Q1$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q2$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q3$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q4$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q5$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q6$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q7$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q8$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q9$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="ERJ-PA3J473V" name="R1$2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R2$2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="CGA2B3X7R1H223K050BB" name="C17" library="mlcc" deviceset="C_0402" />
        <part device="" value="C2012JB1C106K085AC" name="C18" library="mlcc" deviceset="C_0805" />
        <part device="" value="GRM155R71H222JA01J" name="C21" library="mlcc" deviceset="C_0402" />
        <part device="" value="CL05B332JB5NNNC" name="C22" library="mlcc" deviceset="C_0402" />
        <part device="1N4148" value="1N4148,113" name="D11" library="ecad" deviceset="1N4148,113" />
        <part device="" value="PN2907ABU" name="Q6$1" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="ERJ-PA3J683V" name="R47" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R51" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R52" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R53" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R54" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R55" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J274V" name="R56" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R57" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-8GEYJ225V" name="R58" library="resistor" deviceset="RESISTOR_1206" />
        <part device="" value="MF-RES-0603-100" name="R59" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R60" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R61" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R62" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R63" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R64" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R65" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U10" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="TL082CD" name="U9" library="General_Will" deviceset="TL082CD" />
        <part device="" value="CT6EP104" name="VR2" library="General_Will" deviceset="CT6EP104" />
        <part device="2012-METRIC" value="298D105X0050P2T" name="C601" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="06035C101JAT2A" name="C602" library="mlcc" deviceset="C_0603" />
        <part device="" value="C0603X100J5GACTU" name="C603" library="mlcc" deviceset="C_0603" />
        <part device="" value="C0603S103J5RACTU" name="C604" library="mlcc" deviceset="C_0603" />
        <part device="" value="C0603S103J5RACTU" name="C605" library="mlcc" deviceset="C_0603" />
        <part device="6032-METRIC" value="F931C476KCC" name="C606" library="ecad" deviceset="TANT_CAP" />
        <part device="6032-METRIC" value="F931C476KCC" name="C607" library="ecad" deviceset="TANT_CAP" />
        <part device="6032-METRIC" value="F931C476KCC" name="C608" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="KSC1008YBU" name="Q61" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="PN2907ABU" name="Q62" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="MF-RES-0603-100k" name="R601" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J332V" name="R602" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R603" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R604" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-47" name="R605" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100" name="R606" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R607" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R608" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R609" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R610" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-040" value="5002" name="11" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="9" library="keystone" deviceset="TP" />
        <part device="" value="C1005CH1H470J050BA" name="C30" library="mlcc" deviceset="C_0402" />
        <part device="" value="C1005CH1H470J050BA" name="C31" library="mlcc" deviceset="C_0402" />
        <part device="2012-METRIC" value="298D105X0050P2T" name="C32" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="298D105X0050P2T" name="C33" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="298D105X0050P2T" name="C34" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="298D105X0050P2T" name="C35" library="ecad" deviceset="TANT_CAP" />
        <part device="3216-METRIC" value="TAJA474K050RNJ" name="C36" library="ecad" deviceset="TANT_CAP" />
        <part device="3216-METRIC" value="TAJA474K050RNJ" name="C37" library="ecad" deviceset="TANT_CAP" />
        <part device="1N4148" value="1N4148,113" name="D14" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D15" library="ecad" deviceset="1N4148,113" />
        <part device="" value="OS203011MA2QP1" name="Output_Level" library="General_Will" deviceset="OS203011MS1QP1" />
        <part device="" value="2N3819" name="Q10" library="General_Will" deviceset="2N3819" />
        <part device="" value="KSC1008YBU" name="Q11" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q12" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="2N3819" name="Q13" library="General_Will" deviceset="2N3819" />
        <part device="" value="2N3819" name="Q14" library="General_Will" deviceset="2N3819" />
        <part device="" value="2N3819" name="Q15" library="General_Will" deviceset="2N3819" />
        <part device="" value="2N3819" name="Q8$2" library="General_Will" deviceset="2N3819" />
        <part device="" value="2N3819" name="Q9$2" library="General_Will" deviceset="2N3819" />
        <part device="" value="MF-RES-0603-10K" name="R100" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-15K" name="R101" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-15K" name="R102" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R103" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R104" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R105" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R106" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R107" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R77" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R78" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R79" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R80" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J393V" name="R804" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-8.2K" name="R805" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J222V" name="R806" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J393V" name="R807" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-8.2K" name="R808" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J222V" name="R809" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R81" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R810" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J222V" name="R82" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J222V" name="R83" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R84" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R85" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R86" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R87" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R88" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R89" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R90" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R91" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R92" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R93" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R94" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R95" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R96" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R97" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R98" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J563V" name="R99" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U12" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="C0402T103K4RALTU" name="C148" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C149" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C188" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C189" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C19" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C20" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C228" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C229" library="mlcc" deviceset="C_0402" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C39" library="ecad" deviceset="TANT_CAP" />
        <part device="6032-METRIC" value="F931C476KCC" name="C40" library="ecad" deviceset="TANT_CAP" />
        <part device="7343-METRIC" value="TAJE477K010RNJ" name="C41" library="ecad" deviceset="TANT_CAP" />
        <part device="7343-METRIC" value="T491X227M016AT" name="C42" library="ecad" deviceset="TANT_CAP" />
        <part device="7343-METRIC" value="TAJE477K010RNJ" name="C43" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="C0402T103K4RALTU" name="C7" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C8" library="mlcc" deviceset="C_0402" />
        <part device="1N4148" value="1N4148,113" name="D16" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D17" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D18" library="ecad" deviceset="1N4148,113" />
        <part device="" value="KSC1008YBU" name="Q16" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="PN2907ABU" name="Q17" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="MF-RES-0603-10K" name="R108" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R109" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R110" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R111" library="resistor" deviceset="RESISTOR_0603" />
        <part device="7343-METRIC" value="293D106X0050E2TE3" name="C1$1" library="ecad" deviceset="TANT_CAP" />
        <part device="7343-METRIC" value="293D106X0050E2TE3" name="C2$1" library="ecad" deviceset="TANT_CAP" />
        <part device="3216-METRIC" value="T491A105M025AT" name="C3$1" library="ecad" deviceset="TANT_CAP" />
        <part device="7343-METRIC" value="T491X108K006AT" name="C351" library="ecad" deviceset="TANT_CAP" />
        <part device="7343-METRIC" value="T491X108K006AT" name="C352" library="ecad" deviceset="TANT_CAP" />
        <part device="3216-METRIC" value="T491A105M025AT" name="C4$1" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="CRCW0603240RJNEAHP" name="R1$3" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="CRCW0603240RJNEAHP" name="R2$3" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J272V" name="R3$1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J272V" name="R4$1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-310030006" value="LM317EMP" name="U22" library="IC" deviceset="LM317EMP" />
        <part device="" value="LM337H/NOPB" name="U24" library="General_Will" deviceset="LM337H" />
        <part device="" value="W02" name="W02" library="General_Will" deviceset="W02" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C241" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C51" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C52" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="ERJ-PA3J121V" name="R137" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-560" name="R138" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J121V" name="R141" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-560" name="R142" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J121V" name="R424" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J121V" name="R425" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-310030006" value="LM317EMP" name="U21" library="IC" deviceset="LM317EMP" />
        <part device="" value="LM337H/NOPB" name="U23" library="General_Will" deviceset="LM337H" />
        <part device="" value="LM337H/NOPB" name="U70" library="General_Will" deviceset="LM337H" />
        <part device="" value="" name="J1" library="General_Will" deviceset="PJ-102AH" />
        <part device="" value="282834-3" name="A+B" library="General_Will" deviceset="282834-3" />
        <part device="" value="NSJ8HC" name="Guitar/Inst_In" library="General_Will" deviceset="NSJ8HC" />
        <part device="" value="282834-3" name="Guitar_Amp" library="General_Will" deviceset="282834-3" />
        <part device="" value="1708136" name="jBottom" library="General_Will" deviceset="1708136" />
        <part device="" value="NCJ5FI-H-0" name="MIC_In" library="General_Will" deviceset="NCJ5FI-H-0" />
        <part device="" value="NRJ4HF" name="Vocal_Amp_B" library="General_Will" deviceset="NRJ4HF" />
        <part device="" value="NSJ8HC" name="Vocoder/Hold" library="General_Will" deviceset="NSJ8HC" />
      </parts>
      <sheets>
        <sheet>
          <description />
          <plain />
          <instances>
            <instance y="1450.34" part="Q1$1" gate="G$1" x="1518.92" />
            <instance y="1440.18" part="Q2$1" gate="G$1" x="1605.28" />
            <instance y="1424.94" part="Q3$1" gate="G$1" x="1518.92" />
            <instance y="1463.04" part="Q4$1" gate="G$1" x="1605.28" />
            <instance y="1435.10" part="Q5$1" gate="G$1" x="1562.10" />
            <instance y="1473.20" part="Q6" gate="G$1" x="1648.46" />
            <instance y="1496.06" part="Q7" gate="G$1" x="1666.24" />
            <instance y="1518.92" part="Q8" gate="G$1" x="1709.42" />
            <instance y="1518.92" part="Q9" gate="G$1" x="1666.24" />
            <instance y="1488.44" part="R1$1" gate="G$1" x="1635.76" />
            <instance y="1534.16" part="R2$1" gate="G$1" x="1696.72" />
            <instance y="1473.20" part="1" gate="G$1" x="1856.74" />
            <instance y="1417.32" part="2" gate="G$1" x="1732.28" />
            <instance y="1480.82" part="3" gate="G$1" x="1856.74" />
            <instance y="1417.32" part="4" gate="G$1" x="1739.90" />
            <instance y="1203.96" part="5" gate="G$1" x="1198.88" />
            <instance y="1231.90" part="C1" gate="G$1" x="1247.14" />
            <instance y="1239.52" part="C10" gate="G$1" x="1272.54" />
            <instance y="1290.32" part="C11" gate="G$1" x="1325.88" />
            <instance y="1264.92" part="C2" gate="G$1" x="1308.10" />
            <instance y="1132.84" part="C3" gate="G$1" x="1115.06" />
            <instance y="1381.76" part="C4" gate="G$1" x="1445.26" />
            <instance y="1445.26" part="C5" gate="G$1" x="1783.08" />
            <instance y="1437.64" part="C6" gate="G$1" x="1757.68" />
            <instance y="1320.80" part="C9" gate="G$1" x="1351.28" />
            <instance y="1094.74" part="D1" gate="A" x="1036.32" />
            <instance y="1112.52" part="D2" gate="A" x="1049.02" />
            <instance y="1211.58" part="D3" gate="A" x="1226.82" />
            <instance y="1153.16" part="D4" gate="A" x="1135.38" />
            <instance y="1308.10" part="D5" gate="A" x="1353.82" />
            <instance y="1457.96" part="D6" gate="A" x="1823.72" />
            <instance y="1432.56" part="D7" gate="A" x="1767.84" />
            <instance y="1478.28" part="Q1" gate="G$1" x="1826.26" />
            <instance y="1226.82" part="Q2" gate="G$1" x="1231.90" />
            <instance y="1292.86" part="Q3" gate="G$1" x="1341.12" />
            <instance y="1272.54" part="Q4" gate="G$1" x="1295.40" />
            <instance y="1409.70" part="Q5" gate="G$1" x="1470.66" />
            <instance y="1196.34" part="R1" gate="G$1" x="1186.18" />
            <instance y="1112.52" part="R10" gate="G$1" x="1064.26" />
            <instance y="1120.14" part="R11" gate="G$1" x="1064.26" />
            <instance y="1153.16" part="R12" gate="G$1" x="1150.62" />
            <instance y="1145.54" part="R13" gate="G$1" x="1137.92" />
            <instance y="1160.78" part="R14" gate="G$1" x="1150.62" />
            <instance y="1277.62" part="R15" gate="G$1" x="1330.96" />
            <instance y="1196.34" part="R16" gate="G$1" x="1203.96" />
            <instance y="1226.82" part="R17" gate="G$1" x="1259.84" />
            <instance y="1252.22" part="R18" gate="G$1" x="1277.62" />
            <instance y="1219.20" part="R19" gate="G$1" x="1252.22" />
            <instance y="1277.62" part="R2" gate="G$1" x="1313.18" />
            <instance y="1234.44" part="R20" gate="G$1" x="1259.84" />
            <instance y="1394.46" part="R21" gate="G$1" x="1468.12" />
            <instance y="1402.08" part="R22" gate="G$1" x="1490.98" />
            <instance y="1424.94" part="R23" gate="G$1" x="1744.98" />
            <instance y="1341.12" part="R24" gate="G$1" x="1397.00" />
            <instance y="1424.94" part="R25" gate="G$1" x="1762.76" />
            <instance y="1341.12" part="R26" gate="G$1" x="1414.78" />
            <instance y="1211.58" part="R27" gate="G$1" x="1211.58" />
            <instance y="1308.10" part="R28" gate="G$1" x="1338.58" />
            <instance y="1473.20" part="R29" gate="G$1" x="1844.04" />
            <instance y="1348.74" part="R3" gate="G$1" x="1414.78" />
            <instance y="1376.68" part="R30" gate="G$1" x="1432.56" />
            <instance y="1394.46" part="R31" gate="G$1" x="1450.34" />
            <instance y="1409.70" part="R32" gate="G$1" x="1737.36" />
            <instance y="1440.18" part="R33" gate="G$1" x="1770.38" />
            <instance y="1315.72" part="R34" gate="G$1" x="1363.98" />
            <instance y="1252.22" part="R35" gate="G$1" x="1295.40" />
            <instance y="1203.96" part="R36" gate="G$1" x="1211.58" />
            <instance y="1465.58" part="R37" gate="G$1" x="1844.04" />
            <instance y="998.22" part="R4" gate="G$1" x="1021.08" />
            <instance y="982.98" part="R5" gate="G$1" x="1021.08" />
            <instance y="990.60" part="R6" gate="G$1" x="1021.08" />
            <instance y="1160.78" part="R7" gate="G$1" x="1168.40" />
            <instance y="1120.14" part="R8" gate="G$1" x="1082.04" />
            <instance y="1145.54" part="R9" gate="G$1" x="1120.14" />
            <instance y="1178.56" part="U2" gate="G$1" x="1196.34" />
            <instance y="1137.92" part="U3" gate="G$1" x="1092.20" />
            <instance y="1358.90" part="U4" gate="G$1" x="1442.72" />
            <instance y="1468.12" part="U5" gate="G$1" x="1798.32" />
            <instance y="1330.96" part="VR1" gate="G$1" x="1369.06" />
            <instance y="820.42" part="C12" gate="G$1" x="401.32" />
            <instance y="828.04" part="C13" gate="G$1" x="408.94" />
            <instance y="850.90" part="C16" gate="G$1" x="464.82" />
            <instance y="878.84" part="D10" gate="A" x="561.34" />
            <instance y="815.34" part="D8" gate="A" x="411.48" />
            <instance y="845.82" part="D9" gate="A" x="454.66" />
            <instance y="767.08" part="R38" gate="G$1" x="386.08" />
            <instance y="772.16" part="R39" gate="G$1" x="403.86" />
            <instance y="838.20" part="R40" gate="G$1" x="439.42" />
            <instance y="838.20" part="R41" gate="G$1" x="457.20" />
            <instance y="863.60" part="R42" gate="G$1" x="469.90" />
            <instance y="822.96" part="R43" gate="G$1" x="421.64" />
            <instance y="830.58" part="R44" gate="G$1" x="421.64" />
            <instance y="830.58" part="R45" gate="G$1" x="439.42" />
            <instance y="863.60" part="R46" gate="G$1" x="487.68" />
            <instance y="797.56" part="R48" gate="G$1" x="388.62" />
            <instance y="784.86" part="R49" gate="G$1" x="388.62" />
            <instance y="789.94" part="R50" gate="G$1" x="406.40" />
            <instance y="871.22" part="R66" gate="G$1" x="525.78" />
            <instance y="881.38" part="U7" gate="G$1" x="497.84" />
            <instance y="889.00" part="U8" gate="G$1" x="535.94" />
            <instance y="960.12" part="8" gate="G$1" x="368.30" />
            <instance y="965.20" part="C14" gate="G$1" x="360.68" />
            <instance y="947.42" part="C15" gate="G$1" x="360.68" />
            <instance y="899.16" part="C23" gate="G$1" x="266.70" />
            <instance y="802.64" part="C24" gate="G$1" x="152.40" />
            <instance y="853.44" part="C25" gate="G$1" x="231.14" />
            <instance y="934.72" part="D12" gate="A" x="340.36" />
            <instance y="927.10" part="D13" gate="A" x="322.58" />
            <instance y="949.96" part="Q7$1" gate="G$1" x="345.44" />
            <instance y="894.08" part="R67" gate="G$1" x="254.00" />
            <instance y="911.86" part="R68" gate="G$1" x="289.56" />
            <instance y="802.64" part="R69" gate="G$1" x="165.10" />
            <instance y="927.10" part="R70" gate="G$1" x="307.34" />
            <instance y="866.14" part="R71" gate="G$1" x="236.22" />
            <instance y="919.48" part="R72" gate="G$1" x="289.56" />
            <instance y="934.72" part="R73" gate="G$1" x="325.12" />
            <instance y="919.48" part="R74" gate="G$1" x="307.34" />
            <instance y="848.36" part="R75" gate="G$1" x="218.44" />
            <instance y="825.50" part="R76" gate="G$1" x="200.66" />
            <instance y="810.26" part="R801" gate="G$1" x="182.88" />
            <instance y="817.88" part="R802" gate="G$1" x="182.88" />
            <instance y="817.88" part="R803" gate="G$1" x="200.66" />
            <instance y="833.12" part="SW1" gate="G$1" x="226.06" />
            <instance y="876.30" part="U11" gate="G$1" x="264.16" />
            <instance y="1681.48" part="Q1$2" gate="G$1" x="1247.14" />
            <instance y="1671.32" part="Q2$2" gate="G$1" x="1333.50" />
            <instance y="1656.08" part="Q3$2" gate="G$1" x="1247.14" />
            <instance y="1694.18" part="Q4$2" gate="G$1" x="1333.50" />
            <instance y="1666.24" part="Q5$2" gate="G$1" x="1290.32" />
            <instance y="1704.34" part="Q6$2" gate="G$1" x="1376.68" />
            <instance y="1727.20" part="Q7$2" gate="G$1" x="1394.46" />
            <instance y="1750.06" part="Q8$1" gate="G$1" x="1437.64" />
            <instance y="1750.06" part="Q9$1" gate="G$1" x="1394.46" />
            <instance y="1719.58" part="R1$2" gate="G$1" x="1363.98" />
            <instance y="1765.30" part="R2$2" gate="G$1" x="1424.94" />
            <instance y="1582.42" part="C17" gate="G$1" x="1069.34" />
            <instance y="1645.92" part="C18" gate="G$1" x="1206.50" />
            <instance y="1615.44" part="C21" gate="G$1" x="1120.14" />
            <instance y="1564.64" part="C22" gate="G$1" x="1051.56" />
            <instance y="1595.12" part="D11" gate="A" x="1089.66" />
            <instance y="1633.22" part="Q6$1" gate="G$1" x="1173.48" />
            <instance y="1617.98" part="R47" gate="G$1" x="1170.94" />
            <instance y="1577.34" part="R51" gate="G$1" x="1056.64" />
            <instance y="1625.60" part="R52" gate="G$1" x="1193.80" />
            <instance y="1775.46" part="R53" gate="G$1" x="1236.98" />
            <instance y="1783.08" part="R54" gate="G$1" x="1254.76" />
            <instance y="1633.22" part="R55" gate="G$1" x="1211.58" />
            <instance y="1790.70" part="R56" gate="G$1" x="1254.76" />
            <instance y="1610.36" part="R57" gate="G$1" x="1132.84" />
            <instance y="1567.18" part="R58" gate="G$1" x="1016.00" />
            <instance y="1775.46" part="R59" gate="G$1" x="1219.20" />
            <instance y="1783.08" part="R60" gate="G$1" x="1236.98" />
            <instance y="1798.32" part="R61" gate="G$1" x="1272.54" />
            <instance y="1790.70" part="R62" gate="G$1" x="1272.54" />
            <instance y="1602.74" part="R63" gate="G$1" x="1125.22" />
            <instance y="1595.12" part="R64" gate="G$1" x="1074.42" />
            <instance y="1551.94" part="R65" gate="G$1" x="1031.24" />
            <instance y="1600.20" part="U10" gate="G$1" x="1028.70" />
            <instance y="1628.14" part="U9" gate="G$1" x="1143.00" />
            <instance y="1610.36" part="VR2" gate="G$1" x="1097.28" />
            <instance y="609.60" part="C601" gate="G$1" x="170.18" />
            <instance y="635.00" part="C602" gate="G$1" x="205.74" />
            <instance y="670.56" part="C603" gate="G$1" x="246.38" />
            <instance y="721.36" part="C604" gate="G$1" x="304.80" />
            <instance y="703.58" part="C605" gate="G$1" x="304.80" />
            <instance y="739.14" part="C606" gate="G$1" x="312.42" />
            <instance y="652.78" part="C607" gate="G$1" x="223.52" />
            <instance y="721.36" part="C608" gate="G$1" x="312.42" />
            <instance y="673.10" part="Q61" gate="G$1" x="231.14" />
            <instance y="698.50" part="Q62" gate="G$1" x="271.78" />
            <instance y="622.30" part="R601" gate="G$1" x="175.26" />
            <instance y="622.30" part="R602" gate="G$1" x="193.04" />
            <instance y="629.92" part="R603" gate="G$1" x="193.04" />
            <instance y="683.26" part="R604" gate="G$1" x="269.24" />
            <instance y="698.50" part="R605" gate="G$1" x="292.10" />
            <instance y="647.70" part="R606" gate="G$1" x="210.82" />
            <instance y="683.26" part="R607" gate="G$1" x="251.46" />
            <instance y="690.88" part="R608" gate="G$1" x="292.10" />
            <instance y="734.06" part="R609" gate="G$1" x="325.12" />
            <instance y="741.68" part="R610" gate="G$1" x="325.12" />
            <instance y="1341.12" part="11" gate="G$1" x="767.08" />
            <instance y="1348.74" part="9" gate="G$1" x="767.08" />
            <instance y="1338.58" part="C30" gate="G$1" x="736.60" />
            <instance y="1313.18" part="C31" gate="G$1" x="698.50" />
            <instance y="1361.44" part="C32" gate="G$1" x="782.32" />
            <instance y="1361.44" part="C33" gate="G$1" x="774.70" />
            <instance y="1206.50" part="C34" gate="G$1" x="566.42" />
            <instance y="1242.06" part="C35" gate="G$1" x="584.20" />
            <instance y="1386.84" part="C36" gate="G$1" x="800.10" />
            <instance y="1295.40" part="C37" gate="G$1" x="680.72" />
            <instance y="1374.14" part="D14" gate="A" x="802.64" />
            <instance y="1236.98" part="D15" gate="A" x="594.36" />
            <instance y="1457.96" part="Output_Level" gate="G$1" x="944.88" />
            <instance y="1214.12" part="Q10" gate="G$1" x="553.72" />
            <instance y="1348.74" part="Q11" gate="G$1" x="751.84" />
            <instance y="1252.22" part="Q12" gate="G$1" x="617.22" />
            <instance y="1115.06" part="Q13" gate="G$1" x="518.16" />
            <instance y="1148.08" part="Q14" gate="G$1" x="535.94" />
            <instance y="1148.08" part="Q15" gate="G$1" x="518.16" />
            <instance y="1181.10" part="Q8$2" gate="G$1" x="553.72" />
            <instance y="1181.10" part="Q9$2" gate="G$1" x="535.94" />
            <instance y="1071.88" part="R100" gate="G$1" x="429.26" />
            <instance y="1094.74" part="R101" gate="G$1" x="482.60" />
            <instance y="1079.50" part="R102" gate="G$1" x="464.82" />
            <instance y="1282.70" part="R103" gate="G$1" x="668.02" />
            <instance y="1087.12" part="R104" gate="G$1" x="464.82" />
            <instance y="1087.12" part="R105" gate="G$1" x="482.60" />
            <instance y="1071.88" part="R106" gate="G$1" x="447.04" />
            <instance y="1079.50" part="R107" gate="G$1" x="447.04" />
            <instance y="1064.26" part="R77" gate="G$1" x="411.48" />
            <instance y="1414.78" part="R78" gate="G$1" x="848.36" />
            <instance y="1414.78" part="R79" gate="G$1" x="866.14" />
            <instance y="1389.38" part="R80" gate="G$1" x="812.80" />
            <instance y="1422.40" part="R804" gate="G$1" x="866.14" />
            <instance y="1422.40" part="R805" gate="G$1" x="883.92" />
            <instance y="1430.02" part="R806" gate="G$1" x="883.92" />
            <instance y="1430.02" part="R807" gate="G$1" x="901.70" />
            <instance y="1437.64" part="R808" gate="G$1" x="901.70" />
            <instance y="1437.64" part="R809" gate="G$1" x="919.48" />
            <instance y="1389.38" part="R81" gate="G$1" x="830.58" />
            <instance y="1348.74" part="R810" gate="G$1" x="779.78" />
            <instance y="1381.76" part="R82" gate="G$1" x="812.80" />
            <instance y="1397.00" part="R83" gate="G$1" x="830.58" />
            <instance y="1275.08" part="R84" gate="G$1" x="650.24" />
            <instance y="1275.08" part="R85" gate="G$1" x="632.46" />
            <instance y="1267.46" part="R86" gate="G$1" x="614.68" />
            <instance y="1267.46" part="R87" gate="G$1" x="632.46" />
            <instance y="1325.88" part="R88" gate="G$1" x="741.68" />
            <instance y="1308.10" part="R89" gate="G$1" x="685.80" />
            <instance y="1094.74" part="R90" gate="G$1" x="500.38" />
            <instance y="1102.36" part="R91" gate="G$1" x="500.38" />
            <instance y="1374.14" part="R92" gate="G$1" x="787.40" />
            <instance y="1333.50" part="R93" gate="G$1" x="749.30" />
            <instance y="1244.60" part="R94" gate="G$1" x="596.90" />
            <instance y="1290.32" part="R95" gate="G$1" x="668.02" />
            <instance y="1282.70" part="R96" gate="G$1" x="650.24" />
            <instance y="1056.64" part="R97" gate="G$1" x="393.70" />
            <instance y="1056.64" part="R98" gate="G$1" x="411.48" />
            <instance y="1064.26" part="R99" gate="G$1" x="429.26" />
            <instance y="1336.04" part="U12" gate="G$1" x="713.74" />
            <instance y="342.90" part="C148" gate="G$1" x="287.02" />
            <instance y="396.24" part="C149" gate="G$1" x="309.88" />
            <instance y="325.12" part="C188" gate="G$1" x="287.02" />
            <instance y="378.46" part="C189" gate="G$1" x="309.88" />
            <instance y="342.90" part="C19" gate="G$1" x="294.64" />
            <instance y="396.24" part="C20" gate="G$1" x="317.50" />
            <instance y="320.04" part="C228" gate="G$1" x="279.40" />
            <instance y="378.46" part="C229" gate="G$1" x="302.26" />
            <instance y="502.92" part="C39" gate="G$1" x="406.40" />
            <instance y="477.52" part="C40" gate="G$1" x="398.78" />
            <instance y="459.74" part="C41" gate="G$1" x="368.30" />
            <instance y="495.30" part="C42" gate="G$1" x="398.78" />
            <instance y="459.74" part="C43" gate="G$1" x="375.92" />
            <instance y="360.68" part="C7" gate="G$1" x="294.64" />
            <instance y="414.02" part="C8" gate="G$1" x="317.50" />
            <instance y="490.22" part="D16" gate="A" x="408.94" />
            <instance y="424.18" part="D17" gate="A" x="340.36" />
            <instance y="416.56" part="D18" gate="A" x="327.66" />
            <instance y="439.42" part="Q16" gate="G$1" x="375.92" />
            <instance y="480.06" part="Q17" gate="G$1" x="383.54" />
            <instance y="424.18" part="R108" gate="G$1" x="355.60" />
            <instance y="431.80" part="R109" gate="G$1" x="355.60" />
            <instance y="408.94" part="R110" gate="G$1" x="330.20" />
            <instance y="416.56" part="R111" gate="G$1" x="342.90" />
            <instance y="205.74" part="C1$1" gate="G$1" x="185.42" />
            <instance y="241.30" part="C2$1" gate="G$1" x="220.98" />
            <instance y="266.70" part="C3$1" gate="G$1" x="271.78" />
            <instance y="170.18" part="C351" gate="G$1" x="142.24" />
            <instance y="187.96" part="C352" gate="G$1" x="167.64" />
            <instance y="284.48" part="C4$1" gate="G$1" x="271.78" />
            <instance y="254.00" part="R1$3" gate="G$1" x="259.08" />
            <instance y="261.62" part="R2$3" gate="G$1" x="259.08" />
            <instance y="218.44" part="R3$1" gate="G$1" x="190.50" />
            <instance y="236.22" part="R4$1" gate="G$1" x="208.28" />
            <instance y="223.52" part="U22" gate="G$1" x="215.90" />
            <instance y="256.54" part="U24" gate="G$1" x="233.68" />
            <instance y="144.78" part="W02" gate="G$1" x="154.94" />
            <instance y="381.00" part="C241" gate="G$1" x="233.68" />
            <instance y="312.42" part="C51" gate="G$1" x="157.48" />
            <instance y="330.20" part="C52" gate="G$1" x="157.48" />
            <instance y="358.14" part="R137" gate="G$1" x="220.98" />
            <instance y="350.52" part="R138" gate="G$1" x="220.98" />
            <instance y="342.90" part="R141" gate="G$1" x="203.20" />
            <instance y="350.52" part="R142" gate="G$1" x="203.20" />
            <instance y="393.70" part="R424" gate="G$1" x="246.38" />
            <instance y="393.70" part="R425" gate="G$1" x="264.16" />
            <instance y="330.20" part="U21" gate="G$1" x="177.80" />
            <instance y="345.44" part="U23" gate="G$1" x="177.80" />
            <instance y="378.46" part="U70" gate="G$1" x="254.00" />
            <instance y="96.52" part="J1" gate="G$1" x="81.28" />
            <instance y="1584.96" part="A+B" gate="G$1" x="1485.90" />
            <instance y="58.42" part="Guitar/Inst_In" gate="G$1" x="17.78" />
            <instance y="1602.74" part="Guitar_Amp" gate="G$1" x="1503.68" />
            <instance y="1577.34" part="jBottom" gate="G$1" x="1465.58" />
            <instance y="535.94" part="MIC_In" gate="G$1" x="73.66" />
            <instance y="1584.96" part="Vocal_Amp_B" gate="G$1" x="1511.30" />
            <instance y="942.34" part="Vocoder/Hold" gate="G$1" x="1000.76" />
          </instances>
          <busses />
          <nets>
            <net name="N$0">
              <segment>
                <wire x1="1515.11" y1="1457.96" x2="1517.65" y2="1457.96" width="0.3" layer="91" />
                <label x="1517.65" y="1457.96" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1473.20" y1="1394.46" x2="1475.74" y2="1394.46" width="0.3" layer="91" />
                <label x="1475.74" y="1394.46" size="1.27" layer="95" />
                <pinref part="R21" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1499.87" y1="1450.34" x2="1497.33" y2="1450.34" width="0.3" layer="91" />
                <label x="1497.33" y="1450.34" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1537.97" y1="1450.34" x2="1540.51" y2="1450.34" width="0.3" layer="91" />
                <label x="1540.51" y="1450.34" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$1">
              <segment>
                <wire x1="1522.73" y1="1442.72" x2="1520.19" y2="1442.72" width="0.3" layer="91" />
                <label x="1520.19" y="1442.72" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1601.47" y1="1432.56" x2="1604.01" y2="1432.56" width="0.3" layer="91" />
                <label x="1604.01" y="1432.56" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1609.09" y1="1447.80" x2="1606.55" y2="1447.80" width="0.3" layer="91" />
                <label x="1606.55" y="1447.80" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="E1" />
              </segment>
            </net>
            <net name="N$2">
              <segment>
                <wire x1="1515.11" y1="1442.72" x2="1517.65" y2="1442.72" width="0.3" layer="91" />
                <label x="1517.65" y="1442.72" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1522.73" y1="1457.96" x2="1520.19" y2="1457.96" width="0.3" layer="91" />
                <label x="1520.19" y="1457.96" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1336.04" y1="1277.62" x2="1338.58" y2="1277.62" width="0.3" layer="91" />
                <label x="1338.58" y="1277.62" size="1.27" layer="95" />
                <pinref part="R15" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1346.20" y1="1285.24" x2="1348.74" y2="1285.24" width="0.3" layer="91" />
                <label x="1348.74" y="1285.24" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="1115.06" y1="1125.22" x2="1115.06" y2="1122.68" width="0.3" layer="91" />
                <label x="1115.06" y="1122.68" size="1.27" layer="95" />
                <pinref part="C3" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1236.98" y1="1219.20" x2="1239.52" y2="1219.20" width="0.3" layer="91" />
                <label x="1239.52" y="1219.20" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="1145.54" y1="1160.78" x2="1143.00" y2="1160.78" width="0.3" layer="91" />
                <label x="1143.00" y="1160.78" size="1.27" layer="95" />
                <pinref part="R14" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="165.10" y1="347.98" x2="162.56" y2="347.98" width="0.3" layer="91" />
                <label x="162.56" y="347.98" size="1.27" layer="95" />
                <pinref part="U23" gate="G$1" pin="IN" />
              </segment>
              <segment>
                <wire x1="330.20" y1="416.56" x2="332.74" y2="416.56" width="0.3" layer="91" />
                <label x="332.74" y="416.56" size="1.27" layer="95" />
                <pinref part="D18" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="347.98" y1="416.56" x2="350.52" y2="416.56" width="0.3" layer="91" />
                <label x="350.52" y="416.56" size="1.27" layer="95" />
                <pinref part="R111" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="302.26" y1="370.84" x2="302.26" y2="368.30" width="0.3" layer="91" />
                <label x="302.26" y="368.30" size="1.27" layer="95" />
                <pinref part="C229" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="309.88" y1="370.84" x2="309.88" y2="368.30" width="0.3" layer="91" />
                <label x="309.88" y="368.30" size="1.27" layer="95" />
                <pinref part="C189" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="309.88" y1="388.62" x2="309.88" y2="386.08" width="0.3" layer="91" />
                <label x="309.88" y="386.08" size="1.27" layer="95" />
                <pinref part="C149" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="317.50" y1="388.62" x2="317.50" y2="386.08" width="0.3" layer="91" />
                <label x="317.50" y="386.08" size="1.27" layer="95" />
                <pinref part="C20" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="317.50" y1="406.40" x2="317.50" y2="403.86" width="0.3" layer="91" />
                <label x="317.50" y="403.86" size="1.27" layer="95" />
                <pinref part="C8" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="264.16" y1="261.62" x2="266.70" y2="261.62" width="0.3" layer="91" />
                <label x="266.70" y="261.62" size="1.27" layer="95" />
                <pinref part="R2$3" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="246.38" y1="259.08" x2="248.92" y2="259.08" width="0.3" layer="91" />
                <label x="248.92" y="259.08" size="1.27" layer="95" />
                <pinref part="U24" gate="G$1" pin="OUT" />
              </segment>
              <segment>
                <wire x1="271.78" y1="279.40" x2="271.78" y2="276.86" width="0.3" layer="91" />
                <label x="271.78" y="276.86" size="1.27" layer="95" />
                <pinref part="C4$1" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="756.92" y1="1341.12" x2="759.46" y2="1341.12" width="0.3" layer="91" />
                <label x="759.46" y="1341.12" size="1.27" layer="95" />
                <pinref part="Q11" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="495.30" y1="1102.36" x2="492.76" y2="1102.36" width="0.3" layer="91" />
                <label x="492.76" y="1102.36" size="1.27" layer="95" />
                <pinref part="R91" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="800.10" y1="1381.76" x2="800.10" y2="1379.22" width="0.3" layer="91" />
                <label x="800.10" y="1379.22" size="1.27" layer="95" />
                <pinref part="C36" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="622.30" y1="1244.60" x2="624.84" y2="1244.60" width="0.3" layer="91" />
                <label x="624.84" y="1244.60" size="1.27" layer="95" />
                <pinref part="Q12" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="680.72" y1="1290.32" x2="680.72" y2="1287.78" width="0.3" layer="91" />
                <label x="680.72" y="1287.78" size="1.27" layer="95" />
                <pinref part="C37" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1559.56" x2="1442.72" y2="1559.56" width="0.3" layer="91" />
                <label x="1442.72" y="1559.56" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-12" />
              </segment>
              <segment>
                <wire x1="1127.76" y1="1620.52" x2="1125.22" y2="1620.52" width="0.3" layer="91" />
                <label x="1125.22" y="1620.52" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1013.46" y1="1592.58" x2="1010.92" y2="1592.58" width="0.3" layer="91" />
                <label x="1010.92" y="1592.58" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1372.87" y1="1696.72" x2="1375.41" y2="1696.72" width="0.3" layer="91" />
                <label x="1375.41" y="1696.72" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1380.49" y1="1711.96" x2="1377.95" y2="1711.96" width="0.3" layer="91" />
                <label x="1377.95" y="1711.96" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1243.33" y1="1673.86" x2="1245.87" y2="1673.86" width="0.3" layer="91" />
                <label x="1245.87" y="1673.86" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1250.95" y1="1689.10" x2="1248.41" y2="1689.10" width="0.3" layer="91" />
                <label x="1248.41" y="1689.10" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1441.45" y1="1742.44" x2="1438.91" y2="1742.44" width="0.3" layer="91" />
                <label x="1438.91" y="1742.44" size="1.27" layer="95" />
                <pinref part="Q8$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1390.65" y1="1742.44" x2="1393.19" y2="1742.44" width="0.3" layer="91" />
                <label x="1393.19" y="1742.44" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1398.27" y1="1757.68" x2="1395.73" y2="1757.68" width="0.3" layer="91" />
                <label x="1395.73" y="1757.68" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1719.58" x2="1356.36" y2="1719.58" width="0.3" layer="91" />
                <label x="1356.36" y="1719.58" size="1.27" layer="95" />
                <pinref part="R1$2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="482.60" y1="873.76" x2="480.06" y2="873.76" width="0.3" layer="91" />
                <label x="480.06" y="873.76" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="520.70" y1="881.38" x2="518.16" y2="881.38" width="0.3" layer="91" />
                <label x="518.16" y="881.38" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="248.92" y1="868.68" x2="246.38" y2="868.68" width="0.3" layer="91" />
                <label x="246.38" y="868.68" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="360.68" y1="960.12" x2="360.68" y2="957.58" width="0.3" layer="91" />
                <label x="360.68" y="957.58" size="1.27" layer="95" />
                <pinref part="C14" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="330.20" y1="734.06" x2="332.74" y2="734.06" width="0.3" layer="91" />
                <label x="332.74" y="734.06" size="1.27" layer="95" />
                <pinref part="R609" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1076.96" y1="1130.30" x2="1074.42" y2="1130.30" width="0.3" layer="91" />
                <label x="1074.42" y="1130.30" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1181.10" y1="1170.94" x2="1178.56" y2="1170.94" width="0.3" layer="91" />
                <label x="1178.56" y="1170.94" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1427.48" y1="1351.28" x2="1424.94" y2="1351.28" width="0.3" layer="91" />
                <label x="1424.94" y="1351.28" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1783.08" y1="1460.50" x2="1780.54" y2="1460.50" width="0.3" layer="91" />
                <label x="1780.54" y="1460.50" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1644.65" y1="1465.58" x2="1647.19" y2="1465.58" width="0.3" layer="91" />
                <label x="1647.19" y="1465.58" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1652.27" y1="1480.82" x2="1649.73" y2="1480.82" width="0.3" layer="91" />
                <label x="1649.73" y="1480.82" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1662.43" y1="1511.30" x2="1664.97" y2="1511.30" width="0.3" layer="91" />
                <label x="1664.97" y="1511.30" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1670.05" y1="1526.54" x2="1667.51" y2="1526.54" width="0.3" layer="91" />
                <label x="1667.51" y="1526.54" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1713.23" y1="1511.30" x2="1710.69" y2="1511.30" width="0.3" layer="91" />
                <label x="1710.69" y="1511.30" size="1.27" layer="95" />
                <pinref part="Q8" gate="G$1" pin="C1" />
              </segment>
            </net>
            <net name="N$3">
              <segment>
                <wire x1="1609.09" y1="1432.56" x2="1606.55" y2="1432.56" width="0.3" layer="91" />
                <label x="1606.55" y="1432.56" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1522.73" y1="1417.32" x2="1520.19" y2="1417.32" width="0.3" layer="91" />
                <label x="1520.19" y="1417.32" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1581.15" y1="1435.10" x2="1583.69" y2="1435.10" width="0.3" layer="91" />
                <label x="1583.69" y="1435.10" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$4">
              <segment>
                <wire x1="1586.23" y1="1440.18" x2="1583.69" y2="1440.18" width="0.3" layer="91" />
                <label x="1583.69" y="1440.18" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1739.90" y1="1424.94" x2="1737.36" y2="1424.94" width="0.3" layer="91" />
                <label x="1737.36" y="1424.94" size="1.27" layer="95" />
                <pinref part="R23" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1757.68" y1="1424.94" x2="1755.14" y2="1424.94" width="0.3" layer="91" />
                <label x="1755.14" y="1424.94" size="1.27" layer="95" />
                <pinref part="R25" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$5">
              <segment>
                <wire x1="1601.47" y1="1447.80" x2="1604.01" y2="1447.80" width="0.3" layer="91" />
                <label x="1604.01" y="1447.80" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1609.09" y1="1455.42" x2="1606.55" y2="1455.42" width="0.3" layer="91" />
                <label x="1606.55" y="1455.42" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1543.05" y1="1435.10" x2="1540.51" y2="1435.10" width="0.3" layer="91" />
                <label x="1540.51" y="1435.10" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$6">
              <segment>
                <wire x1="1624.33" y1="1440.18" x2="1626.87" y2="1440.18" width="0.3" layer="91" />
                <label x="1626.87" y="1440.18" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1485.90" y1="1402.08" x2="1483.36" y2="1402.08" width="0.3" layer="91" />
                <label x="1483.36" y="1402.08" size="1.27" layer="95" />
                <pinref part="R22" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$7">
              <segment>
                <wire x1="1515.11" y1="1417.32" x2="1517.65" y2="1417.32" width="0.3" layer="91" />
                <label x="1517.65" y="1417.32" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="1226.82" x2="1252.22" y2="1226.82" width="0.3" layer="91" />
                <label x="1252.22" y="1226.82" size="1.27" layer="95" />
                <pinref part="R17" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1209.04" y1="1196.34" x2="1211.58" y2="1196.34" width="0.3" layer="91" />
                <label x="1211.58" y="1196.34" size="1.27" layer="95" />
                <pinref part="R16" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="165.10" y1="330.20" x2="162.56" y2="330.20" width="0.3" layer="91" />
                <label x="162.56" y="330.20" size="1.27" layer="95" />
                <pinref part="U21" gate="G$1" pin="VIN" />
              </segment>
              <segment>
                <wire x1="337.82" y1="424.18" x2="335.28" y2="424.18" width="0.3" layer="91" />
                <label x="335.28" y="424.18" size="1.27" layer="95" />
                <pinref part="D17" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="325.12" y1="408.94" x2="322.58" y2="408.94" width="0.3" layer="91" />
                <label x="322.58" y="408.94" size="1.27" layer="95" />
                <pinref part="R110" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="287.02" y1="325.12" x2="287.02" y2="327.66" width="0.3" layer="91" />
                <label x="287.02" y="327.66" size="1.27" layer="95" />
                <pinref part="C188" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="294.64" y1="360.68" x2="294.64" y2="363.22" width="0.3" layer="91" />
                <label x="294.64" y="363.22" size="1.27" layer="95" />
                <pinref part="C7" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="294.64" y1="342.90" x2="294.64" y2="345.44" width="0.3" layer="91" />
                <label x="294.64" y="345.44" size="1.27" layer="95" />
                <pinref part="C19" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="287.02" y1="342.90" x2="287.02" y2="345.44" width="0.3" layer="91" />
                <label x="287.02" y="345.44" size="1.27" layer="95" />
                <pinref part="C148" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="279.40" y1="320.04" x2="279.40" y2="322.58" width="0.3" layer="91" />
                <label x="279.40" y="322.58" size="1.27" layer="95" />
                <pinref part="C228" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="254.00" y1="254.00" x2="251.46" y2="254.00" width="0.3" layer="91" />
                <label x="251.46" y="254.00" size="1.27" layer="95" />
                <pinref part="R1$3" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="228.60" y1="223.52" x2="231.14" y2="223.52" width="0.3" layer="91" />
                <label x="231.14" y="223.52" size="1.27" layer="95" />
                <pinref part="U22" gate="G$1" pin="VOUT" />
              </segment>
              <segment>
                <wire x1="271.78" y1="269.24" x2="271.78" y2="271.78" width="0.3" layer="91" />
                <label x="271.78" y="271.78" size="1.27" layer="95" />
                <pinref part="C3$1" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="591.82" y1="1244.60" x2="589.28" y2="1244.60" width="0.3" layer="91" />
                <label x="589.28" y="1244.60" size="1.27" layer="95" />
                <pinref part="R94" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="744.22" y1="1333.50" x2="741.68" y2="1333.50" width="0.3" layer="91" />
                <label x="741.68" y="1333.50" size="1.27" layer="95" />
                <pinref part="R93" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1587.50" x2="1442.72" y2="1587.50" width="0.3" layer="91" />
                <label x="1442.72" y="1587.50" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-1" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1569.72" x2="1455.42" y2="1569.72" width="0.3" layer="91" />
                <label x="1455.42" y="1569.72" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-8" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1587.50" x2="1455.42" y2="1587.50" width="0.3" layer="91" />
                <label x="1455.42" y="1587.50" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-1" />
              </segment>
              <segment>
                <wire x1="1329.69" y1="1686.56" x2="1332.23" y2="1686.56" width="0.3" layer="91" />
                <label x="1332.23" y="1686.56" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1337.31" y1="1701.80" x2="1334.77" y2="1701.80" width="0.3" layer="91" />
                <label x="1334.77" y="1701.80" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1398.27" y1="1719.58" x2="1395.73" y2="1719.58" width="0.3" layer="91" />
                <label x="1395.73" y="1719.58" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1243.33" y1="1648.46" x2="1245.87" y2="1648.46" width="0.3" layer="91" />
                <label x="1245.87" y="1648.46" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1250.95" y1="1663.70" x2="1248.41" y2="1663.70" width="0.3" layer="91" />
                <label x="1248.41" y="1663.70" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1765.30" x2="1417.32" y2="1765.30" width="0.3" layer="91" />
                <label x="1417.32" y="1765.30" size="1.27" layer="95" />
                <pinref part="R2$2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1413.51" y1="1727.20" x2="1416.05" y2="1727.20" width="0.3" layer="91" />
                <label x="1416.05" y="1727.20" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="1607.82" x2="1046.48" y2="1607.82" width="0.3" layer="91" />
                <label x="1046.48" y="1607.82" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1158.24" y1="1635.76" x2="1160.78" y2="1635.76" width="0.3" layer="91" />
                <label x="1160.78" y="1635.76" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="513.08" y1="889.00" x2="515.62" y2="889.00" width="0.3" layer="91" />
                <label x="515.62" y="889.00" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="551.18" y1="896.62" x2="553.72" y2="896.62" width="0.3" layer="91" />
                <label x="553.72" y="896.62" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="360.68" y1="949.96" x2="360.68" y2="952.50" width="0.3" layer="91" />
                <label x="360.68" y="952.50" size="1.27" layer="95" />
                <pinref part="C15" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="279.40" y1="883.92" x2="281.94" y2="883.92" width="0.3" layer="91" />
                <label x="281.94" y="883.92" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="350.52" y1="957.58" x2="353.06" y2="957.58" width="0.3" layer="91" />
                <label x="353.06" y="957.58" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="330.20" y1="741.68" x2="332.74" y2="741.68" width="0.3" layer="91" />
                <label x="332.74" y="741.68" size="1.27" layer="95" />
                <pinref part="R610" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1145.54" x2="1109.98" y2="1145.54" width="0.3" layer="91" />
                <label x="1109.98" y="1145.54" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1211.58" y1="1186.18" x2="1214.12" y2="1186.18" width="0.3" layer="91" />
                <label x="1214.12" y="1186.18" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1366.52" x2="1460.50" y2="1366.52" width="0.3" layer="91" />
                <label x="1460.50" y="1366.52" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1813.56" y1="1475.74" x2="1816.10" y2="1475.74" width="0.3" layer="91" />
                <label x="1816.10" y="1475.74" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1691.64" y1="1534.16" x2="1689.10" y2="1534.16" width="0.3" layer="91" />
                <label x="1689.10" y="1534.16" size="1.27" layer="95" />
                <pinref part="R2$1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1601.47" y1="1455.42" x2="1604.01" y2="1455.42" width="0.3" layer="91" />
                <label x="1604.01" y="1455.42" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1609.09" y1="1470.66" x2="1606.55" y2="1470.66" width="0.3" layer="91" />
                <label x="1606.55" y="1470.66" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1670.05" y1="1488.44" x2="1667.51" y2="1488.44" width="0.3" layer="91" />
                <label x="1667.51" y="1488.44" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1522.73" y1="1432.56" x2="1520.19" y2="1432.56" width="0.3" layer="91" />
                <label x="1520.19" y="1432.56" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="E1" />
              </segment>
            </net>
            <net name="N$8">
              <segment>
                <wire x1="1537.97" y1="1424.94" x2="1540.51" y2="1424.94" width="0.3" layer="91" />
                <label x="1540.51" y="1424.94" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1565.91" y1="1442.72" x2="1563.37" y2="1442.72" width="0.3" layer="91" />
                <label x="1563.37" y="1442.72" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1515.11" y1="1432.56" x2="1517.65" y2="1432.56" width="0.3" layer="91" />
                <label x="1517.65" y="1432.56" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1499.87" y1="1424.94" x2="1497.33" y2="1424.94" width="0.3" layer="91" />
                <label x="1497.33" y="1424.94" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$9">
              <segment>
                <wire x1="1601.47" y1="1470.66" x2="1604.01" y2="1470.66" width="0.3" layer="91" />
                <label x="1604.01" y="1470.66" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1558.29" y1="1427.48" x2="1560.83" y2="1427.48" width="0.3" layer="91" />
                <label x="1560.83" y="1427.48" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1586.23" y1="1463.04" x2="1583.69" y2="1463.04" width="0.3" layer="91" />
                <label x="1583.69" y="1463.04" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1624.33" y1="1463.04" x2="1626.87" y2="1463.04" width="0.3" layer="91" />
                <label x="1626.87" y="1463.04" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$10">
              <segment>
                <wire x1="1558.29" y1="1442.72" x2="1560.83" y2="1442.72" width="0.3" layer="91" />
                <label x="1560.83" y="1442.72" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1391.92" y1="1341.12" x2="1389.38" y2="1341.12" width="0.3" layer="91" />
                <label x="1389.38" y="1341.12" size="1.27" layer="95" />
                <pinref part="R24" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1662.43" y1="1503.68" x2="1664.97" y2="1503.68" width="0.3" layer="91" />
                <label x="1664.97" y="1503.68" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1348.74" x2="1422.40" y2="1348.74" width="0.3" layer="91" />
                <label x="1422.40" y="1348.74" size="1.27" layer="95" />
                <pinref part="R3" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1356.36" x2="1460.50" y2="1356.36" width="0.3" layer="91" />
                <label x="1460.50" y="1356.36" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$11">
              <segment>
                <wire x1="1565.91" y1="1427.48" x2="1563.37" y2="1427.48" width="0.3" layer="91" />
                <label x="1563.37" y="1427.48" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1652.27" y1="1465.58" x2="1649.73" y2="1465.58" width="0.3" layer="91" />
                <label x="1649.73" y="1465.58" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1647.19" y1="1496.06" x2="1644.65" y2="1496.06" width="0.3" layer="91" />
                <label x="1644.65" y="1496.06" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$12">
              <segment>
                <wire x1="1629.41" y1="1473.20" x2="1626.87" y2="1473.20" width="0.3" layer="91" />
                <label x="1626.87" y="1473.20" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1662.43" y1="1488.44" x2="1664.97" y2="1488.44" width="0.3" layer="91" />
                <label x="1664.97" y="1488.44" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1667.51" y1="1473.20" x2="1670.05" y2="1473.20" width="0.3" layer="91" />
                <label x="1670.05" y="1473.20" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1644.65" y1="1480.82" x2="1647.19" y2="1480.82" width="0.3" layer="91" />
                <label x="1647.19" y="1480.82" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$13">
              <segment>
                <wire x1="1670.05" y1="1503.68" x2="1667.51" y2="1503.68" width="0.3" layer="91" />
                <label x="1667.51" y="1503.68" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1670.05" y1="1511.30" x2="1667.51" y2="1511.30" width="0.3" layer="91" />
                <label x="1667.51" y="1511.30" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1690.37" y1="1518.92" x2="1687.83" y2="1518.92" width="0.3" layer="91" />
                <label x="1687.83" y="1518.92" size="1.27" layer="95" />
                <pinref part="Q8" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$14">
              <segment>
                <wire x1="1713.23" y1="1526.54" x2="1710.69" y2="1526.54" width="0.3" layer="91" />
                <label x="1710.69" y="1526.54" size="1.27" layer="95" />
                <pinref part="Q8" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1701.80" y1="1534.16" x2="1704.34" y2="1534.16" width="0.3" layer="91" />
                <label x="1704.34" y="1534.16" size="1.27" layer="95" />
                <pinref part="R2$1" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$15">
              <segment>
                <wire x1="1685.29" y1="1518.92" x2="1687.83" y2="1518.92" width="0.3" layer="91" />
                <label x="1687.83" y="1518.92" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1647.19" y1="1518.92" x2="1644.65" y2="1518.92" width="0.3" layer="91" />
                <label x="1644.65" y="1518.92" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1640.84" y1="1488.44" x2="1643.38" y2="1488.44" width="0.3" layer="91" />
                <label x="1643.38" y="1488.44" size="1.27" layer="95" />
                <pinref part="R1$1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1662.43" y1="1526.54" x2="1664.97" y2="1526.54" width="0.3" layer="91" />
                <label x="1664.97" y="1526.54" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$16">
              <segment>
                <wire x1="1856.74" y1="1470.66" x2="1856.74" y2="1468.12" width="0.3" layer="91" />
                <label x="1856.74" y="1468.12" size="1.27" layer="95" />
                <pinref part="1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1783.08" y1="1437.64" x2="1783.08" y2="1435.10" width="0.3" layer="91" />
                <label x="1783.08" y="1435.10" size="1.27" layer="95" />
                <pinref part="C5" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1775.46" y1="1440.18" x2="1778.00" y2="1440.18" width="0.3" layer="91" />
                <label x="1778.00" y="1440.18" size="1.27" layer="95" />
                <pinref part="R33" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1849.12" y1="1473.20" x2="1851.66" y2="1473.20" width="0.3" layer="91" />
                <label x="1851.66" y="1473.20" size="1.27" layer="95" />
                <pinref part="R29" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1828.80" y1="1465.58" x2="1828.80" y2="1463.04" width="0.3" layer="91" />
                <label x="1828.80" y="1463.04" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1572.26" x2="1442.72" y2="1572.26" width="0.3" layer="91" />
                <label x="1442.72" y="1572.26" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-7" />
              </segment>
              <segment>
                <wire x1="1783.08" y1="1475.74" x2="1780.54" y2="1475.74" width="0.3" layer="91" />
                <label x="1780.54" y="1475.74" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$17">
              <segment>
                <wire x1="1732.28" y1="1414.78" x2="1732.28" y2="1412.24" width="0.3" layer="91" />
                <label x="1732.28" y="1412.24" size="1.27" layer="95" />
                <pinref part="2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1732.28" y1="1409.70" x2="1729.74" y2="1409.70" width="0.3" layer="91" />
                <label x="1729.74" y="1409.70" size="1.27" layer="95" />
                <pinref part="R32" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1455.42" y1="1394.46" x2="1457.96" y2="1394.46" width="0.3" layer="91" />
                <label x="1457.96" y="1394.46" size="1.27" layer="95" />
                <pinref part="R31" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1767.84" y1="1424.94" x2="1770.38" y2="1424.94" width="0.3" layer="91" />
                <label x="1770.38" y="1424.94" size="1.27" layer="95" />
                <pinref part="R25" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1361.44" x2="1460.50" y2="1361.44" width="0.3" layer="91" />
                <label x="1460.50" y="1361.44" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$18">
              <segment>
                <wire x1="1856.74" y1="1478.28" x2="1856.74" y2="1475.74" width="0.3" layer="91" />
                <label x="1856.74" y="1475.74" size="1.27" layer="95" />
                <pinref part="3" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1849.12" y1="1465.58" x2="1851.66" y2="1465.58" width="0.3" layer="91" />
                <label x="1851.66" y="1465.58" size="1.27" layer="95" />
                <pinref part="R37" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1813.56" y1="1470.66" x2="1816.10" y2="1470.66" width="0.3" layer="91" />
                <label x="1816.10" y="1470.66" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1272.54" y1="1231.90" x2="1272.54" y2="1229.36" width="0.3" layer="91" />
                <label x="1272.54" y="1229.36" size="1.27" layer="95" />
                <pinref part="C10" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1315.72" x2="1356.36" y2="1315.72" width="0.3" layer="91" />
                <label x="1356.36" y="1315.72" size="1.27" layer="95" />
                <pinref part="R34" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1813.56" y1="1465.58" x2="1816.10" y2="1465.58" width="0.3" layer="91" />
                <label x="1816.10" y="1465.58" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$19">
              <segment>
                <wire x1="1739.90" y1="1414.78" x2="1739.90" y2="1412.24" width="0.3" layer="91" />
                <label x="1739.90" y="1412.24" size="1.27" layer="95" />
                <pinref part="4" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1463.04" y1="1394.46" x2="1460.50" y2="1394.46" width="0.3" layer="91" />
                <label x="1460.50" y="1394.46" size="1.27" layer="95" />
                <pinref part="R21" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1475.74" y1="1419.86" x2="1478.28" y2="1419.86" width="0.3" layer="91" />
                <label x="1478.28" y="1419.86" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$20">
              <segment>
                <wire x1="1198.88" y1="1201.42" x2="1198.88" y2="1198.88" width="0.3" layer="91" />
                <label x="1198.88" y="1198.88" size="1.27" layer="95" />
                <pinref part="5" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1579.88" x2="1455.42" y2="1579.88" width="0.3" layer="91" />
                <label x="1455.42" y="1579.88" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-4" />
              </segment>
              <segment>
                <wire x1="1181.10" y1="1186.18" x2="1178.56" y2="1186.18" width="0.3" layer="91" />
                <label x="1178.56" y="1186.18" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1130.30" x2="1109.98" y2="1130.30" width="0.3" layer="91" />
                <label x="1109.98" y="1130.30" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="1153.16" x2="1158.24" y2="1153.16" width="0.3" layer="91" />
                <label x="1158.24" y="1153.16" size="1.27" layer="95" />
                <pinref part="R12" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$21">
              <segment>
                <wire x1="1247.14" y1="1231.90" x2="1247.14" y2="1234.44" width="0.3" layer="91" />
                <label x="1247.14" y="1234.44" size="1.27" layer="95" />
                <pinref part="C1" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1191.26" y1="1196.34" x2="1193.80" y2="1196.34" width="0.3" layer="91" />
                <label x="1193.80" y="1196.34" size="1.27" layer="95" />
                <pinref part="R1" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$22">
              <segment>
                <wire x1="1247.14" y1="1224.28" x2="1247.14" y2="1221.74" width="0.3" layer="91" />
                <label x="1247.14" y="1221.74" size="1.27" layer="95" />
                <pinref part="C1" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1308.10" y1="1264.92" x2="1308.10" y2="1267.46" width="0.3" layer="91" />
                <label x="1308.10" y="1267.46" size="1.27" layer="95" />
                <pinref part="C2" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1308.10" y1="1277.62" x2="1305.56" y2="1277.62" width="0.3" layer="91" />
                <label x="1305.56" y="1277.62" size="1.27" layer="95" />
                <pinref part="R2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1211.58" y1="1176.02" x2="1214.12" y2="1176.02" width="0.3" layer="91" />
                <label x="1214.12" y="1176.02" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$23">
              <segment>
                <wire x1="1272.54" y1="1239.52" x2="1272.54" y2="1242.06" width="0.3" layer="91" />
                <label x="1272.54" y="1242.06" size="1.27" layer="95" />
                <pinref part="C10" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1290.32" y1="1252.22" x2="1287.78" y2="1252.22" width="0.3" layer="91" />
                <label x="1287.78" y="1252.22" size="1.27" layer="95" />
                <pinref part="R35" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="1203.96" x2="1219.20" y2="1203.96" width="0.3" layer="91" />
                <label x="1219.20" y="1203.96" size="1.27" layer="95" />
                <pinref part="R36" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$24">
              <segment>
                <wire x1="1325.88" y1="1290.32" x2="1325.88" y2="1292.86" width="0.3" layer="91" />
                <label x="1325.88" y="1292.86" size="1.27" layer="95" />
                <pinref part="C11" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1351.28" y1="1308.10" x2="1348.74" y2="1308.10" width="0.3" layer="91" />
                <label x="1348.74" y="1308.10" size="1.27" layer="95" />
                <pinref part="D5" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1206.50" y1="1203.96" x2="1203.96" y2="1203.96" width="0.3" layer="91" />
                <label x="1203.96" y="1203.96" size="1.27" layer="95" />
                <pinref part="R36" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1137.92" y1="1153.16" x2="1140.46" y2="1153.16" width="0.3" layer="91" />
                <label x="1140.46" y="1153.16" size="1.27" layer="95" />
                <pinref part="D4" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1135.38" x2="1109.98" y2="1135.38" width="0.3" layer="91" />
                <label x="1109.98" y="1135.38" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1206.50" y1="1211.58" x2="1203.96" y2="1211.58" width="0.3" layer="91" />
                <label x="1203.96" y="1211.58" size="1.27" layer="95" />
                <pinref part="R27" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$25">
              <segment>
                <wire x1="1325.88" y1="1282.70" x2="1325.88" y2="1280.16" width="0.3" layer="91" />
                <label x="1325.88" y="1280.16" size="1.27" layer="95" />
                <pinref part="C11" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1343.66" y1="1308.10" x2="1346.20" y2="1308.10" width="0.3" layer="91" />
                <label x="1346.20" y="1308.10" size="1.27" layer="95" />
                <pinref part="R28" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$26">
              <segment>
                <wire x1="1308.10" y1="1257.30" x2="1308.10" y2="1254.76" width="0.3" layer="91" />
                <label x="1308.10" y="1254.76" size="1.27" layer="95" />
                <pinref part="C2" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1356.36" y1="1330.96" x2="1353.82" y2="1330.96" width="0.3" layer="91" />
                <label x="1353.82" y="1330.96" size="1.27" layer="95" />
                <pinref part="VR1" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="424.18" y1="1064.26" x2="421.64" y2="1064.26" width="0.3" layer="91" />
                <label x="421.64" y="1064.26" size="1.27" layer="95" />
                <pinref part="R99" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1564.64" x2="1455.42" y2="1564.64" width="0.3" layer="91" />
                <label x="1455.42" y="1564.64" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-10" />
              </segment>
              <segment>
                <wire x1="1318.26" y1="1277.62" x2="1320.80" y2="1277.62" width="0.3" layer="91" />
                <label x="1320.80" y="1277.62" size="1.27" layer="95" />
                <pinref part="R2" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1211.58" y1="1181.10" x2="1214.12" y2="1181.10" width="0.3" layer="91" />
                <label x="1214.12" y="1181.10" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1115.06" y1="1145.54" x2="1112.52" y2="1145.54" width="0.3" layer="91" />
                <label x="1112.52" y="1145.54" size="1.27" layer="95" />
                <pinref part="R9" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="982.98" x2="1028.70" y2="982.98" width="0.3" layer="91" />
                <label x="1028.70" y="982.98" size="1.27" layer="95" />
                <pinref part="R5" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$27">
              <segment>
                <wire x1="1115.06" y1="1132.84" x2="1115.06" y2="1135.38" width="0.3" layer="91" />
                <label x="1115.06" y="1135.38" size="1.27" layer="95" />
                <pinref part="C3" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1087.12" y1="1120.14" x2="1089.66" y2="1120.14" width="0.3" layer="91" />
                <label x="1089.66" y="1120.14" size="1.27" layer="95" />
                <pinref part="R8" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1132.84" y1="1145.54" x2="1130.30" y2="1145.54" width="0.3" layer="91" />
                <label x="1130.30" y="1145.54" size="1.27" layer="95" />
                <pinref part="R13" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$28">
              <segment>
                <wire x1="1445.26" y1="1381.76" x2="1445.26" y2="1384.30" width="0.3" layer="91" />
                <label x="1445.26" y="1384.30" size="1.27" layer="95" />
                <pinref part="C4" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="988.06" y1="934.72" x2="985.52" y2="934.72" width="0.3" layer="91" />
                <label x="985.52" y="934.72" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="TNB" />
              </segment>
              <segment>
                <wire x1="988.06" y1="932.18" x2="985.52" y2="932.18" width="0.3" layer="91" />
                <label x="985.52" y="932.18" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="SB" />
              </segment>
              <segment>
                <wire x1="988.06" y1="929.64" x2="985.52" y2="929.64" width="0.3" layer="91" />
                <label x="985.52" y="929.64" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="SNB" />
              </segment>
              <segment>
                <wire x1="198.12" y1="350.52" x2="195.58" y2="350.52" width="0.3" layer="91" />
                <label x="195.58" y="350.52" size="1.27" layer="95" />
                <pinref part="R142" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="215.90" y1="350.52" x2="213.36" y2="350.52" width="0.3" layer="91" />
                <label x="213.36" y="350.52" size="1.27" layer="95" />
                <pinref part="R138" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="157.48" y1="307.34" x2="157.48" y2="304.80" width="0.3" layer="91" />
                <label x="157.48" y="304.80" size="1.27" layer="95" />
                <pinref part="C51" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="157.48" y1="332.74" x2="157.48" y2="335.28" width="0.3" layer="91" />
                <label x="157.48" y="335.28" size="1.27" layer="95" />
                <pinref part="C52" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1584.96" x2="1455.42" y2="1584.96" width="0.3" layer="91" />
                <label x="1455.42" y="1584.96" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-2" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1567.18" x2="1455.42" y2="1567.18" width="0.3" layer="91" />
                <label x="1455.42" y="1567.18" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-9" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1562.10" x2="1442.72" y2="1562.10" width="0.3" layer="91" />
                <label x="1442.72" y="1562.10" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-11" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1564.64" x2="1442.72" y2="1564.64" width="0.3" layer="91" />
                <label x="1442.72" y="1564.64" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-10" />
              </segment>
              <segment>
                <wire x1="251.46" y1="393.70" x2="254.00" y2="393.70" width="0.3" layer="91" />
                <label x="254.00" y="393.70" size="1.27" layer="95" />
                <pinref part="R424" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="233.68" y1="383.54" x2="233.68" y2="386.08" width="0.3" layer="91" />
                <label x="233.68" y="386.08" size="1.27" layer="95" />
                <pinref part="C241" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="645.16" y1="1275.08" x2="642.62" y2="1275.08" width="0.3" layer="91" />
                <label x="642.62" y="1275.08" size="1.27" layer="95" />
                <pinref part="R84" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="774.70" y1="1348.74" x2="772.16" y2="1348.74" width="0.3" layer="91" />
                <label x="772.16" y="1348.74" size="1.27" layer="95" />
                <pinref part="R810" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1496.06" y1="1605.28" x2="1493.52" y2="1605.28" width="0.3" layer="91" />
                <label x="1493.52" y="1605.28" size="1.27" layer="95" />
                <pinref part="Guitar_Amp" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1478.28" y1="1587.50" x2="1475.74" y2="1587.50" width="0.3" layer="91" />
                <label x="1475.74" y="1587.50" size="1.27" layer="95" />
                <pinref part="A+B" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1493.52" y1="1579.88" x2="1490.98" y2="1579.88" width="0.3" layer="91" />
                <label x="1490.98" y="1579.88" size="1.27" layer="95" />
                <pinref part="Vocal_Amp_B" gate="G$1" pin="SN" />
              </segment>
              <segment>
                <wire x1="1493.52" y1="1582.42" x2="1490.98" y2="1582.42" width="0.3" layer="91" />
                <label x="1490.98" y="1582.42" size="1.27" layer="95" />
                <pinref part="Vocal_Amp_B" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="889.00" y1="1430.02" x2="891.54" y2="1430.02" width="0.3" layer="91" />
                <label x="891.54" y="1430.02" size="1.27" layer="95" />
                <pinref part="R806" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="988.06" y1="947.42" x2="985.52" y2="947.42" width="0.3" layer="91" />
                <label x="985.52" y="947.42" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="ST" />
              </segment>
              <segment>
                <wire x1="988.06" y1="949.96" x2="985.52" y2="949.96" width="0.3" layer="91" />
                <label x="985.52" y="949.96" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="TNT" />
              </segment>
              <segment>
                <wire x1="988.06" y1="944.88" x2="985.52" y2="944.88" width="0.3" layer="91" />
                <label x="985.52" y="944.88" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="SNT" />
              </segment>
              <segment>
                <wire x1="627.38" y1="1267.46" x2="624.84" y2="1267.46" width="0.3" layer="91" />
                <label x="624.84" y="1267.46" size="1.27" layer="95" />
                <pinref part="R87" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="406.40" y1="497.84" x2="406.40" y2="495.30" width="0.3" layer="91" />
                <label x="406.40" y="495.30" size="1.27" layer="95" />
                <pinref part="C39" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="424.18" y1="1071.88" x2="421.64" y2="1071.88" width="0.3" layer="91" />
                <label x="421.64" y="1071.88" size="1.27" layer="95" />
                <pinref part="R100" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="416.56" y1="1064.26" x2="419.10" y2="1064.26" width="0.3" layer="91" />
                <label x="419.10" y="1064.26" size="1.27" layer="95" />
                <pinref part="R77" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="271.78" y1="287.02" x2="271.78" y2="289.56" width="0.3" layer="91" />
                <label x="271.78" y="289.56" size="1.27" layer="95" />
                <pinref part="C4$1" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="220.98" y1="243.84" x2="220.98" y2="246.38" width="0.3" layer="91" />
                <label x="220.98" y="246.38" size="1.27" layer="95" />
                <pinref part="C2$1" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="203.20" y1="236.22" x2="200.66" y2="236.22" width="0.3" layer="91" />
                <label x="200.66" y="236.22" size="1.27" layer="95" />
                <pinref part="R4$1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="142.24" y1="165.10" x2="142.24" y2="162.56" width="0.3" layer="91" />
                <label x="142.24" y="162.56" size="1.27" layer="95" />
                <pinref part="C351" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="988.06" y1="927.10" x2="985.52" y2="927.10" width="0.3" layer="91" />
                <label x="985.52" y="927.10" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="5.08" y1="43.18" x2="2.54" y2="43.18" width="0.3" layer="91" />
                <label x="2.54" y="43.18" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="360.68" y1="967.74" x2="360.68" y2="970.28" width="0.3" layer="91" />
                <label x="360.68" y="970.28" size="1.27" layer="95" />
                <pinref part="C14" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="360.68" y1="942.34" x2="360.68" y2="939.80" width="0.3" layer="91" />
                <label x="360.68" y="939.80" size="1.27" layer="95" />
                <pinref part="C15" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="205.74" y1="817.88" x2="208.28" y2="817.88" width="0.3" layer="91" />
                <label x="208.28" y="817.88" size="1.27" layer="95" />
                <pinref part="R803" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1577.34" x2="1455.42" y2="1577.34" width="0.3" layer="91" />
                <label x="1455.42" y="1577.34" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-5" />
              </segment>
              <segment>
                <wire x1="312.42" y1="723.90" x2="312.42" y2="726.44" width="0.3" layer="91" />
                <label x="312.42" y="726.44" size="1.27" layer="95" />
                <pinref part="C608" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="312.42" y1="734.06" x2="312.42" y2="731.52" width="0.3" layer="91" />
                <label x="312.42" y="731.52" size="1.27" layer="95" />
                <pinref part="C606" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="304.80" y1="703.58" x2="304.80" y2="706.12" width="0.3" layer="91" />
                <label x="304.80" y="706.12" size="1.27" layer="95" />
                <pinref part="C605" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="304.80" y1="713.74" x2="304.80" y2="711.20" width="0.3" layer="91" />
                <label x="304.80" y="711.20" size="1.27" layer="95" />
                <pinref part="C604" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="180.34" y1="622.30" x2="182.88" y2="622.30" width="0.3" layer="91" />
                <label x="182.88" y="622.30" size="1.27" layer="95" />
                <pinref part="R601" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="187.96" y1="622.30" x2="185.42" y2="622.30" width="0.3" layer="91" />
                <label x="185.42" y="622.30" size="1.27" layer="95" />
                <pinref part="R602" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="60.96" y1="546.10" x2="58.42" y2="546.10" width="0.3" layer="91" />
                <label x="58.42" y="546.10" size="1.27" layer="95" />
                <pinref part="MIC_In" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="60.96" y1="535.94" x2="58.42" y2="535.94" width="0.3" layer="91" />
                <label x="58.42" y="535.94" size="1.27" layer="95" />
                <pinref part="MIC_In" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="60.96" y1="548.64" x2="58.42" y2="548.64" width="0.3" layer="91" />
                <label x="58.42" y="548.64" size="1.27" layer="95" />
                <pinref part="MIC_In" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="5.08" y1="66.04" x2="2.54" y2="66.04" width="0.3" layer="91" />
                <label x="2.54" y="66.04" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="TNT" />
              </segment>
              <segment>
                <wire x1="5.08" y1="63.50" x2="2.54" y2="63.50" width="0.3" layer="91" />
                <label x="2.54" y="63.50" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="ST" />
              </segment>
              <segment>
                <wire x1="5.08" y1="60.96" x2="2.54" y2="60.96" width="0.3" layer="91" />
                <label x="2.54" y="60.96" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="SNT" />
              </segment>
              <segment>
                <wire x1="520.70" y1="886.46" x2="518.16" y2="886.46" width="0.3" layer="91" />
                <label x="518.16" y="886.46" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="530.86" y1="871.22" x2="533.40" y2="871.22" width="0.3" layer="91" />
                <label x="533.40" y="871.22" size="1.27" layer="95" />
                <pinref part="R66" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="408.94" y1="828.04" x2="408.94" y2="830.58" width="0.3" layer="91" />
                <label x="408.94" y="830.58" size="1.27" layer="95" />
                <pinref part="C13" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="398.78" y1="772.16" x2="396.24" y2="772.16" width="0.3" layer="91" />
                <label x="396.24" y="772.16" size="1.27" layer="95" />
                <pinref part="R39" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="381.00" y1="767.08" x2="378.46" y2="767.08" width="0.3" layer="91" />
                <label x="378.46" y="767.08" size="1.27" layer="95" />
                <pinref part="R38" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="170.18" y1="802.64" x2="172.72" y2="802.64" width="0.3" layer="91" />
                <label x="172.72" y="802.64" size="1.27" layer="95" />
                <pinref part="R69" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="279.40" y1="868.68" x2="281.94" y2="868.68" width="0.3" layer="91" />
                <label x="281.94" y="868.68" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="284.48" y1="911.86" x2="281.94" y2="911.86" width="0.3" layer="91" />
                <label x="281.94" y="911.86" size="1.27" layer="95" />
                <pinref part="R68" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="259.08" y1="894.08" x2="261.62" y2="894.08" width="0.3" layer="91" />
                <label x="261.62" y="894.08" size="1.27" layer="95" />
                <pinref part="R67" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="5.08" y1="45.72" x2="2.54" y2="45.72" width="0.3" layer="91" />
                <label x="2.54" y="45.72" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="SNB" />
              </segment>
              <segment>
                <wire x1="5.08" y1="50.80" x2="2.54" y2="50.80" width="0.3" layer="91" />
                <label x="2.54" y="50.80" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="TNB" />
              </segment>
              <segment>
                <wire x1="5.08" y1="48.26" x2="2.54" y2="48.26" width="0.3" layer="91" />
                <label x="2.54" y="48.26" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="SB" />
              </segment>
              <segment>
                <wire x1="60.96" y1="541.02" x2="58.42" y2="541.02" width="0.3" layer="91" />
                <label x="58.42" y="541.02" size="1.27" layer="95" />
                <pinref part="MIC_In" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="167.64" y1="190.50" x2="167.64" y2="193.04" width="0.3" layer="91" />
                <label x="167.64" y="193.04" size="1.27" layer="95" />
                <pinref part="C352" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="185.42" y1="200.66" x2="185.42" y2="198.12" width="0.3" layer="91" />
                <label x="185.42" y="198.12" size="1.27" layer="95" />
                <pinref part="C1$1" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="195.58" y1="218.44" x2="198.12" y2="218.44" width="0.3" layer="91" />
                <label x="198.12" y="218.44" size="1.27" layer="95" />
                <pinref part="R3$1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="271.78" y1="261.62" x2="271.78" y2="259.08" width="0.3" layer="91" />
                <label x="271.78" y="259.08" size="1.27" layer="95" />
                <pinref part="C3$1" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="1206.50" y1="1645.92" x2="1206.50" y2="1648.46" width="0.3" layer="91" />
                <label x="1206.50" y="1648.46" size="1.27" layer="95" />
                <pinref part="C18" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1127.76" y1="1625.60" x2="1125.22" y2="1625.60" width="0.3" layer="91" />
                <label x="1125.22" y="1625.60" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1242.06" y1="1775.46" x2="1244.60" y2="1775.46" width="0.3" layer="91" />
                <label x="1244.60" y="1775.46" size="1.27" layer="95" />
                <pinref part="R53" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1259.84" y1="1783.08" x2="1262.38" y2="1783.08" width="0.3" layer="91" />
                <label x="1262.38" y="1783.08" size="1.27" layer="95" />
                <pinref part="R54" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1267.46" y1="1798.32" x2="1264.92" y2="1798.32" width="0.3" layer="91" />
                <label x="1264.92" y="1798.32" size="1.27" layer="95" />
                <pinref part="R61" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1158.24" y1="1620.52" x2="1160.78" y2="1620.52" width="0.3" layer="91" />
                <label x="1160.78" y="1620.52" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1010.92" y1="1567.18" x2="1008.38" y2="1567.18" width="0.3" layer="91" />
                <label x="1008.38" y="1567.18" size="1.27" layer="95" />
                <pinref part="R58" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1069.34" y1="1582.42" x2="1069.34" y2="1584.96" width="0.3" layer="91" />
                <label x="1069.34" y="1584.96" size="1.27" layer="95" />
                <pinref part="C17" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1120.14" y1="1607.82" x2="1120.14" y2="1605.28" width="0.3" layer="91" />
                <label x="1120.14" y="1605.28" size="1.27" layer="95" />
                <pinref part="C21" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1076.96" y1="1135.38" x2="1074.42" y2="1135.38" width="0.3" layer="91" />
                <label x="1074.42" y="1135.38" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1351.28" y1="1320.80" x2="1351.28" y2="1323.34" width="0.3" layer="91" />
                <label x="1351.28" y="1323.34" size="1.27" layer="95" />
                <pinref part="C9" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1264.92" y1="1234.44" x2="1267.46" y2="1234.44" width="0.3" layer="91" />
                <label x="1267.46" y="1234.44" size="1.27" layer="95" />
                <pinref part="R20" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1333.50" y1="1308.10" x2="1330.96" y2="1308.10" width="0.3" layer="91" />
                <label x="1330.96" y="1308.10" size="1.27" layer="95" />
                <pinref part="R28" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1297.94" y1="1259.84" x2="1297.94" y2="1257.30" width="0.3" layer="91" />
                <label x="1297.94" y="1257.30" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="1163.32" y1="1160.78" x2="1160.78" y2="1160.78" width="0.3" layer="91" />
                <label x="1160.78" y="1160.78" size="1.27" layer="95" />
                <pinref part="R7" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1211.58" y1="1170.94" x2="1214.12" y2="1170.94" width="0.3" layer="91" />
                <label x="1214.12" y="1170.94" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1783.08" y1="1465.58" x2="1780.54" y2="1465.58" width="0.3" layer="91" />
                <label x="1780.54" y="1465.58" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1496.06" y1="1402.08" x2="1498.60" y2="1402.08" width="0.3" layer="91" />
                <label x="1498.60" y="1402.08" size="1.27" layer="95" />
                <pinref part="R22" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1750.06" y1="1424.94" x2="1752.60" y2="1424.94" width="0.3" layer="91" />
                <label x="1752.60" y="1424.94" size="1.27" layer="95" />
                <pinref part="R23" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1427.48" y1="1356.36" x2="1424.94" y2="1356.36" width="0.3" layer="91" />
                <label x="1424.94" y="1356.36" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1351.28" x2="1460.50" y2="1351.28" width="0.3" layer="91" />
                <label x="1460.50" y="1351.28" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$29">
              <segment>
                <wire x1="1445.26" y1="1374.14" x2="1445.26" y2="1371.60" width="0.3" layer="91" />
                <label x="1445.26" y="1371.60" size="1.27" layer="95" />
                <pinref part="C4" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1437.64" y1="1376.68" x2="1440.18" y2="1376.68" width="0.3" layer="91" />
                <label x="1440.18" y="1376.68" size="1.27" layer="95" />
                <pinref part="R30" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$30">
              <segment>
                <wire x1="1783.08" y1="1445.26" x2="1783.08" y2="1447.80" width="0.3" layer="91" />
                <label x="1783.08" y="1447.80" size="1.27" layer="95" />
                <pinref part="C5" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1765.30" y1="1440.18" x2="1762.76" y2="1440.18" width="0.3" layer="91" />
                <label x="1762.76" y="1440.18" size="1.27" layer="95" />
                <pinref part="R33" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1828.80" y1="1490.98" x2="1828.80" y2="1493.52" width="0.3" layer="91" />
                <label x="1828.80" y="1493.52" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="1757.68" y1="1430.02" x2="1757.68" y2="1427.48" width="0.3" layer="91" />
                <label x="1757.68" y="1427.48" size="1.27" layer="95" />
                <pinref part="C6" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1783.08" y1="1470.66" x2="1780.54" y2="1470.66" width="0.3" layer="91" />
                <label x="1780.54" y="1470.66" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$31">
              <segment>
                <wire x1="1757.68" y1="1437.64" x2="1757.68" y2="1440.18" width="0.3" layer="91" />
                <label x="1757.68" y="1440.18" size="1.27" layer="95" />
                <pinref part="C6" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1742.44" y1="1409.70" x2="1744.98" y2="1409.70" width="0.3" layer="91" />
                <label x="1744.98" y="1409.70" size="1.27" layer="95" />
                <pinref part="R32" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$32">
              <segment>
                <wire x1="1351.28" y1="1313.18" x2="1351.28" y2="1310.64" width="0.3" layer="91" />
                <label x="1351.28" y="1310.64" size="1.27" layer="95" />
                <pinref part="C9" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1356.36" y1="1308.10" x2="1358.90" y2="1308.10" width="0.3" layer="91" />
                <label x="1358.90" y="1308.10" size="1.27" layer="95" />
                <pinref part="D5" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1300.48" y1="1252.22" x2="1303.02" y2="1252.22" width="0.3" layer="91" />
                <label x="1303.02" y="1252.22" size="1.27" layer="95" />
                <pinref part="R35" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1813.56" y1="1460.50" x2="1816.10" y2="1460.50" width="0.3" layer="91" />
                <label x="1816.10" y="1460.50" size="1.27" layer="95" />
                <pinref part="U5" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$33">
              <segment>
                <wire x1="1033.78" y1="1094.74" x2="1031.24" y2="1094.74" width="0.3" layer="91" />
                <label x="1031.24" y="1094.74" size="1.27" layer="95" />
                <pinref part="D1" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1051.56" y1="1112.52" x2="1054.10" y2="1112.52" width="0.3" layer="91" />
                <label x="1054.10" y="1112.52" size="1.27" layer="95" />
                <pinref part="D2" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1076.96" y1="1145.54" x2="1074.42" y2="1145.54" width="0.3" layer="91" />
                <label x="1074.42" y="1145.54" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$34">
              <segment>
                <wire x1="1038.86" y1="1094.74" x2="1041.40" y2="1094.74" width="0.3" layer="91" />
                <label x="1041.40" y="1094.74" size="1.27" layer="95" />
                <pinref part="D1" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="998.22" x2="1028.70" y2="998.22" width="0.3" layer="91" />
                <label x="1028.70" y="998.22" size="1.27" layer="95" />
                <pinref part="R4" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$35">
              <segment>
                <wire x1="1046.48" y1="1112.52" x2="1043.94" y2="1112.52" width="0.3" layer="91" />
                <label x="1043.94" y="1112.52" size="1.27" layer="95" />
                <pinref part="D2" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1059.18" y1="1120.14" x2="1056.64" y2="1120.14" width="0.3" layer="91" />
                <label x="1056.64" y="1120.14" size="1.27" layer="95" />
                <pinref part="R11" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1059.18" y1="1112.52" x2="1056.64" y2="1112.52" width="0.3" layer="91" />
                <label x="1056.64" y="1112.52" size="1.27" layer="95" />
                <pinref part="R10" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="990.60" x2="1028.70" y2="990.60" width="0.3" layer="91" />
                <label x="1028.70" y="990.60" size="1.27" layer="95" />
                <pinref part="R6" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$36">
              <segment>
                <wire x1="1224.28" y1="1211.58" x2="1221.74" y2="1211.58" width="0.3" layer="91" />
                <label x="1221.74" y="1211.58" size="1.27" layer="95" />
                <pinref part="D3" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1272.54" y1="1252.22" x2="1270.00" y2="1252.22" width="0.3" layer="91" />
                <label x="1270.00" y="1252.22" size="1.27" layer="95" />
                <pinref part="R18" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1198.88" y1="1196.34" x2="1196.34" y2="1196.34" width="0.3" layer="91" />
                <label x="1196.34" y="1196.34" size="1.27" layer="95" />
                <pinref part="R16" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$37">
              <segment>
                <wire x1="1229.36" y1="1211.58" x2="1231.90" y2="1211.58" width="0.3" layer="91" />
                <label x="1231.90" y="1211.58" size="1.27" layer="95" />
                <pinref part="D3" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1264.92" y1="1226.82" x2="1267.46" y2="1226.82" width="0.3" layer="91" />
                <label x="1267.46" y="1226.82" size="1.27" layer="95" />
                <pinref part="R17" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1765.30" y1="1432.56" x2="1762.76" y2="1432.56" width="0.3" layer="91" />
                <label x="1762.76" y="1432.56" size="1.27" layer="95" />
                <pinref part="D7" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1826.26" y1="1457.96" x2="1828.80" y2="1457.96" width="0.3" layer="91" />
                <label x="1828.80" y="1457.96" size="1.27" layer="95" />
                <pinref part="D6" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1236.98" y1="1234.44" x2="1239.52" y2="1234.44" width="0.3" layer="91" />
                <label x="1239.52" y="1234.44" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$38">
              <segment>
                <wire x1="1132.84" y1="1153.16" x2="1130.30" y2="1153.16" width="0.3" layer="91" />
                <label x="1130.30" y="1153.16" size="1.27" layer="95" />
                <pinref part="D4" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1140.46" x2="1109.98" y2="1140.46" width="0.3" layer="91" />
                <label x="1109.98" y="1140.46" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$39">
              <segment>
                <wire x1="1821.18" y1="1457.96" x2="1818.64" y2="1457.96" width="0.3" layer="91" />
                <label x="1818.64" y="1457.96" size="1.27" layer="95" />
                <pinref part="D6" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="1818.64" y1="1478.28" x2="1816.10" y2="1478.28" width="0.3" layer="91" />
                <label x="1816.10" y="1478.28" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="1838.96" y1="1473.20" x2="1836.42" y2="1473.20" width="0.3" layer="91" />
                <label x="1836.42" y="1473.20" size="1.27" layer="95" />
                <pinref part="R29" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$40">
              <segment>
                <wire x1="1770.38" y1="1432.56" x2="1772.92" y2="1432.56" width="0.3" layer="91" />
                <label x="1772.92" y="1432.56" size="1.27" layer="95" />
                <pinref part="D7" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1577.34" x2="1442.72" y2="1577.34" width="0.3" layer="91" />
                <label x="1442.72" y="1577.34" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-5" />
              </segment>
            </net>
            <net name="N$41">
              <segment>
                <wire x1="1221.74" y1="1226.82" x2="1219.20" y2="1226.82" width="0.3" layer="91" />
                <label x="1219.20" y="1226.82" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="1160.78" x2="1158.24" y2="1160.78" width="0.3" layer="91" />
                <label x="1158.24" y="1160.78" size="1.27" layer="95" />
                <pinref part="R14" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1143.00" y1="1145.54" x2="1145.54" y2="1145.54" width="0.3" layer="91" />
                <label x="1145.54" y="1145.54" size="1.27" layer="95" />
                <pinref part="R13" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$42">
              <segment>
                <wire x1="1330.96" y1="1292.86" x2="1328.42" y2="1292.86" width="0.3" layer="91" />
                <label x="1328.42" y="1292.86" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="1325.88" y1="1277.62" x2="1323.34" y2="1277.62" width="0.3" layer="91" />
                <label x="1323.34" y="1277.62" size="1.27" layer="95" />
                <pinref part="R15" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1282.70" y1="1252.22" x2="1285.24" y2="1252.22" width="0.3" layer="91" />
                <label x="1285.24" y="1252.22" size="1.27" layer="95" />
                <pinref part="R18" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$43">
              <segment>
                <wire x1="1346.20" y1="1300.48" x2="1348.74" y2="1300.48" width="0.3" layer="91" />
                <label x="1348.74" y="1300.48" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="1234.44" x2="1252.22" y2="1234.44" width="0.3" layer="91" />
                <label x="1252.22" y="1234.44" size="1.27" layer="95" />
                <pinref part="R20" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="1219.20" x2="1244.60" y2="1219.20" width="0.3" layer="91" />
                <label x="1244.60" y="1219.20" size="1.27" layer="95" />
                <pinref part="R19" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$44">
              <segment>
                <wire x1="1297.94" y1="1285.24" x2="1297.94" y2="1287.78" width="0.3" layer="91" />
                <label x="1297.94" y="1287.78" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="1211.58" x2="1219.20" y2="1211.58" width="0.3" layer="91" />
                <label x="1219.20" y="1211.58" size="1.27" layer="95" />
                <pinref part="R27" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$45">
              <segment>
                <wire x1="1287.78" y1="1272.54" x2="1285.24" y2="1272.54" width="0.3" layer="91" />
                <label x="1285.24" y="1272.54" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="1257.30" y1="1219.20" x2="1259.84" y2="1219.20" width="0.3" layer="91" />
                <label x="1259.84" y="1219.20" size="1.27" layer="95" />
                <pinref part="R19" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$46">
              <segment>
                <wire x1="1475.74" y1="1404.62" x2="1478.28" y2="1404.62" width="0.3" layer="91" />
                <label x="1478.28" y="1404.62" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1341.12" x2="1422.40" y2="1341.12" width="0.3" layer="91" />
                <label x="1422.40" y="1341.12" size="1.27" layer="95" />
                <pinref part="R26" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$47">
              <segment>
                <wire x1="1460.50" y1="1412.24" x2="1457.96" y2="1412.24" width="0.3" layer="91" />
                <label x="1457.96" y="1412.24" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="1427.48" y1="1366.52" x2="1424.94" y2="1366.52" width="0.3" layer="91" />
                <label x="1424.94" y="1366.52" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$48">
              <segment>
                <wire x1="1181.10" y1="1196.34" x2="1178.56" y2="1196.34" width="0.3" layer="91" />
                <label x="1178.56" y="1196.34" size="1.27" layer="95" />
                <pinref part="R1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="246.38" y1="662.94" x2="246.38" y2="660.40" width="0.3" layer="91" />
                <label x="246.38" y="660.40" size="1.27" layer="95" />
                <pinref part="C603" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="276.86" y1="708.66" x2="279.40" y2="708.66" width="0.3" layer="91" />
                <label x="279.40" y="708.66" size="1.27" layer="95" />
                <pinref part="Q62" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="287.02" y1="690.88" x2="284.48" y2="690.88" width="0.3" layer="91" />
                <label x="284.48" y="690.88" size="1.27" layer="95" />
                <pinref part="R608" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="256.54" y1="683.26" x2="259.08" y2="683.26" width="0.3" layer="91" />
                <label x="259.08" y="683.26" size="1.27" layer="95" />
                <pinref part="R607" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$49">
              <segment>
                <wire x1="1069.34" y1="1112.52" x2="1071.88" y2="1112.52" width="0.3" layer="91" />
                <label x="1071.88" y="1112.52" size="1.27" layer="95" />
                <pinref part="R10" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1145.54" y1="1153.16" x2="1143.00" y2="1153.16" width="0.3" layer="91" />
                <label x="1143.00" y="1153.16" size="1.27" layer="95" />
                <pinref part="R12" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1125.22" y1="1145.54" x2="1127.76" y2="1145.54" width="0.3" layer="91" />
                <label x="1127.76" y="1145.54" size="1.27" layer="95" />
                <pinref part="R9" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1181.10" y1="1181.10" x2="1178.56" y2="1181.10" width="0.3" layer="91" />
                <label x="1178.56" y="1181.10" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1069.34" y1="1120.14" x2="1071.88" y2="1120.14" width="0.3" layer="91" />
                <label x="1071.88" y="1120.14" size="1.27" layer="95" />
                <pinref part="R11" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$50">
              <segment>
                <wire x1="1402.08" y1="1341.12" x2="1404.62" y2="1341.12" width="0.3" layer="91" />
                <label x="1404.62" y="1341.12" size="1.27" layer="95" />
                <pinref part="R24" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1427.48" y1="1376.68" x2="1424.94" y2="1376.68" width="0.3" layer="91" />
                <label x="1424.94" y="1376.68" size="1.27" layer="95" />
                <pinref part="R30" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1394.46" x2="1442.72" y2="1394.46" width="0.3" layer="91" />
                <label x="1442.72" y="1394.46" size="1.27" layer="95" />
                <pinref part="R31" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$51">
              <segment>
                <wire x1="1409.70" y1="1341.12" x2="1407.16" y2="1341.12" width="0.3" layer="91" />
                <label x="1407.16" y="1341.12" size="1.27" layer="95" />
                <pinref part="R26" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1369.06" y1="1315.72" x2="1371.60" y2="1315.72" width="0.3" layer="91" />
                <label x="1371.60" y="1315.72" size="1.27" layer="95" />
                <pinref part="R34" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1427.48" y1="1361.44" x2="1424.94" y2="1361.44" width="0.3" layer="91" />
                <label x="1424.94" y="1361.44" size="1.27" layer="95" />
                <pinref part="U4" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$52">
              <segment>
                <wire x1="1409.70" y1="1348.74" x2="1407.16" y2="1348.74" width="0.3" layer="91" />
                <label x="1407.16" y="1348.74" size="1.27" layer="95" />
                <pinref part="R3" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1381.76" y1="1330.96" x2="1384.30" y2="1330.96" width="0.3" layer="91" />
                <label x="1384.30" y="1330.96" size="1.27" layer="95" />
                <pinref part="VR1" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1370.33" y1="1343.66" x2="1370.33" y2="1346.20" width="0.3" layer="91" />
                <label x="1370.33" y="1346.20" size="1.27" layer="95" />
                <pinref part="VR1" gate="G$1" pin="W" />
              </segment>
            </net>
            <net name="N$53">
              <segment>
                <wire x1="1838.96" y1="1465.58" x2="1836.42" y2="1465.58" width="0.3" layer="91" />
                <label x="1836.42" y="1465.58" size="1.27" layer="95" />
                <pinref part="R37" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1569.72" x2="1442.72" y2="1569.72" width="0.3" layer="91" />
                <label x="1442.72" y="1569.72" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-8" />
              </segment>
            </net>
            <net name="N$54">
              <segment>
                <wire x1="1016.00" y1="998.22" x2="1013.46" y2="998.22" width="0.3" layer="91" />
                <label x="1013.46" y="998.22" size="1.27" layer="95" />
                <pinref part="R4" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="982.98" x2="1013.46" y2="982.98" width="0.3" layer="91" />
                <label x="1013.46" y="982.98" size="1.27" layer="95" />
                <pinref part="R5" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="990.60" x2="1013.46" y2="990.60" width="0.3" layer="91" />
                <label x="1013.46" y="990.60" size="1.27" layer="95" />
                <pinref part="R6" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1076.96" y1="1140.46" x2="1074.42" y2="1140.46" width="0.3" layer="91" />
                <label x="1074.42" y="1140.46" size="1.27" layer="95" />
                <pinref part="U3" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$55">
              <segment>
                <wire x1="1173.48" y1="1160.78" x2="1176.02" y2="1160.78" width="0.3" layer="91" />
                <label x="1176.02" y="1160.78" size="1.27" layer="95" />
                <pinref part="R7" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1181.10" y1="1176.02" x2="1178.56" y2="1176.02" width="0.3" layer="91" />
                <label x="1178.56" y="1176.02" size="1.27" layer="95" />
                <pinref part="U2" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$56">
              <segment>
                <wire x1="1076.96" y1="1120.14" x2="1074.42" y2="1120.14" width="0.3" layer="91" />
                <label x="1074.42" y="1120.14" size="1.27" layer="95" />
                <pinref part="R8" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="988.06" y1="939.80" x2="985.52" y2="939.80" width="0.3" layer="91" />
                <label x="985.52" y="939.80" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="TB" />
              </segment>
            </net>
            <net name="N$57">
              <segment>
                <wire x1="401.32" y1="812.80" x2="401.32" y2="810.26" width="0.3" layer="91" />
                <label x="401.32" y="810.26" size="1.27" layer="95" />
                <pinref part="C12" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="452.12" y1="838.20" x2="449.58" y2="838.20" width="0.3" layer="91" />
                <label x="449.58" y="838.20" size="1.27" layer="95" />
                <pinref part="R41" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="408.94" y1="772.16" x2="411.48" y2="772.16" width="0.3" layer="91" />
                <label x="411.48" y="772.16" size="1.27" layer="95" />
                <pinref part="R39" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$58">
              <segment>
                <wire x1="401.32" y1="820.42" x2="401.32" y2="822.96" width="0.3" layer="91" />
                <label x="401.32" y="822.96" size="1.27" layer="95" />
                <pinref part="C12" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="5.08" y1="71.12" x2="2.54" y2="71.12" width="0.3" layer="91" />
                <label x="2.54" y="71.12" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="TT" />
              </segment>
              <segment>
                <wire x1="391.16" y1="767.08" x2="393.70" y2="767.08" width="0.3" layer="91" />
                <label x="393.70" y="767.08" size="1.27" layer="95" />
                <pinref part="R38" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$59">
              <segment>
                <wire x1="408.94" y1="820.42" x2="408.94" y2="817.88" width="0.3" layer="91" />
                <label x="408.94" y="817.88" size="1.27" layer="95" />
                <pinref part="C13" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="434.34" y1="838.20" x2="431.80" y2="838.20" width="0.3" layer="91" />
                <label x="431.80" y="838.20" size="1.27" layer="95" />
                <pinref part="R40" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$60">
              <segment>
                <wire x1="464.82" y1="850.90" x2="464.82" y2="853.44" width="0.3" layer="91" />
                <label x="464.82" y="853.44" size="1.27" layer="95" />
                <pinref part="C16" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="444.50" y1="838.20" x2="447.04" y2="838.20" width="0.3" layer="91" />
                <label x="447.04" y="838.20" size="1.27" layer="95" />
                <pinref part="R40" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="464.82" y1="863.60" x2="462.28" y2="863.60" width="0.3" layer="91" />
                <label x="462.28" y="863.60" size="1.27" layer="95" />
                <pinref part="R42" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="482.60" y1="883.92" x2="480.06" y2="883.92" width="0.3" layer="91" />
                <label x="480.06" y="883.92" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$61">
              <segment>
                <wire x1="464.82" y1="843.28" x2="464.82" y2="840.74" width="0.3" layer="91" />
                <label x="464.82" y="840.74" size="1.27" layer="95" />
                <pinref part="C16" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="393.70" y1="784.86" x2="396.24" y2="784.86" width="0.3" layer="91" />
                <label x="396.24" y="784.86" size="1.27" layer="95" />
                <pinref part="R49" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="416.56" y1="822.96" x2="414.02" y2="822.96" width="0.3" layer="91" />
                <label x="414.02" y="822.96" size="1.27" layer="95" />
                <pinref part="R43" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="482.60" y1="889.00" x2="480.06" y2="889.00" width="0.3" layer="91" />
                <label x="480.06" y="889.00" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1084.58" y1="1610.36" x2="1082.04" y2="1610.36" width="0.3" layer="91" />
                <label x="1082.04" y="1610.36" size="1.27" layer="95" />
                <pinref part="VR2" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="223.52" y1="848.36" x2="226.06" y2="848.36" width="0.3" layer="91" />
                <label x="226.06" y="848.36" size="1.27" layer="95" />
                <pinref part="R75" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="474.98" y1="863.60" x2="477.52" y2="863.60" width="0.3" layer="91" />
                <label x="477.52" y="863.60" size="1.27" layer="95" />
                <pinref part="R42" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$62">
              <segment>
                <wire x1="563.88" y1="878.84" x2="566.42" y2="878.84" width="0.3" layer="91" />
                <label x="566.42" y="878.84" size="1.27" layer="95" />
                <pinref part="D10" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="551.18" y1="886.46" x2="553.72" y2="886.46" width="0.3" layer="91" />
                <label x="553.72" y="886.46" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="1551.94" x2="1023.62" y2="1551.94" width="0.3" layer="91" />
                <label x="1023.62" y="1551.94" size="1.27" layer="95" />
                <pinref part="R65" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1051.56" y1="1577.34" x2="1049.02" y2="1577.34" width="0.3" layer="91" />
                <label x="1049.02" y="1577.34" size="1.27" layer="95" />
                <pinref part="R51" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1021.08" y1="1567.18" x2="1023.62" y2="1567.18" width="0.3" layer="91" />
                <label x="1023.62" y="1567.18" size="1.27" layer="95" />
                <pinref part="R58" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1087.12" y1="1595.12" x2="1084.58" y2="1595.12" width="0.3" layer="91" />
                <label x="1084.58" y="1595.12" size="1.27" layer="95" />
                <pinref part="D11" gate="A" pin="+" />
              </segment>
            </net>
            <net name="N$63">
              <segment>
                <wire x1="558.80" y1="878.84" x2="556.26" y2="878.84" width="0.3" layer="91" />
                <label x="556.26" y="878.84" size="1.27" layer="95" />
                <pinref part="D10" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="551.18" y1="891.54" x2="553.72" y2="891.54" width="0.3" layer="91" />
                <label x="553.72" y="891.54" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$64">
              <segment>
                <wire x1="408.94" y1="815.34" x2="406.40" y2="815.34" width="0.3" layer="91" />
                <label x="406.40" y="815.34" size="1.27" layer="95" />
                <pinref part="D8" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="520.70" y1="896.62" x2="518.16" y2="896.62" width="0.3" layer="91" />
                <label x="518.16" y="896.62" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="457.20" y1="845.82" x2="459.74" y2="845.82" width="0.3" layer="91" />
                <label x="459.74" y="845.82" size="1.27" layer="95" />
                <pinref part="D9" gate="A" pin="-" />
              </segment>
            </net>
            <net name="N$65">
              <segment>
                <wire x1="414.02" y1="815.34" x2="416.56" y2="815.34" width="0.3" layer="91" />
                <label x="416.56" y="815.34" size="1.27" layer="95" />
                <pinref part="D8" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="393.70" y1="797.56" x2="396.24" y2="797.56" width="0.3" layer="91" />
                <label x="396.24" y="797.56" size="1.27" layer="95" />
                <pinref part="R48" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$66">
              <segment>
                <wire x1="452.12" y1="845.82" x2="449.58" y2="845.82" width="0.3" layer="91" />
                <label x="449.58" y="845.82" size="1.27" layer="95" />
                <pinref part="D9" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="411.48" y1="789.94" x2="414.02" y2="789.94" width="0.3" layer="91" />
                <label x="414.02" y="789.94" size="1.27" layer="95" />
                <pinref part="R50" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="416.56" y1="830.58" x2="414.02" y2="830.58" width="0.3" layer="91" />
                <label x="414.02" y="830.58" size="1.27" layer="95" />
                <pinref part="R44" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="434.34" y1="830.58" x2="431.80" y2="830.58" width="0.3" layer="91" />
                <label x="431.80" y="830.58" size="1.27" layer="95" />
                <pinref part="R45" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$67">
              <segment>
                <wire x1="462.28" y1="838.20" x2="464.82" y2="838.20" width="0.3" layer="91" />
                <label x="464.82" y="838.20" size="1.27" layer="95" />
                <pinref part="R41" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="482.60" y1="878.84" x2="480.06" y2="878.84" width="0.3" layer="91" />
                <label x="480.06" y="878.84" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$68">
              <segment>
                <wire x1="426.72" y1="822.96" x2="429.26" y2="822.96" width="0.3" layer="91" />
                <label x="429.26" y="822.96" size="1.27" layer="95" />
                <pinref part="R43" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="513.08" y1="878.84" x2="515.62" y2="878.84" width="0.3" layer="91" />
                <label x="515.62" y="878.84" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="482.60" y1="863.60" x2="480.06" y2="863.60" width="0.3" layer="91" />
                <label x="480.06" y="863.60" size="1.27" layer="95" />
                <pinref part="R46" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="444.50" y1="830.58" x2="447.04" y2="830.58" width="0.3" layer="91" />
                <label x="447.04" y="830.58" size="1.27" layer="95" />
                <pinref part="R45" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="426.72" y1="830.58" x2="429.26" y2="830.58" width="0.3" layer="91" />
                <label x="429.26" y="830.58" size="1.27" layer="95" />
                <pinref part="R44" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$69">
              <segment>
                <wire x1="492.76" y1="863.60" x2="495.30" y2="863.60" width="0.3" layer="91" />
                <label x="495.30" y="863.60" size="1.27" layer="95" />
                <pinref part="R46" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="551.18" y1="881.38" x2="553.72" y2="881.38" width="0.3" layer="91" />
                <label x="553.72" y="881.38" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1582.42" x2="1455.42" y2="1582.42" width="0.3" layer="91" />
                <label x="1455.42" y="1582.42" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-3" />
              </segment>
              <segment>
                <wire x1="513.08" y1="883.92" x2="515.62" y2="883.92" width="0.3" layer="91" />
                <label x="515.62" y="883.92" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$70">
              <segment>
                <wire x1="383.54" y1="797.56" x2="381.00" y2="797.56" width="0.3" layer="91" />
                <label x="381.00" y="797.56" size="1.27" layer="95" />
                <pinref part="R48" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="520.70" y1="891.54" x2="518.16" y2="891.54" width="0.3" layer="91" />
                <label x="518.16" y="891.54" size="1.27" layer="95" />
                <pinref part="U8" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="383.54" y1="784.86" x2="381.00" y2="784.86" width="0.3" layer="91" />
                <label x="381.00" y="784.86" size="1.27" layer="95" />
                <pinref part="R49" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="401.32" y1="789.94" x2="398.78" y2="789.94" width="0.3" layer="91" />
                <label x="398.78" y="789.94" size="1.27" layer="95" />
                <pinref part="R50" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$71">
              <segment>
                <wire x1="520.70" y1="871.22" x2="518.16" y2="871.22" width="0.3" layer="91" />
                <label x="518.16" y="871.22" size="1.27" layer="95" />
                <pinref part="R66" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="513.08" y1="873.76" x2="515.62" y2="873.76" width="0.3" layer="91" />
                <label x="515.62" y="873.76" size="1.27" layer="95" />
                <pinref part="U7" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$72">
              <segment>
                <wire x1="368.30" y1="957.58" x2="368.30" y2="955.04" width="0.3" layer="91" />
                <label x="368.30" y="955.04" size="1.27" layer="95" />
                <pinref part="8" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1574.80" x2="1455.42" y2="1574.80" width="0.3" layer="91" />
                <label x="1455.42" y="1574.80" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-6" />
              </segment>
              <segment>
                <wire x1="350.52" y1="942.34" x2="353.06" y2="942.34" width="0.3" layer="91" />
                <label x="353.06" y="942.34" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="E" />
              </segment>
            </net>
            <net name="N$73">
              <segment>
                <wire x1="266.70" y1="899.16" x2="266.70" y2="901.70" width="0.3" layer="91" />
                <label x="266.70" y="901.70" size="1.27" layer="95" />
                <pinref part="C23" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="248.92" y1="894.08" x2="246.38" y2="894.08" width="0.3" layer="91" />
                <label x="246.38" y="894.08" size="1.27" layer="95" />
                <pinref part="R67" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="238.76" y1="833.12" x2="241.30" y2="833.12" width="0.3" layer="91" />
                <label x="241.30" y="833.12" size="1.27" layer="95" />
                <pinref part="SW1" gate="G$1" pin="COM" />
              </segment>
              <segment>
                <wire x1="187.96" y1="817.88" x2="190.50" y2="817.88" width="0.3" layer="91" />
                <label x="190.50" y="817.88" size="1.27" layer="95" />
                <pinref part="R802" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="213.36" y1="833.12" x2="210.82" y2="833.12" width="0.3" layer="91" />
                <label x="210.82" y="833.12" size="1.27" layer="95" />
                <pinref part="SW1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="187.96" y1="810.26" x2="190.50" y2="810.26" width="0.3" layer="91" />
                <label x="190.50" y="810.26" size="1.27" layer="95" />
                <pinref part="R801" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$74">
              <segment>
                <wire x1="266.70" y1="891.54" x2="266.70" y2="889.00" width="0.3" layer="91" />
                <label x="266.70" y="889.00" size="1.27" layer="95" />
                <pinref part="C23" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="302.26" y1="927.10" x2="299.72" y2="927.10" width="0.3" layer="91" />
                <label x="299.72" y="927.10" size="1.27" layer="95" />
                <pinref part="R70" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="294.64" y1="911.86" x2="297.18" y2="911.86" width="0.3" layer="91" />
                <label x="297.18" y="911.86" size="1.27" layer="95" />
                <pinref part="R68" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$75">
              <segment>
                <wire x1="152.40" y1="795.02" x2="152.40" y2="792.48" width="0.3" layer="91" />
                <label x="152.40" y="792.48" size="1.27" layer="95" />
                <pinref part="C24" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="231.14" y1="853.44" x2="231.14" y2="855.98" width="0.3" layer="91" />
                <label x="231.14" y="855.98" size="1.27" layer="95" />
                <pinref part="C25" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="231.14" y1="866.14" x2="228.60" y2="866.14" width="0.3" layer="91" />
                <label x="228.60" y="866.14" size="1.27" layer="95" />
                <pinref part="R71" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="248.92" y1="878.84" x2="246.38" y2="878.84" width="0.3" layer="91" />
                <label x="246.38" y="878.84" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$76">
              <segment>
                <wire x1="152.40" y1="802.64" x2="152.40" y2="805.18" width="0.3" layer="91" />
                <label x="152.40" y="805.18" size="1.27" layer="95" />
                <pinref part="C24" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="160.02" y1="802.64" x2="157.48" y2="802.64" width="0.3" layer="91" />
                <label x="157.48" y="802.64" size="1.27" layer="95" />
                <pinref part="R69" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$77">
              <segment>
                <wire x1="231.14" y1="845.82" x2="231.14" y2="843.28" width="0.3" layer="91" />
                <label x="231.14" y="843.28" size="1.27" layer="95" />
                <pinref part="C25" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="284.48" y1="919.48" x2="281.94" y2="919.48" width="0.3" layer="91" />
                <label x="281.94" y="919.48" size="1.27" layer="95" />
                <pinref part="R72" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="248.92" y1="883.92" x2="246.38" y2="883.92" width="0.3" layer="91" />
                <label x="246.38" y="883.92" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1584.96" x2="1442.72" y2="1584.96" width="0.3" layer="91" />
                <label x="1442.72" y="1584.96" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-2" />
              </segment>
              <segment>
                <wire x1="205.74" y1="825.50" x2="208.28" y2="825.50" width="0.3" layer="91" />
                <label x="208.28" y="825.50" size="1.27" layer="95" />
                <pinref part="R76" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="241.30" y1="866.14" x2="243.84" y2="866.14" width="0.3" layer="91" />
                <label x="243.84" y="866.14" size="1.27" layer="95" />
                <pinref part="R71" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$78">
              <segment>
                <wire x1="342.90" y1="934.72" x2="345.44" y2="934.72" width="0.3" layer="91" />
                <label x="345.44" y="934.72" size="1.27" layer="95" />
                <pinref part="D12" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="335.28" y1="949.96" x2="332.74" y2="949.96" width="0.3" layer="91" />
                <label x="332.74" y="949.96" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="330.20" y1="934.72" x2="332.74" y2="934.72" width="0.3" layer="91" />
                <label x="332.74" y="934.72" size="1.27" layer="95" />
                <pinref part="R73" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$79">
              <segment>
                <wire x1="337.82" y1="934.72" x2="335.28" y2="934.72" width="0.3" layer="91" />
                <label x="335.28" y="934.72" size="1.27" layer="95" />
                <pinref part="D12" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="325.12" y1="927.10" x2="327.66" y2="927.10" width="0.3" layer="91" />
                <label x="327.66" y="927.10" size="1.27" layer="95" />
                <pinref part="D13" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="279.40" y1="878.84" x2="281.94" y2="878.84" width="0.3" layer="91" />
                <label x="281.94" y="878.84" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$80">
              <segment>
                <wire x1="320.04" y1="927.10" x2="317.50" y2="927.10" width="0.3" layer="91" />
                <label x="317.50" y="927.10" size="1.27" layer="95" />
                <pinref part="D13" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="279.40" y1="873.76" x2="281.94" y2="873.76" width="0.3" layer="91" />
                <label x="281.94" y="873.76" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="302.26" y1="919.48" x2="299.72" y2="919.48" width="0.3" layer="91" />
                <label x="299.72" y="919.48" size="1.27" layer="95" />
                <pinref part="R74" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$81">
              <segment>
                <wire x1="312.42" y1="927.10" x2="314.96" y2="927.10" width="0.3" layer="91" />
                <label x="314.96" y="927.10" size="1.27" layer="95" />
                <pinref part="R70" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="248.92" y1="873.76" x2="246.38" y2="873.76" width="0.3" layer="91" />
                <label x="246.38" y="873.76" size="1.27" layer="95" />
                <pinref part="U11" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$82">
              <segment>
                <wire x1="294.64" y1="919.48" x2="297.18" y2="919.48" width="0.3" layer="91" />
                <label x="297.18" y="919.48" size="1.27" layer="95" />
                <pinref part="R72" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="312.42" y1="919.48" x2="314.96" y2="919.48" width="0.3" layer="91" />
                <label x="314.96" y="919.48" size="1.27" layer="95" />
                <pinref part="R74" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="320.04" y1="934.72" x2="317.50" y2="934.72" width="0.3" layer="91" />
                <label x="317.50" y="934.72" size="1.27" layer="95" />
                <pinref part="R73" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$83">
              <segment>
                <wire x1="213.36" y1="848.36" x2="210.82" y2="848.36" width="0.3" layer="91" />
                <label x="210.82" y="848.36" size="1.27" layer="95" />
                <pinref part="R75" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="388.62" y1="1056.64" x2="386.08" y2="1056.64" width="0.3" layer="91" />
                <label x="386.08" y="1056.64" size="1.27" layer="95" />
                <pinref part="R97" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="406.40" y1="1064.26" x2="403.86" y2="1064.26" width="0.3" layer="91" />
                <label x="403.86" y="1064.26" size="1.27" layer="95" />
                <pinref part="R77" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="406.40" y1="1056.64" x2="403.86" y2="1056.64" width="0.3" layer="91" />
                <label x="403.86" y="1056.64" size="1.27" layer="95" />
                <pinref part="R98" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="195.58" y1="825.50" x2="193.04" y2="825.50" width="0.3" layer="91" />
                <label x="193.04" y="825.50" size="1.27" layer="95" />
                <pinref part="R76" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$84">
              <segment>
                <wire x1="177.80" y1="810.26" x2="175.26" y2="810.26" width="0.3" layer="91" />
                <label x="175.26" y="810.26" size="1.27" layer="95" />
                <pinref part="R801" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="213.36" y1="835.66" x2="210.82" y2="835.66" width="0.3" layer="91" />
                <label x="210.82" y="835.66" size="1.27" layer="95" />
                <pinref part="SW1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="5.08" y1="55.88" x2="2.54" y2="55.88" width="0.3" layer="91" />
                <label x="2.54" y="55.88" size="1.27" layer="95" />
                <pinref part="Guitar/Inst_In" gate="G$1" pin="TB" />
              </segment>
            </net>
            <net name="N$85">
              <segment>
                <wire x1="177.80" y1="817.88" x2="175.26" y2="817.88" width="0.3" layer="91" />
                <label x="175.26" y="817.88" size="1.27" layer="95" />
                <pinref part="R802" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="213.36" y1="830.58" x2="210.82" y2="830.58" width="0.3" layer="91" />
                <label x="210.82" y="830.58" size="1.27" layer="95" />
                <pinref part="SW1" gate="G$1" pin="3" />
              </segment>
              <segment>
                <wire x1="195.58" y1="817.88" x2="193.04" y2="817.88" width="0.3" layer="91" />
                <label x="193.04" y="817.88" size="1.27" layer="95" />
                <pinref part="R803" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$86">
              <segment>
                <wire x1="1266.19" y1="1681.48" x2="1268.73" y2="1681.48" width="0.3" layer="91" />
                <label x="1268.73" y="1681.48" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1228.09" y1="1681.48" x2="1225.55" y2="1681.48" width="0.3" layer="91" />
                <label x="1225.55" y="1681.48" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1243.33" y1="1689.10" x2="1245.87" y2="1689.10" width="0.3" layer="91" />
                <label x="1245.87" y="1689.10" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1198.88" y1="1625.60" x2="1201.42" y2="1625.60" width="0.3" layer="91" />
                <label x="1201.42" y="1625.60" size="1.27" layer="95" />
                <pinref part="R52" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$87">
              <segment>
                <wire x1="1250.95" y1="1673.86" x2="1248.41" y2="1673.86" width="0.3" layer="91" />
                <label x="1248.41" y="1673.86" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1329.69" y1="1663.70" x2="1332.23" y2="1663.70" width="0.3" layer="91" />
                <label x="1332.23" y="1663.70" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1337.31" y1="1678.94" x2="1334.77" y2="1678.94" width="0.3" layer="91" />
                <label x="1334.77" y="1678.94" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="E1" />
              </segment>
            </net>
            <net name="N$88">
              <segment>
                <wire x1="1337.31" y1="1663.70" x2="1334.77" y2="1663.70" width="0.3" layer="91" />
                <label x="1334.77" y="1663.70" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1250.95" y1="1648.46" x2="1248.41" y2="1648.46" width="0.3" layer="91" />
                <label x="1248.41" y="1648.46" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1309.37" y1="1666.24" x2="1311.91" y2="1666.24" width="0.3" layer="91" />
                <label x="1311.91" y="1666.24" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$89">
              <segment>
                <wire x1="1352.55" y1="1671.32" x2="1355.09" y2="1671.32" width="0.3" layer="91" />
                <label x="1355.09" y="1671.32" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1231.90" y1="1775.46" x2="1229.36" y2="1775.46" width="0.3" layer="91" />
                <label x="1229.36" y="1775.46" size="1.27" layer="95" />
                <pinref part="R53" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$90">
              <segment>
                <wire x1="1314.45" y1="1671.32" x2="1311.91" y2="1671.32" width="0.3" layer="91" />
                <label x="1311.91" y="1671.32" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1249.68" y1="1783.08" x2="1247.14" y2="1783.08" width="0.3" layer="91" />
                <label x="1247.14" y="1783.08" size="1.27" layer="95" />
                <pinref part="R54" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1249.68" y1="1790.70" x2="1247.14" y2="1790.70" width="0.3" layer="91" />
                <label x="1247.14" y="1790.70" size="1.27" layer="95" />
                <pinref part="R56" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$91">
              <segment>
                <wire x1="1329.69" y1="1678.94" x2="1332.23" y2="1678.94" width="0.3" layer="91" />
                <label x="1332.23" y="1678.94" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1337.31" y1="1686.56" x2="1334.77" y2="1686.56" width="0.3" layer="91" />
                <label x="1334.77" y="1686.56" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1271.27" y1="1666.24" x2="1268.73" y2="1666.24" width="0.3" layer="91" />
                <label x="1268.73" y="1666.24" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$92">
              <segment>
                <wire x1="1243.33" y1="1663.70" x2="1245.87" y2="1663.70" width="0.3" layer="91" />
                <label x="1245.87" y="1663.70" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1294.13" y1="1673.86" x2="1291.59" y2="1673.86" width="0.3" layer="91" />
                <label x="1291.59" y="1673.86" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1266.19" y1="1656.08" x2="1268.73" y2="1656.08" width="0.3" layer="91" />
                <label x="1268.73" y="1656.08" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1228.09" y1="1656.08" x2="1225.55" y2="1656.08" width="0.3" layer="91" />
                <label x="1225.55" y="1656.08" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$93">
              <segment>
                <wire x1="1352.55" y1="1694.18" x2="1355.09" y2="1694.18" width="0.3" layer="91" />
                <label x="1355.09" y="1694.18" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1286.51" y1="1658.62" x2="1289.05" y2="1658.62" width="0.3" layer="91" />
                <label x="1289.05" y="1658.62" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1314.45" y1="1694.18" x2="1311.91" y2="1694.18" width="0.3" layer="91" />
                <label x="1311.91" y="1694.18" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1329.69" y1="1701.80" x2="1332.23" y2="1701.80" width="0.3" layer="91" />
                <label x="1332.23" y="1701.80" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$94">
              <segment>
                <wire x1="1294.13" y1="1658.62" x2="1291.59" y2="1658.62" width="0.3" layer="91" />
                <label x="1291.59" y="1658.62" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1380.49" y1="1696.72" x2="1377.95" y2="1696.72" width="0.3" layer="91" />
                <label x="1377.95" y="1696.72" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1375.41" y1="1727.20" x2="1372.87" y2="1727.20" width="0.3" layer="91" />
                <label x="1372.87" y="1727.20" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$95">
              <segment>
                <wire x1="1286.51" y1="1673.86" x2="1289.05" y2="1673.86" width="0.3" layer="91" />
                <label x="1289.05" y="1673.86" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1390.65" y1="1734.82" x2="1393.19" y2="1734.82" width="0.3" layer="91" />
                <label x="1393.19" y="1734.82" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1206.50" y1="1633.22" x2="1203.96" y2="1633.22" width="0.3" layer="91" />
                <label x="1203.96" y="1633.22" size="1.27" layer="95" />
                <pinref part="R55" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1176.02" y1="1617.98" x2="1178.56" y2="1617.98" width="0.3" layer="91" />
                <label x="1178.56" y="1617.98" size="1.27" layer="95" />
                <pinref part="R47" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1158.24" y1="1625.60" x2="1160.78" y2="1625.60" width="0.3" layer="91" />
                <label x="1160.78" y="1625.60" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$96">
              <segment>
                <wire x1="1372.87" y1="1711.96" x2="1375.41" y2="1711.96" width="0.3" layer="91" />
                <label x="1375.41" y="1711.96" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="1390.65" y1="1719.58" x2="1393.19" y2="1719.58" width="0.3" layer="91" />
                <label x="1393.19" y="1719.58" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="1357.63" y1="1704.34" x2="1355.09" y2="1704.34" width="0.3" layer="91" />
                <label x="1355.09" y="1704.34" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1395.73" y1="1704.34" x2="1398.27" y2="1704.34" width="0.3" layer="91" />
                <label x="1398.27" y="1704.34" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$97">
              <segment>
                <wire x1="1398.27" y1="1734.82" x2="1395.73" y2="1734.82" width="0.3" layer="91" />
                <label x="1395.73" y="1734.82" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1398.27" y1="1742.44" x2="1395.73" y2="1742.44" width="0.3" layer="91" />
                <label x="1395.73" y="1742.44" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="1418.59" y1="1750.06" x2="1416.05" y2="1750.06" width="0.3" layer="91" />
                <label x="1416.05" y="1750.06" size="1.27" layer="95" />
                <pinref part="Q8$1" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$98">
              <segment>
                <wire x1="1441.45" y1="1757.68" x2="1438.91" y2="1757.68" width="0.3" layer="91" />
                <label x="1438.91" y="1757.68" size="1.27" layer="95" />
                <pinref part="Q8$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="1430.02" y1="1765.30" x2="1432.56" y2="1765.30" width="0.3" layer="91" />
                <label x="1432.56" y="1765.30" size="1.27" layer="95" />
                <pinref part="R2$2" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$99">
              <segment>
                <wire x1="1413.51" y1="1750.06" x2="1416.05" y2="1750.06" width="0.3" layer="91" />
                <label x="1416.05" y="1750.06" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="1375.41" y1="1750.06" x2="1372.87" y2="1750.06" width="0.3" layer="91" />
                <label x="1372.87" y="1750.06" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="1369.06" y1="1719.58" x2="1371.60" y2="1719.58" width="0.3" layer="91" />
                <label x="1371.60" y="1719.58" size="1.27" layer="95" />
                <pinref part="R1$2" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1390.65" y1="1757.68" x2="1393.19" y2="1757.68" width="0.3" layer="91" />
                <label x="1393.19" y="1757.68" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$100">
              <segment>
                <wire x1="1069.34" y1="1574.80" x2="1069.34" y2="1572.26" width="0.3" layer="91" />
                <label x="1069.34" y="1572.26" size="1.27" layer="95" />
                <pinref part="C17" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1061.72" y1="1577.34" x2="1064.26" y2="1577.34" width="0.3" layer="91" />
                <label x="1064.26" y="1577.34" size="1.27" layer="95" />
                <pinref part="R51" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$101">
              <segment>
                <wire x1="1206.50" y1="1638.30" x2="1206.50" y2="1635.76" width="0.3" layer="91" />
                <label x="1206.50" y="1635.76" size="1.27" layer="95" />
                <pinref part="C18" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1224.28" y1="1775.46" x2="1226.82" y2="1775.46" width="0.3" layer="91" />
                <label x="1226.82" y="1775.46" size="1.27" layer="95" />
                <pinref part="R59" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$102">
              <segment>
                <wire x1="1120.14" y1="1615.44" x2="1120.14" y2="1617.98" width="0.3" layer="91" />
                <label x="1120.14" y="1617.98" size="1.27" layer="95" />
                <pinref part="C21" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1092.20" y1="1595.12" x2="1094.74" y2="1595.12" width="0.3" layer="91" />
                <label x="1094.74" y="1595.12" size="1.27" layer="95" />
                <pinref part="D11" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1013.46" y1="1597.66" x2="1010.92" y2="1597.66" width="0.3" layer="91" />
                <label x="1010.92" y="1597.66" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1079.50" y1="1595.12" x2="1082.04" y2="1595.12" width="0.3" layer="91" />
                <label x="1082.04" y="1595.12" size="1.27" layer="95" />
                <pinref part="R64" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$103">
              <segment>
                <wire x1="1051.56" y1="1564.64" x2="1051.56" y2="1567.18" width="0.3" layer="91" />
                <label x="1051.56" y="1567.18" size="1.27" layer="95" />
                <pinref part="C22" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1069.34" y1="1595.12" x2="1066.80" y2="1595.12" width="0.3" layer="91" />
                <label x="1066.80" y="1595.12" size="1.27" layer="95" />
                <pinref part="R64" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1036.32" y1="1551.94" x2="1038.86" y2="1551.94" width="0.3" layer="91" />
                <label x="1038.86" y="1551.94" size="1.27" layer="95" />
                <pinref part="R65" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$104">
              <segment>
                <wire x1="1051.56" y1="1557.02" x2="1051.56" y2="1554.48" width="0.3" layer="91" />
                <label x="1051.56" y="1554.48" size="1.27" layer="95" />
                <pinref part="C22" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1013.46" y1="1602.74" x2="1010.92" y2="1602.74" width="0.3" layer="91" />
                <label x="1010.92" y="1602.74" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1013.46" y1="1607.82" x2="1010.92" y2="1607.82" width="0.3" layer="91" />
                <label x="1010.92" y="1607.82" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1120.14" y1="1602.74" x2="1117.60" y2="1602.74" width="0.3" layer="91" />
                <label x="1117.60" y="1602.74" size="1.27" layer="95" />
                <pinref part="R63" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$105">
              <segment>
                <wire x1="1178.56" y1="1628.14" x2="1181.10" y2="1628.14" width="0.3" layer="91" />
                <label x="1181.10" y="1628.14" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="1137.92" y1="1610.36" x2="1140.46" y2="1610.36" width="0.3" layer="91" />
                <label x="1140.46" y="1610.36" size="1.27" layer="95" />
                <pinref part="R57" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$106">
              <segment>
                <wire x1="1163.32" y1="1635.76" x2="1160.78" y2="1635.76" width="0.3" layer="91" />
                <label x="1160.78" y="1635.76" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="1127.76" y1="1635.76" x2="1125.22" y2="1635.76" width="0.3" layer="91" />
                <label x="1125.22" y="1635.76" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$107">
              <segment>
                <wire x1="1178.56" y1="1643.38" x2="1181.10" y2="1643.38" width="0.3" layer="91" />
                <label x="1181.10" y="1643.38" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="1188.72" y1="1625.60" x2="1186.18" y2="1625.60" width="0.3" layer="91" />
                <label x="1186.18" y="1625.60" size="1.27" layer="95" />
                <pinref part="R52" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$108">
              <segment>
                <wire x1="1165.86" y1="1617.98" x2="1163.32" y2="1617.98" width="0.3" layer="91" />
                <label x="1163.32" y="1617.98" size="1.27" layer="95" />
                <pinref part="R47" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1109.98" y1="1610.36" x2="1112.52" y2="1610.36" width="0.3" layer="91" />
                <label x="1112.52" y="1610.36" size="1.27" layer="95" />
                <pinref part="VR2" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1098.55" y1="1623.06" x2="1098.55" y2="1625.60" width="0.3" layer="91" />
                <label x="1098.55" y="1625.60" size="1.27" layer="95" />
                <pinref part="VR2" gate="G$1" pin="W" />
              </segment>
            </net>
            <net name="N$109">
              <segment>
                <wire x1="1216.66" y1="1633.22" x2="1219.20" y2="1633.22" width="0.3" layer="91" />
                <label x="1219.20" y="1633.22" size="1.27" layer="95" />
                <pinref part="R55" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1214.12" y1="1775.46" x2="1211.58" y2="1775.46" width="0.3" layer="91" />
                <label x="1211.58" y="1775.46" size="1.27" layer="95" />
                <pinref part="R59" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1231.90" y1="1783.08" x2="1229.36" y2="1783.08" width="0.3" layer="91" />
                <label x="1229.36" y="1783.08" size="1.27" layer="95" />
                <pinref part="R60" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$110">
              <segment>
                <wire x1="1259.84" y1="1790.70" x2="1262.38" y2="1790.70" width="0.3" layer="91" />
                <label x="1262.38" y="1790.70" size="1.27" layer="95" />
                <pinref part="R56" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1267.46" y1="1790.70" x2="1264.92" y2="1790.70" width="0.3" layer="91" />
                <label x="1264.92" y="1790.70" size="1.27" layer="95" />
                <pinref part="R62" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1242.06" y1="1783.08" x2="1244.60" y2="1783.08" width="0.3" layer="91" />
                <label x="1244.60" y="1783.08" size="1.27" layer="95" />
                <pinref part="R60" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1158.24" y1="1630.68" x2="1160.78" y2="1630.68" width="0.3" layer="91" />
                <label x="1160.78" y="1630.68" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$111">
              <segment>
                <wire x1="1127.76" y1="1610.36" x2="1125.22" y2="1610.36" width="0.3" layer="91" />
                <label x="1125.22" y="1610.36" size="1.27" layer="95" />
                <pinref part="R57" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1127.76" y1="1630.68" x2="1125.22" y2="1630.68" width="0.3" layer="91" />
                <label x="1125.22" y="1630.68" size="1.27" layer="95" />
                <pinref part="U9" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1130.30" y1="1602.74" x2="1132.84" y2="1602.74" width="0.3" layer="91" />
                <label x="1132.84" y="1602.74" size="1.27" layer="95" />
                <pinref part="R63" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$112">
              <segment>
                <wire x1="1277.62" y1="1798.32" x2="1280.16" y2="1798.32" width="0.3" layer="91" />
                <label x="1280.16" y="1798.32" size="1.27" layer="95" />
                <pinref part="R61" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1277.62" y1="1790.70" x2="1280.16" y2="1790.70" width="0.3" layer="91" />
                <label x="1280.16" y="1790.70" size="1.27" layer="95" />
                <pinref part="R62" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="1592.58" x2="1046.48" y2="1592.58" width="0.3" layer="91" />
                <label x="1046.48" y="1592.58" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$113">
              <segment>
                <wire x1="1043.94" y1="1597.66" x2="1046.48" y2="1597.66" width="0.3" layer="91" />
                <label x="1046.48" y="1597.66" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="1602.74" x2="1046.48" y2="1602.74" width="0.3" layer="91" />
                <label x="1046.48" y="1602.74" size="1.27" layer="95" />
                <pinref part="U10" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1582.42" x2="1442.72" y2="1582.42" width="0.3" layer="91" />
                <label x="1442.72" y="1582.42" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-3" />
              </segment>
            </net>
            <net name="N$114">
              <segment>
                <wire x1="170.18" y1="604.52" x2="170.18" y2="601.98" width="0.3" layer="91" />
                <label x="170.18" y="601.98" size="1.27" layer="95" />
                <pinref part="C601" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="187.96" y1="629.92" x2="185.42" y2="629.92" width="0.3" layer="91" />
                <label x="185.42" y="629.92" size="1.27" layer="95" />
                <pinref part="R603" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="198.12" y1="622.30" x2="200.66" y2="622.30" width="0.3" layer="91" />
                <label x="200.66" y="622.30" size="1.27" layer="95" />
                <pinref part="R602" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$115">
              <segment>
                <wire x1="170.18" y1="612.14" x2="170.18" y2="614.68" width="0.3" layer="91" />
                <label x="170.18" y="614.68" size="1.27" layer="95" />
                <pinref part="C601" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="170.18" y1="622.30" x2="167.64" y2="622.30" width="0.3" layer="91" />
                <label x="167.64" y="622.30" size="1.27" layer="95" />
                <pinref part="R601" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="60.96" y1="543.56" x2="58.42" y2="543.56" width="0.3" layer="91" />
                <label x="58.42" y="543.56" size="1.27" layer="95" />
                <pinref part="MIC_In" gate="G$1" pin="3" />
              </segment>
              <segment>
                <wire x1="60.96" y1="538.48" x2="58.42" y2="538.48" width="0.3" layer="91" />
                <label x="58.42" y="538.48" size="1.27" layer="95" />
                <pinref part="MIC_In" gate="G$1" pin="T" />
              </segment>
            </net>
            <net name="N$116">
              <segment>
                <wire x1="205.74" y1="627.38" x2="205.74" y2="624.84" width="0.3" layer="91" />
                <label x="205.74" y="624.84" size="1.27" layer="95" />
                <pinref part="C602" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="205.74" y1="647.70" x2="203.20" y2="647.70" width="0.3" layer="91" />
                <label x="203.20" y="647.70" size="1.27" layer="95" />
                <pinref part="R606" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="246.38" y1="683.26" x2="243.84" y2="683.26" width="0.3" layer="91" />
                <label x="243.84" y="683.26" size="1.27" layer="95" />
                <pinref part="R607" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="236.22" y1="665.48" x2="238.76" y2="665.48" width="0.3" layer="91" />
                <label x="238.76" y="665.48" size="1.27" layer="95" />
                <pinref part="Q61" gate="G$1" pin="E" />
              </segment>
            </net>
            <net name="N$117">
              <segment>
                <wire x1="205.74" y1="635.00" x2="205.74" y2="637.54" width="0.3" layer="91" />
                <label x="205.74" y="637.54" size="1.27" layer="95" />
                <pinref part="C602" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="220.98" y1="673.10" x2="218.44" y2="673.10" width="0.3" layer="91" />
                <label x="218.44" y="673.10" size="1.27" layer="95" />
                <pinref part="Q61" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="198.12" y1="629.92" x2="200.66" y2="629.92" width="0.3" layer="91" />
                <label x="200.66" y="629.92" size="1.27" layer="95" />
                <pinref part="R603" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$118">
              <segment>
                <wire x1="246.38" y1="670.56" x2="246.38" y2="673.10" width="0.3" layer="91" />
                <label x="246.38" y="673.10" size="1.27" layer="95" />
                <pinref part="C603" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="261.62" y1="701.04" x2="259.08" y2="701.04" width="0.3" layer="91" />
                <label x="259.08" y="701.04" size="1.27" layer="95" />
                <pinref part="Q62" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="264.16" y1="683.26" x2="261.62" y2="683.26" width="0.3" layer="91" />
                <label x="261.62" y="683.26" size="1.27" layer="95" />
                <pinref part="R604" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="236.22" y1="680.72" x2="238.76" y2="680.72" width="0.3" layer="91" />
                <label x="238.76" y="680.72" size="1.27" layer="95" />
                <pinref part="Q61" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$119">
              <segment>
                <wire x1="304.80" y1="721.36" x2="304.80" y2="723.90" width="0.3" layer="91" />
                <label x="304.80" y="723.90" size="1.27" layer="95" />
                <pinref part="C604" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="312.42" y1="741.68" x2="312.42" y2="744.22" width="0.3" layer="91" />
                <label x="312.42" y="744.22" size="1.27" layer="95" />
                <pinref part="C606" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="320.04" y1="741.68" x2="317.50" y2="741.68" width="0.3" layer="91" />
                <label x="317.50" y="741.68" size="1.27" layer="95" />
                <pinref part="R610" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="274.32" y1="683.26" x2="276.86" y2="683.26" width="0.3" layer="91" />
                <label x="276.86" y="683.26" size="1.27" layer="95" />
                <pinref part="R604" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="287.02" y1="698.50" x2="284.48" y2="698.50" width="0.3" layer="91" />
                <label x="284.48" y="698.50" size="1.27" layer="95" />
                <pinref part="R605" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$120">
              <segment>
                <wire x1="304.80" y1="695.96" x2="304.80" y2="693.42" width="0.3" layer="91" />
                <label x="304.80" y="693.42" size="1.27" layer="95" />
                <pinref part="C605" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="320.04" y1="734.06" x2="317.50" y2="734.06" width="0.3" layer="91" />
                <label x="317.50" y="734.06" size="1.27" layer="95" />
                <pinref part="R609" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="297.18" y1="690.88" x2="299.72" y2="690.88" width="0.3" layer="91" />
                <label x="299.72" y="690.88" size="1.27" layer="95" />
                <pinref part="R608" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="312.42" y1="716.28" x2="312.42" y2="713.74" width="0.3" layer="91" />
                <label x="312.42" y="713.74" size="1.27" layer="95" />
                <pinref part="C608" gate="G$1" pin="-" />
              </segment>
            </net>
            <net name="N$121">
              <segment>
                <wire x1="223.52" y1="647.70" x2="223.52" y2="645.16" width="0.3" layer="91" />
                <label x="223.52" y="645.16" size="1.27" layer="95" />
                <pinref part="C607" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="215.90" y1="647.70" x2="218.44" y2="647.70" width="0.3" layer="91" />
                <label x="218.44" y="647.70" size="1.27" layer="95" />
                <pinref part="R606" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$122">
              <segment>
                <wire x1="223.52" y1="655.32" x2="223.52" y2="657.86" width="0.3" layer="91" />
                <label x="223.52" y="657.86" size="1.27" layer="95" />
                <pinref part="C607" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1572.26" x2="1455.42" y2="1572.26" width="0.3" layer="91" />
                <label x="1455.42" y="1572.26" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-7" />
              </segment>
            </net>
            <net name="N$123">
              <segment>
                <wire x1="276.86" y1="693.42" x2="279.40" y2="693.42" width="0.3" layer="91" />
                <label x="279.40" y="693.42" size="1.27" layer="95" />
                <pinref part="Q62" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="297.18" y1="698.50" x2="299.72" y2="698.50" width="0.3" layer="91" />
                <label x="299.72" y="698.50" size="1.27" layer="95" />
                <pinref part="R605" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$124">
              <segment>
                <wire x1="767.08" y1="1338.58" x2="767.08" y2="1336.04" width="0.3" layer="91" />
                <label x="767.08" y="1336.04" size="1.27" layer="95" />
                <pinref part="11" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="690.88" y1="1308.10" x2="693.42" y2="1308.10" width="0.3" layer="91" />
                <label x="693.42" y="1308.10" size="1.27" layer="95" />
                <pinref part="R89" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="698.50" y1="1305.56" x2="698.50" y2="1303.02" width="0.3" layer="91" />
                <label x="698.50" y="1303.02" size="1.27" layer="95" />
                <pinref part="C31" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="774.70" y1="1363.98" x2="774.70" y2="1366.52" width="0.3" layer="91" />
                <label x="774.70" y="1366.52" size="1.27" layer="95" />
                <pinref part="C33" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="698.50" y1="1343.66" x2="695.96" y2="1343.66" width="0.3" layer="91" />
                <label x="695.96" y="1343.66" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$125">
              <segment>
                <wire x1="767.08" y1="1346.20" x2="767.08" y2="1343.66" width="0.3" layer="91" />
                <label x="767.08" y="1343.66" size="1.27" layer="95" />
                <pinref part="9" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="736.60" y1="1330.96" x2="736.60" y2="1328.42" width="0.3" layer="91" />
                <label x="736.60" y="1328.42" size="1.27" layer="95" />
                <pinref part="C30" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="746.76" y1="1325.88" x2="749.30" y2="1325.88" width="0.3" layer="91" />
                <label x="749.30" y="1325.88" size="1.27" layer="95" />
                <pinref part="R88" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="782.32" y1="1363.98" x2="782.32" y2="1366.52" width="0.3" layer="91" />
                <label x="782.32" y="1366.52" size="1.27" layer="95" />
                <pinref part="C32" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="728.98" y1="1338.58" x2="731.52" y2="1338.58" width="0.3" layer="91" />
                <label x="731.52" y="1338.58" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$126">
              <segment>
                <wire x1="736.60" y1="1338.58" x2="736.60" y2="1341.12" width="0.3" layer="91" />
                <label x="736.60" y="1341.12" size="1.27" layer="95" />
                <pinref part="C30" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="728.98" y1="1333.50" x2="731.52" y2="1333.50" width="0.3" layer="91" />
                <label x="731.52" y="1333.50" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="566.42" y1="1209.04" x2="566.42" y2="1211.58" width="0.3" layer="91" />
                <label x="566.42" y="1211.58" size="1.27" layer="95" />
                <pinref part="C34" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="736.60" y1="1325.88" x2="734.06" y2="1325.88" width="0.3" layer="91" />
                <label x="734.06" y="1325.88" size="1.27" layer="95" />
                <pinref part="R88" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$127">
              <segment>
                <wire x1="698.50" y1="1313.18" x2="698.50" y2="1315.72" width="0.3" layer="91" />
                <label x="698.50" y="1315.72" size="1.27" layer="95" />
                <pinref part="C31" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="698.50" y1="1338.58" x2="695.96" y2="1338.58" width="0.3" layer="91" />
                <label x="695.96" y="1338.58" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="584.20" y1="1244.60" x2="584.20" y2="1247.14" width="0.3" layer="91" />
                <label x="584.20" y="1247.14" size="1.27" layer="95" />
                <pinref part="C35" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="680.72" y1="1308.10" x2="678.18" y2="1308.10" width="0.3" layer="91" />
                <label x="678.18" y="1308.10" size="1.27" layer="95" />
                <pinref part="R89" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$128">
              <segment>
                <wire x1="782.32" y1="1356.36" x2="782.32" y2="1353.82" width="0.3" layer="91" />
                <label x="782.32" y="1353.82" size="1.27" layer="95" />
                <pinref part="C32" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="861.06" y1="1414.78" x2="858.52" y2="1414.78" width="0.3" layer="91" />
                <label x="858.52" y="1414.78" size="1.27" layer="95" />
                <pinref part="R79" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="825.50" y1="1389.38" x2="822.96" y2="1389.38" width="0.3" layer="91" />
                <label x="822.96" y="1389.38" size="1.27" layer="95" />
                <pinref part="R81" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="825.50" y1="1397.00" x2="822.96" y2="1397.00" width="0.3" layer="91" />
                <label x="822.96" y="1397.00" size="1.27" layer="95" />
                <pinref part="R83" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$129">
              <segment>
                <wire x1="774.70" y1="1356.36" x2="774.70" y2="1353.82" width="0.3" layer="91" />
                <label x="774.70" y="1353.82" size="1.27" layer="95" />
                <pinref part="C33" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="843.28" y1="1414.78" x2="840.74" y2="1414.78" width="0.3" layer="91" />
                <label x="840.74" y="1414.78" size="1.27" layer="95" />
                <pinref part="R78" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="807.72" y1="1389.38" x2="805.18" y2="1389.38" width="0.3" layer="91" />
                <label x="805.18" y="1389.38" size="1.27" layer="95" />
                <pinref part="R80" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="807.72" y1="1381.76" x2="805.18" y2="1381.76" width="0.3" layer="91" />
                <label x="805.18" y="1381.76" size="1.27" layer="95" />
                <pinref part="R82" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$130">
              <segment>
                <wire x1="566.42" y1="1201.42" x2="566.42" y2="1198.88" width="0.3" layer="91" />
                <label x="566.42" y="1198.88" size="1.27" layer="95" />
                <pinref part="C34" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="487.68" y1="1094.74" x2="490.22" y2="1094.74" width="0.3" layer="91" />
                <label x="490.22" y="1094.74" size="1.27" layer="95" />
                <pinref part="R101" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="1087.12" x2="490.22" y2="1087.12" width="0.3" layer="91" />
                <label x="490.22" y="1087.12" size="1.27" layer="95" />
                <pinref part="R105" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="469.90" y1="1087.12" x2="472.44" y2="1087.12" width="0.3" layer="91" />
                <label x="472.44" y="1087.12" size="1.27" layer="95" />
                <pinref part="R104" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$131">
              <segment>
                <wire x1="584.20" y1="1236.98" x2="584.20" y2="1234.44" width="0.3" layer="91" />
                <label x="584.20" y="1234.44" size="1.27" layer="95" />
                <pinref part="C35" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="469.90" y1="1079.50" x2="472.44" y2="1079.50" width="0.3" layer="91" />
                <label x="472.44" y="1079.50" size="1.27" layer="95" />
                <pinref part="R102" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="452.12" y1="1079.50" x2="454.66" y2="1079.50" width="0.3" layer="91" />
                <label x="454.66" y="1079.50" size="1.27" layer="95" />
                <pinref part="R107" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="452.12" y1="1071.88" x2="454.66" y2="1071.88" width="0.3" layer="91" />
                <label x="454.66" y="1071.88" size="1.27" layer="95" />
                <pinref part="R106" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$132">
              <segment>
                <wire x1="800.10" y1="1389.38" x2="800.10" y2="1391.92" width="0.3" layer="91" />
                <label x="800.10" y="1391.92" size="1.27" layer="95" />
                <pinref part="C36" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="792.48" y1="1374.14" x2="795.02" y2="1374.14" width="0.3" layer="91" />
                <label x="795.02" y="1374.14" size="1.27" layer="95" />
                <pinref part="R92" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="805.18" y1="1374.14" x2="807.72" y2="1374.14" width="0.3" layer="91" />
                <label x="807.72" y="1374.14" size="1.27" layer="95" />
                <pinref part="D14" gate="A" pin="-" />
              </segment>
            </net>
            <net name="N$133">
              <segment>
                <wire x1="680.72" y1="1297.94" x2="680.72" y2="1300.48" width="0.3" layer="91" />
                <label x="680.72" y="1300.48" size="1.27" layer="95" />
                <pinref part="C37" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="596.90" y1="1236.98" x2="599.44" y2="1236.98" width="0.3" layer="91" />
                <label x="599.44" y="1236.98" size="1.27" layer="95" />
                <pinref part="D15" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="655.32" y1="1282.70" x2="657.86" y2="1282.70" width="0.3" layer="91" />
                <label x="657.86" y="1282.70" size="1.27" layer="95" />
                <pinref part="R96" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$134">
              <segment>
                <wire x1="800.10" y1="1374.14" x2="797.56" y2="1374.14" width="0.3" layer="91" />
                <label x="797.56" y="1374.14" size="1.27" layer="95" />
                <pinref part="D14" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="546.10" y1="1214.12" x2="543.56" y2="1214.12" width="0.3" layer="91" />
                <label x="543.56" y="1214.12" size="1.27" layer="95" />
                <pinref part="Q10" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="546.10" y1="1181.10" x2="543.56" y2="1181.10" width="0.3" layer="91" />
                <label x="543.56" y="1181.10" size="1.27" layer="95" />
                <pinref part="Q8$2" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="528.32" y1="1181.10" x2="525.78" y2="1181.10" width="0.3" layer="91" />
                <label x="525.78" y="1181.10" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="G" />
              </segment>
            </net>
            <net name="N$135">
              <segment>
                <wire x1="591.82" y1="1236.98" x2="589.28" y2="1236.98" width="0.3" layer="91" />
                <label x="589.28" y="1236.98" size="1.27" layer="95" />
                <pinref part="D15" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="510.54" y1="1148.08" x2="508.00" y2="1148.08" width="0.3" layer="91" />
                <label x="508.00" y="1148.08" size="1.27" layer="95" />
                <pinref part="Q15" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="528.32" y1="1148.08" x2="525.78" y2="1148.08" width="0.3" layer="91" />
                <label x="525.78" y="1148.08" size="1.27" layer="95" />
                <pinref part="Q14" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="510.54" y1="1115.06" x2="508.00" y2="1115.06" width="0.3" layer="91" />
                <label x="508.00" y="1115.06" size="1.27" layer="95" />
                <pinref part="Q13" gate="G$1" pin="G" />
              </segment>
            </net>
            <net name="N$136">
              <segment>
                <wire x1="957.58" y1="1457.96" x2="960.12" y2="1457.96" width="0.3" layer="91" />
                <label x="960.12" y="1457.96" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="7" />
              </segment>
              <segment>
                <wire x1="1493.52" y1="1590.04" x2="1490.98" y2="1590.04" width="0.3" layer="91" />
                <label x="1490.98" y="1590.04" size="1.27" layer="95" />
                <pinref part="Vocal_Amp_B" gate="G$1" pin="T" />
              </segment>
            </net>
            <net name="N$137">
              <segment>
                <wire x1="932.18" y1="1455.42" x2="929.64" y2="1455.42" width="0.3" layer="91" />
                <label x="929.64" y="1455.42" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="8" />
              </segment>
              <segment>
                <wire x1="914.40" y1="1437.64" x2="911.86" y2="1437.64" width="0.3" layer="91" />
                <label x="911.86" y="1437.64" size="1.27" layer="95" />
                <pinref part="R809" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="896.62" y1="1437.64" x2="894.08" y2="1437.64" width="0.3" layer="91" />
                <label x="894.08" y="1437.64" size="1.27" layer="95" />
                <pinref part="R808" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$138">
              <segment>
                <wire x1="932.18" y1="1460.50" x2="929.64" y2="1460.50" width="0.3" layer="91" />
                <label x="929.64" y="1460.50" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="5" />
              </segment>
              <segment>
                <wire x1="896.62" y1="1430.02" x2="894.08" y2="1430.02" width="0.3" layer="91" />
                <label x="894.08" y="1430.02" size="1.27" layer="95" />
                <pinref part="R807" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="835.66" y1="1397.00" x2="838.20" y2="1397.00" width="0.3" layer="91" />
                <label x="838.20" y="1397.00" size="1.27" layer="95" />
                <pinref part="R83" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$139">
              <segment>
                <wire x1="932.18" y1="1470.66" x2="929.64" y2="1470.66" width="0.3" layer="91" />
                <label x="929.64" y="1470.66" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="861.06" y1="1422.40" x2="858.52" y2="1422.40" width="0.3" layer="91" />
                <label x="858.52" y="1422.40" size="1.27" layer="95" />
                <pinref part="R804" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="817.88" y1="1381.76" x2="820.42" y2="1381.76" width="0.3" layer="91" />
                <label x="820.42" y="1381.76" size="1.27" layer="95" />
                <pinref part="R82" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$140">
              <segment>
                <wire x1="957.58" y1="1468.12" x2="960.12" y2="1468.12" width="0.3" layer="91" />
                <label x="960.12" y="1468.12" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="3" />
              </segment>
              <segment>
                <wire x1="1478.28" y1="1582.42" x2="1475.74" y2="1582.42" width="0.3" layer="91" />
                <label x="1475.74" y="1582.42" size="1.27" layer="95" />
                <pinref part="A+B" gate="G$1" pin="P$3" />
              </segment>
              <segment>
                <wire x1="1493.52" y1="1584.96" x2="1490.98" y2="1584.96" width="0.3" layer="91" />
                <label x="1490.98" y="1584.96" size="1.27" layer="95" />
                <pinref part="Vocal_Amp_B" gate="G$1" pin="TN" />
              </segment>
            </net>
            <net name="N$141">
              <segment>
                <wire x1="932.18" y1="1468.12" x2="929.64" y2="1468.12" width="0.3" layer="91" />
                <label x="929.64" y="1468.12" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="889.00" y1="1422.40" x2="891.54" y2="1422.40" width="0.3" layer="91" />
                <label x="891.54" y="1422.40" size="1.27" layer="95" />
                <pinref part="R805" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="871.22" y1="1422.40" x2="873.76" y2="1422.40" width="0.3" layer="91" />
                <label x="873.76" y="1422.40" size="1.27" layer="95" />
                <pinref part="R804" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$142">
              <segment>
                <wire x1="932.18" y1="1457.96" x2="929.64" y2="1457.96" width="0.3" layer="91" />
                <label x="929.64" y="1457.96" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="6" />
              </segment>
              <segment>
                <wire x1="906.78" y1="1437.64" x2="909.32" y2="1437.64" width="0.3" layer="91" />
                <label x="909.32" y="1437.64" size="1.27" layer="95" />
                <pinref part="R808" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="906.78" y1="1430.02" x2="909.32" y2="1430.02" width="0.3" layer="91" />
                <label x="909.32" y="1430.02" size="1.27" layer="95" />
                <pinref part="R807" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$143">
              <segment>
                <wire x1="932.18" y1="1465.58" x2="929.64" y2="1465.58" width="0.3" layer="91" />
                <label x="929.64" y="1465.58" size="1.27" layer="95" />
                <pinref part="Output_Level" gate="G$1" pin="4" />
              </segment>
              <segment>
                <wire x1="878.84" y1="1430.02" x2="876.30" y2="1430.02" width="0.3" layer="91" />
                <label x="876.30" y="1430.02" size="1.27" layer="95" />
                <pinref part="R806" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="878.84" y1="1422.40" x2="876.30" y2="1422.40" width="0.3" layer="91" />
                <label x="876.30" y="1422.40" size="1.27" layer="95" />
                <pinref part="R805" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$144">
              <segment>
                <wire x1="556.26" y1="1201.42" x2="556.26" y2="1198.88" width="0.3" layer="91" />
                <label x="556.26" y="1198.88" size="1.27" layer="95" />
                <pinref part="Q10" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="662.94" y1="1282.70" x2="660.40" y2="1282.70" width="0.3" layer="91" />
                <label x="660.40" y="1282.70" size="1.27" layer="95" />
                <pinref part="R103" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$145">
              <segment>
                <wire x1="556.26" y1="1226.82" x2="556.26" y2="1229.36" width="0.3" layer="91" />
                <label x="556.26" y="1229.36" size="1.27" layer="95" />
                <pinref part="Q10" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="853.44" y1="1414.78" x2="855.98" y2="1414.78" width="0.3" layer="91" />
                <label x="855.98" y="1414.78" size="1.27" layer="95" />
                <pinref part="R78" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="871.22" y1="1414.78" x2="873.76" y2="1414.78" width="0.3" layer="91" />
                <label x="873.76" y="1414.78" size="1.27" layer="95" />
                <pinref part="R79" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1478.28" y1="1584.96" x2="1475.74" y2="1584.96" width="0.3" layer="91" />
                <label x="1475.74" y="1584.96" size="1.27" layer="95" />
                <pinref part="A+B" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$146">
              <segment>
                <wire x1="756.92" y1="1356.36" x2="759.46" y2="1356.36" width="0.3" layer="91" />
                <label x="759.46" y="1356.36" size="1.27" layer="95" />
                <pinref part="Q11" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="782.32" y1="1374.14" x2="779.78" y2="1374.14" width="0.3" layer="91" />
                <label x="779.78" y="1374.14" size="1.27" layer="95" />
                <pinref part="R92" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="754.38" y1="1333.50" x2="756.92" y2="1333.50" width="0.3" layer="91" />
                <label x="756.92" y="1333.50" size="1.27" layer="95" />
                <pinref part="R93" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$147">
              <segment>
                <wire x1="741.68" y1="1348.74" x2="739.14" y2="1348.74" width="0.3" layer="91" />
                <label x="739.14" y="1348.74" size="1.27" layer="95" />
                <pinref part="Q11" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="673.10" y1="1290.32" x2="675.64" y2="1290.32" width="0.3" layer="91" />
                <label x="675.64" y="1290.32" size="1.27" layer="95" />
                <pinref part="R95" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$148">
              <segment>
                <wire x1="607.06" y1="1252.22" x2="604.52" y2="1252.22" width="0.3" layer="91" />
                <label x="604.52" y="1252.22" size="1.27" layer="95" />
                <pinref part="Q12" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="505.46" y1="1102.36" x2="508.00" y2="1102.36" width="0.3" layer="91" />
                <label x="508.00" y="1102.36" size="1.27" layer="95" />
                <pinref part="R91" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="505.46" y1="1094.74" x2="508.00" y2="1094.74" width="0.3" layer="91" />
                <label x="508.00" y="1094.74" size="1.27" layer="95" />
                <pinref part="R90" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$149">
              <segment>
                <wire x1="622.30" y1="1259.84" x2="624.84" y2="1259.84" width="0.3" layer="91" />
                <label x="624.84" y="1259.84" size="1.27" layer="95" />
                <pinref part="Q12" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="662.94" y1="1290.32" x2="660.40" y2="1290.32" width="0.3" layer="91" />
                <label x="660.40" y="1290.32" size="1.27" layer="95" />
                <pinref part="R95" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="645.16" y1="1282.70" x2="642.62" y2="1282.70" width="0.3" layer="91" />
                <label x="642.62" y="1282.70" size="1.27" layer="95" />
                <pinref part="R96" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="601.98" y1="1244.60" x2="604.52" y2="1244.60" width="0.3" layer="91" />
                <label x="604.52" y="1244.60" size="1.27" layer="95" />
                <pinref part="R94" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$150">
              <segment>
                <wire x1="520.70" y1="1102.36" x2="520.70" y2="1099.82" width="0.3" layer="91" />
                <label x="520.70" y="1099.82" size="1.27" layer="95" />
                <pinref part="Q13" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="673.10" y1="1282.70" x2="675.64" y2="1282.70" width="0.3" layer="91" />
                <label x="675.64" y="1282.70" size="1.27" layer="95" />
                <pinref part="R103" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="784.86" y1="1348.74" x2="787.40" y2="1348.74" width="0.3" layer="91" />
                <label x="787.40" y="1348.74" size="1.27" layer="95" />
                <pinref part="R810" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1496.06" y1="1600.20" x2="1493.52" y2="1600.20" width="0.3" layer="91" />
                <label x="1493.52" y="1600.20" size="1.27" layer="95" />
                <pinref part="Guitar_Amp" gate="G$1" pin="P$3" />
              </segment>
            </net>
            <net name="N$151">
              <segment>
                <wire x1="520.70" y1="1127.76" x2="520.70" y2="1130.30" width="0.3" layer="91" />
                <label x="520.70" y="1130.30" size="1.27" layer="95" />
                <pinref part="Q13" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="398.78" y1="1056.64" x2="401.32" y2="1056.64" width="0.3" layer="91" />
                <label x="401.32" y="1056.64" size="1.27" layer="95" />
                <pinref part="R97" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$152">
              <segment>
                <wire x1="538.48" y1="1160.78" x2="538.48" y2="1163.32" width="0.3" layer="91" />
                <label x="538.48" y="1163.32" size="1.27" layer="95" />
                <pinref part="Q14" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="416.56" y1="1056.64" x2="419.10" y2="1056.64" width="0.3" layer="91" />
                <label x="419.10" y="1056.64" size="1.27" layer="95" />
                <pinref part="R98" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$153">
              <segment>
                <wire x1="538.48" y1="1135.38" x2="538.48" y2="1132.84" width="0.3" layer="91" />
                <label x="538.48" y="1132.84" size="1.27" layer="95" />
                <pinref part="Q14" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="441.96" y1="1071.88" x2="439.42" y2="1071.88" width="0.3" layer="91" />
                <label x="439.42" y="1071.88" size="1.27" layer="95" />
                <pinref part="R106" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="459.74" y1="1087.12" x2="457.20" y2="1087.12" width="0.3" layer="91" />
                <label x="457.20" y="1087.12" size="1.27" layer="95" />
                <pinref part="R104" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1496.06" y1="1602.74" x2="1493.52" y2="1602.74" width="0.3" layer="91" />
                <label x="1493.52" y="1602.74" size="1.27" layer="95" />
                <pinref part="Guitar_Amp" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$154">
              <segment>
                <wire x1="520.70" y1="1135.38" x2="520.70" y2="1132.84" width="0.3" layer="91" />
                <label x="520.70" y="1132.84" size="1.27" layer="95" />
                <pinref part="Q15" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="477.52" y1="1087.12" x2="474.98" y2="1087.12" width="0.3" layer="91" />
                <label x="474.98" y="1087.12" size="1.27" layer="95" />
                <pinref part="R105" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="441.96" y1="1079.50" x2="439.42" y2="1079.50" width="0.3" layer="91" />
                <label x="439.42" y="1079.50" size="1.27" layer="95" />
                <pinref part="R107" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$155">
              <segment>
                <wire x1="520.70" y1="1160.78" x2="520.70" y2="1163.32" width="0.3" layer="91" />
                <label x="520.70" y="1163.32" size="1.27" layer="95" />
                <pinref part="Q15" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="434.34" y1="1071.88" x2="436.88" y2="1071.88" width="0.3" layer="91" />
                <label x="436.88" y="1071.88" size="1.27" layer="95" />
                <pinref part="R100" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="434.34" y1="1064.26" x2="436.88" y2="1064.26" width="0.3" layer="91" />
                <label x="436.88" y="1064.26" size="1.27" layer="95" />
                <pinref part="R99" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$156">
              <segment>
                <wire x1="556.26" y1="1193.80" x2="556.26" y2="1196.34" width="0.3" layer="91" />
                <label x="556.26" y="1196.34" size="1.27" layer="95" />
                <pinref part="Q8$2" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1559.56" x2="1455.42" y2="1559.56" width="0.3" layer="91" />
                <label x="1455.42" y="1559.56" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-12" />
              </segment>
            </net>
            <net name="N$157">
              <segment>
                <wire x1="556.26" y1="1168.40" x2="556.26" y2="1165.86" width="0.3" layer="91" />
                <label x="556.26" y="1165.86" size="1.27" layer="95" />
                <pinref part="Q8$2" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="477.52" y1="1094.74" x2="474.98" y2="1094.74" width="0.3" layer="91" />
                <label x="474.98" y="1094.74" size="1.27" layer="95" />
                <pinref part="R101" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$158">
              <segment>
                <wire x1="538.48" y1="1193.80" x2="538.48" y2="1196.34" width="0.3" layer="91" />
                <label x="538.48" y="1196.34" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="1457.96" y1="1562.10" x2="1455.42" y2="1562.10" width="0.3" layer="91" />
                <label x="1455.42" y="1562.10" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="2-11" />
              </segment>
            </net>
            <net name="N$159">
              <segment>
                <wire x1="538.48" y1="1168.40" x2="538.48" y2="1165.86" width="0.3" layer="91" />
                <label x="538.48" y="1165.86" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="459.74" y1="1079.50" x2="457.20" y2="1079.50" width="0.3" layer="91" />
                <label x="457.20" y="1079.50" size="1.27" layer="95" />
                <pinref part="R102" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$160">
              <segment>
                <wire x1="817.88" y1="1389.38" x2="820.42" y2="1389.38" width="0.3" layer="91" />
                <label x="820.42" y="1389.38" size="1.27" layer="95" />
                <pinref part="R80" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="835.66" y1="1389.38" x2="838.20" y2="1389.38" width="0.3" layer="91" />
                <label x="838.20" y="1389.38" size="1.27" layer="95" />
                <pinref part="R81" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$161">
              <segment>
                <wire x1="655.32" y1="1275.08" x2="657.86" y2="1275.08" width="0.3" layer="91" />
                <label x="657.86" y="1275.08" size="1.27" layer="95" />
                <pinref part="R84" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="637.54" y1="1275.08" x2="640.08" y2="1275.08" width="0.3" layer="91" />
                <label x="640.08" y="1275.08" size="1.27" layer="95" />
                <pinref part="R85" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="728.98" y1="1328.42" x2="731.52" y2="1328.42" width="0.3" layer="91" />
                <label x="731.52" y="1328.42" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$162">
              <segment>
                <wire x1="627.38" y1="1275.08" x2="624.84" y2="1275.08" width="0.3" layer="91" />
                <label x="624.84" y="1275.08" size="1.27" layer="95" />
                <pinref part="R85" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="406.40" y1="505.46" x2="406.40" y2="508.00" width="0.3" layer="91" />
                <label x="406.40" y="508.00" size="1.27" layer="95" />
                <pinref part="C39" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="411.48" y1="490.22" x2="414.02" y2="490.22" width="0.3" layer="91" />
                <label x="414.02" y="490.22" size="1.27" layer="95" />
                <pinref part="D16" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="609.60" y1="1267.46" x2="607.06" y2="1267.46" width="0.3" layer="91" />
                <label x="607.06" y="1267.46" size="1.27" layer="95" />
                <pinref part="R86" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$163">
              <segment>
                <wire x1="619.76" y1="1267.46" x2="622.30" y2="1267.46" width="0.3" layer="91" />
                <label x="622.30" y="1267.46" size="1.27" layer="95" />
                <pinref part="R86" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="637.54" y1="1267.46" x2="640.08" y2="1267.46" width="0.3" layer="91" />
                <label x="640.08" y="1267.46" size="1.27" layer="95" />
                <pinref part="R87" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="698.50" y1="1333.50" x2="695.96" y2="1333.50" width="0.3" layer="91" />
                <label x="695.96" y="1333.50" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$164">
              <segment>
                <wire x1="495.30" y1="1094.74" x2="492.76" y2="1094.74" width="0.3" layer="91" />
                <label x="492.76" y="1094.74" size="1.27" layer="95" />
                <pinref part="R90" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="988.06" y1="955.04" x2="985.52" y2="955.04" width="0.3" layer="91" />
                <label x="985.52" y="955.04" size="1.27" layer="95" />
                <pinref part="Vocoder/Hold" gate="G$1" pin="TT" />
              </segment>
            </net>
            <net name="N$165">
              <segment>
                <wire x1="698.50" y1="1328.42" x2="695.96" y2="1328.42" width="0.3" layer="91" />
                <label x="695.96" y="1328.42" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="398.78" y1="490.22" x2="398.78" y2="487.68" width="0.3" layer="91" />
                <label x="398.78" y="487.68" size="1.27" layer="95" />
                <pinref part="C42" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="388.62" y1="474.98" x2="391.16" y2="474.98" width="0.3" layer="91" />
                <label x="391.16" y="474.98" size="1.27" layer="95" />
                <pinref part="Q17" gate="G$1" pin="E" />
              </segment>
            </net>
            <net name="N$166">
              <segment>
                <wire x1="728.98" y1="1343.66" x2="731.52" y2="1343.66" width="0.3" layer="91" />
                <label x="731.52" y="1343.66" size="1.27" layer="95" />
                <pinref part="U12" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="398.78" y1="480.06" x2="398.78" y2="482.60" width="0.3" layer="91" />
                <label x="398.78" y="482.60" size="1.27" layer="95" />
                <pinref part="C40" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="406.40" y1="490.22" x2="403.86" y2="490.22" width="0.3" layer="91" />
                <label x="403.86" y="490.22" size="1.27" layer="95" />
                <pinref part="D16" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="381.00" y1="431.80" x2="383.54" y2="431.80" width="0.3" layer="91" />
                <label x="383.54" y="431.80" size="1.27" layer="95" />
                <pinref part="Q16" gate="G$1" pin="E" />
              </segment>
            </net>
            <net name="N$167">
              <segment>
                <wire x1="287.02" y1="335.28" x2="287.02" y2="332.74" width="0.3" layer="91" />
                <label x="287.02" y="332.74" size="1.27" layer="95" />
                <pinref part="C148" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="317.50" y1="414.02" x2="317.50" y2="416.56" width="0.3" layer="91" />
                <label x="317.50" y="416.56" size="1.27" layer="95" />
                <pinref part="C8" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="317.50" y1="396.24" x2="317.50" y2="398.78" width="0.3" layer="91" />
                <label x="317.50" y="398.78" size="1.27" layer="95" />
                <pinref part="C20" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="309.88" y1="396.24" x2="309.88" y2="398.78" width="0.3" layer="91" />
                <label x="309.88" y="398.78" size="1.27" layer="95" />
                <pinref part="C149" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="309.88" y1="378.46" x2="309.88" y2="381.00" width="0.3" layer="91" />
                <label x="309.88" y="381.00" size="1.27" layer="95" />
                <pinref part="C189" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="302.26" y1="378.46" x2="302.26" y2="381.00" width="0.3" layer="91" />
                <label x="302.26" y="381.00" size="1.27" layer="95" />
                <pinref part="C229" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="279.40" y1="312.42" x2="279.40" y2="309.88" width="0.3" layer="91" />
                <label x="279.40" y="309.88" size="1.27" layer="95" />
                <pinref part="C228" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="287.02" y1="317.50" x2="287.02" y2="314.96" width="0.3" layer="91" />
                <label x="287.02" y="314.96" size="1.27" layer="95" />
                <pinref part="C188" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="294.64" y1="335.28" x2="294.64" y2="332.74" width="0.3" layer="91" />
                <label x="294.64" y="332.74" size="1.27" layer="95" />
                <pinref part="C19" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="294.64" y1="353.06" x2="294.64" y2="350.52" width="0.3" layer="91" />
                <label x="294.64" y="350.52" size="1.27" layer="95" />
                <pinref part="C7" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$168">
              <segment>
                <wire x1="398.78" y1="472.44" x2="398.78" y2="469.90" width="0.3" layer="91" />
                <label x="398.78" y="469.90" size="1.27" layer="95" />
                <pinref part="C40" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="398.78" y1="497.84" x2="398.78" y2="500.38" width="0.3" layer="91" />
                <label x="398.78" y="500.38" size="1.27" layer="95" />
                <pinref part="C42" gate="G$1" pin="+" />
              </segment>
            </net>
            <net name="N$169">
              <segment>
                <wire x1="368.30" y1="462.28" x2="368.30" y2="464.82" width="0.3" layer="91" />
                <label x="368.30" y="464.82" size="1.27" layer="95" />
                <pinref part="C41" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="365.76" y1="439.42" x2="363.22" y2="439.42" width="0.3" layer="91" />
                <label x="363.22" y="439.42" size="1.27" layer="95" />
                <pinref part="Q16" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="360.68" y1="424.18" x2="363.22" y2="424.18" width="0.3" layer="91" />
                <label x="363.22" y="424.18" size="1.27" layer="95" />
                <pinref part="R108" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$170">
              <segment>
                <wire x1="368.30" y1="454.66" x2="368.30" y2="452.12" width="0.3" layer="91" />
                <label x="368.30" y="452.12" size="1.27" layer="95" />
                <pinref part="C41" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="375.92" y1="462.28" x2="375.92" y2="464.82" width="0.3" layer="91" />
                <label x="375.92" y="464.82" size="1.27" layer="95" />
                <pinref part="C43" gate="G$1" pin="+" />
              </segment>
            </net>
            <net name="N$171">
              <segment>
                <wire x1="375.92" y1="454.66" x2="375.92" y2="452.12" width="0.3" layer="91" />
                <label x="375.92" y="452.12" size="1.27" layer="95" />
                <pinref part="C43" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="373.38" y1="482.60" x2="370.84" y2="482.60" width="0.3" layer="91" />
                <label x="370.84" y="482.60" size="1.27" layer="95" />
                <pinref part="Q17" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="360.68" y1="431.80" x2="363.22" y2="431.80" width="0.3" layer="91" />
                <label x="363.22" y="431.80" size="1.27" layer="95" />
                <pinref part="R109" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$172">
              <segment>
                <wire x1="342.90" y1="424.18" x2="345.44" y2="424.18" width="0.3" layer="91" />
                <label x="345.44" y="424.18" size="1.27" layer="95" />
                <pinref part="D17" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="350.52" y1="424.18" x2="347.98" y2="424.18" width="0.3" layer="91" />
                <label x="347.98" y="424.18" size="1.27" layer="95" />
                <pinref part="R108" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="381.00" y1="447.04" x2="383.54" y2="447.04" width="0.3" layer="91" />
                <label x="383.54" y="447.04" size="1.27" layer="95" />
                <pinref part="Q16" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="335.28" y1="408.94" x2="337.82" y2="408.94" width="0.3" layer="91" />
                <label x="337.82" y="408.94" size="1.27" layer="95" />
                <pinref part="R110" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$173">
              <segment>
                <wire x1="325.12" y1="416.56" x2="322.58" y2="416.56" width="0.3" layer="91" />
                <label x="322.58" y="416.56" size="1.27" layer="95" />
                <pinref part="D18" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="388.62" y1="490.22" x2="391.16" y2="490.22" width="0.3" layer="91" />
                <label x="391.16" y="490.22" size="1.27" layer="95" />
                <pinref part="Q17" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="350.52" y1="431.80" x2="347.98" y2="431.80" width="0.3" layer="91" />
                <label x="347.98" y="431.80" size="1.27" layer="95" />
                <pinref part="R109" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="337.82" y1="416.56" x2="335.28" y2="416.56" width="0.3" layer="91" />
                <label x="335.28" y="416.56" size="1.27" layer="95" />
                <pinref part="R111" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$174">
              <segment>
                <wire x1="185.42" y1="208.28" x2="185.42" y2="210.82" width="0.3" layer="91" />
                <label x="185.42" y="210.82" size="1.27" layer="95" />
                <pinref part="C1$1" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="215.90" y1="215.90" x2="215.90" y2="213.36" width="0.3" layer="91" />
                <label x="215.90" y="213.36" size="1.27" layer="95" />
                <pinref part="U22" gate="G$1" pin="ADJ" />
              </segment>
              <segment>
                <wire x1="264.16" y1="254.00" x2="266.70" y2="254.00" width="0.3" layer="91" />
                <label x="266.70" y="254.00" size="1.27" layer="95" />
                <pinref part="R1$3" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="185.42" y1="218.44" x2="182.88" y2="218.44" width="0.3" layer="91" />
                <label x="182.88" y="218.44" size="1.27" layer="95" />
                <pinref part="R3$1" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$175">
              <segment>
                <wire x1="220.98" y1="236.22" x2="220.98" y2="233.68" width="0.3" layer="91" />
                <label x="220.98" y="233.68" size="1.27" layer="95" />
                <pinref part="C2$1" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="254.00" y1="261.62" x2="251.46" y2="261.62" width="0.3" layer="91" />
                <label x="251.46" y="261.62" size="1.27" layer="95" />
                <pinref part="R2$3" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="233.68" y1="251.46" x2="233.68" y2="248.92" width="0.3" layer="91" />
                <label x="233.68" y="248.92" size="1.27" layer="95" />
                <pinref part="U24" gate="G$1" pin="ADJ" />
              </segment>
              <segment>
                <wire x1="213.36" y1="236.22" x2="215.90" y2="236.22" width="0.3" layer="91" />
                <label x="215.90" y="236.22" size="1.27" layer="95" />
                <pinref part="R4$1" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$176">
              <segment>
                <wire x1="142.24" y1="172.72" x2="142.24" y2="175.26" width="0.3" layer="91" />
                <label x="142.24" y="175.26" size="1.27" layer="95" />
                <pinref part="C351" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="203.20" y1="223.52" x2="200.66" y2="223.52" width="0.3" layer="91" />
                <label x="200.66" y="223.52" size="1.27" layer="95" />
                <pinref part="U22" gate="G$1" pin="VIN" />
              </segment>
              <segment>
                <wire x1="154.94" y1="157.48" x2="154.94" y2="160.02" width="0.3" layer="91" />
                <label x="154.94" y="160.02" size="1.27" layer="95" />
                <pinref part="W02" gate="G$1" pin="+" />
              </segment>
            </net>
            <net name="N$177">
              <segment>
                <wire x1="167.64" y1="182.88" x2="167.64" y2="180.34" width="0.3" layer="91" />
                <label x="167.64" y="180.34" size="1.27" layer="95" />
                <pinref part="C352" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="220.98" y1="259.08" x2="218.44" y2="259.08" width="0.3" layer="91" />
                <label x="218.44" y="259.08" size="1.27" layer="95" />
                <pinref part="U24" gate="G$1" pin="IN" />
              </segment>
              <segment>
                <wire x1="154.94" y1="132.08" x2="154.94" y2="129.54" width="0.3" layer="91" />
                <label x="154.94" y="129.54" size="1.27" layer="95" />
                <pinref part="W02" gate="G$1" pin="-" />
              </segment>
            </net>
            <net name="N$178">
              <segment>
                <wire x1="167.64" y1="144.78" x2="170.18" y2="144.78" width="0.3" layer="91" />
                <label x="170.18" y="144.78" size="1.27" layer="95" />
                <pinref part="W02" gate="G$1" pin="~2" />
              </segment>
              <segment>
                <wire x1="91.44" y1="101.60" x2="93.98" y2="101.60" width="0.3" layer="91" />
                <label x="93.98" y="101.60" size="1.27" layer="95" />
                <pinref part="J1" gate="G$1" pin="SLEEVE" />
              </segment>
            </net>
            <net name="N$179">
              <segment>
                <wire x1="142.24" y1="144.78" x2="139.70" y2="144.78" width="0.3" layer="91" />
                <label x="139.70" y="144.78" size="1.27" layer="95" />
                <pinref part="W02" gate="G$1" pin="~1" />
              </segment>
              <segment>
                <wire x1="91.44" y1="96.52" x2="93.98" y2="96.52" width="0.3" layer="91" />
                <label x="93.98" y="96.52" size="1.27" layer="95" />
                <pinref part="J1" gate="G$1" pin="SHUNT" />
              </segment>
              <segment>
                <wire x1="91.44" y1="93.98" x2="93.98" y2="93.98" width="0.3" layer="91" />
                <label x="93.98" y="93.98" size="1.27" layer="95" />
                <pinref part="J1" gate="G$1" pin="TIP" />
              </segment>
            </net>
            <net name="N$180">
              <segment>
                <wire x1="233.68" y1="375.92" x2="233.68" y2="373.38" width="0.3" layer="91" />
                <label x="233.68" y="373.38" size="1.27" layer="95" />
                <pinref part="C241" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="241.30" y1="393.70" x2="238.76" y2="393.70" width="0.3" layer="91" />
                <label x="238.76" y="393.70" size="1.27" layer="95" />
                <pinref part="R424" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="259.08" y1="393.70" x2="256.54" y2="393.70" width="0.3" layer="91" />
                <label x="256.54" y="393.70" size="1.27" layer="95" />
                <pinref part="R425" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="254.00" y1="373.38" x2="254.00" y2="370.84" width="0.3" layer="91" />
                <label x="254.00" y="370.84" size="1.27" layer="95" />
                <pinref part="U70" gate="G$1" pin="ADJ" />
              </segment>
            </net>
            <net name="N$181">
              <segment>
                <wire x1="157.48" y1="314.96" x2="157.48" y2="317.50" width="0.3" layer="91" />
                <label x="157.48" y="317.50" size="1.27" layer="95" />
                <pinref part="C51" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="177.80" y1="322.58" x2="177.80" y2="320.04" width="0.3" layer="91" />
                <label x="177.80" y="320.04" size="1.27" layer="95" />
                <pinref part="U21" gate="G$1" pin="ADJ" />
              </segment>
              <segment>
                <wire x1="208.28" y1="350.52" x2="210.82" y2="350.52" width="0.3" layer="91" />
                <label x="210.82" y="350.52" size="1.27" layer="95" />
                <pinref part="R142" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="198.12" y1="342.90" x2="195.58" y2="342.90" width="0.3" layer="91" />
                <label x="195.58" y="342.90" size="1.27" layer="95" />
                <pinref part="R141" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$182">
              <segment>
                <wire x1="157.48" y1="325.12" x2="157.48" y2="322.58" width="0.3" layer="91" />
                <label x="157.48" y="322.58" size="1.27" layer="95" />
                <pinref part="C52" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="177.80" y1="340.36" x2="177.80" y2="337.82" width="0.3" layer="91" />
                <label x="177.80" y="337.82" size="1.27" layer="95" />
                <pinref part="U23" gate="G$1" pin="ADJ" />
              </segment>
              <segment>
                <wire x1="226.06" y1="350.52" x2="228.60" y2="350.52" width="0.3" layer="91" />
                <label x="228.60" y="350.52" size="1.27" layer="95" />
                <pinref part="R138" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="215.90" y1="358.14" x2="213.36" y2="358.14" width="0.3" layer="91" />
                <label x="213.36" y="358.14" size="1.27" layer="95" />
                <pinref part="R137" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$183">
              <segment>
                <wire x1="226.06" y1="358.14" x2="228.60" y2="358.14" width="0.3" layer="91" />
                <label x="228.60" y="358.14" size="1.27" layer="95" />
                <pinref part="R137" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="190.50" y1="347.98" x2="193.04" y2="347.98" width="0.3" layer="91" />
                <label x="193.04" y="347.98" size="1.27" layer="95" />
                <pinref part="U23" gate="G$1" pin="OUT" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1567.18" x2="1442.72" y2="1567.18" width="0.3" layer="91" />
                <label x="1442.72" y="1567.18" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-9" />
              </segment>
              <segment>
                <wire x1="241.30" y1="381.00" x2="238.76" y2="381.00" width="0.3" layer="91" />
                <label x="238.76" y="381.00" size="1.27" layer="95" />
                <pinref part="U70" gate="G$1" pin="IN" />
              </segment>
            </net>
            <net name="N$184">
              <segment>
                <wire x1="208.28" y1="342.90" x2="210.82" y2="342.90" width="0.3" layer="91" />
                <label x="210.82" y="342.90" size="1.27" layer="95" />
                <pinref part="R141" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="190.50" y1="330.20" x2="193.04" y2="330.20" width="0.3" layer="91" />
                <label x="193.04" y="330.20" size="1.27" layer="95" />
                <pinref part="U21" gate="G$1" pin="VOUT" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1579.88" x2="1442.72" y2="1579.88" width="0.3" layer="91" />
                <label x="1442.72" y="1579.88" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-4" />
              </segment>
            </net>
            <net name="N$185">
              <segment>
                <wire x1="269.24" y1="393.70" x2="271.78" y2="393.70" width="0.3" layer="91" />
                <label x="271.78" y="393.70" size="1.27" layer="95" />
                <pinref part="R425" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="266.70" y1="381.00" x2="269.24" y2="381.00" width="0.3" layer="91" />
                <label x="269.24" y="381.00" size="1.27" layer="95" />
                <pinref part="U70" gate="G$1" pin="OUT" />
              </segment>
              <segment>
                <wire x1="1445.26" y1="1574.80" x2="1442.72" y2="1574.80" width="0.3" layer="91" />
                <label x="1442.72" y="1574.80" size="1.27" layer="95" />
                <pinref part="jBottom" gate="G$1" pin="1-6" />
              </segment>
            </net>
          </nets>
        </sheet>
      </sheets>
      <errors />
    </schematic>
  </drawing>
  <compatibility />
</eagle>
