<?xml version="1.0"?>
<eagle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="6.5.0" xmlns="eagle">
  <compatibility />
  <drawing>
    <settings>
      <setting alwaysvectorfont="no" />
      <setting />
    </settings>
    <grid distance="0.01" unitdist="inch" unit="inch" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch" />
    <layers>
      <layer number="1" name="Top" color="4" fill="1" visible="no" active="no" />
      <layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no" />
      <layer number="17" name="Pads" color="2" fill="1" visible="no" active="no" />
      <layer number="18" name="Vias" color="2" fill="1" visible="no" active="no" />
      <layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no" />
      <layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no" />
      <layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="25" name="tNames" color="7" fill="1" visible="no" active="no" />
      <layer number="26" name="bNames" color="7" fill="1" visible="no" active="no" />
      <layer number="27" name="tValues" color="7" fill="1" visible="no" active="no" />
      <layer number="28" name="bValues" color="7" fill="1" visible="no" active="no" />
      <layer number="29" name="tStop" color="7" fill="3" visible="no" active="no" />
      <layer number="30" name="bStop" color="7" fill="6" visible="no" active="no" />
      <layer number="31" name="tCream" color="7" fill="4" visible="no" active="no" />
      <layer number="32" name="bCream" color="7" fill="5" visible="no" active="no" />
      <layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no" />
      <layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no" />
      <layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no" />
      <layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no" />
      <layer number="37" name="tTest" color="7" fill="1" visible="no" active="no" />
      <layer number="38" name="bTest" color="7" fill="1" visible="no" active="no" />
      <layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no" />
      <layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no" />
      <layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no" />
      <layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no" />
      <layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no" />
      <layer number="44" name="Drills" color="7" fill="1" visible="no" active="no" />
      <layer number="45" name="Holes" color="7" fill="1" visible="no" active="no" />
      <layer number="46" name="Milling" color="3" fill="1" visible="no" active="no" />
      <layer number="47" name="Measures" color="7" fill="1" visible="no" active="no" />
      <layer number="48" name="Document" color="7" fill="1" visible="no" active="no" />
      <layer number="49" name="Reference" color="7" fill="1" visible="no" active="no" />
      <layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="91" name="Nets" color="2" fill="1" />
      <layer number="92" name="Busses" color="1" fill="1" />
      <layer number="93" name="Pins" color="2" fill="1" visible="no" />
      <layer number="94" name="Symbols" color="4" fill="1" />
      <layer number="95" name="Names" color="7" fill="1" />
      <layer number="96" name="Values" color="7" fill="1" />
      <layer number="97" name="Info" color="7" fill="1" />
      <layer number="98" name="Guide" color="6" fill="1" />
    </layers>
    <schematic xrefpart="/%S.%C%R" xreflabel="%F%N/%S.%C%R">
      <description />
      <libraries>
        <library name="mlcc">
          <description />
          <packages>
            <package name="C_0402">
              <description>&lt;B&gt; 0402&lt;/B&gt; (1005 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.0762" layer="51" />
              <wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.0762" layer="51" />
              <smd name="1" x="-0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <smd name="2" x="0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <text x="-1" y="0.6" size="0.508" layer="51" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0603">
              <description>&lt;B&gt; 0603&lt;/B&gt; (1608 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="-0.5334" x2="0.1016" y2="-0.5334" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="0.5334" x2="0.1016" y2="0.5334" width="0.1524" layer="21" />
              <smd name="1" x="-0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <smd name="2" x="0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <text x="-1.6" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0805">
              <description>&lt;B&gt; 0805&lt;/B&gt; (2012 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.0762" layer="51" />
              <wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.0762" layer="51" />
              <wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.0762" layer="51" />
              <wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="0.7112" x2="0.1016" y2="0.7112" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="-0.7112" x2="0.1016" y2="-0.7112" width="0.1524" layer="21" />
              <smd name="1" x="-1" y="0" dx="1.35" dy="1.55" layer="1" />
              <smd name="2" x="1" y="0" dx="1.35" dy="1.55" layer="1" />
              <text x="-1.8" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="CAP_NP">
              <description>&lt;B&gt;Capacitor&lt;/B&gt; -- non-polarized</description>
              <wire x1="-1.905" y1="-3.175" x2="0" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="0" y1="-3.175" x2="1.905" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="-1.905" y1="-4.445" x2="0" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-4.445" x2="1.905" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-2.54" x2="0" y2="-3.175" width="0.254" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-4.445" width="0.254" layer="94" />
              <pin name="P$1" x="0" y="0" visible="off" length="short" direction="pas" rot="R270" />
              <pin name="P$2" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90" />
              <text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
              <text x="-5.08" y="-7.62" size="1.778" layer="95" rot="R90">&gt;NAME</text>
              <text x="0.508" y="-2.286" size="1.778" layer="95">1</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="C" name="C_0402">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0402">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0603">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0805">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0805">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="General_Will">
          <description />
          <packages>
            <package name="TO-92">
              <description>&lt;b&gt;TO 92&lt;/b&gt;</description>
              <wire x1="-1.0403" y1="2.4215" x2="-2.0946" y2="-1.651" width="0.1524" layer="21" curve="111.098962" />
              <wire x1="2.0945" y1="-1.651" x2="1.0403" y2="2.421396875" width="0.1524" layer="21" curve="111.099507" />
              <wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.1524" layer="21" />
              <wire x1="-2.6549" y1="-0.254" x2="-2.3807" y2="-0.254" width="0.1524" layer="21" />
              <wire x1="-0.1593" y1="-0.254" x2="0.1593" y2="-0.254" width="0.1524" layer="21" />
              <wire x1="2.3807" y1="-0.254" x2="2.6549" y2="-0.254" width="0.1524" layer="21" />
              <pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796" />
              <pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796" />
              <pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796" />
              <text x="-2.159" y="-2.794" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="8-SOP">
              <description />
              <smd name="P$1" x="-2.75" y="1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$2" x="-2.75" y="0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$3" x="-2.75" y="-0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$4" x="-2.75" y="-1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$5" x="2.75" y="-1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$6" x="2.75" y="-0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$7" x="2.75" y="0.635" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <smd name="P$8" x="2.75" y="1.905" dx="0.55" dy="1.5" layer="1" rot="R90" />
              <wire x1="-2.65" y1="2.65" x2="2.65" y2="2.65" width="0.127" layer="21" />
              <wire x1="-2.65" y1="-2.65" x2="2.65" y2="-2.65" width="0.127" layer="21" />
              <circle x="-3.99" y="2.31" radius="0.1542" width="0" layer="21" />
              <text x="-3.36" y="2.94" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="R-PDSO-G14">
              <description />
              <smd name="P$1" x="-2.7" y="3.81" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$2" x="-2.7" y="2.54" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$3" x="-2.7" y="1.27" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$4" x="-2.7" y="0" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$5" x="-2.7" y="-1.27" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$6" x="-2.7" y="-2.54" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$7" x="-2.7" y="-3.81" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$8" x="2.7" y="-3.81" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$9" x="2.7" y="-2.54" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$10" x="2.7" y="-1.27" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$11" x="2.7" y="0" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$12" x="2.7" y="1.27" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$13" x="2.7" y="2.54" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <smd name="P$14" x="2.7" y="3.81" dx="1.55" dy="0.6" layer="1" rot="R180" />
              <wire x1="-2.2" y1="4.4" x2="2.2" y2="4.4" width="0.127" layer="21" />
              <wire x1="-2.2" y1="-4.4" x2="2.2" y2="-4.4" width="0.127" layer="21" />
              <circle x="-3.81" y="4.445" radius="0.1542" width="0" layer="21" />
              <text x="-3.6" y="4.8" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="R-PDSO-G8">
              <description />
              <smd name="P$1" x="-2.7" y="1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$2" x="-2.7" y="0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$3" x="-2.7" y="-0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$4" x="-2.7" y="-1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$5" x="2.7" y="-1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$6" x="2.7" y="-0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$7" x="2.7" y="0.635" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <smd name="P$8" x="2.7" y="1.905" dx="0.6" dy="1.3" layer="1" rot="R90" />
              <wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.127" layer="21" />
              <wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21" />
              <circle x="-3.8" y="2.4" radius="0.1542" width="0" layer="21" />
              <text x="-3.6" y="2.8" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="TRIMPOT">
              <description />
              <pad name="P$1" x="-2.5" y="0" drill="0.8" />
              <pad name="P$2" x="0" y="2.5" drill="0.8" />
              <pad name="P$3" x="2.5" y="0" drill="0.8" />
              <wire x1="-3.6" y1="3.6" x2="-3.6" y2="-3.6" width="0.127" layer="21" />
              <wire x1="-3.6" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21" />
              <wire x1="3.6" y1="-3.6" x2="3.6" y2="3.6" width="0.127" layer="21" />
              <wire x1="3.6" y1="3.6" x2="-3.6" y2="3.6" width="0.127" layer="21" />
              <circle x="-4.445" y="1.27" radius="0.1542" width="0" layer="21" />
              <text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
            </package>
            <package name="SOT363">
              <description />
              <circle x="-1.505" y="1.12" radius="0.1542" width="0" layer="21" />
              <text x="-1.45" y="1.35" size="1.106" layer="25" font="vector" ratio="15" distance="40">&gt;NAME</text>
              <smd name="P$1" x="-0.75" y="0.75" dx="0.5" dy="0.5" layer="1" />
              <smd name="P$2" x="-0.75" y="0" dx="0.5" dy="0.4" layer="1" />
              <smd name="P$3" x="-0.75" y="-0.75" dx="0.5" dy="0.5" layer="1" />
              <smd name="P$4" x="0.75" y="-0.75" dx="0.5" dy="0.5" layer="1" />
              <smd name="P$5" x="0.75" y="0" dx="0.5" dy="0.4" layer="1" />
              <smd name="P$6" x="0.75" y="0.75" dx="0.5" dy="0.5" layer="1" />
              <wire x1="-0.275" y1="1.175" x2="0.275" y2="1.175" width="0.127" layer="21" />
              <wire x1="-0.275" y1="-1.175" x2="0.275" y2="-1.175" width="0.127" layer="21" />
            </package>
            <package name="3X12TERMINAL">
              <description />
              <pad name="1-1" x="0" y="0" drill="1.8" shape="square" />
              <pad name="2-1" x="-1.9" y="10.16" drill="1.8" />
              <pad name="3-1" x="0" y="19.05" drill="1.8" />
              <wire x1="-1.9" y1="-3.6" x2="47.62" y2="-3.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="-3.6" x2="47.62" y2="3.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="3.6" x2="45.72" y2="3.6" width="0.1524" layer="21" />
              <wire x1="45.72" y1="3.6" x2="45.72" y2="14.4" width="0.1524" layer="21" />
              <wire x1="45.72" y1="14.4" x2="47.62" y2="14.4" width="0.1524" layer="21" />
              <wire x1="47.62" y1="14.4" x2="47.62" y2="21.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="21.6" x2="-1.9" y2="21.6" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="21.6" x2="-1.9" y2="14.4" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="14.4" x2="-3.8" y2="14.4" width="0.1524" layer="21" />
              <wire x1="-3.8" y1="14.4" x2="-3.8" y2="3.6" width="0.1524" layer="21" />
              <wire x1="-3.8" y1="3.6" x2="-1.9" y2="3.6" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="3.6" x2="-1.9" y2="-3.6" width="0.1524" layer="21" />
              <text x="-5.08" y="4.445" size="1.016" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
              <pad name="1-2" x="3.81" y="0" drill="1.8" />
              <pad name="1-3" x="7.62" y="0" drill="1.8" />
              <pad name="1-4" x="11.43" y="0" drill="1.8" />
              <pad name="1-5" x="15.24" y="0" drill="1.8" />
              <pad name="1-6" x="19.05" y="0" drill="1.8" />
              <pad name="1-7" x="22.86" y="0" drill="1.8" />
              <pad name="1-8" x="26.67" y="0" drill="1.8" />
              <pad name="1-9" x="30.48" y="0" drill="1.8" />
              <pad name="1-10" x="34.29" y="0" drill="1.8" />
              <pad name="1-11" x="38.1" y="0" drill="1.8" />
              <pad name="1-12" x="41.91" y="0" drill="1.8" />
              <pad name="2-2" x="1.91" y="10.16" drill="1.8" />
              <pad name="2-3" x="5.72" y="10.16" drill="1.8" />
              <pad name="2-4" x="9.53" y="10.16" drill="1.8" />
              <pad name="2-5" x="13.34" y="10.16" drill="1.8" />
              <pad name="2-6" x="17.15" y="10.16" drill="1.8" />
              <pad name="2-7" x="20.96" y="10.16" drill="1.8" />
              <pad name="2-8" x="24.77" y="10.16" drill="1.8" />
              <pad name="2-9" x="28.58" y="10.16" drill="1.8" />
              <pad name="2-10" x="32.39" y="10.16" drill="1.8" />
              <pad name="2-11" x="36.2" y="10.16" drill="1.8" />
              <pad name="2-12" x="40.01" y="10.16" drill="1.8" />
              <pad name="3-2" x="3.81" y="19.05" drill="1.8" />
              <pad name="3-3" x="7.62" y="19.05" drill="1.8" />
              <pad name="3-4" x="11.43" y="19.05" drill="1.8" />
              <pad name="3-5" x="15.24" y="19.05" drill="1.8" />
              <pad name="3-6" x="19.05" y="19.05" drill="1.8" />
              <pad name="3-7" x="22.86" y="19.05" drill="1.8" />
              <pad name="3-8" x="26.67" y="19.05" drill="1.8" />
              <pad name="3-9" x="30.48" y="19.05" drill="1.8" />
              <pad name="3-10" x="34.29" y="19.05" drill="1.8" />
              <pad name="3-11" x="38.1" y="19.05" drill="1.8" />
              <pad name="3-12" x="41.91" y="19.05" drill="1.8" />
            </package>
          </packages>
          <symbols>
            <symbol name="NPN">
              <description />
              <pin name="B" x="-10.16" y="0" length="middle" direction="pas" />
              <wire x1="-5.08" y1="5.08" x2="-5.08" y2="2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="2.54" x2="0" y2="7.62" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="0" y2="-7.62" width="0.254" layer="94" />
              <wire x1="0" y1="-7.62" x2="-1.016" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-1.016" y1="-5.08" x2="-2.54" y2="-6.604" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-6.604" x2="0" y2="-7.62" width="0.254" layer="94" />
              <pin name="C" x="5.08" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="E" x="5.08" y="-7.62" length="middle" direction="pas" rot="R180" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="DUALOPAMP">
              <description />
              <pin name="OUT1" x="-15.24" y="7.62" length="middle" />
              <pin name="I1-" x="-15.24" y="2.54" length="middle" />
              <pin name="I1+" x="-15.24" y="-2.54" length="middle" />
              <pin name="V-" x="-15.24" y="-7.62" length="middle" />
              <pin name="I2+" x="15.24" y="-7.62" length="middle" rot="R180" />
              <pin name="I2-" x="15.24" y="-2.54" length="middle" rot="R180" />
              <pin name="OUT2" x="15.24" y="2.54" length="middle" rot="R180" />
              <pin name="V+" x="15.24" y="7.62" length="middle" rot="R180" />
              <wire x1="-10.16" y1="10.16" x2="-10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="-10.16" y1="7.62" x2="-10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="-10.16" y1="2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-2.54" x2="-10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="-10.16" x2="10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="10.16" y1="2.54" x2="10.16" y2="10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94" />
              <wire x1="-7.62" y1="2.54" x2="-5.08" y2="7.62" width="0.254" layer="94" />
              <wire x1="-5.08" y1="7.62" x2="-2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="2.54" x2="-4.318" y2="2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="2.54" x2="-6.096" y2="2.54" width="0.254" layer="94" />
              <wire x1="-6.096" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94" />
              <wire x1="5.08" y1="0" x2="7.62" y2="-5.08" width="0.254" layer="94" />
              <wire x1="7.62" y1="-5.08" x2="6.096" y2="-5.08" width="0.254" layer="94" />
              <text x="-6.35" y="3.048" size="1.27" layer="94">-</text>
              <text x="5.842" y="-4.826" size="1.27" layer="94">-</text>
              <text x="-4.826" y="3.048" size="1.27" layer="94">+</text>
              <text x="3.556" y="-5.08" size="1.27" layer="94">+</text>
              <wire x1="6.096" y1="-5.08" x2="4.064" y2="-5.08" width="0.254" layer="94" />
              <wire x1="4.064" y1="-5.08" x2="2.54" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="7.62" x2="-5.08" y2="8.89" width="0.254" layer="94" />
              <wire x1="-5.08" y1="8.89" x2="-7.62" y2="8.89" width="0.254" layer="94" />
              <wire x1="-7.62" y1="8.89" x2="-7.62" y2="7.62" width="0.254" layer="94" />
              <wire x1="-7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="-6.096" y1="2.54" x2="-6.096" y2="1.524" width="0.254" layer="94" />
              <wire x1="-6.096" y1="1.524" x2="-8.89" y2="1.524" width="0.254" layer="94" />
              <wire x1="-8.89" y1="1.524" x2="-8.89" y2="2.54" width="0.254" layer="94" />
              <wire x1="-8.89" y1="2.54" x2="-10.16" y2="2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="2.54" x2="-4.318" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="-2.54" x2="-10.16" y2="-2.54" width="0.254" layer="94" />
              <wire x1="4.064" y1="-5.08" x2="4.064" y2="-7.62" width="0.254" layer="94" />
              <wire x1="4.064" y1="-7.62" x2="9.906" y2="-7.62" width="0.254" layer="94" />
              <wire x1="6.096" y1="-5.08" x2="6.096" y2="-6.35" width="0.254" layer="94" />
              <wire x1="6.096" y1="-6.35" x2="8.89" y2="-6.35" width="0.254" layer="94" />
              <wire x1="8.89" y1="-6.35" x2="8.89" y2="-2.54" width="0.254" layer="94" />
              <wire x1="8.89" y1="-2.54" x2="10.414" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.27" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="CD4066">
              <description />
              <pin name="AIN" x="-15.24" y="7.62" length="middle" />
              <pin name="AOUT" x="-15.24" y="5.08" length="middle" />
              <pin name="BOUT" x="-15.24" y="2.54" length="middle" />
              <pin name="BIN" x="-15.24" y="0" length="middle" />
              <pin name="CTRLB" x="-15.24" y="-2.54" length="middle" />
              <pin name="CTRLC" x="-15.24" y="-5.08" length="middle" />
              <pin name="VSS" x="-15.24" y="-7.62" length="middle" />
              <pin name="CIN" x="15.24" y="-7.62" length="middle" rot="R180" />
              <pin name="COUT" x="15.24" y="-5.08" length="middle" rot="R180" />
              <pin name="DOUT" x="15.24" y="-2.54" length="middle" rot="R180" />
              <pin name="DIN" x="15.24" y="0" length="middle" rot="R180" />
              <pin name="CTRLD" x="15.24" y="2.54" length="middle" rot="R180" />
              <pin name="CTRLA" x="15.24" y="5.08" length="middle" rot="R180" />
              <pin name="VDD" x="15.24" y="7.62" length="middle" rot="R180" />
              <wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94" />
              <wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94" />
              <text x="-10.16" y="12.7" size="1.27" layer="95">&gt;NAME</text>
              <text x="-10.16" y="10.16" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="NFET">
              <description />
              <wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" />
              <wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="-2.54" x2="2.54" y2="-7.62" width="0.254" layer="94" />
              <wire x1="2.54" y1="2.54" x2="2.54" y2="7.62" width="0.254" layer="94" />
              <pin name="G" x="-7.62" y="0" length="middle" direction="pas" />
              <pin name="D" x="2.54" y="12.7" length="middle" direction="pas" rot="R270" />
              <pin name="S" x="2.54" y="-12.7" length="middle" direction="pas" rot="R90" />
              <text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
              <text x="-7.62" y="7.62" size="1.778" layer="95">&gt;VALUE</text>
              <wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94" />
            </symbol>
            <symbol name="TRIMPOT">
              <description />
              <pin name="P$1" x="-12.7" y="0" length="middle" />
              <pin name="P$2" x="12.7" y="0" length="middle" rot="R180" />
              <pin name="W" x="1.27" y="12.7" length="middle" rot="R270" />
              <wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="-3.81" y2="2.54" width="0.254" layer="94" />
              <wire x1="-3.81" y1="2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.254" layer="94" />
              <wire x1="1.27" y1="2.54" x2="3.81" y2="-2.54" width="0.254" layer="94" />
              <wire x1="3.81" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94" />
              <wire x1="6.35" y1="2.54" x2="7.62" y2="0" width="0.254" layer="94" />
              <wire x1="1.27" y1="7.62" x2="1.27" y2="3.302" width="0.254" layer="94" />
              <wire x1="1.27" y1="3.302" x2="0.762" y2="4.064" width="0.254" layer="94" />
              <wire x1="0.762" y1="4.064" x2="1.778" y2="4.064" width="0.254" layer="94" />
              <wire x1="1.778" y1="4.064" x2="1.27" y2="3.302" width="0.254" layer="94" />
              <text x="-10.16" y="7.62" size="1.27" layer="95">&gt;NAME</text>
              <text x="-10.16" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="DUALNPN">
              <description />
              <wire x1="-13.97" y1="5.08" x2="-13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-13.97" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-8.89" y2="7.62" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-8.89" y2="-7.62" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
              <pin name="B1" x="19.05" y="0" length="middle" direction="pas" rot="R180" />
              <wire x1="13.97" y1="-5.08" x2="13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="13.97" y2="5.08" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="8.89" y2="-7.62" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="8.89" y2="7.62" width="0.254" layer="94" />
              <pin name="C1" x="3.81" y="-7.62" length="middle" direction="pas" />
              <pin name="E1" x="3.81" y="7.62" length="middle" direction="pas" />
              <wire x1="-8.89" y1="-7.62" x2="-8.89" y2="-6.35" width="0.254" layer="94" />
              <wire x1="-8.89" y1="-6.35" x2="-10.16" y2="-7.62" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-7.62" x2="-8.89" y2="-7.62" width="0.254" layer="94" />
              <wire x1="8.89" y1="7.62" x2="8.89" y2="6.35" width="0.254" layer="94" />
              <wire x1="8.89" y1="6.35" x2="10.16" y2="7.62" width="0.254" layer="94" />
              <wire x1="10.16" y1="7.62" x2="8.89" y2="7.62" width="0.254" layer="94" />
              <pin name="E2" x="-3.81" y="-7.62" length="middle" direction="pas" rot="R180" />
              <pin name="C2" x="-3.81" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="B2" x="-19.05" y="0" length="middle" direction="pas" />
            </symbol>
            <symbol name="DUALPNP">
              <description />
              <wire x1="-13.97" y1="5.08" x2="-13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-13.97" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-13.97" y1="2.54" x2="-8.89" y2="7.62" width="0.254" layer="94" />
              <wire x1="-13.97" y1="-2.54" x2="-8.89" y2="-7.62" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
              <wire x1="-13.97" y1="-2.54" x2="-13.208" y2="-4.572" width="0.254" layer="94" />
              <wire x1="-13.208" y1="-4.572" x2="-11.938" y2="-3.302" width="0.254" layer="94" />
              <wire x1="-11.938" y1="-3.302" x2="-13.97" y2="-2.54" width="0.254" layer="94" />
              <pin name="B1" x="19.05" y="0" length="middle" direction="pas" rot="R180" />
              <wire x1="13.97" y1="-5.08" x2="13.97" y2="-2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="13.97" y2="2.54" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="13.97" y2="5.08" width="0.254" layer="94" />
              <wire x1="13.97" y1="-2.54" x2="8.89" y2="-7.62" width="0.254" layer="94" />
              <wire x1="13.97" y1="2.54" x2="8.89" y2="7.62" width="0.254" layer="94" />
              <pin name="C1" x="3.81" y="-7.62" length="middle" direction="pas" />
              <pin name="E1" x="3.81" y="7.62" length="middle" direction="pas" />
              <wire x1="13.97" y1="2.54" x2="13.208" y2="4.572" width="0.254" layer="94" />
              <wire x1="13.208" y1="4.572" x2="11.938" y2="3.302" width="0.254" layer="94" />
              <wire x1="11.938" y1="3.302" x2="13.97" y2="2.54" width="0.254" layer="94" />
              <pin name="E2" x="-3.81" y="-7.62" length="middle" direction="pas" rot="R180" />
              <pin name="C2" x="-3.81" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="B2" x="-19.05" y="0" length="middle" direction="pas" />
            </symbol>
            <symbol name="PNP">
              <description />
              <pin name="B" x="-10.16" y="2.54" length="middle" direction="pas" />
              <wire x1="-5.08" y1="7.62" x2="-5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="0" y2="10.16" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="0" y2="-5.08" width="0.254" layer="94" />
              <pin name="C" x="5.08" y="10.16" length="middle" direction="pas" rot="R180" />
              <pin name="E" x="5.08" y="-5.08" length="middle" direction="pas" rot="R180" />
              <text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="15.24" size="1.778" layer="96">&gt;VALUE</text>
              <wire x1="-5.08" y1="0" x2="-4.318" y2="-2.032" width="0.254" layer="94" />
              <wire x1="-4.318" y1="-2.032" x2="-3.048" y2="-0.762" width="0.254" layer="94" />
              <wire x1="-3.048" y1="-0.762" x2="-5.08" y2="0" width="0.254" layer="94" />
            </symbol>
            <symbol name="36TERMINALBLOCK">
              <description />
              <pin name="1-1" x="-12.7" y="10.16" length="middle" direction="pas" />
              <pin name="2-1" x="0" y="10.16" length="middle" direction="pas" />
              <pin name="3-1" x="12.7" y="10.16" length="middle" direction="pas" />
              <pin name="1-2" x="-12.7" y="7.62" length="middle" direction="pas" />
              <pin name="2-2" x="0" y="7.62" length="middle" direction="pas" />
              <pin name="3-2" x="12.7" y="7.62" length="middle" direction="pas" />
              <pin name="1-3" x="-12.7" y="5.08" length="middle" direction="pas" />
              <pin name="2-3" x="0" y="5.08" length="middle" direction="pas" />
              <pin name="3-3" x="12.7" y="5.08" length="middle" direction="pas" />
              <pin name="1-4" x="-12.7" y="2.54" length="middle" direction="pas" />
              <pin name="2-4" x="0" y="2.54" length="middle" direction="pas" />
              <pin name="3-4" x="12.7" y="2.54" length="middle" direction="pas" />
              <pin name="1-5" x="-12.7" y="0" length="middle" direction="pas" />
              <pin name="2-5" x="0" y="0" length="middle" direction="pas" />
              <pin name="3-5" x="12.7" y="0" length="middle" direction="pas" />
              <pin name="1-6" x="-12.7" y="-2.54" length="middle" direction="pas" />
              <pin name="2-6" x="0" y="-2.54" length="middle" direction="pas" />
              <pin name="3-6" x="12.7" y="-2.54" length="middle" direction="pas" />
              <pin name="1-7" x="-12.7" y="-5.08" length="middle" direction="pas" />
              <pin name="2-7" x="0" y="-5.08" length="middle" direction="pas" />
              <pin name="3-7" x="12.7" y="-5.08" length="middle" direction="pas" />
              <pin name="1-8" x="-12.7" y="-7.62" length="middle" direction="pas" />
              <pin name="2-8" x="0" y="-7.62" length="middle" direction="pas" />
              <pin name="3-8" x="12.7" y="-7.62" length="middle" direction="pas" />
              <pin name="1-9" x="-12.7" y="-10.16" length="middle" direction="pas" />
              <pin name="2-9" x="0" y="-10.16" length="middle" direction="pas" />
              <pin name="3-9" x="12.7" y="-10.16" length="middle" direction="pas" />
              <pin name="1-10" x="-12.7" y="-12.7" length="middle" direction="pas" />
              <pin name="2-10" x="0" y="-12.7" length="middle" direction="pas" />
              <pin name="3-10" x="12.7" y="-12.7" length="middle" direction="pas" />
              <pin name="1-11" x="-12.7" y="-15.24" length="middle" direction="pas" />
              <pin name="2-11" x="0" y="-15.24" length="middle" direction="pas" />
              <pin name="3-11" x="12.7" y="-15.24" length="middle" direction="pas" />
              <pin name="1-12" x="-12.7" y="-17.78" length="middle" direction="pas" />
              <pin name="2-12" x="0" y="-17.78" length="middle" direction="pas" />
              <pin name="3-12" x="12.7" y="-17.78" length="middle" direction="pas" />
              <wire x1="-10.16" y1="12.7" x2="-10.16" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-20.32" x2="-5.08" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-10.16" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94" />
              <wire x1="2.54" y1="12.7" x2="2.54" y2="-20.32" width="0.254" layer="94" />
              <wire x1="2.54" y1="-20.32" x2="7.62" y2="-20.32" width="0.254" layer="94" />
              <wire x1="2.54" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94" />
              <wire x1="15.24" y1="12.7" x2="15.24" y2="-20.32" width="0.254" layer="94" />
              <wire x1="15.24" y1="-20.32" x2="20.32" y2="-20.32" width="0.254" layer="94" />
              <wire x1="15.24" y1="12.7" x2="20.32" y2="12.7" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset name="KSC1008YBU">
              <description />
              <gates>
                <gate name="G$1" symbol="NPN" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="B" pad="2" />
                    <connect gate="G$1" pin="C" pad="3" />
                    <connect gate="G$1" pin="E" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="UPC4558G2">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALOPAMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="8-SOP">
                  <connects>
                    <connect gate="G$1" pin="I1+" pad="P$3" />
                    <connect gate="G$1" pin="I1-" pad="P$2" />
                    <connect gate="G$1" pin="I2+" pad="P$5" />
                    <connect gate="G$1" pin="I2-" pad="P$6" />
                    <connect gate="G$1" pin="OUT1" pad="P$1" />
                    <connect gate="G$1" pin="OUT2" pad="P$7" />
                    <connect gate="G$1" pin="V+" pad="P$8" />
                    <connect gate="G$1" pin="V-" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="CD4066BM96G4">
              <description />
              <gates>
                <gate name="G$1" symbol="CD4066" x="0" y="0" />
              </gates>
              <devices>
                <device package="R-PDSO-G14">
                  <connects>
                    <connect gate="G$1" pin="AIN" pad="P$1" />
                    <connect gate="G$1" pin="AOUT" pad="P$2" />
                    <connect gate="G$1" pin="BIN" pad="P$4" />
                    <connect gate="G$1" pin="BOUT" pad="P$3" />
                    <connect gate="G$1" pin="CIN" pad="P$8" />
                    <connect gate="G$1" pin="COUT" pad="P$9" />
                    <connect gate="G$1" pin="CTRLA" pad="P$13" />
                    <connect gate="G$1" pin="CTRLB" pad="P$5" />
                    <connect gate="G$1" pin="CTRLC" pad="P$6" />
                    <connect gate="G$1" pin="CTRLD" pad="P$12" />
                    <connect gate="G$1" pin="DIN" pad="P$11" />
                    <connect gate="G$1" pin="DOUT" pad="P$10" />
                    <connect gate="G$1" pin="VDD" pad="P$14" />
                    <connect gate="G$1" pin="VSS" pad="P$7" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="TL082CD">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALOPAMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="R-PDSO-G8">
                  <connects>
                    <connect gate="G$1" pin="I1+" pad="P$3" />
                    <connect gate="G$1" pin="I1-" pad="P$2" />
                    <connect gate="G$1" pin="I2+" pad="P$5" />
                    <connect gate="G$1" pin="I2-" pad="P$6" />
                    <connect gate="G$1" pin="OUT1" pad="P$1" />
                    <connect gate="G$1" pin="OUT2" pad="P$7" />
                    <connect gate="G$1" pin="V+" pad="P$8" />
                    <connect gate="G$1" pin="V-" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="2N3819">
              <description />
              <gates>
                <gate name="G$1" symbol="NFET" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="D" pad="1" />
                    <connect gate="G$1" pin="G" pad="2" />
                    <connect gate="G$1" pin="S" pad="3" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="LF353DR">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALOPAMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="R-PDSO-G8">
                  <connects>
                    <connect gate="G$1" pin="I1+" pad="P$3" />
                    <connect gate="G$1" pin="I1-" pad="P$2" />
                    <connect gate="G$1" pin="I2+" pad="P$5" />
                    <connect gate="G$1" pin="I2-" pad="P$6" />
                    <connect gate="G$1" pin="OUT1" pad="P$1" />
                    <connect gate="G$1" pin="OUT2" pad="P$7" />
                    <connect gate="G$1" pin="V+" pad="P$8" />
                    <connect gate="G$1" pin="V-" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="CT6EP202">
              <description />
              <gates>
                <gate name="G$1" symbol="TRIMPOT" x="-2.54" y="0" />
              </gates>
              <devices>
                <device package="TRIMPOT">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="P$1" />
                    <connect gate="G$1" pin="P$2" pad="P$3" />
                    <connect gate="G$1" pin="W" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PMP4201Y">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALNPN" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOT363">
                  <connects>
                    <connect gate="G$1" pin="B1" pad="P$1" />
                    <connect gate="G$1" pin="B2" pad="P$2" />
                    <connect gate="G$1" pin="C1" pad="P$6" />
                    <connect gate="G$1" pin="C2" pad="P$3" />
                    <connect gate="G$1" pin="E1" pad="P$5" />
                    <connect gate="G$1" pin="E2" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PMP5201Y">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALPNP" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOT363">
                  <connects>
                    <connect gate="G$1" pin="B1" pad="P$1" />
                    <connect gate="G$1" pin="B2" pad="P$2" />
                    <connect gate="G$1" pin="C1" pad="P$6" />
                    <connect gate="G$1" pin="C2" pad="P$3" />
                    <connect gate="G$1" pin="E1" pad="P$5" />
                    <connect gate="G$1" pin="E2" pad="P$4" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PN2907ABU">
              <description />
              <gates>
                <gate name="G$1" symbol="PNP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="B" pad="2" />
                    <connect gate="G$1" pin="C" pad="3" />
                    <connect gate="G$1" pin="E" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="CT6EP104">
              <description />
              <gates>
                <gate name="G$1" symbol="TRIMPOT" x="-2.54" y="0" />
              </gates>
              <devices>
                <device package="TRIMPOT">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="P$1" />
                    <connect gate="G$1" pin="P$2" pad="P$3" />
                    <connect gate="G$1" pin="W" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="1727832">
              <description />
              <gates>
                <gate name="G$1" symbol="36TERMINALBLOCK" x="0" y="0" />
              </gates>
              <devices>
                <device package="3X12TERMINAL">
                  <connects>
                    <connect gate="G$1" pin="1-1" pad="1-1" />
                    <connect gate="G$1" pin="1-10" pad="1-10" />
                    <connect gate="G$1" pin="1-11" pad="1-11" />
                    <connect gate="G$1" pin="1-12" pad="1-12" />
                    <connect gate="G$1" pin="1-2" pad="1-2" />
                    <connect gate="G$1" pin="1-3" pad="1-3" />
                    <connect gate="G$1" pin="1-4" pad="1-4" />
                    <connect gate="G$1" pin="1-5" pad="1-5" />
                    <connect gate="G$1" pin="1-6" pad="1-6" />
                    <connect gate="G$1" pin="1-7" pad="1-7" />
                    <connect gate="G$1" pin="1-8" pad="1-8" />
                    <connect gate="G$1" pin="1-9" pad="1-9" />
                    <connect gate="G$1" pin="2-1" pad="2-1" />
                    <connect gate="G$1" pin="2-10" pad="2-10" />
                    <connect gate="G$1" pin="2-11" pad="2-11" />
                    <connect gate="G$1" pin="2-12" pad="2-12" />
                    <connect gate="G$1" pin="2-2" pad="2-2" />
                    <connect gate="G$1" pin="2-3" pad="2-3" />
                    <connect gate="G$1" pin="2-4" pad="2-4" />
                    <connect gate="G$1" pin="2-5" pad="2-5" />
                    <connect gate="G$1" pin="2-6" pad="2-6" />
                    <connect gate="G$1" pin="2-7" pad="2-7" />
                    <connect gate="G$1" pin="2-8" pad="2-8" />
                    <connect gate="G$1" pin="2-9" pad="2-9" />
                    <connect gate="G$1" pin="3-1" pad="3-1" />
                    <connect gate="G$1" pin="3-10" pad="3-10" />
                    <connect gate="G$1" pin="3-11" pad="3-11" />
                    <connect gate="G$1" pin="3-12" pad="3-12" />
                    <connect gate="G$1" pin="3-2" pad="3-2" />
                    <connect gate="G$1" pin="3-3" pad="3-3" />
                    <connect gate="G$1" pin="3-4" pad="3-4" />
                    <connect gate="G$1" pin="3-5" pad="3-5" />
                    <connect gate="G$1" pin="3-6" pad="3-6" />
                    <connect gate="G$1" pin="3-7" pad="3-7" />
                    <connect gate="G$1" pin="3-8" pad="3-8" />
                    <connect gate="G$1" pin="3-9" pad="3-9" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="resistor">
          <description />
          <packages>
            <package name="R_0603">
              <description>&lt;B&gt;
0603
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-0.1" y1="0.3" x2="0.1" y2="0.3" width="0.1524" layer="21" />
              <wire x1="-0.1" y1="-0.3" x2="0.1" y2="-0.3" width="0.1524" layer="21" />
              <smd name="P$1" x="-0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <smd name="P$2" x="0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <text x="-1.3" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="R">
              <description>&lt;B&gt;Resistor&lt;/B&gt;</description>
              <wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94" />
              <wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94" />
              <wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94" />
              <pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" />
              <pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180" />
              <text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
              <text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="R" name="RESISTOR_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="0" y="0" />
              </gates>
              <devices>
                <device package="R_0603">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="keystone">
          <description />
          <packages>
            <package name="TP-040">
              <description>&lt;b&gt;TEST POINT&lt;/b&gt;&lt;p&gt; 
Series 5000-5004, Hole 0.040"</description>
              <circle x="0" y="0" radius="1.27" width="0.1524" layer="51" />
              <pad name="1" x="0" y="0" drill="1.016" diameter="2.159" shape="octagon" />
              <text x="-1.175421875" y="1.385528125" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <rectangle x1="-0.9525" y1="-0.3175" x2="0.9525" y2="0.3175" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="TP">
              <description />
              <wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94" />
              <wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94" />
              <wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94" />
              <text x="-1.524" y="0.508" size="1.778" layer="95">&gt;NAME</text>
              <pin name="1" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90" />
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="TP" name="TP">
              <description />
              <gates>
                <gate name="G$1" symbol="TP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TP-040" name="-040">
                  <connects>
                    <connect gate="G$1" pin="1" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="ecad(3)">
          <description />
          <packages>
            <package name="14-SOIC">
              <description />
              <smd name="14" x="2.7" y="3.81" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="13" x="2.7" y="2.54" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="12" x="2.7" y="1.27" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="11" x="2.7" y="0" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="10" x="2.7" y="-1.27" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="9" x="2.7" y="-2.54" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="8" x="2.7" y="-3.81" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="1" x="-2.7" y="3.81" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="2" x="-2.7" y="2.54" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="3" x="-2.7" y="1.27" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="4" x="-2.7" y="0" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="5" x="-2.7" y="-1.27" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="6" x="-2.7" y="-2.54" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <smd name="7" x="-2.7" y="-3.81" dx="0.67" dy="1.55" layer="1" rot="R270" />
              <circle x="-3.25" y="4.6" radius="0.1542" width="0" layer="21" />
              <wire x1="2" y1="4.375" x2="2" y2="-4.375" width="0.1542" layer="51" />
              <wire x1="2" y1="-4.375" x2="-2" y2="-4.375" width="0.1542" layer="51" />
              <wire x1="-2" y1="-4.375" x2="-2" y2="4.375" width="0.1542" layer="51" />
              <wire x1="2" y1="4.375" x2="-2" y2="4.375" width="0.1542" layer="51" />
              <text x="2.36" y="5.74" size="1.016" layer="25" font="vector" ratio="15" rot="R180">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="QUADCOMP">
              <description />
              <text x="2.54" y="26.035" size="1.778" layer="95">&gt;NAME</text>
              <text x="2.54" y="22.86" size="1.778" layer="96">&gt;VALUE</text>
              <wire x1="-5.08" y1="22.86" x2="-5.08" y2="12.7" width="0.4064" layer="94" />
              <wire x1="-5.08" y1="12.7" x2="5.08" y2="17.78" width="0.4064" layer="94" />
              <wire x1="5.08" y1="17.78" x2="-5.08" y2="22.86" width="0.4064" layer="94" />
              <wire x1="-3.81" y1="20.955" x2="-3.81" y2="19.685" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="20.32" x2="-3.175" y2="20.32" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="15.24" x2="-3.175" y2="15.24" width="0.1524" layer="94" />
              <pin name="-IN1" x="-7.62" y="15.24" visible="pad" length="short" direction="pas" />
              <pin name="+IN1" x="-7.62" y="20.32" visible="pad" length="short" direction="pas" />
              <pin name="OUT1" x="7.62" y="17.78" visible="pad" length="short" direction="pas" rot="R180" />
              <wire x1="-5.08" y1="10.16" x2="-5.08" y2="0" width="0.4064" layer="94" />
              <wire x1="-5.08" y1="0" x2="5.08" y2="5.08" width="0.4064" layer="94" />
              <wire x1="5.08" y1="5.08" x2="-5.08" y2="10.16" width="0.4064" layer="94" />
              <wire x1="-3.81" y1="8.255" x2="-3.81" y2="6.985" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94" />
              <pin name="-IN2" x="-7.62" y="2.54" visible="pad" length="short" direction="pas" />
              <pin name="+IN2" x="-7.62" y="7.62" visible="pad" length="short" direction="pas" />
              <pin name="OUT2" x="7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R180" />
              <wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-12.7" width="0.4064" layer="94" />
              <wire x1="-5.08" y1="-12.7" x2="5.08" y2="-7.62" width="0.4064" layer="94" />
              <wire x1="5.08" y1="-7.62" x2="-5.08" y2="-2.54" width="0.4064" layer="94" />
              <wire x1="-3.81" y1="-4.445" x2="-3.81" y2="-5.715" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="-5.08" x2="-3.175" y2="-5.08" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="-10.16" x2="-3.175" y2="-10.16" width="0.1524" layer="94" />
              <pin name="-IN3" x="-7.62" y="-10.16" visible="pad" length="short" direction="pas" />
              <pin name="+IN3" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas" />
              <pin name="OUT3" x="7.62" y="-7.62" visible="pad" length="short" direction="pas" rot="R180" />
              <wire x1="-5.08" y1="-15.24" x2="-5.08" y2="-25.4" width="0.4064" layer="94" />
              <wire x1="-5.08" y1="-25.4" x2="5.08" y2="-20.32" width="0.4064" layer="94" />
              <wire x1="5.08" y1="-20.32" x2="-5.08" y2="-15.24" width="0.4064" layer="94" />
              <wire x1="-3.81" y1="-17.145" x2="-3.81" y2="-18.415" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="-17.78" x2="-3.175" y2="-17.78" width="0.1524" layer="94" />
              <wire x1="-4.445" y1="-22.86" x2="-3.175" y2="-22.86" width="0.1524" layer="94" />
              <pin name="-IN4" x="-7.62" y="-22.86" visible="pad" length="short" direction="pas" />
              <pin name="+IN4" x="-7.62" y="-17.78" visible="pad" length="short" direction="pas" />
              <pin name="OUT4" x="7.62" y="-20.32" visible="pad" length="short" direction="pas" rot="R180" />
              <pin name="V+" x="0" y="30.48" length="middle" rot="R270" />
              <pin name="V-" x="0" y="-33.02" length="middle" rot="R90" />
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="U" name="LM139_COMPARATOR">
              <description />
              <gates>
                <gate name="G$1" symbol="QUADCOMP" x="0" y="0" />
              </gates>
              <devices>
                <device package="14-SOIC">
                  <connects>
                    <connect gate="G$1" pin="+IN1" pad="7" />
                    <connect gate="G$1" pin="+IN2" pad="5" />
                    <connect gate="G$1" pin="+IN3" pad="9" />
                    <connect gate="G$1" pin="+IN4" pad="11" />
                    <connect gate="G$1" pin="-IN1" pad="6" />
                    <connect gate="G$1" pin="-IN2" pad="4" />
                    <connect gate="G$1" pin="-IN3" pad="8" />
                    <connect gate="G$1" pin="-IN4" pad="10" />
                    <connect gate="G$1" pin="OUT1" pad="1" />
                    <connect gate="G$1" pin="OUT2" pad="2" />
                    <connect gate="G$1" pin="OUT3" pad="14" />
                    <connect gate="G$1" pin="OUT4" pad="13" />
                    <connect gate="G$1" pin="V+" pad="3" />
                    <connect gate="G$1" pin="V-" pad="12" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="ecad">
          <description />
          <packages>
            <package name="DIODES_DO-35">
              <description>Diode, DO-35 package, 0.2in lead spacing, 0.7mm drill</description>
              <wire x1="-1.605" y1="0.9525" x2="1.605" y2="0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="-1.605" y1="-0.9525" x2="1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="-1.605" y1="0.9525" x2="-1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="1.605" y1="0.9525" x2="1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <pad name="+" x="-2.54" y="0" drill="0.7" />
              <pad name="-" x="2.54" y="0" drill="0.7" />
              <text x="-3.505" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="TANT_2012-METRIC">
              <description />
              <smd name="P$1" x="-0.955" y="0" dx="1.14" dy="1.27" layer="1" />
              <smd name="P$2" x="0.955" y="0" dx="1.14" dy="1.27" layer="1" />
              <circle x="-1.96" y="0.34" radius="0.1542" width="0" layer="21" />
              <wire x1="-0.2" y1="0.8" x2="0.2" y2="0.8" width="0.127" layer="21" />
              <wire x1="-0.2" y1="-0.8" x2="0.2" y2="-0.8" width="0.127" layer="21" />
              <text x="-3.4" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.025" y1="0.65" x2="-1.025" y2="-0.65" width="0.127" layer="51" />
              <wire x1="-1.025" y1="-0.65" x2="1.025" y2="-0.65" width="0.127" layer="51" />
              <wire x1="1.025" y1="-0.65" x2="1.025" y2="0.65" width="0.127" layer="51" />
              <wire x1="1.025" y1="0.65" x2="-1.025" y2="0.65" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0.4" x2="-0.2" y2="0" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0" x2="-0.2" y2="-0.4" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0" x2="-1" y2="0" width="0.127" layer="51" />
              <wire x1="0.4" y1="0.4" x2="0" y2="0" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0" y1="0" x2="0.4" y2="-0.4" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0" y1="0" x2="1" y2="0" width="0.127" layer="51" />
              <text x="-2.54" y="-0.635" size="1.27" layer="51">+</text>
            </package>
          </packages>
          <symbols>
            <symbol name="DIODES_DIODE">
              <description />
              <wire x1="-1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94" />
              <wire x1="1.27" y1="0" x2="-1.27" y2="1.905" width="0.254" layer="94" />
              <wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94" />
              <wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94" />
              <pin name="+" x="-2.54" y="0" visible="off" length="short" direction="pas" />
              <pin name="-" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180" />
              <text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
              <text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="TANT_CAP">
              <description />
              <wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94" />
              <wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat" />
              <wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat" />
              <text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
              <text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
              <rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94" />
              <rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94" />
              <pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270" />
              <pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90" />
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="D" name="1N4148,113">
              <description />
              <gates>
                <gate name="A" symbol="DIODES_DIODE" x="0" y="0" />
              </gates>
              <devices>
                <device package="DIODES_DO-35" name="1N4148">
                  <connects>
                    <connect gate="A" pin="+" pad="+" />
                    <connect gate="A" pin="-" pad="-" />
                  </connects>
                  <technologies>
                    <technology name="">
                      <attribute name="SPICE" value="D{NAME} {+.NET} {-.NET} 1N4148" />
                      <attribute name="SPICEMOD" value=".model 1N4148 D (IS=0.1PA, RS=16 CJO=2PF TT=12N BV=100 IBV=1nA)" />
                    </technology>
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="TANT_CAP">
              <description />
              <gates>
                <gate name="G$1" symbol="TANT_CAP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TANT_2012-METRIC" name="2012-METRIC">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
      </libraries>
      <attributes />
      <variantdefs />
      <classes>
        <class number="0" name="default" />
      </classes>
      <parts>
        <part device="" value="GRM155R71H222JA01J" name="C231" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C232" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C233" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C234" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C235" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C236" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C237" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C238" library="mlcc" deviceset="C_0402" />
        <part device="" value="KSC1008YBU" name="Q49" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q50" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q51" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="MF-RES-0603-10K" name="R401" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-4.7K" name="R402" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J123V" name="R403" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R404" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R405" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R406" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R407" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R408" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J123V" name="R409" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R410" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R411" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R412" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R413" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R419" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-040" value="5002" name="35" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="37" library="keystone" deviceset="TP" />
        <part device="" value="GRM155R71H222JA01J" name="C243" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C563K4RACTU" name="C251" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C563K4RACTU" name="C252" library="mlcc" deviceset="C_0402" />
        <part device="" value="CGJ2B2X7R1C683K050BA" name="C253" library="mlcc" deviceset="C_0402" />
        <part device="" value="CGJ2B2X7R1C683K050BA" name="C254" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C393K8RACTU" name="C261" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C393K8RACTU" name="C262" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C473K4RACTU" name="C263" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C473K4RACTU" name="C264" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C273K4RACTU" name="C271" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C273K4RACTU" name="C272" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C273" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C274" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C281" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C282" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C283" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C284" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C291" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C292" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C293" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C294" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C301" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C302" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C303" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C304" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C311" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C312" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C313" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C314" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C682K4RACTU" name="C321" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C322" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C123K4RACTU" name="C323" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C123K4RACTU" name="C324" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C331" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C272K5RACTU" name="C332" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C333" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C334" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C122K5RACTU" name="C341" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C342" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C682K4RACTU" name="C343" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C472K4RACTU" name="C344" library="mlcc" deviceset="C_0402" />
        <part device="" value="MF-RES-0603-10K" name="R420" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R421" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R422" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R434" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R441" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R442" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R443" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R444" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R445" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R446" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R447" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R448" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R449" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R451" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R452" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R453" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R454" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R455" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R456" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R457" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R458" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R459" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R461" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R462" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R463" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R464" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R465" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R466" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R467" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J112V" name="R468" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R469" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R471" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J152V" name="R472" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J274V" name="R473" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R474" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R475" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R476" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J243V" name="R477" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J112V" name="R478" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R479" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J164V" name="R481" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R482" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J204V" name="R483" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R484" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R485" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R486" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J203V" name="R487" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J911V" name="R488" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J164V" name="R489" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R491" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R492" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R493" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R494" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R495" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R496" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-15K" name="R497" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J681V" name="R498" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J124V" name="R499" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R501" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J431V" name="R502" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J823V" name="R503" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R504" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R505" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R506" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-18K" name="R507" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J821V" name="R508" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J154V" name="R509" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J334V" name="R511" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R512" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J274V" name="R513" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R514" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R515" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R516" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J912V" name="R517" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-390" name="R518" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J753V" name="R519" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R521" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J132V" name="R522" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J274V" name="R523" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R524" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R525" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R526" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J912V" name="R527" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J431V" name="R528" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J753V" name="R529" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J134V" name="R531" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R532" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J244V" name="R533" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R534" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R535" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R536" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J912V" name="R537" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J361V" name="R538" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R539" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U27" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U28" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U29" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U30" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U31" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="CD4066BM96G4" name="U32" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="LM139DR" name="U33" library="ecad(3)" deviceset="LM139_COMPARATOR" />
        <part device="" value="CD4066BM96G4" name="U34" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="LM139DR" name="U35" library="ecad(3)" deviceset="LM139_COMPARATOR" />
        <part device="" value="CD4066BM96G4" name="U36" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="LM139DR" name="U37" library="ecad(3)" deviceset="LM139_COMPARATOR" />
        <part device="" value="CD4066BM96G4" name="U38" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="UPC4558G2" name="U39" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U40" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U41" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U42" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U43" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="C0402C223K4RACAUTO" name="C135" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C136" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C137" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C145" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C146" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C147" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C155" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C156" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C157" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C165" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C166" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C167" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C175" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C176" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C177" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C185" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C186" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C187" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C195" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C196" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C197" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C205" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C206" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C207" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C215" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C216" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C217" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C225" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C226" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C223K4RACAUTO" name="C227" library="mlcc" deviceset="C_0402" />
        <part device="1N4148" value="1N4148,113" name="D37" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D38" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D39" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D40" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D41" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D42" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D43" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D44" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D45" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D46" library="ecad" deviceset="1N4148,113" />
        <part device="" value="MF-RES-0603-1M" name="R299" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R300" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R307" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R308" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R309" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R317" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R318" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R319" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R327" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R328" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R329" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R337" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R338" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R339" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R347" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R348" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R349" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R357" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R358" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R359" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R367" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R368" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R369" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R377" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R378" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R379" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R387" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R388" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R389" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470K" name="R397" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R398" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R399" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="CD4066BM96G4" name="U56" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="CD4066BM96G4" name="U57" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="CD4066BM96G4" name="U58" library="General_Will" deviceset="CD4066BM96G4" />
        <part device="" value="TL082CD" name="U59" library="General_Will" deviceset="TL082CD" />
        <part device="" value="TL082CD" name="U60" library="General_Will" deviceset="TL082CD" />
        <part device="" value="TL082CD" name="U61" library="General_Will" deviceset="TL082CD" />
        <part device="" value="TL082CD" name="U62" library="General_Will" deviceset="TL082CD" />
        <part device="" value="TL082CD" name="U63" library="General_Will" deviceset="TL082CD" />
        <part device="" value="CGJ2B2X7R1C683K050BA" name="C131" library="mlcc" deviceset="C_0402" />
        <part device="" value="CGJ2B2X7R1C683K050BA" name="C132" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C823K4RACTU" name="C133" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C823K4RACTU" name="C134" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C473K4RACTU" name="C141" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C473K4RACTU" name="C142" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C563K4RACTU" name="C143" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C563K4RACTU" name="C144" library="mlcc" deviceset="C_0402" />
        <part device="" value="ERJ-PA3J333V" name="R301" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R302" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R303" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R304" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R305" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R306" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J303V" name="R311" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R312" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J204V" name="R313" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R314" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J911V" name="R315" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R316" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U50" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U55" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="C0402C682K4RACTU" name="C191" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C682K4RACTU" name="C192" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C193" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C822K4RACTU" name="C194" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C472K4RACTU" name="C201" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C472K4RACTU" name="C202" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C562K4RACTU" name="C203" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C562K4RACTU" name="C204" library="mlcc" deviceset="C_0402" />
        <part device="" value="ERJ-PA3J303V" name="R361" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J112V" name="R362" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J204V" name="R363" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J243V" name="R364" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J911V" name="R365" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J164V" name="R366" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J303V" name="R371" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R372" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J204V" name="R373" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R374" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J911V" name="R375" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R376" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U47" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U52" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="C0402C273K4RACTU" name="C151" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C273K4RACTU" name="C152" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C153" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C333K4RACTU" name="C154" library="mlcc" deviceset="C_0402" />
        <part device="" value="CGA2B3X7R1H223K050BB" name="C161" library="mlcc" deviceset="C_0402" />
        <part device="" value="CGA2B3X7R1H223K050BB" name="C162" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C273K4RACTU" name="C163" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C273K4RACTU" name="C164" library="mlcc" deviceset="C_0402" />
        <part device="" value="ERJ-PA3J333V" name="R321" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R322" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R323" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R324" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R325" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R326" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R331" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R332" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R333" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R334" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R335" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R336" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U49" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U54" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="C0402C272K5RACTU" name="C211" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C272K5RACTU" name="C212" library="mlcc" deviceset="C_0402" />
        <part device="" value="CL05B332JB5NNNC" name="C213" library="mlcc" deviceset="C_0402" />
        <part device="" value="CL05B332JB5NNNC" name="C214" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C122K5RACTU" name="C221" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C152K5RACTU" name="C221$1" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C122K5RACTU" name="C222" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C152K5RACTU" name="C224" library="mlcc" deviceset="C_0402" />
        <part device="" value="ERJ-PA3J333V" name="R381" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R382" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R383" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R384" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R385" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R386" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R391" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J122V" name="R392" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R393" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J273V" name="R394" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J911V" name="R395" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J184V" name="R396" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U46" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U51" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="C0402C123K4RACTU" name="C171" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C123K4RACTU" name="C172" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C173" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C153K4RACTU" name="C174" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C181" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402T103K4RALTU" name="C182" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C682K4RACTU" name="C183" library="mlcc" deviceset="C_0402" />
        <part device="" value="CGJ2B2X7R1C683K050BA" name="C184" library="mlcc" deviceset="C_0402" />
        <part device="" value="ERJ-PA3J363V" name="R341" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J132V" name="R342" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J244V" name="R343" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J303V" name="R344" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J112V" name="R345" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J204V" name="R346" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J303V" name="R351" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J112V" name="R352" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J204V" name="R353" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J432V" name="R354" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J301V" name="R355" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J164V" name="R356" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U48" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U53" library="General_Will" deviceset="UPC4558G2" />
        <part device="-040" value="5002" name="49" library="keystone" deviceset="TP" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C130" library="ecad" deviceset="TANT_CAP" />
        <part device="1N4148" value="1N4148,113" name="D36" library="ecad" deviceset="1N4148,113" />
        <part device="" value="ESR03EZPJ225" name="R298" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-040" value="5002" name="36" library="keystone" deviceset="TP" />
        <part device="" value="C0402T103K4RALTU" name="C239" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C240" library="mlcc" deviceset="C_0402" />
        <part device="" value="CL10B104JB8NNNC" name="C242" library="mlcc" deviceset="C_0603" />
        <part device="1N4148" value="1N4148,113" name="D47" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D48" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D49" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D50" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D51" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D52" library="ecad" deviceset="1N4148,113" />
        <part device="" value="2N3819" name="Q52" library="General_Will" deviceset="2N3819" />
        <part device="" value="MF-RES-0603-5.6K" name="R414" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J272V" name="R415" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R416" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R417" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-5.6K" name="R418" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R426" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R427" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R428" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R429" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R430" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R431" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R432" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J154V" name="R433" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J511V" name="RVR9Ext" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="LF353DR" name="U44" library="General_Will" deviceset="LF353DR" />
        <part device="" value="UPC4558G2" name="U45" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="CT6EP202" name="VR8" library="General_Will" deviceset="CT6EP202" />
        <part device="" value="CT6EP202" name="VR9" library="General_Will" deviceset="CT6EP202" />
        <part device="" value="PMP4201Y,115" name="Q1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q3" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q4" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q5" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q6" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q7" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q8" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q9" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="ERJ-PA3J473V" name="R1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PMP4201Y,115" name="Q1$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q2$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q3$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q4$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q5$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q6$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q7$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q8$1" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q9$1" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="ERJ-PA3J473V" name="R1$1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R2$1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-040" value="5002" name="15" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="16" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="17" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="18" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="19" library="keystone" deviceset="TP" />
        <part device="" value="GRM155R71H222JA01J" name="C53" library="mlcc" deviceset="C_0402" />
        <part device="2012-METRIC" value="TM8P475K025UBA" name="C54" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="GRM155R71H222JA01J" name="C55" library="mlcc" deviceset="C_0402" />
        <part device="" value="PN2907ABU" name="Q22" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="PN2907ABU" name="Q23" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="2N3819" name="Q27" library="General_Will" deviceset="2N3819" />
        <part device="" value="KSC1008YBU" name="Q29" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="MF-RES-0603-220K" name="R144" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R145" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R146" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-18K" name="R147" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R148" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-18K" name="R149" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ESR03EZPJ335" name="R150" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R151" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R152" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R153" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R154" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ESR03EZPJ335" name="R155" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R156" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R157" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R158" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R159" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R160" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R161" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R163" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R172" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R174" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R175" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100" name="R176" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R177" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U17" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="UPC4558G2" name="U19" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="CT6EP104" name="VR4" library="General_Will" deviceset="CT6EP104" />
        <part device="" value="CT6EP104" name="VR5" library="General_Will" deviceset="CT6EP104" />
        <part device="" value="CT6EP104" name="VR6" library="General_Will" deviceset="CT6EP104" />
        <part device="" value="CT6EP104" name="VR7" library="General_Will" deviceset="CT6EP104" />
        <part device="" value="C0402C223K4RACAUTO" name="C701" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0402C332K5RACTU" name="C702" library="mlcc" deviceset="C_0402" />
        <part device="" value="06035C101JAT2A" name="C703" library="mlcc" deviceset="C_0603" />
        <part device="2012-METRIC" value="298D105X0050P2T" name="C704" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C705" library="ecad" deviceset="TANT_CAP" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C706" library="ecad" deviceset="TANT_CAP" />
        <part device="" value="KSC1008YBU" name="Q76" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="ERJ-PA3J223V" name="R713" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100" name="R716" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R717" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R718" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R719" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R720" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R721" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U71" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="PMP4201Y,115" name="Q1$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q2$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q3$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q4$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP5201Y,115" name="Q5$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q6$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP4201Y,115" name="Q7$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="PMP5201Y,115" name="Q8$2" library="General_Will" deviceset="PMP5201Y" />
        <part device="" value="PMP4201Y,115" name="Q9$2" library="General_Will" deviceset="PMP4201Y" />
        <part device="" value="ERJ-PA3J473V" name="R1$2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R2$2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="-040" value="5002" name="10" library="keystone" deviceset="TP" />
        <part device="-040" value="5002" name="12" library="keystone" deviceset="TP" />
        <part device="" value="C2012JB1C106K085AC" name="C44" library="mlcc" deviceset="C_0805" />
        <part device="" value="C1608JB1H105K080AB" name="C45" library="mlcc" deviceset="C_0603" />
        <part device="" value="C0402C331J5RACTU" name="C46" library="mlcc" deviceset="C_0402" />
        <part device="" value="GRM155R71H222JA01J" name="C47" library="mlcc" deviceset="C_0402" />
        <part device="" value="C0603S103J5RACTU" name="C48" library="mlcc" deviceset="C_0603" />
        <part device="" value="CL10B104JB8NNNC" name="C49" library="mlcc" deviceset="C_0603" />
        <part device="1N4148" value="1N4148,113" name="D19" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D20" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D21" library="ecad" deviceset="1N4148,113" />
        <part device="1N4148" value="1N4148,113" name="D22" library="ecad" deviceset="1N4148,113" />
        <part device="" value="KSC1008YBU" name="Q18" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="PN2907ABU" name="Q19" library="General_Will" deviceset="PN2907ABU" />
        <part device="" value="ERJ-PA3J223V" name="R112" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R113" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R114" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-5.6K" name="R115" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J683V" name="R116" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R117" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R118" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R119" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R120" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1M" name="R121" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R122" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J274V" name="R123" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J473V" name="R124" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R125" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-470" name="R126" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R127" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R128" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R129" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J333V" name="R130" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100" name="R131" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R132" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R133" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R134" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R135" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ESR03EZPJ475" name="R136" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="UPC4558G2" name="U14" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="TL082CD" name="U15" library="General_Will" deviceset="TL082CD" />
        <part device="" value="UPC4558G2" name="U16" library="General_Will" deviceset="UPC4558G2" />
        <part device="" value="CT6EP104" name="VR3" library="General_Will" deviceset="CT6EP104" />
        <part device="" value="1727832" name="JMiddle" library="General_Will" deviceset="1727832" />
      </parts>
      <sheets>
        <sheet>
          <description />
          <plain />
          <instances>
            <instance y="1277.62" part="C231" gate="G$1" x="1036.32" />
            <instance y="1292.86" part="C232" gate="G$1" x="1061.72" />
            <instance y="1308.10" part="C233" gate="G$1" x="1092.20" />
            <instance y="1216.66" part="C234" gate="G$1" x="982.98" />
            <instance y="1259.84" part="C235" gate="G$1" x="1036.32" />
            <instance y="1308.10" part="C236" gate="G$1" x="1099.82" />
            <instance y="1341.12" part="C237" gate="G$1" x="1140.46" />
            <instance y="1242.06" part="C238" gate="G$1" x="1018.54" />
            <instance y="1351.28" part="Q49" gate="G$1" x="1173.48" />
            <instance y="1303.02" part="Q50" gate="G$1" x="1076.96" />
            <instance y="1336.04" part="Q51" gate="G$1" x="1125.22" />
            <instance y="1229.36" part="R401" gate="G$1" x="1005.84" />
            <instance y="1236.98" part="R402" gate="G$1" x="1005.84" />
            <instance y="1280.16" part="R403" gate="G$1" x="1049.02" />
            <instance y="1320.80" part="R404" gate="G$1" x="1104.90" />
            <instance y="1328.42" part="R405" gate="G$1" x="1145.54" />
            <instance y="1295.40" part="R406" gate="G$1" x="1097.28" />
            <instance y="1272.54" part="R407" gate="G$1" x="1049.02" />
            <instance y="1287.78" part="R408" gate="G$1" x="1074.42" />
            <instance y="1229.36" part="R409" gate="G$1" x="988.06" />
            <instance y="1336.04" part="R410" gate="G$1" x="1153.16" />
            <instance y="1343.66" part="R411" gate="G$1" x="1153.16" />
            <instance y="1320.80" part="R412" gate="G$1" x="1122.68" />
            <instance y="1254.76" part="R413" gate="G$1" x="1023.62" />
            <instance y="1280.16" part="R419" gate="G$1" x="1066.80" />
            <instance y="1841.50" part="35" gate="G$1" x="1508.76" />
            <instance y="1818.64" part="37" gate="G$1" x="1457.96" />
            <instance y="1846.58" part="C243" gate="G$1" x="1501.14" />
            <instance y="1036.32" part="C251" gate="G$1" x="840.74" />
            <instance y="1036.32" part="C252" gate="G$1" x="833.12" />
            <instance y="1983.74" part="C253" gate="G$1" x="1727.20" />
            <instance y="1965.96" part="C254" gate="G$1" x="1711.96" />
            <instance y="1071.88" part="C261" gate="G$1" x="858.52" />
            <instance y="1054.10" part="C262" gate="G$1" x="858.52" />
            <instance y="1965.96" part="C263" gate="G$1" x="1719.58" />
            <instance y="1948.18" part="C264" gate="G$1" x="1704.34" />
            <instance y="1097.28" part="C271" gate="G$1" x="883.92" />
            <instance y="1079.50" part="C272" gate="G$1" x="883.92" />
            <instance y="1983.74" part="C273" gate="G$1" x="1719.58" />
            <instance y="1948.18" part="C274" gate="G$1" x="1711.96" />
            <instance y="1115.06" part="C281" gate="G$1" x="891.54" />
            <instance y="1097.28" part="C282" gate="G$1" x="891.54" />
            <instance y="2026.92" part="C283" gate="G$1" x="1762.76" />
            <instance y="2009.14" part="C284" gate="G$1" x="1762.76" />
            <instance y="1122.68" part="C291" gate="G$1" x="906.78" />
            <instance y="1122.68" part="C292" gate="G$1" x="899.16" />
            <instance y="2034.54" part="C293" gate="G$1" x="1778.00" />
            <instance y="2034.54" part="C294" gate="G$1" x="1770.38" />
            <instance y="1158.24" part="C301" gate="G$1" x="924.56" />
            <instance y="1140.46" part="C302" gate="G$1" x="924.56" />
            <instance y="2070.10" part="C303" gate="G$1" x="1795.78" />
            <instance y="2052.32" part="C304" gate="G$1" x="1795.78" />
            <instance y="1165.86" part="C311" gate="G$1" x="939.80" />
            <instance y="1165.86" part="C312" gate="G$1" x="932.18" />
            <instance y="2077.72" part="C313" gate="G$1" x="1811.02" />
            <instance y="2077.72" part="C314" gate="G$1" x="1803.40" />
            <instance y="1369.06" part="C321" gate="G$1" x="965.20" />
            <instance y="1369.06" part="C322" gate="G$1" x="957.58" />
            <instance y="2095.50" part="C323" gate="G$1" x="1828.80" />
            <instance y="2113.28" part="C324" gate="G$1" x="1828.80" />
            <instance y="1404.62" part="C331" gate="G$1" x="982.98" />
            <instance y="1386.84" part="C332" gate="G$1" x="982.98" />
            <instance y="2120.90" part="C333" gate="G$1" x="1844.04" />
            <instance y="2120.90" part="C334" gate="G$1" x="1836.42" />
            <instance y="1412.24" part="C341" gate="G$1" x="998.22" />
            <instance y="1412.24" part="C342" gate="G$1" x="990.60" />
            <instance y="2156.46" part="C343" gate="G$1" x="1861.82" />
            <instance y="2138.68" part="C344" gate="G$1" x="1861.82" />
            <instance y="1917.70" part="R420" gate="G$1" x="1691.64" />
            <instance y="1546.86" part="R421" gate="G$1" x="1186.18" />
            <instance y="1546.86" part="R422" gate="G$1" x="1203.96" />
            <instance y="1811.02" part="R434" gate="G$1" x="1463.04" />
            <instance y="789.94" part="R441" gate="G$1" x="815.34" />
            <instance y="1049.02" part="R442" gate="G$1" x="845.82" />
            <instance y="1424.94" part="R443" gate="G$1" x="1021.08" />
            <instance y="1554.48" part="R444" gate="G$1" x="1203.96" />
            <instance y="1849.12" part="R445" gate="G$1" x="1531.62" />
            <instance y="1767.84" part="R446" gate="G$1" x="1371.60" />
            <instance y="1849.12" part="R447" gate="G$1" x="1513.84" />
            <instance y="1996.44" part="R448" gate="G$1" x="1732.28" />
            <instance y="2159.00" part="R449" gate="G$1" x="1874.52" />
            <instance y="830.58" part="R451" gate="G$1" x="815.34" />
            <instance y="1066.80" part="R452" gate="G$1" x="871.22" />
            <instance y="1432.56" part="R453" gate="G$1" x="1059.18" />
            <instance y="1554.48" part="R454" gate="G$1" x="1221.74" />
            <instance y="1871.98" part="R455" gate="G$1" x="1584.96" />
            <instance y="1767.84" part="R456" gate="G$1" x="1389.38" />
            <instance y="1856.74" part="R457" gate="G$1" x="1531.62" />
            <instance y="1996.44" part="R458" gate="G$1" x="1750.06" />
            <instance y="2159.00" part="R459" gate="G$1" x="1892.30" />
            <instance y="863.60" part="R461" gate="G$1" x="815.34" />
            <instance y="1074.42" part="R462" gate="G$1" x="871.22" />
            <instance y="1440.18" part="R463" gate="G$1" x="1059.18" />
            <instance y="1562.10" part="R464" gate="G$1" x="1239.52" />
            <instance y="1864.36" part="R465" gate="G$1" x="1549.40" />
            <instance y="1775.46" part="R466" gate="G$1" x="1389.38" />
            <instance y="1856.74" part="R467" gate="G$1" x="1549.40" />
            <instance y="2004.06" part="R468" gate="G$1" x="1750.06" />
            <instance y="2166.62" part="R469" gate="G$1" x="1892.30" />
            <instance y="896.62" part="R471" gate="G$1" x="815.34" />
            <instance y="1109.98" part="R472" gate="G$1" x="904.24" />
            <instance y="1468.12" part="R473" gate="G$1" x="1094.74" />
            <instance y="1562.10" part="R474" gate="G$1" x="1221.74" />
            <instance y="1871.98" part="R475" gate="G$1" x="1567.18" />
            <instance y="1775.46" part="R476" gate="G$1" x="1407.16" />
            <instance y="1864.36" part="R477" gate="G$1" x="1567.18" />
            <instance y="2021.84" part="R478" gate="G$1" x="1775.46" />
            <instance y="2202.18" part="R479" gate="G$1" x="1965.96" />
            <instance y="919.48" part="R481" gate="G$1" x="815.34" />
            <instance y="1135.38" part="R482" gate="G$1" x="911.86" />
            <instance y="1475.74" part="R483" gate="G$1" x="1094.74" />
            <instance y="1569.72" part="R484" gate="G$1" x="1239.52" />
            <instance y="1879.60" part="R485" gate="G$1" x="1602.74" />
            <instance y="1818.64" part="R486" gate="G$1" x="1470.66" />
            <instance y="1879.60" part="R487" gate="G$1" x="1584.96" />
            <instance y="2047.24" part="R488" gate="G$1" x="1783.08" />
            <instance y="2209.80" part="R489" gate="G$1" x="1965.96" />
            <instance y="939.80" part="R491" gate="G$1" x="817.88" />
            <instance y="1153.16" part="R492" gate="G$1" x="937.26" />
            <instance y="1503.68" part="R493" gate="G$1" x="1112.52" />
            <instance y="1569.72" part="R494" gate="G$1" x="1257.30" />
            <instance y="1887.22" part="R495" gate="G$1" x="1620.52" />
            <instance y="1783.08" part="R496" gate="G$1" x="1407.16" />
            <instance y="1887.22" part="R497" gate="G$1" x="1602.74" />
            <instance y="2065.02" part="R498" gate="G$1" x="1808.48" />
            <instance y="2237.74" part="R499" gate="G$1" x="1983.74" />
            <instance y="960.12" part="R501" gate="G$1" x="820.42" />
            <instance y="1178.56" part="R502" gate="G$1" x="944.88" />
            <instance y="1468.12" part="R503" gate="G$1" x="1076.96" />
            <instance y="1577.34" part="R504" gate="G$1" x="1257.30" />
            <instance y="1894.84" part="R505" gate="G$1" x="1638.30" />
            <instance y="1826.26" part="R506" gate="G$1" x="1470.66" />
            <instance y="1894.84" part="R507" gate="G$1" x="1620.52" />
            <instance y="2090.42" part="R508" gate="G$1" x="1816.10" />
            <instance y="2237.74" part="R509" gate="G$1" x="2001.52" />
            <instance y="977.90" part="R511" gate="G$1" x="820.42" />
            <instance y="1381.76" part="R512" gate="G$1" x="970.28" />
            <instance y="1531.62" part="R513" gate="G$1" x="1130.30" />
            <instance y="1651.00" part="R514" gate="G$1" x="1297.94" />
            <instance y="1910.08" part="R515" gate="G$1" x="1656.08" />
            <instance y="1833.88" part="R516" gate="G$1" x="1506.22" />
            <instance y="1902.46" part="R517" gate="G$1" x="1638.30" />
            <instance y="2108.20" part="R518" gate="G$1" x="1841.50" />
            <instance y="2194.56" part="R519" gate="G$1" x="1910.08" />
            <instance y="1000.76" part="R521" gate="G$1" x="820.42" />
            <instance y="1399.54" part="R522" gate="G$1" x="995.68" />
            <instance y="1531.62" part="R523" gate="G$1" x="1148.08" />
            <instance y="1658.62" part="R524" gate="G$1" x="1297.94" />
            <instance y="1910.08" part="R525" gate="G$1" x="1673.86" />
            <instance y="1826.26" part="R526" gate="G$1" x="1488.44" />
            <instance y="1902.46" part="R527" gate="G$1" x="1656.08" />
            <instance y="2133.60" part="R528" gate="G$1" x="1849.12" />
            <instance y="2194.56" part="R529" gate="G$1" x="1927.86" />
            <instance y="1023.62" part="R531" gate="G$1" x="820.42" />
            <instance y="1424.94" part="R532" gate="G$1" x="1003.30" />
            <instance y="1539.24" part="R533" gate="G$1" x="1186.18" />
            <instance y="1658.62" part="R534" gate="G$1" x="1315.72" />
            <instance y="1917.70" part="R535" gate="G$1" x="1673.86" />
            <instance y="1833.88" part="R536" gate="G$1" x="1488.44" />
            <instance y="1925.32" part="R537" gate="G$1" x="1691.64" />
            <instance y="2151.38" part="R538" gate="G$1" x="1874.52" />
            <instance y="2273.30" part="R539" gate="G$1" x="2039.62" />
            <instance y="1442.72" part="U27" gate="G$1" x="1031.24" />
            <instance y="1450.34" part="U28" gate="G$1" x="1087.12" />
            <instance y="1485.90" part="U29" gate="G$1" x="1122.68" />
            <instance y="1513.84" part="U30" gate="G$1" x="1140.46" />
            <instance y="1549.40" part="U31" gate="G$1" x="1158.24" />
            <instance y="1676.40" part="U32" gate="G$1" x="1343.66" />
            <instance y="1610.36" part="U33" gate="G$1" x="1277.62" />
            <instance y="1704.34" part="U34" gate="G$1" x="1343.66" />
            <instance y="1684.02" part="U35" gate="G$1" x="1277.62" />
            <instance y="1793.24" part="U36" gate="G$1" x="1435.10" />
            <instance y="1727.20" part="U37" gate="G$1" x="1374.14" />
            <instance y="1821.18" part="U38" gate="G$1" x="1435.10" />
            <instance y="2212.34" part="U39" gate="G$1" x="1938.02" />
            <instance y="2176.78" part="U40" gate="G$1" x="1920.24" />
            <instance y="2219.96" part="U41" gate="G$1" x="1993.90" />
            <instance y="2255.52" part="U42" gate="G$1" x="2011.68" />
            <instance y="2255.52" part="U43" gate="G$1" x="2049.78" />
            <instance y="116.84" part="C135" gate="G$1" x="830.58" />
            <instance y="439.42" part="C136" gate="G$1" x="1087.12" />
            <instance y="551.18" part="C137" gate="G$1" x="1178.56" />
            <instance y="137.16" part="C145" gate="G$1" x="830.58" />
            <instance y="457.20" part="C146" gate="G$1" x="1087.12" />
            <instance y="568.96" part="C147" gate="G$1" x="1178.56" />
            <instance y="157.48" part="C155" gate="G$1" x="830.58" />
            <instance y="457.20" part="C156" gate="G$1" x="1094.74" />
            <instance y="568.96" part="C157" gate="G$1" x="1186.18" />
            <instance y="177.80" part="C165" gate="G$1" x="820.42" />
            <instance y="490.22" part="C166" gate="G$1" x="1148.08" />
            <instance y="586.74" part="C167" gate="G$1" x="1186.18" />
            <instance y="287.02" part="C175" gate="G$1" x="833.12" />
            <instance y="508.00" part="C176" gate="G$1" x="1148.08" />
            <instance y="586.74" part="C177" gate="G$1" x="1193.80" />
            <instance y="304.80" part="C185" gate="G$1" x="833.12" />
            <instance y="515.62" part="C186" gate="G$1" x="1155.70" />
            <instance y="604.52" part="C187" gate="G$1" x="1193.80" />
            <instance y="304.80" part="C195" gate="G$1" x="840.74" />
            <instance y="515.62" part="C196" gate="G$1" x="1163.32" />
            <instance y="604.52" part="C197" gate="G$1" x="1201.42" />
            <instance y="322.58" part="C205" gate="G$1" x="840.74" />
            <instance y="533.40" part="C206" gate="G$1" x="1163.32" />
            <instance y="622.30" part="C207" gate="G$1" x="1201.42" />
            <instance y="322.58" part="C215" gate="G$1" x="848.36" />
            <instance y="533.40" part="C216" gate="G$1" x="1170.94" />
            <instance y="622.30" part="C217" gate="G$1" x="1209.04" />
            <instance y="340.36" part="C225" gate="G$1" x="848.36" />
            <instance y="551.18" part="C226" gate="G$1" x="1170.94" />
            <instance y="640.08" part="C227" gate="G$1" x="1209.04" />
            <instance y="104.14" part="D37" gate="A" x="835.66" />
            <instance y="124.46" part="D38" gate="A" x="817.88" />
            <instance y="144.78" part="D39" gate="A" x="817.88" />
            <instance y="165.10" part="D40" gate="A" x="820.42" />
            <instance y="185.42" part="D41" gate="A" x="830.58" />
            <instance y="205.74" part="D42" gate="A" x="822.96" />
            <instance y="228.60" part="D43" gate="A" x="822.96" />
            <instance y="248.92" part="D44" gate="A" x="822.96" />
            <instance y="274.32" part="D45" gate="A" x="822.96" />
            <instance y="297.18" part="D46" gate="A" x="822.96" />
            <instance y="502.92" part="R299" gate="G$1" x="1160.78" />
            <instance y="335.28" part="R300" gate="G$1" x="861.06" />
            <instance y="342.90" part="R307" gate="G$1" x="861.06" />
            <instance y="381.00" part="R308" gate="G$1" x="949.96" />
            <instance y="419.10" part="R309" gate="G$1" x="1038.86" />
            <instance y="342.90" part="R317" gate="G$1" x="878.84" />
            <instance y="381.00" part="R318" gate="G$1" x="967.74" />
            <instance y="419.10" part="R319" gate="G$1" x="1056.64" />
            <instance y="350.52" part="R327" gate="G$1" x="878.84" />
            <instance y="388.62" part="R328" gate="G$1" x="967.74" />
            <instance y="469.90" part="R329" gate="G$1" x="1099.82" />
            <instance y="350.52" part="R337" gate="G$1" x="896.62" />
            <instance y="388.62" part="R338" gate="G$1" x="985.52" />
            <instance y="469.90" part="R339" gate="G$1" x="1117.60" />
            <instance y="358.14" part="R347" gate="G$1" x="896.62" />
            <instance y="396.24" part="R348" gate="G$1" x="985.52" />
            <instance y="477.52" part="R349" gate="G$1" x="1117.60" />
            <instance y="358.14" part="R357" gate="G$1" x="914.40" />
            <instance y="396.24" part="R358" gate="G$1" x="1003.30" />
            <instance y="477.52" part="R359" gate="G$1" x="1135.38" />
            <instance y="365.76" part="R367" gate="G$1" x="914.40" />
            <instance y="403.86" part="R368" gate="G$1" x="1003.30" />
            <instance y="426.72" part="R369" gate="G$1" x="1056.64" />
            <instance y="365.76" part="R377" gate="G$1" x="932.18" />
            <instance y="403.86" part="R378" gate="G$1" x="1021.08" />
            <instance y="426.72" part="R379" gate="G$1" x="1074.42" />
            <instance y="373.38" part="R387" gate="G$1" x="932.18" />
            <instance y="411.48" part="R388" gate="G$1" x="1038.86" />
            <instance y="485.14" part="R389" gate="G$1" x="1135.38" />
            <instance y="373.38" part="R397" gate="G$1" x="949.96" />
            <instance y="411.48" part="R398" gate="G$1" x="1021.08" />
            <instance y="434.34" part="R399" gate="G$1" x="1074.42" />
            <instance y="728.98" part="U56" gate="G$1" x="1308.10" />
            <instance y="701.04" part="U57" gate="G$1" x="1270.00" />
            <instance y="673.10" part="U58" gate="G$1" x="1231.90" />
            <instance y="728.98" part="U59" gate="G$1" x="1346.20" />
            <instance y="701.04" part="U60" gate="G$1" x="1308.10" />
            <instance y="756.92" part="U61" gate="G$1" x="1346.20" />
            <instance y="673.10" part="U62" gate="G$1" x="1270.00" />
            <instance y="645.16" part="U63" gate="G$1" x="1231.90" />
            <instance y="165.10" part="C131" gate="G$1" x="187.96" />
            <instance y="147.32" part="C132" gate="G$1" x="187.96" />
            <instance y="243.84" part="C133" gate="G$1" x="274.32" />
            <instance y="261.62" part="C134" gate="G$1" x="292.10" />
            <instance y="182.88" part="C141" gate="G$1" x="195.58" />
            <instance y="165.10" part="C142" gate="G$1" x="195.58" />
            <instance y="279.40" part="C143" gate="G$1" x="299.72" />
            <instance y="279.40" part="C144" gate="G$1" x="292.10" />
            <instance y="109.22" part="R301" gate="G$1" x="162.56" />
            <instance y="142.24" part="R302" gate="G$1" x="175.26" />
            <instance y="185.42" part="R303" gate="G$1" x="208.28" />
            <instance y="213.36" part="R304" gate="G$1" x="243.84" />
            <instance y="238.76" part="R305" gate="G$1" x="261.62" />
            <instance y="292.10" part="R306" gate="G$1" x="304.80" />
            <instance y="134.62" part="R311" gate="G$1" x="165.10" />
            <instance y="177.80" part="R312" gate="G$1" x="208.28" />
            <instance y="213.36" part="R313" gate="G$1" x="226.06" />
            <instance y="220.98" part="R314" gate="G$1" x="243.84" />
            <instance y="256.54" part="R315" gate="G$1" x="279.40" />
            <instance y="320.04" part="R316" gate="G$1" x="322.58" />
            <instance y="195.58" part="U50" gate="G$1" x="236.22" />
            <instance y="302.26" part="U55" gate="G$1" x="332.74" />
            <instance y="363.22" part="C191" gate="G$1" x="601.98" />
            <instance y="345.44" part="C192" gate="G$1" x="601.98" />
            <instance y="449.58" part="C193" gate="G$1" x="688.34" />
            <instance y="431.80" part="C194" gate="G$1" x="688.34" />
            <instance y="381.00" part="C201" gate="G$1" x="609.60" />
            <instance y="363.22" part="C202" gate="G$1" x="609.60" />
            <instance y="467.36" part="C203" gate="G$1" x="695.96" />
            <instance y="449.58" part="C204" gate="G$1" x="695.96" />
            <instance y="332.74" part="R361" gate="G$1" x="589.28" />
            <instance y="342.90" part="R362" gate="G$1" x="589.28" />
            <instance y="383.54" part="R363" gate="G$1" x="622.30" />
            <instance y="411.48" part="R364" gate="G$1" x="657.86" />
            <instance y="419.10" part="R365" gate="G$1" x="657.86" />
            <instance y="469.90" part="R366" gate="G$1" x="708.66" />
            <instance y="355.60" part="R371" gate="G$1" x="571.50" />
            <instance y="375.92" part="R372" gate="G$1" x="622.30" />
            <instance y="411.48" part="R373" gate="G$1" x="640.08" />
            <instance y="419.10" part="R374" gate="G$1" x="675.64" />
            <instance y="462.28" part="R375" gate="G$1" x="708.66" />
            <instance y="497.84" part="R376" gate="G$1" x="726.44" />
            <instance y="393.70" part="U47" gate="G$1" x="650.24" />
            <instance y="480.06" part="U52" gate="G$1" x="736.60" />
            <instance y="167.64" part="C151" gate="G$1" x="393.70" />
            <instance y="149.86" part="C152" gate="G$1" x="393.70" />
            <instance y="264.16" part="C153" gate="G$1" x="480.06" />
            <instance y="246.38" part="C154" gate="G$1" x="480.06" />
            <instance y="185.42" part="C161" gate="G$1" x="401.32" />
            <instance y="167.64" part="C162" gate="G$1" x="401.32" />
            <instance y="271.78" part="C163" gate="G$1" x="495.30" />
            <instance y="271.78" part="C164" gate="G$1" x="487.68" />
            <instance y="114.30" part="R321" gate="G$1" x="368.30" />
            <instance y="144.78" part="R322" gate="G$1" x="381.00" />
            <instance y="187.96" part="R323" gate="G$1" x="414.02" />
            <instance y="215.90" part="R324" gate="G$1" x="449.58" />
            <instance y="241.30" part="R325" gate="G$1" x="467.36" />
            <instance y="284.48" part="R326" gate="G$1" x="500.38" />
            <instance y="137.16" part="R331" gate="G$1" x="370.84" />
            <instance y="180.34" part="R332" gate="G$1" x="414.02" />
            <instance y="215.90" part="R333" gate="G$1" x="431.80" />
            <instance y="223.52" part="R334" gate="G$1" x="449.58" />
            <instance y="259.08" part="R335" gate="G$1" x="492.76" />
            <instance y="312.42" part="R336" gate="G$1" x="518.16" />
            <instance y="198.12" part="U49" gate="G$1" x="441.96" />
            <instance y="294.64" part="U54" gate="G$1" x="528.32" />
            <instance y="568.96" part="C211" gate="G$1" x="594.36" />
            <instance y="551.18" part="C212" gate="G$1" x="594.36" />
            <instance y="665.48" part="C213" gate="G$1" x="680.72" />
            <instance y="647.70" part="C214" gate="G$1" x="680.72" />
            <instance y="586.74" part="C221" gate="G$1" x="601.98" />
            <instance y="683.26" part="C221$1" gate="G$1" x="688.34" />
            <instance y="568.96" part="C222" gate="G$1" x="601.98" />
            <instance y="665.48" part="C224" gate="G$1" x="688.34" />
            <instance y="515.62" part="R381" gate="G$1" x="568.96" />
            <instance y="546.10" part="R382" gate="G$1" x="581.66" />
            <instance y="589.28" part="R383" gate="G$1" x="614.68" />
            <instance y="617.22" part="R384" gate="G$1" x="650.24" />
            <instance y="642.62" part="R385" gate="G$1" x="668.02" />
            <instance y="685.80" part="R386" gate="G$1" x="701.04" />
            <instance y="538.48" part="R391" gate="G$1" x="571.50" />
            <instance y="581.66" part="R392" gate="G$1" x="614.68" />
            <instance y="617.22" part="R393" gate="G$1" x="632.46" />
            <instance y="624.84" part="R394" gate="G$1" x="650.24" />
            <instance y="678.18" part="R395" gate="G$1" x="701.04" />
            <instance y="713.74" part="R396" gate="G$1" x="718.82" />
            <instance y="599.44" part="U46" gate="G$1" x="642.62" />
            <instance y="695.96" part="U51" gate="G$1" x="728.98" />
            <instance y="383.54" part="C171" gate="G$1" x="388.62" />
            <instance y="375.92" part="C172" gate="G$1" x="381.00" />
            <instance y="462.28" part="C173" gate="G$1" x="474.98" />
            <instance y="444.50" part="C174" gate="G$1" x="457.20" />
            <instance y="401.32" part="C181" gate="G$1" x="396.24" />
            <instance y="383.54" part="C182" gate="G$1" x="396.24" />
            <instance y="497.84" part="C183" gate="G$1" x="492.76" />
            <instance y="480.06" part="C184" gate="G$1" x="492.76" />
            <instance y="330.20" part="R341" gate="G$1" x="368.30" />
            <instance y="370.84" part="R342" gate="G$1" x="393.70" />
            <instance y="403.86" part="R343" gate="G$1" x="408.94" />
            <instance y="431.80" part="R344" gate="G$1" x="444.50" />
            <instance y="474.98" part="R345" gate="G$1" x="480.06" />
            <instance y="492.76" part="R346" gate="G$1" x="505.46" />
            <instance y="363.22" part="R351" gate="G$1" x="368.30" />
            <instance y="396.24" part="R352" gate="G$1" x="408.94" />
            <instance y="431.80" part="R353" gate="G$1" x="426.72" />
            <instance y="439.42" part="R354" gate="G$1" x="444.50" />
            <instance y="500.38" part="R355" gate="G$1" x="505.46" />
            <instance y="528.32" part="R356" gate="G$1" x="523.24" />
            <instance y="414.02" part="U48" gate="G$1" x="436.88" />
            <instance y="510.54" part="U53" gate="G$1" x="533.40" />
            <instance y="525.78" part="49" gate="G$1" x="772.16" />
            <instance y="515.62" part="C130" gate="G$1" x="795.02" />
            <instance y="518.16" part="D36" gate="A" x="784.86" />
            <instance y="510.54" part="R298" gate="G$1" x="777.24" />
            <instance y="299.72" part="36" gate="G$1" x="2273.30" />
            <instance y="203.20" part="C239" gate="G$1" x="2164.08" />
            <instance y="203.20" part="C240" gate="G$1" x="2171.70" />
            <instance y="170.18" part="C242" gate="G$1" x="2133.60" />
            <instance y="330.20" part="D47" gate="A" x="2293.62" />
            <instance y="330.20" part="D48" gate="A" x="2306.32" />
            <instance y="299.72" part="D49" gate="A" x="2263.14" />
            <instance y="292.10" part="D50" gate="A" x="2263.14" />
            <instance y="190.50" part="D51" gate="A" x="2153.92" />
            <instance y="190.50" part="D52" gate="A" x="2166.62" />
            <instance y="320.04" part="Q52" gate="G$1" x="2278.38" />
            <instance y="261.62" part="R414" gate="G$1" x="2212.34" />
            <instance y="215.90" part="R415" gate="G$1" x="2176.78" />
            <instance y="233.68" part="R416" gate="G$1" x="2194.56" />
            <instance y="261.62" part="R417" gate="G$1" x="2230.12" />
            <instance y="269.24" part="R418" gate="G$1" x="2230.12" />
            <instance y="137.16" part="R426" gate="G$1" x="2103.12" />
            <instance y="104.14" part="R427" gate="G$1" x="2082.80" />
            <instance y="129.54" part="R428" gate="G$1" x="2103.12" />
            <instance y="121.92" part="R429" gate="G$1" x="2085.34" />
            <instance y="129.54" part="R430" gate="G$1" x="2085.34" />
            <instance y="182.88" part="R431" gate="G$1" x="2156.46" />
            <instance y="182.88" part="R432" gate="G$1" x="2138.68" />
            <instance y="165.10" part="R433" gate="G$1" x="2120.90" />
            <instance y="292.10" part="RVR9Ext" gate="G$1" x="2247.90" />
            <instance y="243.84" part="U44" gate="G$1" x="2222.50" />
            <instance y="147.32" part="U45" gate="G$1" x="2131.06" />
            <instance y="276.86" part="VR8" gate="G$1" x="2252.98" />
            <instance y="314.96" part="VR9" gate="G$1" x="2301.24" />
            <instance y="2692.40" part="Q1" gate="G$1" x="510.54" />
            <instance y="2682.24" part="Q2" gate="G$1" x="596.90" />
            <instance y="2667.00" part="Q3" gate="G$1" x="510.54" />
            <instance y="2705.10" part="Q4" gate="G$1" x="596.90" />
            <instance y="2677.16" part="Q5" gate="G$1" x="553.72" />
            <instance y="2715.26" part="Q6" gate="G$1" x="640.08" />
            <instance y="2738.12" part="Q7" gate="G$1" x="657.86" />
            <instance y="2760.98" part="Q8" gate="G$1" x="701.04" />
            <instance y="2760.98" part="Q9" gate="G$1" x="657.86" />
            <instance y="2730.50" part="R1" gate="G$1" x="627.38" />
            <instance y="2776.22" part="R2" gate="G$1" x="688.34" />
            <instance y="2557.78" part="Q1$1" gate="G$1" x="510.54" />
            <instance y="2547.62" part="Q2$1" gate="G$1" x="596.90" />
            <instance y="2532.38" part="Q3$1" gate="G$1" x="510.54" />
            <instance y="2570.48" part="Q4$1" gate="G$1" x="596.90" />
            <instance y="2542.54" part="Q5$1" gate="G$1" x="553.72" />
            <instance y="2580.64" part="Q6$1" gate="G$1" x="640.08" />
            <instance y="2603.50" part="Q7$1" gate="G$1" x="657.86" />
            <instance y="2626.36" part="Q8$1" gate="G$1" x="701.04" />
            <instance y="2626.36" part="Q9$1" gate="G$1" x="657.86" />
            <instance y="2595.88" part="R1$1" gate="G$1" x="627.38" />
            <instance y="2641.60" part="R2$1" gate="G$1" x="688.34" />
            <instance y="2463.80" part="15" gate="G$1" x="347.98" />
            <instance y="2494.28" part="16" gate="G$1" x="424.18" />
            <instance y="2783.84" part="17" gate="G$1" x="889.00" />
            <instance y="2760.98" part="18" gate="G$1" x="881.38" />
            <instance y="2806.70" part="19" gate="G$1" x="896.62" />
            <instance y="2748.28" part="C53" gate="G$1" x="881.38" />
            <instance y="2392.68" part="C54" gate="G$1" x="281.94" />
            <instance y="2730.50" part="C55" gate="G$1" x="848.36" />
            <instance y="2486.66" part="Q22" gate="G$1" x="373.38" />
            <instance y="2791.46" part="Q23" gate="G$1" x="904.24" />
            <instance y="2443.48" part="Q27" gate="G$1" x="330.20" />
            <instance y="2471.42" part="Q29" gate="G$1" x="332.74" />
            <instance y="2689.86" part="R144" gate="G$1" x="817.88" />
            <instance y="2697.48" part="R145" gate="G$1" x="817.88" />
            <instance y="2682.24" part="R146" gate="G$1" x="800.10" />
            <instance y="2430.78" part="R147" gate="G$1" x="312.42" />
            <instance y="2689.86" part="R148" gate="G$1" x="800.10" />
            <instance y="2651.76" part="R149" gate="G$1" x="728.98" />
            <instance y="2509.52" part="R150" gate="G$1" x="447.04" />
            <instance y="2494.28" part="R151" gate="G$1" x="411.48" />
            <instance y="2517.14" part="R152" gate="G$1" x="464.82" />
            <instance y="2509.52" part="R153" gate="G$1" x="464.82" />
            <instance y="2471.42" part="R154" gate="G$1" x="353.06" />
            <instance y="2501.90" part="R155" gate="G$1" x="447.04" />
            <instance y="2501.90" part="R156" gate="G$1" x="429.26" />
            <instance y="2486.66" part="R157" gate="G$1" x="393.70" />
            <instance y="2806.70" part="R158" gate="G$1" x="909.32" />
            <instance y="2725.42" part="R159" gate="G$1" x="835.66" />
            <instance y="2486.66" part="R160" gate="G$1" x="411.48" />
            <instance y="2479.04" part="R161" gate="G$1" x="393.70" />
            <instance y="2423.16" part="R163" gate="G$1" x="312.42" />
            <instance y="2339.34" part="R172" gate="G$1" x="284.48" />
            <instance y="2331.72" part="R174" gate="G$1" x="284.48" />
            <instance y="2397.76" part="R175" gate="G$1" x="294.64" />
            <instance y="2471.42" part="R176" gate="G$1" x="370.84" />
            <instance y="2405.38" part="R177" gate="G$1" x="294.64" />
            <instance y="2707.64" part="U17" gate="G$1" x="845.82" />
            <instance y="2692.40" part="U19" gate="G$1" x="772.16" />
            <instance y="2667.00" part="VR4" gate="G$1" x="767.08" />
            <instance y="2667.00" part="VR5" gate="G$1" x="734.06" />
            <instance y="2768.60" part="VR6" gate="G$1" x="899.16" />
            <instance y="2750.82" part="VR7" gate="G$1" x="858.52" />
            <instance y="2382.52" part="C701" gate="G$1" x="193.04" />
            <instance y="2319.02" part="C702" gate="G$1" x="121.92" />
            <instance y="2364.74" part="C703" gate="G$1" x="170.18" />
            <instance y="2346.96" part="C704" gate="G$1" x="152.40" />
            <instance y="2296.16" part="C705" gate="G$1" x="114.30" />
            <instance y="2301.24" part="C706" gate="G$1" x="121.92" />
            <instance y="2385.06" part="Q76" gate="G$1" x="177.80" />
            <instance y="2339.34" part="R713" gate="G$1" x="121.92" />
            <instance y="2359.66" part="R716" gate="G$1" x="157.48" />
            <instance y="2341.88" part="R717" gate="G$1" x="139.70" />
            <instance y="2316.48" part="R718" gate="G$1" x="134.62" />
            <instance y="2395.22" part="R719" gate="G$1" x="215.90" />
            <instance y="2402.84" part="R720" gate="G$1" x="254.00" />
            <instance y="2395.22" part="R721" gate="G$1" x="198.12" />
            <instance y="2413.00" part="U71" gate="G$1" x="226.06" />
            <instance y="3063.24" part="Q1$2" gate="G$1" x="574.04" />
            <instance y="3053.08" part="Q2$2" gate="G$1" x="660.40" />
            <instance y="3037.84" part="Q3$2" gate="G$1" x="574.04" />
            <instance y="3075.94" part="Q4$2" gate="G$1" x="660.40" />
            <instance y="3048.00" part="Q5$2" gate="G$1" x="617.22" />
            <instance y="3086.10" part="Q6$2" gate="G$1" x="703.58" />
            <instance y="3108.96" part="Q7$2" gate="G$1" x="721.36" />
            <instance y="3131.82" part="Q8$2" gate="G$1" x="764.54" />
            <instance y="3131.82" part="Q9$2" gate="G$1" x="721.36" />
            <instance y="3101.34" part="R1$2" gate="G$1" x="690.88" />
            <instance y="3147.06" part="R2$2" gate="G$1" x="751.84" />
            <instance y="2923.54" part="10" gate="G$1" x="434.34" />
            <instance y="3037.84" part="12" gate="G$1" x="822.96" />
            <instance y="3009.90" part="C44" gate="G$1" x="541.02" />
            <instance y="3093.72" part="C45" gate="G$1" x="881.38" />
            <instance y="3068.32" part="C46" gate="G$1" x="858.52" />
            <instance y="3050.54" part="C47" gate="G$1" x="840.74" />
            <instance y="2928.62" part="C48" gate="G$1" x="426.72" />
            <instance y="2877.82" part="C49" gate="G$1" x="353.06" />
            <instance y="2837.18" part="D19" gate="A" x="297.18" />
            <instance y="2844.80" part="D20" gate="A" x="297.18" />
            <instance y="2931.16" part="D21" gate="A" x="436.88" />
            <instance y="2908.30" part="D22" gate="A" x="411.48" />
            <instance y="3088.64" part="Q18" gate="G$1" x="866.14" />
            <instance y="2997.20" part="Q19" gate="G$1" x="490.22" />
            <instance y="2824.48" part="R112" gate="G$1" x="322.58" />
            <instance y="2832.10" part="R113" gate="G$1" x="322.58" />
            <instance y="2832.10" part="R114" gate="G$1" x="340.36" />
            <instance y="2849.88" part="R115" gate="G$1" x="312.42" />
            <instance y="2954.02" part="R116" gate="G$1" x="469.90" />
            <instance y="2829.56" part="R117" gate="G$1" x="304.80" />
            <instance y="2816.86" part="R118" gate="G$1" x="304.80" />
            <instance y="2824.48" part="R119" gate="G$1" x="287.02" />
            <instance y="2872.74" part="R120" gate="G$1" x="340.36" />
            <instance y="2989.58" part="R121" gate="G$1" x="510.54" />
            <instance y="2997.20" part="R122" gate="G$1" x="510.54" />
            <instance y="3030.22" part="R123" gate="G$1" x="810.26" />
            <instance y="2997.20" part="R124" gate="G$1" x="528.32" />
            <instance y="3022.60" part="R125" gate="G$1" x="792.48" />
            <instance y="3037.84" part="R126" gate="G$1" x="810.26" />
            <instance y="3030.22" part="R127" gate="G$1" x="792.48" />
            <instance y="3045.46" part="R128" gate="G$1" x="828.04" />
            <instance y="2961.64" part="R129" gate="G$1" x="469.90" />
            <instance y="2954.02" part="R130" gate="G$1" x="452.12" />
            <instance y="3004.82" part="R131" gate="G$1" x="528.32" />
            <instance y="3081.02" part="R132" gate="G$1" x="886.46" />
            <instance y="3063.24" part="R133" gate="G$1" x="845.82" />
            <instance y="2915.92" part="R134" gate="G$1" x="431.80" />
            <instance y="2915.92" part="R135" gate="G$1" x="414.02" />
            <instance y="2890.52" part="R136" gate="G$1" x="358.14" />
            <instance y="2854.96" part="U14" gate="G$1" x="340.36" />
            <instance y="2971.80" part="U15" gate="G$1" x="497.84" />
            <instance y="2918.46" part="U16" gate="G$1" x="386.08" />
            <instance y="2938.78" part="VR3" gate="G$1" x="457.20" />
            <instance y="76.20" part="JMiddle" gate="G$1" x="45.72" />
          </instances>
          <busses />
          <nets>
            <net name="N$0">
              <segment>
                <wire x1="1036.32" y1="1270.00" x2="1036.32" y2="1267.46" width="0.3" layer="91" />
                <label x="1036.32" y="1267.46" size="1.27" layer="95" />
                <pinref part="C231" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1061.72" y1="1292.86" x2="1061.72" y2="1295.40" width="0.3" layer="91" />
                <label x="1061.72" y="1295.40" size="1.27" layer="95" />
                <pinref part="C232" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="1280.16" x2="1041.40" y2="1280.16" width="0.3" layer="91" />
                <label x="1041.40" y="1280.16" size="1.27" layer="95" />
                <pinref part="R403" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$1">
              <segment>
                <wire x1="1036.32" y1="1277.62" x2="1036.32" y2="1280.16" width="0.3" layer="91" />
                <label x="1036.32" y="1280.16" size="1.27" layer="95" />
                <pinref part="C231" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1000.76" y1="1236.98" x2="998.22" y2="1236.98" width="0.3" layer="91" />
                <label x="998.22" y="1236.98" size="1.27" layer="95" />
                <pinref part="R402" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1010.92" y1="1229.36" x2="1013.46" y2="1229.36" width="0.3" layer="91" />
                <label x="1013.46" y="1229.36" size="1.27" layer="95" />
                <pinref part="R401" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$2">
              <segment>
                <wire x1="1061.72" y1="1285.24" x2="1061.72" y2="1282.70" width="0.3" layer="91" />
                <label x="1061.72" y="1282.70" size="1.27" layer="95" />
                <pinref part="C232" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1092.20" y1="1295.40" x2="1089.66" y2="1295.40" width="0.3" layer="91" />
                <label x="1089.66" y="1295.40" size="1.27" layer="95" />
                <pinref part="R406" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1092.20" y1="1308.10" x2="1092.20" y2="1310.64" width="0.3" layer="91" />
                <label x="1092.20" y="1310.64" size="1.27" layer="95" />
                <pinref part="C233" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$3">
              <segment>
                <wire x1="1092.20" y1="1300.48" x2="1092.20" y2="1297.94" width="0.3" layer="91" />
                <label x="1092.20" y="1297.94" size="1.27" layer="95" />
                <pinref part="C233" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1115.06" y1="1336.04" x2="1112.52" y2="1336.04" width="0.3" layer="91" />
                <label x="1112.52" y="1336.04" size="1.27" layer="95" />
                <pinref part="Q51" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="1109.98" y1="1320.80" x2="1112.52" y2="1320.80" width="0.3" layer="91" />
                <label x="1112.52" y="1320.80" size="1.27" layer="95" />
                <pinref part="R404" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$4">
              <segment>
                <wire x1="982.98" y1="1216.66" x2="982.98" y2="1219.20" width="0.3" layer="91" />
                <label x="982.98" y="1219.20" size="1.27" layer="95" />
                <pinref part="C234" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1150.62" y1="1328.42" x2="1153.16" y2="1328.42" width="0.3" layer="91" />
                <label x="1153.16" y="1328.42" size="1.27" layer="95" />
                <pinref part="R405" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1130.30" y1="1328.42" x2="1132.84" y2="1328.42" width="0.3" layer="91" />
                <label x="1132.84" y="1328.42" size="1.27" layer="95" />
                <pinref part="Q51" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="1102.36" y1="1295.40" x2="1104.90" y2="1295.40" width="0.3" layer="91" />
                <label x="1104.90" y="1295.40" size="1.27" layer="95" />
                <pinref part="R406" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$5">
              <segment>
                <wire x1="982.98" y1="1209.04" x2="982.98" y2="1206.50" width="0.3" layer="91" />
                <label x="982.98" y="1206.50" size="1.27" layer="95" />
                <pinref part="C234" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1036.32" y1="1259.84" x2="1036.32" y2="1262.38" width="0.3" layer="91" />
                <label x="1036.32" y="1262.38" size="1.27" layer="95" />
                <pinref part="C235" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="982.98" y1="1229.36" x2="980.44" y2="1229.36" width="0.3" layer="91" />
                <label x="980.44" y="1229.36" size="1.27" layer="95" />
                <pinref part="R409" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$6">
              <segment>
                <wire x1="1036.32" y1="1252.22" x2="1036.32" y2="1249.68" width="0.3" layer="91" />
                <label x="1036.32" y="1249.68" size="1.27" layer="95" />
                <pinref part="C235" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1054.10" y1="1272.54" x2="1056.64" y2="1272.54" width="0.3" layer="91" />
                <label x="1056.64" y="1272.54" size="1.27" layer="95" />
                <pinref part="R407" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1066.80" y1="1303.02" x2="1064.26" y2="1303.02" width="0.3" layer="91" />
                <label x="1064.26" y="1303.02" size="1.27" layer="95" />
                <pinref part="Q50" gate="G$1" pin="B" />
              </segment>
            </net>
            <net name="N$7">
              <segment>
                <wire x1="1099.82" y1="1300.48" x2="1099.82" y2="1297.94" width="0.3" layer="91" />
                <label x="1099.82" y="1297.94" size="1.27" layer="95" />
                <pinref part="C236" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1117.60" y1="1320.80" x2="1115.06" y2="1320.80" width="0.3" layer="91" />
                <label x="1115.06" y="1320.80" size="1.27" layer="95" />
                <pinref part="R412" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1140.46" y1="1341.12" x2="1140.46" y2="1343.66" width="0.3" layer="91" />
                <label x="1140.46" y="1343.66" size="1.27" layer="95" />
                <pinref part="C237" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$8">
              <segment>
                <wire x1="1099.82" y1="1308.10" x2="1099.82" y2="1310.64" width="0.3" layer="91" />
                <label x="1099.82" y="1310.64" size="1.27" layer="95" />
                <pinref part="C236" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1079.50" y1="1287.78" x2="1082.04" y2="1287.78" width="0.3" layer="91" />
                <label x="1082.04" y="1287.78" size="1.27" layer="95" />
                <pinref part="R408" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1082.04" y1="1295.40" x2="1084.58" y2="1295.40" width="0.3" layer="91" />
                <label x="1084.58" y="1295.40" size="1.27" layer="95" />
                <pinref part="Q50" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="993.14" y1="1229.36" x2="995.68" y2="1229.36" width="0.3" layer="91" />
                <label x="995.68" y="1229.36" size="1.27" layer="95" />
                <pinref part="R409" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$9">
              <segment>
                <wire x1="1140.46" y1="1333.50" x2="1140.46" y2="1330.96" width="0.3" layer="91" />
                <label x="1140.46" y="1330.96" size="1.27" layer="95" />
                <pinref part="C237" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1163.32" y1="1351.28" x2="1160.78" y2="1351.28" width="0.3" layer="91" />
                <label x="1160.78" y="1351.28" size="1.27" layer="95" />
                <pinref part="Q49" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="1158.24" y1="1336.04" x2="1160.78" y2="1336.04" width="0.3" layer="91" />
                <label x="1160.78" y="1336.04" size="1.27" layer="95" />
                <pinref part="R410" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$10">
              <segment>
                <wire x1="1018.54" y1="1234.44" x2="1018.54" y2="1231.90" width="0.3" layer="91" />
                <label x="1018.54" y="1231.90" size="1.27" layer="95" />
                <pinref part="C238" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1061.72" y1="1280.16" x2="1059.18" y2="1280.16" width="0.3" layer="91" />
                <label x="1059.18" y="1280.16" size="1.27" layer="95" />
                <pinref part="R419" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1028.70" y1="1254.76" x2="1031.24" y2="1254.76" width="0.3" layer="91" />
                <label x="1031.24" y="1254.76" size="1.27" layer="95" />
                <pinref part="R413" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$11">
              <segment>
                <wire x1="1018.54" y1="1242.06" x2="1018.54" y2="1244.60" width="0.3" layer="91" />
                <label x="1018.54" y="1244.60" size="1.27" layer="95" />
                <pinref part="C238" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1158.24" y1="1343.66" x2="1160.78" y2="1343.66" width="0.3" layer="91" />
                <label x="1160.78" y="1343.66" size="1.27" layer="95" />
                <pinref part="R411" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1178.56" y1="1343.66" x2="1181.10" y2="1343.66" width="0.3" layer="91" />
                <label x="1181.10" y="1343.66" size="1.27" layer="95" />
                <pinref part="Q49" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="1127.76" y1="1320.80" x2="1130.30" y2="1320.80" width="0.3" layer="91" />
                <label x="1130.30" y="1320.80" size="1.27" layer="95" />
                <pinref part="R412" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$12">
              <segment>
                <wire x1="1178.56" y1="1358.90" x2="1181.10" y2="1358.90" width="0.3" layer="91" />
                <label x="1181.10" y="1358.90" size="1.27" layer="95" />
                <pinref part="Q49" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="1181.10" y1="1546.86" x2="1178.56" y2="1546.86" width="0.3" layer="91" />
                <label x="1178.56" y="1546.86" size="1.27" layer="95" />
                <pinref part="R421" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1046.48" y1="1450.34" x2="1049.02" y2="1450.34" width="0.3" layer="91" />
                <label x="1049.02" y="1450.34" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1173.48" y1="1557.02" x2="1176.02" y2="1557.02" width="0.3" layer="91" />
                <label x="1176.02" y="1557.02" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="1521.46" x2="1158.24" y2="1521.46" width="0.3" layer="91" />
                <label x="1158.24" y="1521.46" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1137.92" y1="1493.52" x2="1140.46" y2="1493.52" width="0.3" layer="91" />
                <label x="1140.46" y="1493.52" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1102.36" y1="1457.96" x2="1104.90" y2="1457.96" width="0.3" layer="91" />
                <label x="1104.90" y="1457.96" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1953.26" y1="2219.96" x2="1955.80" y2="2219.96" width="0.3" layer="91" />
                <label x="1955.80" y="2219.96" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="2065.02" y1="2263.14" x2="2067.56" y2="2263.14" width="0.3" layer="91" />
                <label x="2067.56" y="2263.14" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="2026.92" y1="2263.14" x2="2029.46" y2="2263.14" width="0.3" layer="91" />
                <label x="2029.46" y="2263.14" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="2009.14" y1="2227.58" x2="2011.68" y2="2227.58" width="0.3" layer="91" />
                <label x="2011.68" y="2227.58" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1935.48" y1="2184.40" x2="1938.02" y2="2184.40" width="0.3" layer="91" />
                <label x="1938.02" y="2184.40" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1277.62" y1="1640.84" x2="1277.62" y2="1643.38" width="0.3" layer="91" />
                <label x="1277.62" y="1643.38" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1277.62" y1="1714.50" x2="1277.62" y2="1717.04" width="0.3" layer="91" />
                <label x="1277.62" y="1717.04" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1374.14" y1="1757.68" x2="1374.14" y2="1760.22" width="0.3" layer="91" />
                <label x="1374.14" y="1760.22" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="855.98" y1="335.28" x2="853.44" y2="335.28" width="0.3" layer="91" />
                <label x="853.44" y="335.28" size="1.27" layer="95" />
                <pinref part="R300" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="736.60" x2="1363.98" y2="736.60" width="0.3" layer="91" />
                <label x="1363.98" y="736.60" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="708.66" x2="1325.88" y2="708.66" width="0.3" layer="91" />
                <label x="1325.88" y="708.66" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="764.54" x2="1363.98" y2="764.54" width="0.3" layer="91" />
                <label x="1363.98" y="764.54" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="680.72" x2="1287.78" y2="680.72" width="0.3" layer="91" />
                <label x="1287.78" y="680.72" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="652.78" x2="1249.68" y2="652.78" width="0.3" layer="91" />
                <label x="1249.68" y="652.78" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="736.60" x2="1325.88" y2="736.60" width="0.3" layer="91" />
                <label x="1325.88" y="736.60" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="708.66" x2="1287.78" y2="708.66" width="0.3" layer="91" />
                <label x="1287.78" y="708.66" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="680.72" x2="1249.68" y2="680.72" width="0.3" layer="91" />
                <label x="1249.68" y="680.72" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="251.46" y1="203.20" x2="254.00" y2="203.20" width="0.3" layer="91" />
                <label x="254.00" y="203.20" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="347.98" y1="309.88" x2="350.52" y2="309.88" width="0.3" layer="91" />
                <label x="350.52" y="309.88" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="772.16" y1="510.54" x2="769.62" y2="510.54" width="0.3" layer="91" />
                <label x="769.62" y="510.54" size="1.27" layer="95" />
                <pinref part="R298" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="457.20" y1="205.74" x2="459.74" y2="205.74" width="0.3" layer="91" />
                <label x="459.74" y="205.74" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="543.56" y1="302.26" x2="546.10" y2="302.26" width="0.3" layer="91" />
                <label x="546.10" y="302.26" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="452.12" y1="421.64" x2="454.66" y2="421.64" width="0.3" layer="91" />
                <label x="454.66" y="421.64" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="548.64" y1="518.16" x2="551.18" y2="518.16" width="0.3" layer="91" />
                <label x="551.18" y="518.16" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="665.48" y1="401.32" x2="668.02" y2="401.32" width="0.3" layer="91" />
                <label x="668.02" y="401.32" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="751.84" y1="487.68" x2="754.38" y2="487.68" width="0.3" layer="91" />
                <label x="754.38" y="487.68" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="657.86" y1="607.06" x2="660.40" y2="607.06" width="0.3" layer="91" />
                <label x="660.40" y="607.06" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="744.22" y1="703.58" x2="746.76" y2="703.58" width="0.3" layer="91" />
                <label x="746.76" y="703.58" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="2237.74" y1="251.46" x2="2240.28" y2="251.46" width="0.3" layer="91" />
                <label x="2240.28" y="251.46" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="2254.25" y1="289.56" x2="2254.25" y2="292.10" width="0.3" layer="91" />
                <label x="2254.25" y="292.10" size="1.27" layer="95" />
                <pinref part="VR8" gate="G$1" pin="W" />
              </segment>
              <segment>
                <wire x1="2265.68" y1="276.86" x2="2268.22" y2="276.86" width="0.3" layer="91" />
                <label x="2268.22" y="276.86" size="1.27" layer="95" />
                <pinref part="VR8" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2146.30" y1="154.94" x2="2148.84" y2="154.94" width="0.3" layer="91" />
                <label x="2148.84" y="154.94" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="114.30" y1="2298.70" x2="114.30" y2="2301.24" width="0.3" layer="91" />
                <label x="114.30" y="2301.24" size="1.27" layer="95" />
                <pinref part="C705" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="241.30" y1="2420.62" x2="243.84" y2="2420.62" width="0.3" layer="91" />
                <label x="243.84" y="2420.62" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="754.38" y1="2667.00" x2="751.84" y2="2667.00" width="0.3" layer="91" />
                <label x="751.84" y="2667.00" size="1.27" layer="95" />
                <pinref part="VR4" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="593.09" y1="2697.48" x2="595.63" y2="2697.48" width="0.3" layer="91" />
                <label x="595.63" y="2697.48" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="600.71" y1="2712.72" x2="598.17" y2="2712.72" width="0.3" layer="91" />
                <label x="598.17" y="2712.72" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2659.38" x2="509.27" y2="2659.38" width="0.3" layer="91" />
                <label x="509.27" y="2659.38" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="514.35" y1="2674.62" x2="511.81" y2="2674.62" width="0.3" layer="91" />
                <label x="511.81" y="2674.62" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="683.26" y1="2776.22" x2="680.72" y2="2776.22" width="0.3" layer="91" />
                <label x="680.72" y="2776.22" size="1.27" layer="95" />
                <pinref part="R2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="661.67" y1="2730.50" x2="659.13" y2="2730.50" width="0.3" layer="91" />
                <label x="659.13" y="2730.50" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="676.91" y1="2738.12" x2="679.45" y2="2738.12" width="0.3" layer="91" />
                <label x="679.45" y="2738.12" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="721.36" y1="2667.00" x2="718.82" y2="2667.00" width="0.3" layer="91" />
                <label x="718.82" y="2667.00" size="1.27" layer="95" />
                <pinref part="VR5" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="593.09" y1="2562.86" x2="595.63" y2="2562.86" width="0.3" layer="91" />
                <label x="595.63" y="2562.86" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="600.71" y1="2578.10" x2="598.17" y2="2578.10" width="0.3" layer="91" />
                <label x="598.17" y="2578.10" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2524.76" x2="509.27" y2="2524.76" width="0.3" layer="91" />
                <label x="509.27" y="2524.76" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="514.35" y1="2540.00" x2="511.81" y2="2540.00" width="0.3" layer="91" />
                <label x="511.81" y="2540.00" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="661.67" y1="2595.88" x2="659.13" y2="2595.88" width="0.3" layer="91" />
                <label x="659.13" y="2595.88" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="683.26" y1="2641.60" x2="680.72" y2="2641.60" width="0.3" layer="91" />
                <label x="680.72" y="2641.60" size="1.27" layer="95" />
                <pinref part="R2$1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="676.91" y1="2603.50" x2="679.45" y2="2603.50" width="0.3" layer="91" />
                <label x="679.45" y="2603.50" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="787.40" y1="2700.02" x2="789.94" y2="2700.02" width="0.3" layer="91" />
                <label x="789.94" y="2700.02" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="861.06" y1="2715.26" x2="863.60" y2="2715.26" width="0.3" layer="91" />
                <label x="863.60" y="2715.26" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="289.56" y1="2397.76" x2="287.02" y2="2397.76" width="0.3" layer="91" />
                <label x="287.02" y="2397.76" size="1.27" layer="95" />
                <pinref part="R175" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="513.08" y1="2979.42" x2="515.62" y2="2979.42" width="0.3" layer="91" />
                <label x="515.62" y="2979.42" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="656.59" y1="3068.32" x2="659.13" y2="3068.32" width="0.3" layer="91" />
                <label x="659.13" y="3068.32" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="664.21" y1="3083.56" x2="661.67" y2="3083.56" width="0.3" layer="91" />
                <label x="661.67" y="3083.56" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="746.76" y1="3147.06" x2="744.22" y2="3147.06" width="0.3" layer="91" />
                <label x="744.22" y="3147.06" size="1.27" layer="95" />
                <pinref part="R2$2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="725.17" y1="3101.34" x2="722.63" y2="3101.34" width="0.3" layer="91" />
                <label x="722.63" y="3101.34" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="570.23" y1="3030.22" x2="572.77" y2="3030.22" width="0.3" layer="91" />
                <label x="572.77" y="3030.22" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="577.85" y1="3045.46" x2="575.31" y2="3045.46" width="0.3" layer="91" />
                <label x="575.31" y="3045.46" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="740.41" y1="3108.96" x2="742.95" y2="3108.96" width="0.3" layer="91" />
                <label x="742.95" y="3108.96" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="401.32" y1="2926.08" x2="403.86" y2="2926.08" width="0.3" layer="91" />
                <label x="403.86" y="2926.08" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="355.60" y1="2862.58" x2="358.14" y2="2862.58" width="0.3" layer="91" />
                <label x="358.14" y="2862.58" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="871.22" y1="3096.26" x2="873.76" y2="3096.26" width="0.3" layer="91" />
                <label x="873.76" y="3096.26" size="1.27" layer="95" />
                <pinref part="Q18" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="58.42" y1="86.36" x2="55.88" y2="86.36" width="0.3" layer="91" />
                <label x="55.88" y="86.36" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-1" />
              </segment>
              <segment>
                <wire x1="1130.30" y1="1343.66" x2="1132.84" y2="1343.66" width="0.3" layer="91" />
                <label x="1132.84" y="1343.66" size="1.27" layer="95" />
                <pinref part="Q51" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="1082.04" y1="1310.64" x2="1084.58" y2="1310.64" width="0.3" layer="91" />
                <label x="1084.58" y="1310.64" size="1.27" layer="95" />
                <pinref part="Q50" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$13">
              <segment>
                <wire x1="1000.76" y1="1229.36" x2="998.22" y2="1229.36" width="0.3" layer="91" />
                <label x="998.22" y="1229.36" size="1.27" layer="95" />
                <pinref part="R401" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="157.48" y1="109.22" x2="154.94" y2="109.22" width="0.3" layer="91" />
                <label x="154.94" y="109.22" size="1.27" layer="95" />
                <pinref part="R301" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="160.02" y1="134.62" x2="157.48" y2="134.62" width="0.3" layer="91" />
                <label x="157.48" y="134.62" size="1.27" layer="95" />
                <pinref part="R311" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="365.76" y1="137.16" x2="363.22" y2="137.16" width="0.3" layer="91" />
                <label x="363.22" y="137.16" size="1.27" layer="95" />
                <pinref part="R331" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="363.22" y1="114.30" x2="360.68" y2="114.30" width="0.3" layer="91" />
                <label x="360.68" y="114.30" size="1.27" layer="95" />
                <pinref part="R321" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="363.22" y1="330.20" x2="360.68" y2="330.20" width="0.3" layer="91" />
                <label x="360.68" y="330.20" size="1.27" layer="95" />
                <pinref part="R341" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="363.22" y1="363.22" x2="360.68" y2="363.22" width="0.3" layer="91" />
                <label x="360.68" y="363.22" size="1.27" layer="95" />
                <pinref part="R351" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="584.20" y1="332.74" x2="581.66" y2="332.74" width="0.3" layer="91" />
                <label x="581.66" y="332.74" size="1.27" layer="95" />
                <pinref part="R361" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="566.42" y1="355.60" x2="563.88" y2="355.60" width="0.3" layer="91" />
                <label x="563.88" y="355.60" size="1.27" layer="95" />
                <pinref part="R371" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="563.88" y1="515.62" x2="561.34" y2="515.62" width="0.3" layer="91" />
                <label x="561.34" y="515.62" size="1.27" layer="95" />
                <pinref part="R381" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="566.42" y1="538.48" x2="563.88" y2="538.48" width="0.3" layer="91" />
                <label x="563.88" y="538.48" size="1.27" layer="95" />
                <pinref part="R391" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="58.42" y1="71.12" x2="55.88" y2="71.12" width="0.3" layer="91" />
                <label x="55.88" y="71.12" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-7" />
              </segment>
            </net>
            <net name="N$14">
              <segment>
                <wire x1="1010.92" y1="1236.98" x2="1013.46" y2="1236.98" width="0.3" layer="91" />
                <label x="1013.46" y="1236.98" size="1.27" layer="95" />
                <pinref part="R402" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="1440.18" x2="1013.46" y2="1440.18" width="0.3" layer="91" />
                <label x="1013.46" y="1440.18" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1046.48" y1="1435.10" x2="1049.02" y2="1435.10" width="0.3" layer="91" />
                <label x="1049.02" y="1435.10" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1071.88" y1="1447.80" x2="1069.34" y2="1447.80" width="0.3" layer="91" />
                <label x="1069.34" y="1447.80" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1102.36" y1="1442.72" x2="1104.90" y2="1442.72" width="0.3" layer="91" />
                <label x="1104.90" y="1442.72" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1143.00" y1="1546.86" x2="1140.46" y2="1546.86" width="0.3" layer="91" />
                <label x="1140.46" y="1546.86" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1173.48" y1="1541.78" x2="1176.02" y2="1541.78" width="0.3" layer="91" />
                <label x="1176.02" y="1541.78" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1125.22" y1="1511.30" x2="1122.68" y2="1511.30" width="0.3" layer="91" />
                <label x="1122.68" y="1511.30" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="1506.22" x2="1158.24" y2="1506.22" width="0.3" layer="91" />
                <label x="1158.24" y="1506.22" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1483.36" x2="1104.90" y2="1483.36" width="0.3" layer="91" />
                <label x="1104.90" y="1483.36" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1137.92" y1="1478.28" x2="1140.46" y2="1478.28" width="0.3" layer="91" />
                <label x="1140.46" y="1478.28" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="850.90" y1="1049.02" x2="853.44" y2="1049.02" width="0.3" layer="91" />
                <label x="853.44" y="1049.02" size="1.27" layer="95" />
                <pinref part="R442" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="876.30" y1="1066.80" x2="878.84" y2="1066.80" width="0.3" layer="91" />
                <label x="878.84" y="1066.80" size="1.27" layer="95" />
                <pinref part="R452" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1008.38" y1="1424.94" x2="1010.92" y2="1424.94" width="0.3" layer="91" />
                <label x="1010.92" y="1424.94" size="1.27" layer="95" />
                <pinref part="R532" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1000.76" y1="1399.54" x2="1003.30" y2="1399.54" width="0.3" layer="91" />
                <label x="1003.30" y="1399.54" size="1.27" layer="95" />
                <pinref part="R522" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="975.36" y1="1381.76" x2="977.90" y2="1381.76" width="0.3" layer="91" />
                <label x="977.90" y="1381.76" size="1.27" layer="95" />
                <pinref part="R512" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="949.96" y1="1178.56" x2="952.50" y2="1178.56" width="0.3" layer="91" />
                <label x="952.50" y="1178.56" size="1.27" layer="95" />
                <pinref part="R502" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="942.34" y1="1153.16" x2="944.88" y2="1153.16" width="0.3" layer="91" />
                <label x="944.88" y="1153.16" size="1.27" layer="95" />
                <pinref part="R492" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="916.94" y1="1135.38" x2="919.48" y2="1135.38" width="0.3" layer="91" />
                <label x="919.48" y="1135.38" size="1.27" layer="95" />
                <pinref part="R482" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="909.32" y1="1109.98" x2="911.86" y2="1109.98" width="0.3" layer="91" />
                <label x="911.86" y="1109.98" size="1.27" layer="95" />
                <pinref part="R472" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="876.30" y1="1074.42" x2="878.84" y2="1074.42" width="0.3" layer="91" />
                <label x="878.84" y="1074.42" size="1.27" layer="95" />
                <pinref part="R462" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1209.04" y1="1546.86" x2="1211.58" y2="1546.86" width="0.3" layer="91" />
                <label x="1211.58" y="1546.86" size="1.27" layer="95" />
                <pinref part="R422" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="45.72" y1="78.74" x2="43.18" y2="78.74" width="0.3" layer="91" />
                <label x="43.18" y="78.74" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-4" />
              </segment>
              <segment>
                <wire x1="45.72" y1="76.20" x2="43.18" y2="76.20" width="0.3" layer="91" />
                <label x="43.18" y="76.20" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-5" />
              </segment>
              <segment>
                <wire x1="45.72" y1="63.50" x2="43.18" y2="63.50" width="0.3" layer="91" />
                <label x="43.18" y="63.50" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-10" />
              </segment>
              <segment>
                <wire x1="58.42" y1="60.96" x2="55.88" y2="60.96" width="0.3" layer="91" />
                <label x="55.88" y="60.96" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-11" />
              </segment>
              <segment>
                <wire x1="58.42" y1="63.50" x2="55.88" y2="63.50" width="0.3" layer="91" />
                <label x="55.88" y="63.50" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-10" />
              </segment>
              <segment>
                <wire x1="33.02" y1="86.36" x2="30.48" y2="86.36" width="0.3" layer="91" />
                <label x="30.48" y="86.36" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-1" />
              </segment>
              <segment>
                <wire x1="830.58" y1="116.84" x2="830.58" y2="119.38" width="0.3" layer="91" />
                <label x="830.58" y="119.38" size="1.27" layer="95" />
                <pinref part="C135" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="848.36" y1="340.36" x2="848.36" y2="342.90" width="0.3" layer="91" />
                <label x="848.36" y="342.90" size="1.27" layer="95" />
                <pinref part="C225" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="848.36" y1="322.58" x2="848.36" y2="325.12" width="0.3" layer="91" />
                <label x="848.36" y="325.12" size="1.27" layer="95" />
                <pinref part="C215" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="840.74" y1="322.58" x2="840.74" y2="325.12" width="0.3" layer="91" />
                <label x="840.74" y="325.12" size="1.27" layer="95" />
                <pinref part="C205" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="840.74" y1="304.80" x2="840.74" y2="307.34" width="0.3" layer="91" />
                <label x="840.74" y="307.34" size="1.27" layer="95" />
                <pinref part="C195" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="833.12" y1="304.80" x2="833.12" y2="307.34" width="0.3" layer="91" />
                <label x="833.12" y="307.34" size="1.27" layer="95" />
                <pinref part="C185" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="833.12" y1="287.02" x2="833.12" y2="289.56" width="0.3" layer="91" />
                <label x="833.12" y="289.56" size="1.27" layer="95" />
                <pinref part="C175" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="820.42" y1="177.80" x2="820.42" y2="180.34" width="0.3" layer="91" />
                <label x="820.42" y="180.34" size="1.27" layer="95" />
                <pinref part="C165" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="830.58" y1="157.48" x2="830.58" y2="160.02" width="0.3" layer="91" />
                <label x="830.58" y="160.02" size="1.27" layer="95" />
                <pinref part="C155" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="830.58" y1="137.16" x2="830.58" y2="139.70" width="0.3" layer="91" />
                <label x="830.58" y="139.70" size="1.27" layer="95" />
                <pinref part="C145" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1178.56" y1="551.18" x2="1178.56" y2="553.72" width="0.3" layer="91" />
                <label x="1178.56" y="553.72" size="1.27" layer="95" />
                <pinref part="C137" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1209.04" y1="640.08" x2="1209.04" y2="642.62" width="0.3" layer="91" />
                <label x="1209.04" y="642.62" size="1.27" layer="95" />
                <pinref part="C227" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1209.04" y1="622.30" x2="1209.04" y2="624.84" width="0.3" layer="91" />
                <label x="1209.04" y="624.84" size="1.27" layer="95" />
                <pinref part="C217" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1201.42" y1="622.30" x2="1201.42" y2="624.84" width="0.3" layer="91" />
                <label x="1201.42" y="624.84" size="1.27" layer="95" />
                <pinref part="C207" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1201.42" y1="604.52" x2="1201.42" y2="607.06" width="0.3" layer="91" />
                <label x="1201.42" y="607.06" size="1.27" layer="95" />
                <pinref part="C197" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1193.80" y1="604.52" x2="1193.80" y2="607.06" width="0.3" layer="91" />
                <label x="1193.80" y="607.06" size="1.27" layer="95" />
                <pinref part="C187" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1193.80" y1="586.74" x2="1193.80" y2="589.28" width="0.3" layer="91" />
                <label x="1193.80" y="589.28" size="1.27" layer="95" />
                <pinref part="C177" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1186.18" y1="586.74" x2="1186.18" y2="589.28" width="0.3" layer="91" />
                <label x="1186.18" y="589.28" size="1.27" layer="95" />
                <pinref part="C167" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1186.18" y1="568.96" x2="1186.18" y2="571.50" width="0.3" layer="91" />
                <label x="1186.18" y="571.50" size="1.27" layer="95" />
                <pinref part="C157" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1178.56" y1="568.96" x2="1178.56" y2="571.50" width="0.3" layer="91" />
                <label x="1178.56" y="571.50" size="1.27" layer="95" />
                <pinref part="C147" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="680.72" x2="1214.12" y2="680.72" width="0.3" layer="91" />
                <label x="1214.12" y="680.72" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="AIN" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="665.48" x2="1249.68" y2="665.48" width="0.3" layer="91" />
                <label x="1249.68" y="665.48" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="CIN" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="673.10" x2="1249.68" y2="673.10" width="0.3" layer="91" />
                <label x="1249.68" y="673.10" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="DIN" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="673.10" x2="1214.12" y2="673.10" width="0.3" layer="91" />
                <label x="1214.12" y="673.10" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="BIN" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="708.66" x2="1252.22" y2="708.66" width="0.3" layer="91" />
                <label x="1252.22" y="708.66" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="AIN" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="701.04" x2="1287.78" y2="701.04" width="0.3" layer="91" />
                <label x="1287.78" y="701.04" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="DIN" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="693.42" x2="1287.78" y2="693.42" width="0.3" layer="91" />
                <label x="1287.78" y="693.42" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="CIN" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="728.98" x2="1325.88" y2="728.98" width="0.3" layer="91" />
                <label x="1325.88" y="728.98" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="DIN" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="721.36" x2="1325.88" y2="721.36" width="0.3" layer="91" />
                <label x="1325.88" y="721.36" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="CIN" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="728.98" x2="1290.32" y2="728.98" width="0.3" layer="91" />
                <label x="1290.32" y="728.98" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="BIN" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="736.60" x2="1290.32" y2="736.60" width="0.3" layer="91" />
                <label x="1290.32" y="736.60" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="AIN" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="734.06" x2="1290.32" y2="734.06" width="0.3" layer="91" />
                <label x="1290.32" y="734.06" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="AOUT" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="734.06" x2="1325.88" y2="734.06" width="0.3" layer="91" />
                <label x="1325.88" y="734.06" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="CTRLA" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="701.04" x2="1252.22" y2="701.04" width="0.3" layer="91" />
                <label x="1252.22" y="701.04" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="BIN" />
              </segment>
              <segment>
                <wire x1="220.98" y1="193.04" x2="218.44" y2="193.04" width="0.3" layer="91" />
                <label x="218.44" y="193.04" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="256.54" y1="238.76" x2="254.00" y2="238.76" width="0.3" layer="91" />
                <label x="254.00" y="238.76" size="1.27" layer="95" />
                <pinref part="R305" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="213.36" y1="177.80" x2="215.90" y2="177.80" width="0.3" layer="91" />
                <label x="215.90" y="177.80" size="1.27" layer="95" />
                <pinref part="R312" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="284.48" y1="256.54" x2="287.02" y2="256.54" width="0.3" layer="91" />
                <label x="287.02" y="256.54" size="1.27" layer="95" />
                <pinref part="R315" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="180.34" y1="142.24" x2="182.88" y2="142.24" width="0.3" layer="91" />
                <label x="182.88" y="142.24" size="1.27" layer="95" />
                <pinref part="R302" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="251.46" y1="187.96" x2="254.00" y2="187.96" width="0.3" layer="91" />
                <label x="254.00" y="187.96" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="787.40" y1="518.16" x2="789.94" y2="518.16" width="0.3" layer="91" />
                <label x="789.94" y="518.16" size="1.27" layer="95" />
                <pinref part="D36" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="795.02" y1="510.54" x2="795.02" y2="508.00" width="0.3" layer="91" />
                <label x="795.02" y="508.00" size="1.27" layer="95" />
                <pinref part="C130" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="426.72" y1="195.58" x2="424.18" y2="195.58" width="0.3" layer="91" />
                <label x="424.18" y="195.58" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="487.68" y1="259.08" x2="485.14" y2="259.08" width="0.3" layer="91" />
                <label x="485.14" y="259.08" size="1.27" layer="95" />
                <pinref part="R335" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="462.28" y1="241.30" x2="459.74" y2="241.30" width="0.3" layer="91" />
                <label x="459.74" y="241.30" size="1.27" layer="95" />
                <pinref part="R325" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="457.20" y1="190.50" x2="459.74" y2="190.50" width="0.3" layer="91" />
                <label x="459.74" y="190.50" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="419.10" y1="180.34" x2="421.64" y2="180.34" width="0.3" layer="91" />
                <label x="421.64" y="180.34" size="1.27" layer="95" />
                <pinref part="R332" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="386.08" y1="144.78" x2="388.62" y2="144.78" width="0.3" layer="91" />
                <label x="388.62" y="144.78" size="1.27" layer="95" />
                <pinref part="R322" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="421.64" y1="411.48" x2="419.10" y2="411.48" width="0.3" layer="91" />
                <label x="419.10" y="411.48" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="414.02" y1="396.24" x2="416.56" y2="396.24" width="0.3" layer="91" />
                <label x="416.56" y="396.24" size="1.27" layer="95" />
                <pinref part="R352" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="485.14" y1="474.98" x2="487.68" y2="474.98" width="0.3" layer="91" />
                <label x="487.68" y="474.98" size="1.27" layer="95" />
                <pinref part="R345" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="452.12" y1="406.40" x2="454.66" y2="406.40" width="0.3" layer="91" />
                <label x="454.66" y="406.40" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="398.78" y1="370.84" x2="401.32" y2="370.84" width="0.3" layer="91" />
                <label x="401.32" y="370.84" size="1.27" layer="95" />
                <pinref part="R342" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="510.54" y1="500.38" x2="513.08" y2="500.38" width="0.3" layer="91" />
                <label x="513.08" y="500.38" size="1.27" layer="95" />
                <pinref part="R355" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="635.00" y1="391.16" x2="632.46" y2="391.16" width="0.3" layer="91" />
                <label x="632.46" y="391.16" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="713.74" y1="462.28" x2="716.28" y2="462.28" width="0.3" layer="91" />
                <label x="716.28" y="462.28" size="1.27" layer="95" />
                <pinref part="R375" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="652.78" y1="419.10" x2="650.24" y2="419.10" width="0.3" layer="91" />
                <label x="650.24" y="419.10" size="1.27" layer="95" />
                <pinref part="R365" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="665.48" y1="386.08" x2="668.02" y2="386.08" width="0.3" layer="91" />
                <label x="668.02" y="386.08" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="627.38" y1="375.92" x2="629.92" y2="375.92" width="0.3" layer="91" />
                <label x="629.92" y="375.92" size="1.27" layer="95" />
                <pinref part="R372" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="594.36" y1="342.90" x2="596.90" y2="342.90" width="0.3" layer="91" />
                <label x="596.90" y="342.90" size="1.27" layer="95" />
                <pinref part="R362" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="627.38" y1="596.90" x2="624.84" y2="596.90" width="0.3" layer="91" />
                <label x="624.84" y="596.90" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="619.76" y1="581.66" x2="622.30" y2="581.66" width="0.3" layer="91" />
                <label x="622.30" y="581.66" size="1.27" layer="95" />
                <pinref part="R392" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="657.86" y1="591.82" x2="660.40" y2="591.82" width="0.3" layer="91" />
                <label x="660.40" y="591.82" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="706.12" y1="678.18" x2="708.66" y2="678.18" width="0.3" layer="91" />
                <label x="708.66" y="678.18" size="1.27" layer="95" />
                <pinref part="R395" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="662.94" y1="642.62" x2="660.40" y2="642.62" width="0.3" layer="91" />
                <label x="660.40" y="642.62" size="1.27" layer="95" />
                <pinref part="R385" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="586.74" y1="546.10" x2="589.28" y2="546.10" width="0.3" layer="91" />
                <label x="589.28" y="546.10" size="1.27" layer="95" />
                <pinref part="R382" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2080.26" y1="129.54" x2="2077.72" y2="129.54" width="0.3" layer="91" />
                <label x="2077.72" y="129.54" size="1.27" layer="95" />
                <pinref part="R430" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2080.26" y1="121.92" x2="2077.72" y2="121.92" width="0.3" layer="91" />
                <label x="2077.72" y="121.92" size="1.27" layer="95" />
                <pinref part="R429" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2171.70" y1="203.20" x2="2171.70" y2="205.74" width="0.3" layer="91" />
                <label x="2171.70" y="205.74" size="1.27" layer="95" />
                <pinref part="C240" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="2171.70" y1="215.90" x2="2169.16" y2="215.90" width="0.3" layer="91" />
                <label x="2169.16" y="215.90" size="1.27" layer="95" />
                <pinref part="R415" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2235.20" y1="269.24" x2="2237.74" y2="269.24" width="0.3" layer="91" />
                <label x="2237.74" y="269.24" size="1.27" layer="95" />
                <pinref part="R418" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="121.92" y1="2303.78" x2="121.92" y2="2306.32" width="0.3" layer="91" />
                <label x="121.92" y="2306.32" size="1.27" layer="95" />
                <pinref part="C706" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="114.30" y1="2291.08" x2="114.30" y2="2288.54" width="0.3" layer="91" />
                <label x="114.30" y="2288.54" size="1.27" layer="95" />
                <pinref part="C705" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="210.82" y1="2410.46" x2="208.28" y2="2410.46" width="0.3" layer="91" />
                <label x="208.28" y="2410.46" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="337.82" y1="2463.80" x2="340.36" y2="2463.80" width="0.3" layer="91" />
                <label x="340.36" y="2463.80" size="1.27" layer="95" />
                <pinref part="Q29" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="459.74" y1="2509.52" x2="457.20" y2="2509.52" width="0.3" layer="91" />
                <label x="457.20" y="2509.52" size="1.27" layer="95" />
                <pinref part="R153" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="281.94" y1="2387.60" x2="281.94" y2="2385.06" width="0.3" layer="91" />
                <label x="281.94" y="2385.06" size="1.27" layer="95" />
                <pinref part="C54" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="861.06" y1="2700.02" x2="863.60" y2="2700.02" width="0.3" layer="91" />
                <label x="863.60" y="2700.02" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="830.58" y1="2705.10" x2="828.04" y2="2705.10" width="0.3" layer="91" />
                <label x="828.04" y="2705.10" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="787.40" y1="2684.78" x2="789.94" y2="2684.78" width="0.3" layer="91" />
                <label x="789.94" y="2684.78" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="279.40" y1="2339.34" x2="276.86" y2="2339.34" width="0.3" layer="91" />
                <label x="276.86" y="2339.34" size="1.27" layer="95" />
                <pinref part="R172" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="406.40" y1="2494.28" x2="403.86" y2="2494.28" width="0.3" layer="91" />
                <label x="403.86" y="2494.28" size="1.27" layer="95" />
                <pinref part="R151" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="317.50" y1="2423.16" x2="320.04" y2="2423.16" width="0.3" layer="91" />
                <label x="320.04" y="2423.16" size="1.27" layer="95" />
                <pinref part="R163" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="388.62" y1="2486.66" x2="386.08" y2="2486.66" width="0.3" layer="91" />
                <label x="386.08" y="2486.66" size="1.27" layer="95" />
                <pinref part="R157" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="424.18" y1="2501.90" x2="421.64" y2="2501.90" width="0.3" layer="91" />
                <label x="421.64" y="2501.90" size="1.27" layer="95" />
                <pinref part="R156" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="756.92" y1="2689.86" x2="754.38" y2="2689.86" width="0.3" layer="91" />
                <label x="754.38" y="2689.86" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="307.34" y1="2849.88" x2="304.80" y2="2849.88" width="0.3" layer="91" />
                <label x="304.80" y="2849.88" size="1.27" layer="95" />
                <pinref part="R115" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="353.06" y1="2877.82" x2="353.06" y2="2880.36" width="0.3" layer="91" />
                <label x="353.06" y="2880.36" size="1.27" layer="95" />
                <pinref part="C49" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="353.06" y1="2890.52" x2="350.52" y2="2890.52" width="0.3" layer="91" />
                <label x="350.52" y="2890.52" size="1.27" layer="95" />
                <pinref part="R136" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="541.02" y1="3009.90" x2="541.02" y2="3012.44" width="0.3" layer="91" />
                <label x="541.02" y="3012.44" size="1.27" layer="95" />
                <pinref part="C44" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="505.46" y1="2989.58" x2="502.92" y2="2989.58" width="0.3" layer="91" />
                <label x="502.92" y="2989.58" size="1.27" layer="95" />
                <pinref part="R121" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="797.56" y1="3022.60" x2="800.10" y2="3022.60" width="0.3" layer="91" />
                <label x="800.10" y="3022.60" size="1.27" layer="95" />
                <pinref part="R125" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="815.34" y1="3037.84" x2="817.88" y2="3037.84" width="0.3" layer="91" />
                <label x="817.88" y="3037.84" size="1.27" layer="95" />
                <pinref part="R126" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="513.08" y1="2964.18" x2="515.62" y2="2964.18" width="0.3" layer="91" />
                <label x="515.62" y="2964.18" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="858.52" y1="3068.32" x2="858.52" y2="3070.86" width="0.3" layer="91" />
                <label x="858.52" y="3070.86" size="1.27" layer="95" />
                <pinref part="C46" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="325.12" y1="2852.42" x2="322.58" y2="2852.42" width="0.3" layer="91" />
                <label x="322.58" y="2852.42" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="482.60" y1="2969.26" x2="480.06" y2="2969.26" width="0.3" layer="91" />
                <label x="480.06" y="2969.26" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="426.72" y1="2928.62" x2="426.72" y2="2931.16" width="0.3" layer="91" />
                <label x="426.72" y="2931.16" size="1.27" layer="95" />
                <pinref part="C48" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1501.14" y1="1838.96" x2="1501.14" y2="1836.42" width="0.3" layer="91" />
                <label x="1501.14" y="1836.42" size="1.27" layer="95" />
                <pinref part="C243" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1922.78" y1="2209.80" x2="1920.24" y2="2209.80" width="0.3" layer="91" />
                <label x="1920.24" y="2209.80" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1953.26" y1="2204.72" x2="1955.80" y2="2204.72" width="0.3" layer="91" />
                <label x="1955.80" y="2204.72" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1905.00" y1="2174.24" x2="1902.46" y2="2174.24" width="0.3" layer="91" />
                <label x="1902.46" y="2174.24" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1935.48" y1="2169.16" x2="1938.02" y2="2169.16" width="0.3" layer="91" />
                <label x="1938.02" y="2169.16" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1737.36" y1="1996.44" x2="1739.90" y2="1996.44" width="0.3" layer="91" />
                <label x="1739.90" y="1996.44" size="1.27" layer="95" />
                <pinref part="R448" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1755.14" y1="1996.44" x2="1757.68" y2="1996.44" width="0.3" layer="91" />
                <label x="1757.68" y="1996.44" size="1.27" layer="95" />
                <pinref part="R458" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1755.14" y1="2004.06" x2="1757.68" y2="2004.06" width="0.3" layer="91" />
                <label x="1757.68" y="2004.06" size="1.27" layer="95" />
                <pinref part="R468" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1879.60" y1="2151.38" x2="1882.14" y2="2151.38" width="0.3" layer="91" />
                <label x="1882.14" y="2151.38" size="1.27" layer="95" />
                <pinref part="R538" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1854.20" y1="2133.60" x2="1856.74" y2="2133.60" width="0.3" layer="91" />
                <label x="1856.74" y="2133.60" size="1.27" layer="95" />
                <pinref part="R528" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2034.54" y1="2252.98" x2="2032.00" y2="2252.98" width="0.3" layer="91" />
                <label x="2032.00" y="2252.98" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="2065.02" y1="2247.90" x2="2067.56" y2="2247.90" width="0.3" layer="91" />
                <label x="2067.56" y="2247.90" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1846.58" y1="2108.20" x2="1849.12" y2="2108.20" width="0.3" layer="91" />
                <label x="1849.12" y="2108.20" size="1.27" layer="95" />
                <pinref part="R518" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1821.18" y1="2090.42" x2="1823.72" y2="2090.42" width="0.3" layer="91" />
                <label x="1823.72" y="2090.42" size="1.27" layer="95" />
                <pinref part="R508" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1996.44" y1="2252.98" x2="1993.90" y2="2252.98" width="0.3" layer="91" />
                <label x="1993.90" y="2252.98" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="2026.92" y1="2247.90" x2="2029.46" y2="2247.90" width="0.3" layer="91" />
                <label x="2029.46" y="2247.90" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1813.56" y1="2065.02" x2="1816.10" y2="2065.02" width="0.3" layer="91" />
                <label x="1816.10" y="2065.02" size="1.27" layer="95" />
                <pinref part="R498" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1788.16" y1="2047.24" x2="1790.70" y2="2047.24" width="0.3" layer="91" />
                <label x="1790.70" y="2047.24" size="1.27" layer="95" />
                <pinref part="R488" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1978.66" y1="2217.42" x2="1976.12" y2="2217.42" width="0.3" layer="91" />
                <label x="1976.12" y="2217.42" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="2009.14" y1="2212.34" x2="2011.68" y2="2212.34" width="0.3" layer="91" />
                <label x="2011.68" y="2212.34" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1780.54" y1="2021.84" x2="1783.08" y2="2021.84" width="0.3" layer="91" />
                <label x="1783.08" y="2021.84" size="1.27" layer="95" />
                <pinref part="R478" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1366.52" y1="1767.84" x2="1363.98" y2="1767.84" width="0.3" layer="91" />
                <label x="1363.98" y="1767.84" size="1.27" layer="95" />
                <pinref part="R446" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1384.30" y1="1767.84" x2="1381.76" y2="1767.84" width="0.3" layer="91" />
                <label x="1381.76" y="1767.84" size="1.27" layer="95" />
                <pinref part="R456" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1402.08" y1="1775.46" x2="1399.54" y2="1775.46" width="0.3" layer="91" />
                <label x="1399.54" y="1775.46" size="1.27" layer="95" />
                <pinref part="R476" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1483.36" y1="1833.88" x2="1480.82" y2="1833.88" width="0.3" layer="91" />
                <label x="1480.82" y="1833.88" size="1.27" layer="95" />
                <pinref part="R536" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1483.36" y1="1826.26" x2="1480.82" y2="1826.26" width="0.3" layer="91" />
                <label x="1480.82" y="1826.26" size="1.27" layer="95" />
                <pinref part="R526" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1501.14" y1="1833.88" x2="1498.60" y2="1833.88" width="0.3" layer="91" />
                <label x="1498.60" y="1833.88" size="1.27" layer="95" />
                <pinref part="R516" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1465.58" y1="1826.26" x2="1463.04" y2="1826.26" width="0.3" layer="91" />
                <label x="1463.04" y="1826.26" size="1.27" layer="95" />
                <pinref part="R506" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1402.08" y1="1783.08" x2="1399.54" y2="1783.08" width="0.3" layer="91" />
                <label x="1399.54" y="1783.08" size="1.27" layer="95" />
                <pinref part="R496" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1465.58" y1="1818.64" x2="1463.04" y2="1818.64" width="0.3" layer="91" />
                <label x="1463.04" y="1818.64" size="1.27" layer="95" />
                <pinref part="R486" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1384.30" y1="1775.46" x2="1381.76" y2="1775.46" width="0.3" layer="91" />
                <label x="1381.76" y="1775.46" size="1.27" layer="95" />
                <pinref part="R466" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1018.54" y1="1254.76" x2="1016.00" y2="1254.76" width="0.3" layer="91" />
                <label x="1016.00" y="1254.76" size="1.27" layer="95" />
                <pinref part="R413" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1099.82" y1="1320.80" x2="1097.28" y2="1320.80" width="0.3" layer="91" />
                <label x="1097.28" y="1320.80" size="1.27" layer="95" />
                <pinref part="R404" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="1272.54" x2="1041.40" y2="1272.54" width="0.3" layer="91" />
                <label x="1041.40" y="1272.54" size="1.27" layer="95" />
                <pinref part="R407" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1148.08" y1="1336.04" x2="1145.54" y2="1336.04" width="0.3" layer="91" />
                <label x="1145.54" y="1336.04" size="1.27" layer="95" />
                <pinref part="R410" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1054.10" y1="1280.16" x2="1056.64" y2="1280.16" width="0.3" layer="91" />
                <label x="1056.64" y="1280.16" size="1.27" layer="95" />
                <pinref part="R403" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$15">
              <segment>
                <wire x1="1140.46" y1="1328.42" x2="1137.92" y2="1328.42" width="0.3" layer="91" />
                <label x="1137.92" y="1328.42" size="1.27" layer="95" />
                <pinref part="R405" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1905.00" y1="2169.16" x2="1902.46" y2="2169.16" width="0.3" layer="91" />
                <label x="1902.46" y="2169.16" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1978.66" y1="2212.34" x2="1976.12" y2="2212.34" width="0.3" layer="91" />
                <label x="1976.12" y="2212.34" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1996.44" y1="2247.90" x2="1993.90" y2="2247.90" width="0.3" layer="91" />
                <label x="1993.90" y="2247.90" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="2034.54" y1="2247.90" x2="2032.00" y2="2247.90" width="0.3" layer="91" />
                <label x="2032.00" y="2247.90" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1922.78" y1="2204.72" x2="1920.24" y2="2204.72" width="0.3" layer="91" />
                <label x="1920.24" y="2204.72" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1071.88" y1="1442.72" x2="1069.34" y2="1442.72" width="0.3" layer="91" />
                <label x="1069.34" y="1442.72" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1478.28" x2="1104.90" y2="1478.28" width="0.3" layer="91" />
                <label x="1104.90" y="1478.28" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1125.22" y1="1506.22" x2="1122.68" y2="1506.22" width="0.3" layer="91" />
                <label x="1122.68" y="1506.22" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1143.00" y1="1541.78" x2="1140.46" y2="1541.78" width="0.3" layer="91" />
                <label x="1140.46" y="1541.78" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="1435.10" x2="1013.46" y2="1435.10" width="0.3" layer="91" />
                <label x="1013.46" y="1435.10" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="637.54" x2="1214.12" y2="637.54" width="0.3" layer="91" />
                <label x="1214.12" y="637.54" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="665.48" x2="1252.22" y2="665.48" width="0.3" layer="91" />
                <label x="1252.22" y="665.48" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="749.30" x2="1328.42" y2="749.30" width="0.3" layer="91" />
                <label x="1328.42" y="749.30" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="693.42" x2="1290.32" y2="693.42" width="0.3" layer="91" />
                <label x="1290.32" y="693.42" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="721.36" x2="1328.42" y2="721.36" width="0.3" layer="91" />
                <label x="1328.42" y="721.36" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="317.50" y1="294.64" x2="314.96" y2="294.64" width="0.3" layer="91" />
                <label x="314.96" y="294.64" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="220.98" y1="187.96" x2="218.44" y2="187.96" width="0.3" layer="91" />
                <label x="218.44" y="187.96" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="513.08" y1="287.02" x2="510.54" y2="287.02" width="0.3" layer="91" />
                <label x="510.54" y="287.02" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="426.72" y1="190.50" x2="424.18" y2="190.50" width="0.3" layer="91" />
                <label x="424.18" y="190.50" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="518.16" y1="502.92" x2="515.62" y2="502.92" width="0.3" layer="91" />
                <label x="515.62" y="502.92" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="421.64" y1="406.40" x2="419.10" y2="406.40" width="0.3" layer="91" />
                <label x="419.10" y="406.40" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="721.36" y1="472.44" x2="718.82" y2="472.44" width="0.3" layer="91" />
                <label x="718.82" y="472.44" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="635.00" y1="386.08" x2="632.46" y2="386.08" width="0.3" layer="91" />
                <label x="632.46" y="386.08" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="713.74" y1="688.34" x2="711.20" y2="688.34" width="0.3" layer="91" />
                <label x="711.20" y="688.34" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="627.38" y1="591.82" x2="624.84" y2="591.82" width="0.3" layer="91" />
                <label x="624.84" y="591.82" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="2115.82" y1="139.70" x2="2113.28" y2="139.70" width="0.3" layer="91" />
                <label x="2113.28" y="139.70" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="2207.26" y1="236.22" x2="2204.72" y2="236.22" width="0.3" layer="91" />
                <label x="2204.72" y="236.22" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="121.92" y1="2296.16" x2="121.92" y2="2293.62" width="0.3" layer="91" />
                <label x="121.92" y="2293.62" size="1.27" layer="95" />
                <pinref part="C706" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="210.82" y1="2405.38" x2="208.28" y2="2405.38" width="0.3" layer="91" />
                <label x="208.28" y="2405.38" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="152.40" y1="2341.88" x2="152.40" y2="2339.34" width="0.3" layer="91" />
                <label x="152.40" y="2339.34" size="1.27" layer="95" />
                <pinref part="C704" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="116.84" y1="2339.34" x2="114.30" y2="2339.34" width="0.3" layer="91" />
                <label x="114.30" y="2339.34" size="1.27" layer="95" />
                <pinref part="R713" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="830.58" y1="2700.02" x2="828.04" y2="2700.02" width="0.3" layer="91" />
                <label x="828.04" y="2700.02" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="756.92" y1="2684.78" x2="754.38" y2="2684.78" width="0.3" layer="91" />
                <label x="754.38" y="2684.78" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="746.76" y1="2667.00" x2="749.30" y2="2667.00" width="0.3" layer="91" />
                <label x="749.30" y="2667.00" size="1.27" layer="95" />
                <pinref part="VR5" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2684.78" x2="509.27" y2="2684.78" width="0.3" layer="91" />
                <label x="509.27" y="2684.78" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="514.35" y1="2700.02" x2="511.81" y2="2700.02" width="0.3" layer="91" />
                <label x="511.81" y="2700.02" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="636.27" y1="2707.64" x2="638.81" y2="2707.64" width="0.3" layer="91" />
                <label x="638.81" y="2707.64" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="643.89" y1="2722.88" x2="641.35" y2="2722.88" width="0.3" layer="91" />
                <label x="641.35" y="2722.88" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="704.85" y1="2753.36" x2="702.31" y2="2753.36" width="0.3" layer="91" />
                <label x="702.31" y="2753.36" size="1.27" layer="95" />
                <pinref part="Q8" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2753.36" x2="656.59" y2="2753.36" width="0.3" layer="91" />
                <label x="656.59" y="2753.36" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="661.67" y1="2768.60" x2="659.13" y2="2768.60" width="0.3" layer="91" />
                <label x="659.13" y="2768.60" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="622.30" y1="2730.50" x2="619.76" y2="2730.50" width="0.3" layer="91" />
                <label x="619.76" y="2730.50" size="1.27" layer="95" />
                <pinref part="R1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2550.16" x2="509.27" y2="2550.16" width="0.3" layer="91" />
                <label x="509.27" y="2550.16" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="514.35" y1="2565.40" x2="511.81" y2="2565.40" width="0.3" layer="91" />
                <label x="511.81" y="2565.40" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="636.27" y1="2573.02" x2="638.81" y2="2573.02" width="0.3" layer="91" />
                <label x="638.81" y="2573.02" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="643.89" y1="2588.26" x2="641.35" y2="2588.26" width="0.3" layer="91" />
                <label x="641.35" y="2588.26" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="704.85" y1="2618.74" x2="702.31" y2="2618.74" width="0.3" layer="91" />
                <label x="702.31" y="2618.74" size="1.27" layer="95" />
                <pinref part="Q8$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2618.74" x2="656.59" y2="2618.74" width="0.3" layer="91" />
                <label x="656.59" y="2618.74" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="661.67" y1="2633.98" x2="659.13" y2="2633.98" width="0.3" layer="91" />
                <label x="659.13" y="2633.98" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="622.30" y1="2595.88" x2="619.76" y2="2595.88" width="0.3" layer="91" />
                <label x="619.76" y="2595.88" size="1.27" layer="95" />
                <pinref part="R1$1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="779.78" y1="2667.00" x2="782.32" y2="2667.00" width="0.3" layer="91" />
                <label x="782.32" y="2667.00" size="1.27" layer="95" />
                <pinref part="VR4" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="370.84" y1="2910.84" x2="368.30" y2="2910.84" width="0.3" layer="91" />
                <label x="368.30" y="2910.84" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="325.12" y1="2847.34" x2="322.58" y2="2847.34" width="0.3" layer="91" />
                <label x="322.58" y="2847.34" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="699.77" y1="3078.48" x2="702.31" y2="3078.48" width="0.3" layer="91" />
                <label x="702.31" y="3078.48" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="707.39" y1="3093.72" x2="704.85" y2="3093.72" width="0.3" layer="91" />
                <label x="704.85" y="3093.72" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="570.23" y1="3055.62" x2="572.77" y2="3055.62" width="0.3" layer="91" />
                <label x="572.77" y="3055.62" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="577.85" y1="3070.86" x2="575.31" y2="3070.86" width="0.3" layer="91" />
                <label x="575.31" y="3070.86" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="768.35" y1="3124.20" x2="765.81" y2="3124.20" width="0.3" layer="91" />
                <label x="765.81" y="3124.20" size="1.27" layer="95" />
                <pinref part="Q8$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="717.55" y1="3124.20" x2="720.09" y2="3124.20" width="0.3" layer="91" />
                <label x="720.09" y="3124.20" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="725.17" y1="3139.44" x2="722.63" y2="3139.44" width="0.3" layer="91" />
                <label x="722.63" y="3139.44" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="685.80" y1="3101.34" x2="683.26" y2="3101.34" width="0.3" layer="91" />
                <label x="683.26" y="3101.34" size="1.27" layer="95" />
                <pinref part="R1$2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="482.60" y1="2964.18" x2="480.06" y2="2964.18" width="0.3" layer="91" />
                <label x="480.06" y="2964.18" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="881.38" y1="3081.02" x2="878.84" y2="3081.02" width="0.3" layer="91" />
                <label x="878.84" y="3081.02" size="1.27" layer="95" />
                <pinref part="R132" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="58.42" y1="58.42" x2="55.88" y2="58.42" width="0.3" layer="91" />
                <label x="55.88" y="58.42" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-12" />
              </segment>
              <segment>
                <wire x1="1069.34" y1="1287.78" x2="1066.80" y2="1287.78" width="0.3" layer="91" />
                <label x="1066.80" y="1287.78" size="1.27" layer="95" />
                <pinref part="R408" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1148.08" y1="1343.66" x2="1145.54" y2="1343.66" width="0.3" layer="91" />
                <label x="1145.54" y="1343.66" size="1.27" layer="95" />
                <pinref part="R411" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$16">
              <segment>
                <wire x1="1071.88" y1="1280.16" x2="1074.42" y2="1280.16" width="0.3" layer="91" />
                <label x="1074.42" y="1280.16" size="1.27" layer="95" />
                <pinref part="R419" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1828.80" x2="1417.32" y2="1828.80" width="0.3" layer="91" />
                <label x="1417.32" y="1828.80" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="AIN" />
              </segment>
            </net>
            <net name="N$17">
              <segment>
                <wire x1="1508.76" y1="1838.96" x2="1508.76" y2="1836.42" width="0.3" layer="91" />
                <label x="1508.76" y="1836.42" size="1.27" layer="95" />
                <pinref part="35" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1501.14" y1="1846.58" x2="1501.14" y2="1849.12" width="0.3" layer="91" />
                <label x="1501.14" y="1849.12" size="1.27" layer="95" />
                <pinref part="C243" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1468.12" y1="1811.02" x2="1470.66" y2="1811.02" width="0.3" layer="91" />
                <label x="1470.66" y="1811.02" size="1.27" layer="95" />
                <pinref part="R434" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="58.42" x2="30.48" y2="58.42" width="0.3" layer="91" />
                <label x="30.48" y="58.42" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-12" />
              </segment>
            </net>
            <net name="N$18">
              <segment>
                <wire x1="1457.96" y1="1816.10" x2="1457.96" y2="1813.56" width="0.3" layer="91" />
                <label x="1457.96" y="1813.56" size="1.27" layer="95" />
                <pinref part="37" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1366.52" y1="1704.34" x2="1363.98" y2="1704.34" width="0.3" layer="91" />
                <label x="1363.98" y="1704.34" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="-IN4" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1661.16" x2="1267.46" y2="1661.16" width="0.3" layer="91" />
                <label x="1267.46" y="1661.16" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="-IN4" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1673.86" x2="1267.46" y2="1673.86" width="0.3" layer="91" />
                <label x="1267.46" y="1673.86" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="-IN3" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1699.26" x2="1267.46" y2="1699.26" width="0.3" layer="91" />
                <label x="1267.46" y="1699.26" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="-IN1" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1587.50" x2="1267.46" y2="1587.50" width="0.3" layer="91" />
                <label x="1267.46" y="1587.50" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="-IN4" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1600.20" x2="1267.46" y2="1600.20" width="0.3" layer="91" />
                <label x="1267.46" y="1600.20" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="-IN3" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1625.60" x2="1267.46" y2="1625.60" width="0.3" layer="91" />
                <label x="1267.46" y="1625.60" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="-IN1" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1612.90" x2="1267.46" y2="1612.90" width="0.3" layer="91" />
                <label x="1267.46" y="1612.90" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="-IN2" />
              </segment>
              <segment>
                <wire x1="1270.00" y1="1686.56" x2="1267.46" y2="1686.56" width="0.3" layer="91" />
                <label x="1267.46" y="1686.56" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="-IN2" />
              </segment>
              <segment>
                <wire x1="1366.52" y1="1717.04" x2="1363.98" y2="1717.04" width="0.3" layer="91" />
                <label x="1363.98" y="1717.04" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="-IN3" />
              </segment>
              <segment>
                <wire x1="1366.52" y1="1729.74" x2="1363.98" y2="1729.74" width="0.3" layer="91" />
                <label x="1363.98" y="1729.74" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="-IN2" />
              </segment>
              <segment>
                <wire x1="2207.26" y1="251.46" x2="2204.72" y2="251.46" width="0.3" layer="91" />
                <label x="2204.72" y="251.46" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="2207.26" y1="246.38" x2="2204.72" y2="246.38" width="0.3" layer="91" />
                <label x="2204.72" y="246.38" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$19">
              <segment>
                <wire x1="840.74" y1="1028.70" x2="840.74" y2="1026.16" width="0.3" layer="91" />
                <label x="840.74" y="1026.16" size="1.27" layer="95" />
                <pinref part="C251" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="1445.26" x2="1013.46" y2="1445.26" width="0.3" layer="91" />
                <label x="1013.46" y="1445.26" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="1424.94" x2="1013.46" y2="1424.94" width="0.3" layer="91" />
                <label x="1013.46" y="1424.94" size="1.27" layer="95" />
                <pinref part="R443" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$20">
              <segment>
                <wire x1="840.74" y1="1036.32" x2="840.74" y2="1038.86" width="0.3" layer="91" />
                <label x="840.74" y="1038.86" size="1.27" layer="95" />
                <pinref part="C251" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="833.12" y1="1036.32" x2="833.12" y2="1038.86" width="0.3" layer="91" />
                <label x="833.12" y="1038.86" size="1.27" layer="95" />
                <pinref part="C252" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="840.74" y1="1049.02" x2="838.20" y2="1049.02" width="0.3" layer="91" />
                <label x="838.20" y="1049.02" size="1.27" layer="95" />
                <pinref part="R442" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="820.42" y1="789.94" x2="822.96" y2="789.94" width="0.3" layer="91" />
                <label x="822.96" y="789.94" size="1.27" layer="95" />
                <pinref part="R441" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$21">
              <segment>
                <wire x1="833.12" y1="1028.70" x2="833.12" y2="1026.16" width="0.3" layer="91" />
                <label x="833.12" y="1026.16" size="1.27" layer="95" />
                <pinref part="C252" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="1450.34" x2="1013.46" y2="1450.34" width="0.3" layer="91" />
                <label x="1013.46" y="1450.34" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="1424.94" x2="1028.70" y2="1424.94" width="0.3" layer="91" />
                <label x="1028.70" y="1424.94" size="1.27" layer="95" />
                <pinref part="R443" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1198.88" y1="1554.48" x2="1196.34" y2="1554.48" width="0.3" layer="91" />
                <label x="1196.34" y="1554.48" size="1.27" layer="95" />
                <pinref part="R444" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$22">
              <segment>
                <wire x1="1727.20" y1="1976.12" x2="1727.20" y2="1973.58" width="0.3" layer="91" />
                <label x="1727.20" y="1973.58" size="1.27" layer="95" />
                <pinref part="C253" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1922.78" y1="2214.88" x2="1920.24" y2="2214.88" width="0.3" layer="91" />
                <label x="1920.24" y="2214.88" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1869.44" y1="2159.00" x2="1866.90" y2="2159.00" width="0.3" layer="91" />
                <label x="1866.90" y="2159.00" size="1.27" layer="95" />
                <pinref part="R449" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$23">
              <segment>
                <wire x1="1727.20" y1="1983.74" x2="1727.20" y2="1986.28" width="0.3" layer="91" />
                <label x="1727.20" y="1986.28" size="1.27" layer="95" />
                <pinref part="C253" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1727.20" y1="1996.44" x2="1724.66" y2="1996.44" width="0.3" layer="91" />
                <label x="1724.66" y="1996.44" size="1.27" layer="95" />
                <pinref part="R448" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1518.92" y1="1849.12" x2="1521.46" y2="1849.12" width="0.3" layer="91" />
                <label x="1521.46" y="1849.12" size="1.27" layer="95" />
                <pinref part="R447" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1711.96" y1="1965.96" x2="1711.96" y2="1968.50" width="0.3" layer="91" />
                <label x="1711.96" y="1968.50" size="1.27" layer="95" />
                <pinref part="C254" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$24">
              <segment>
                <wire x1="1711.96" y1="1958.34" x2="1711.96" y2="1955.80" width="0.3" layer="91" />
                <label x="1711.96" y="1955.80" size="1.27" layer="95" />
                <pinref part="C254" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1879.60" y1="2159.00" x2="1882.14" y2="2159.00" width="0.3" layer="91" />
                <label x="1882.14" y="2159.00" size="1.27" layer="95" />
                <pinref part="R449" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="83.82" x2="30.48" y2="83.82" width="0.3" layer="91" />
                <label x="30.48" y="83.82" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-2" />
              </segment>
              <segment>
                <wire x1="1922.78" y1="2219.96" x2="1920.24" y2="2219.96" width="0.3" layer="91" />
                <label x="1920.24" y="2219.96" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$25">
              <segment>
                <wire x1="858.52" y1="1071.88" x2="858.52" y2="1074.42" width="0.3" layer="91" />
                <label x="858.52" y="1074.42" size="1.27" layer="95" />
                <pinref part="C261" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="858.52" y1="1054.10" x2="858.52" y2="1056.64" width="0.3" layer="91" />
                <label x="858.52" y="1056.64" size="1.27" layer="95" />
                <pinref part="C262" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="866.14" y1="1066.80" x2="863.60" y2="1066.80" width="0.3" layer="91" />
                <label x="863.60" y="1066.80" size="1.27" layer="95" />
                <pinref part="R452" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="820.42" y1="830.58" x2="822.96" y2="830.58" width="0.3" layer="91" />
                <label x="822.96" y="830.58" size="1.27" layer="95" />
                <pinref part="R451" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$26">
              <segment>
                <wire x1="858.52" y1="1064.26" x2="858.52" y2="1061.72" width="0.3" layer="91" />
                <label x="858.52" y="1061.72" size="1.27" layer="95" />
                <pinref part="C261" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1046.48" y1="1440.18" x2="1049.02" y2="1440.18" width="0.3" layer="91" />
                <label x="1049.02" y="1440.18" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1054.10" y1="1432.56" x2="1051.56" y2="1432.56" width="0.3" layer="91" />
                <label x="1051.56" y="1432.56" size="1.27" layer="95" />
                <pinref part="R453" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$27">
              <segment>
                <wire x1="858.52" y1="1046.48" x2="858.52" y2="1043.94" width="0.3" layer="91" />
                <label x="858.52" y="1043.94" size="1.27" layer="95" />
                <pinref part="C262" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1064.26" y1="1432.56" x2="1066.80" y2="1432.56" width="0.3" layer="91" />
                <label x="1066.80" y="1432.56" size="1.27" layer="95" />
                <pinref part="R453" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="1554.48" x2="1214.12" y2="1554.48" width="0.3" layer="91" />
                <label x="1214.12" y="1554.48" size="1.27" layer="95" />
                <pinref part="R454" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1046.48" y1="1445.26" x2="1049.02" y2="1445.26" width="0.3" layer="91" />
                <label x="1049.02" y="1445.26" size="1.27" layer="95" />
                <pinref part="U27" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$28">
              <segment>
                <wire x1="1719.58" y1="1958.34" x2="1719.58" y2="1955.80" width="0.3" layer="91" />
                <label x="1719.58" y="1955.80" size="1.27" layer="95" />
                <pinref part="C263" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1953.26" y1="2209.80" x2="1955.80" y2="2209.80" width="0.3" layer="91" />
                <label x="1955.80" y="2209.80" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1887.22" y1="2159.00" x2="1884.68" y2="2159.00" width="0.3" layer="91" />
                <label x="1884.68" y="2159.00" size="1.27" layer="95" />
                <pinref part="R459" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$29">
              <segment>
                <wire x1="1719.58" y1="1965.96" x2="1719.58" y2="1968.50" width="0.3" layer="91" />
                <label x="1719.58" y="1968.50" size="1.27" layer="95" />
                <pinref part="C263" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1704.34" y1="1948.18" x2="1704.34" y2="1950.72" width="0.3" layer="91" />
                <label x="1704.34" y="1950.72" size="1.27" layer="95" />
                <pinref part="C264" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1744.98" y1="1996.44" x2="1742.44" y2="1996.44" width="0.3" layer="91" />
                <label x="1742.44" y="1996.44" size="1.27" layer="95" />
                <pinref part="R458" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1536.70" y1="1856.74" x2="1539.24" y2="1856.74" width="0.3" layer="91" />
                <label x="1539.24" y="1856.74" size="1.27" layer="95" />
                <pinref part="R457" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$30">
              <segment>
                <wire x1="1704.34" y1="1940.56" x2="1704.34" y2="1938.02" width="0.3" layer="91" />
                <label x="1704.34" y="1938.02" size="1.27" layer="95" />
                <pinref part="C264" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1897.38" y1="2159.00" x2="1899.92" y2="2159.00" width="0.3" layer="91" />
                <label x="1899.92" y="2159.00" size="1.27" layer="95" />
                <pinref part="R459" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1953.26" y1="2214.88" x2="1955.80" y2="2214.88" width="0.3" layer="91" />
                <label x="1955.80" y="2214.88" size="1.27" layer="95" />
                <pinref part="U39" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="81.28" x2="30.48" y2="81.28" width="0.3" layer="91" />
                <label x="30.48" y="81.28" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-3" />
              </segment>
            </net>
            <net name="N$31">
              <segment>
                <wire x1="883.92" y1="1097.28" x2="883.92" y2="1099.82" width="0.3" layer="91" />
                <label x="883.92" y="1099.82" size="1.27" layer="95" />
                <pinref part="C271" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="883.92" y1="1079.50" x2="883.92" y2="1082.04" width="0.3" layer="91" />
                <label x="883.92" y="1082.04" size="1.27" layer="95" />
                <pinref part="C272" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="866.14" y1="1074.42" x2="863.60" y2="1074.42" width="0.3" layer="91" />
                <label x="863.60" y="1074.42" size="1.27" layer="95" />
                <pinref part="R462" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="820.42" y1="863.60" x2="822.96" y2="863.60" width="0.3" layer="91" />
                <label x="822.96" y="863.60" size="1.27" layer="95" />
                <pinref part="R461" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$32">
              <segment>
                <wire x1="883.92" y1="1089.66" x2="883.92" y2="1087.12" width="0.3" layer="91" />
                <label x="883.92" y="1087.12" size="1.27" layer="95" />
                <pinref part="C271" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1054.10" y1="1440.18" x2="1051.56" y2="1440.18" width="0.3" layer="91" />
                <label x="1051.56" y="1440.18" size="1.27" layer="95" />
                <pinref part="R463" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1071.88" y1="1452.88" x2="1069.34" y2="1452.88" width="0.3" layer="91" />
                <label x="1069.34" y="1452.88" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$33">
              <segment>
                <wire x1="883.92" y1="1071.88" x2="883.92" y2="1069.34" width="0.3" layer="91" />
                <label x="883.92" y="1069.34" size="1.27" layer="95" />
                <pinref part="C272" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1064.26" y1="1440.18" x2="1066.80" y2="1440.18" width="0.3" layer="91" />
                <label x="1066.80" y="1440.18" size="1.27" layer="95" />
                <pinref part="R463" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1234.44" y1="1562.10" x2="1231.90" y2="1562.10" width="0.3" layer="91" />
                <label x="1231.90" y="1562.10" size="1.27" layer="95" />
                <pinref part="R464" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1071.88" y1="1457.96" x2="1069.34" y2="1457.96" width="0.3" layer="91" />
                <label x="1069.34" y="1457.96" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$34">
              <segment>
                <wire x1="1719.58" y1="1983.74" x2="1719.58" y2="1986.28" width="0.3" layer="91" />
                <label x="1719.58" y="1986.28" size="1.27" layer="95" />
                <pinref part="C273" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1711.96" y1="1948.18" x2="1711.96" y2="1950.72" width="0.3" layer="91" />
                <label x="1711.96" y="1950.72" size="1.27" layer="95" />
                <pinref part="C274" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1554.48" y1="1856.74" x2="1557.02" y2="1856.74" width="0.3" layer="91" />
                <label x="1557.02" y="1856.74" size="1.27" layer="95" />
                <pinref part="R467" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1744.98" y1="2004.06" x2="1742.44" y2="2004.06" width="0.3" layer="91" />
                <label x="1742.44" y="2004.06" size="1.27" layer="95" />
                <pinref part="R468" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$35">
              <segment>
                <wire x1="1719.58" y1="1976.12" x2="1719.58" y2="1973.58" width="0.3" layer="91" />
                <label x="1719.58" y="1973.58" size="1.27" layer="95" />
                <pinref part="C273" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1887.22" y1="2166.62" x2="1884.68" y2="2166.62" width="0.3" layer="91" />
                <label x="1884.68" y="2166.62" size="1.27" layer="95" />
                <pinref part="R469" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1905.00" y1="2179.32" x2="1902.46" y2="2179.32" width="0.3" layer="91" />
                <label x="1902.46" y="2179.32" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$36">
              <segment>
                <wire x1="1711.96" y1="1940.56" x2="1711.96" y2="1938.02" width="0.3" layer="91" />
                <label x="1711.96" y="1938.02" size="1.27" layer="95" />
                <pinref part="C274" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="78.74" x2="30.48" y2="78.74" width="0.3" layer="91" />
                <label x="30.48" y="78.74" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-4" />
              </segment>
              <segment>
                <wire x1="1905.00" y1="2184.40" x2="1902.46" y2="2184.40" width="0.3" layer="91" />
                <label x="1902.46" y="2184.40" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1897.38" y1="2166.62" x2="1899.92" y2="2166.62" width="0.3" layer="91" />
                <label x="1899.92" y="2166.62" size="1.27" layer="95" />
                <pinref part="R469" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$37">
              <segment>
                <wire x1="891.54" y1="1107.44" x2="891.54" y2="1104.90" width="0.3" layer="91" />
                <label x="891.54" y="1104.90" size="1.27" layer="95" />
                <pinref part="C281" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1102.36" y1="1447.80" x2="1104.90" y2="1447.80" width="0.3" layer="91" />
                <label x="1104.90" y="1447.80" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1089.66" y1="1468.12" x2="1087.12" y2="1468.12" width="0.3" layer="91" />
                <label x="1087.12" y="1468.12" size="1.27" layer="95" />
                <pinref part="R473" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$38">
              <segment>
                <wire x1="891.54" y1="1115.06" x2="891.54" y2="1117.60" width="0.3" layer="91" />
                <label x="891.54" y="1117.60" size="1.27" layer="95" />
                <pinref part="C281" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="899.16" y1="1109.98" x2="896.62" y2="1109.98" width="0.3" layer="91" />
                <label x="896.62" y="1109.98" size="1.27" layer="95" />
                <pinref part="R472" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="820.42" y1="896.62" x2="822.96" y2="896.62" width="0.3" layer="91" />
                <label x="822.96" y="896.62" size="1.27" layer="95" />
                <pinref part="R471" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="891.54" y1="1097.28" x2="891.54" y2="1099.82" width="0.3" layer="91" />
                <label x="891.54" y="1099.82" size="1.27" layer="95" />
                <pinref part="C282" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$39">
              <segment>
                <wire x1="891.54" y1="1089.66" x2="891.54" y2="1087.12" width="0.3" layer="91" />
                <label x="891.54" y="1087.12" size="1.27" layer="95" />
                <pinref part="C282" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1099.82" y1="1468.12" x2="1102.36" y2="1468.12" width="0.3" layer="91" />
                <label x="1102.36" y="1468.12" size="1.27" layer="95" />
                <pinref part="R473" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="1562.10" x2="1214.12" y2="1562.10" width="0.3" layer="91" />
                <label x="1214.12" y="1562.10" size="1.27" layer="95" />
                <pinref part="R474" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1102.36" y1="1452.88" x2="1104.90" y2="1452.88" width="0.3" layer="91" />
                <label x="1104.90" y="1452.88" size="1.27" layer="95" />
                <pinref part="U28" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$40">
              <segment>
                <wire x1="1762.76" y1="2019.30" x2="1762.76" y2="2016.76" width="0.3" layer="91" />
                <label x="1762.76" y="2016.76" size="1.27" layer="95" />
                <pinref part="C283" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1935.48" y1="2174.24" x2="1938.02" y2="2174.24" width="0.3" layer="91" />
                <label x="1938.02" y="2174.24" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1960.88" y1="2202.18" x2="1958.34" y2="2202.18" width="0.3" layer="91" />
                <label x="1958.34" y="2202.18" size="1.27" layer="95" />
                <pinref part="R479" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$41">
              <segment>
                <wire x1="1762.76" y1="2026.92" x2="1762.76" y2="2029.46" width="0.3" layer="91" />
                <label x="1762.76" y="2029.46" size="1.27" layer="95" />
                <pinref part="C283" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1572.26" y1="1864.36" x2="1574.80" y2="1864.36" width="0.3" layer="91" />
                <label x="1574.80" y="1864.36" size="1.27" layer="95" />
                <pinref part="R477" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1762.76" y1="2009.14" x2="1762.76" y2="2011.68" width="0.3" layer="91" />
                <label x="1762.76" y="2011.68" size="1.27" layer="95" />
                <pinref part="C284" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1770.38" y1="2021.84" x2="1767.84" y2="2021.84" width="0.3" layer="91" />
                <label x="1767.84" y="2021.84" size="1.27" layer="95" />
                <pinref part="R478" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$42">
              <segment>
                <wire x1="1762.76" y1="2001.52" x2="1762.76" y2="1998.98" width="0.3" layer="91" />
                <label x="1762.76" y="1998.98" size="1.27" layer="95" />
                <pinref part="C284" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="76.20" x2="30.48" y2="76.20" width="0.3" layer="91" />
                <label x="30.48" y="76.20" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-5" />
              </segment>
              <segment>
                <wire x1="1971.04" y1="2202.18" x2="1973.58" y2="2202.18" width="0.3" layer="91" />
                <label x="1973.58" y="2202.18" size="1.27" layer="95" />
                <pinref part="R479" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1935.48" y1="2179.32" x2="1938.02" y2="2179.32" width="0.3" layer="91" />
                <label x="1938.02" y="2179.32" size="1.27" layer="95" />
                <pinref part="U40" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$43">
              <segment>
                <wire x1="906.78" y1="1115.06" x2="906.78" y2="1112.52" width="0.3" layer="91" />
                <label x="906.78" y="1112.52" size="1.27" layer="95" />
                <pinref part="C291" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1488.44" x2="1104.90" y2="1488.44" width="0.3" layer="91" />
                <label x="1104.90" y="1488.44" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1089.66" y1="1475.74" x2="1087.12" y2="1475.74" width="0.3" layer="91" />
                <label x="1087.12" y="1475.74" size="1.27" layer="95" />
                <pinref part="R483" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$44">
              <segment>
                <wire x1="906.78" y1="1122.68" x2="906.78" y2="1125.22" width="0.3" layer="91" />
                <label x="906.78" y="1125.22" size="1.27" layer="95" />
                <pinref part="C291" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="899.16" y1="1122.68" x2="899.16" y2="1125.22" width="0.3" layer="91" />
                <label x="899.16" y="1125.22" size="1.27" layer="95" />
                <pinref part="C292" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="820.42" y1="919.48" x2="822.96" y2="919.48" width="0.3" layer="91" />
                <label x="822.96" y="919.48" size="1.27" layer="95" />
                <pinref part="R481" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="906.78" y1="1135.38" x2="904.24" y2="1135.38" width="0.3" layer="91" />
                <label x="904.24" y="1135.38" size="1.27" layer="95" />
                <pinref part="R482" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$45">
              <segment>
                <wire x1="899.16" y1="1115.06" x2="899.16" y2="1112.52" width="0.3" layer="91" />
                <label x="899.16" y="1112.52" size="1.27" layer="95" />
                <pinref part="C292" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1099.82" y1="1475.74" x2="1102.36" y2="1475.74" width="0.3" layer="91" />
                <label x="1102.36" y="1475.74" size="1.27" layer="95" />
                <pinref part="R483" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1234.44" y1="1569.72" x2="1231.90" y2="1569.72" width="0.3" layer="91" />
                <label x="1231.90" y="1569.72" size="1.27" layer="95" />
                <pinref part="R484" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1493.52" x2="1104.90" y2="1493.52" width="0.3" layer="91" />
                <label x="1104.90" y="1493.52" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$46">
              <segment>
                <wire x1="1778.00" y1="2026.92" x2="1778.00" y2="2024.38" width="0.3" layer="91" />
                <label x="1778.00" y="2024.38" size="1.27" layer="95" />
                <pinref part="C293" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1960.88" y1="2209.80" x2="1958.34" y2="2209.80" width="0.3" layer="91" />
                <label x="1958.34" y="2209.80" size="1.27" layer="95" />
                <pinref part="R489" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1978.66" y1="2222.50" x2="1976.12" y2="2222.50" width="0.3" layer="91" />
                <label x="1976.12" y="2222.50" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$47">
              <segment>
                <wire x1="1778.00" y1="2034.54" x2="1778.00" y2="2037.08" width="0.3" layer="91" />
                <label x="1778.00" y="2037.08" size="1.27" layer="95" />
                <pinref part="C293" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1778.00" y1="2047.24" x2="1775.46" y2="2047.24" width="0.3" layer="91" />
                <label x="1775.46" y="2047.24" size="1.27" layer="95" />
                <pinref part="R488" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1590.04" y1="1879.60" x2="1592.58" y2="1879.60" width="0.3" layer="91" />
                <label x="1592.58" y="1879.60" size="1.27" layer="95" />
                <pinref part="R487" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1770.38" y1="2034.54" x2="1770.38" y2="2037.08" width="0.3" layer="91" />
                <label x="1770.38" y="2037.08" size="1.27" layer="95" />
                <pinref part="C294" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$48">
              <segment>
                <wire x1="1770.38" y1="2026.92" x2="1770.38" y2="2024.38" width="0.3" layer="91" />
                <label x="1770.38" y="2024.38" size="1.27" layer="95" />
                <pinref part="C294" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1971.04" y1="2209.80" x2="1973.58" y2="2209.80" width="0.3" layer="91" />
                <label x="1973.58" y="2209.80" size="1.27" layer="95" />
                <pinref part="R489" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="73.66" x2="30.48" y2="73.66" width="0.3" layer="91" />
                <label x="30.48" y="73.66" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-6" />
              </segment>
              <segment>
                <wire x1="1978.66" y1="2227.58" x2="1976.12" y2="2227.58" width="0.3" layer="91" />
                <label x="1976.12" y="2227.58" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$49">
              <segment>
                <wire x1="924.56" y1="1158.24" x2="924.56" y2="1160.78" width="0.3" layer="91" />
                <label x="924.56" y="1160.78" size="1.27" layer="95" />
                <pinref part="C301" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="932.18" y1="1153.16" x2="929.64" y2="1153.16" width="0.3" layer="91" />
                <label x="929.64" y="1153.16" size="1.27" layer="95" />
                <pinref part="R492" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="822.96" y1="939.80" x2="825.50" y2="939.80" width="0.3" layer="91" />
                <label x="825.50" y="939.80" size="1.27" layer="95" />
                <pinref part="R491" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="924.56" y1="1140.46" x2="924.56" y2="1143.00" width="0.3" layer="91" />
                <label x="924.56" y="1143.00" size="1.27" layer="95" />
                <pinref part="C302" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$50">
              <segment>
                <wire x1="924.56" y1="1150.62" x2="924.56" y2="1148.08" width="0.3" layer="91" />
                <label x="924.56" y="1148.08" size="1.27" layer="95" />
                <pinref part="C301" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1107.44" y1="1503.68" x2="1104.90" y2="1503.68" width="0.3" layer="91" />
                <label x="1104.90" y="1503.68" size="1.27" layer="95" />
                <pinref part="R493" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1137.92" y1="1483.36" x2="1140.46" y2="1483.36" width="0.3" layer="91" />
                <label x="1140.46" y="1483.36" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$51">
              <segment>
                <wire x1="924.56" y1="1132.84" x2="924.56" y2="1130.30" width="0.3" layer="91" />
                <label x="924.56" y="1130.30" size="1.27" layer="95" />
                <pinref part="C302" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1137.92" y1="1488.44" x2="1140.46" y2="1488.44" width="0.3" layer="91" />
                <label x="1140.46" y="1488.44" size="1.27" layer="95" />
                <pinref part="U29" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1117.60" y1="1503.68" x2="1120.14" y2="1503.68" width="0.3" layer="91" />
                <label x="1120.14" y="1503.68" size="1.27" layer="95" />
                <pinref part="R493" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1252.22" y1="1569.72" x2="1249.68" y2="1569.72" width="0.3" layer="91" />
                <label x="1249.68" y="1569.72" size="1.27" layer="95" />
                <pinref part="R494" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$52">
              <segment>
                <wire x1="1795.78" y1="2062.48" x2="1795.78" y2="2059.94" width="0.3" layer="91" />
                <label x="1795.78" y="2059.94" size="1.27" layer="95" />
                <pinref part="C303" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1978.66" y1="2237.74" x2="1976.12" y2="2237.74" width="0.3" layer="91" />
                <label x="1976.12" y="2237.74" size="1.27" layer="95" />
                <pinref part="R499" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2009.14" y1="2217.42" x2="2011.68" y2="2217.42" width="0.3" layer="91" />
                <label x="2011.68" y="2217.42" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$53">
              <segment>
                <wire x1="1795.78" y1="2070.10" x2="1795.78" y2="2072.64" width="0.3" layer="91" />
                <label x="1795.78" y="2072.64" size="1.27" layer="95" />
                <pinref part="C303" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1803.40" y1="2065.02" x2="1800.86" y2="2065.02" width="0.3" layer="91" />
                <label x="1800.86" y="2065.02" size="1.27" layer="95" />
                <pinref part="R498" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1607.82" y1="1887.22" x2="1610.36" y2="1887.22" width="0.3" layer="91" />
                <label x="1610.36" y="1887.22" size="1.27" layer="95" />
                <pinref part="R497" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1795.78" y1="2052.32" x2="1795.78" y2="2054.86" width="0.3" layer="91" />
                <label x="1795.78" y="2054.86" size="1.27" layer="95" />
                <pinref part="C304" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$54">
              <segment>
                <wire x1="1795.78" y1="2044.70" x2="1795.78" y2="2042.16" width="0.3" layer="91" />
                <label x="1795.78" y="2042.16" size="1.27" layer="95" />
                <pinref part="C304" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2009.14" y1="2222.50" x2="2011.68" y2="2222.50" width="0.3" layer="91" />
                <label x="2011.68" y="2222.50" size="1.27" layer="95" />
                <pinref part="U41" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1988.82" y1="2237.74" x2="1991.36" y2="2237.74" width="0.3" layer="91" />
                <label x="1991.36" y="2237.74" size="1.27" layer="95" />
                <pinref part="R499" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="71.12" x2="30.48" y2="71.12" width="0.3" layer="91" />
                <label x="30.48" y="71.12" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-7" />
              </segment>
            </net>
            <net name="N$55">
              <segment>
                <wire x1="939.80" y1="1158.24" x2="939.80" y2="1155.70" width="0.3" layer="91" />
                <label x="939.80" y="1155.70" size="1.27" layer="95" />
                <pinref part="C311" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1071.88" y1="1468.12" x2="1069.34" y2="1468.12" width="0.3" layer="91" />
                <label x="1069.34" y="1468.12" size="1.27" layer="95" />
                <pinref part="R503" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1125.22" y1="1516.38" x2="1122.68" y2="1516.38" width="0.3" layer="91" />
                <label x="1122.68" y="1516.38" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$56">
              <segment>
                <wire x1="939.80" y1="1165.86" x2="939.80" y2="1168.40" width="0.3" layer="91" />
                <label x="939.80" y="1168.40" size="1.27" layer="95" />
                <pinref part="C311" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="825.50" y1="960.12" x2="828.04" y2="960.12" width="0.3" layer="91" />
                <label x="828.04" y="960.12" size="1.27" layer="95" />
                <pinref part="R501" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="939.80" y1="1178.56" x2="937.26" y2="1178.56" width="0.3" layer="91" />
                <label x="937.26" y="1178.56" size="1.27" layer="95" />
                <pinref part="R502" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="932.18" y1="1165.86" x2="932.18" y2="1168.40" width="0.3" layer="91" />
                <label x="932.18" y="1168.40" size="1.27" layer="95" />
                <pinref part="C312" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$57">
              <segment>
                <wire x1="932.18" y1="1158.24" x2="932.18" y2="1155.70" width="0.3" layer="91" />
                <label x="932.18" y="1155.70" size="1.27" layer="95" />
                <pinref part="C312" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1125.22" y1="1521.46" x2="1122.68" y2="1521.46" width="0.3" layer="91" />
                <label x="1122.68" y="1521.46" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1082.04" y1="1468.12" x2="1084.58" y2="1468.12" width="0.3" layer="91" />
                <label x="1084.58" y="1468.12" size="1.27" layer="95" />
                <pinref part="R503" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1252.22" y1="1577.34" x2="1249.68" y2="1577.34" width="0.3" layer="91" />
                <label x="1249.68" y="1577.34" size="1.27" layer="95" />
                <pinref part="R504" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$58">
              <segment>
                <wire x1="1811.02" y1="2077.72" x2="1811.02" y2="2080.26" width="0.3" layer="91" />
                <label x="1811.02" y="2080.26" size="1.27" layer="95" />
                <pinref part="C313" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1625.60" y1="1894.84" x2="1628.14" y2="1894.84" width="0.3" layer="91" />
                <label x="1628.14" y="1894.84" size="1.27" layer="95" />
                <pinref part="R507" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1803.40" y1="2077.72" x2="1803.40" y2="2080.26" width="0.3" layer="91" />
                <label x="1803.40" y="2080.26" size="1.27" layer="95" />
                <pinref part="C314" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1811.02" y1="2090.42" x2="1808.48" y2="2090.42" width="0.3" layer="91" />
                <label x="1808.48" y="2090.42" size="1.27" layer="95" />
                <pinref part="R508" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$59">
              <segment>
                <wire x1="1811.02" y1="2070.10" x2="1811.02" y2="2067.56" width="0.3" layer="91" />
                <label x="1811.02" y="2067.56" size="1.27" layer="95" />
                <pinref part="C313" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1996.44" y1="2237.74" x2="1993.90" y2="2237.74" width="0.3" layer="91" />
                <label x="1993.90" y="2237.74" size="1.27" layer="95" />
                <pinref part="R509" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1996.44" y1="2258.06" x2="1993.90" y2="2258.06" width="0.3" layer="91" />
                <label x="1993.90" y="2258.06" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$60">
              <segment>
                <wire x1="1803.40" y1="2070.10" x2="1803.40" y2="2067.56" width="0.3" layer="91" />
                <label x="1803.40" y="2067.56" size="1.27" layer="95" />
                <pinref part="C314" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="68.58" x2="30.48" y2="68.58" width="0.3" layer="91" />
                <label x="30.48" y="68.58" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-8" />
              </segment>
              <segment>
                <wire x1="1996.44" y1="2263.14" x2="1993.90" y2="2263.14" width="0.3" layer="91" />
                <label x="1993.90" y="2263.14" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="2006.60" y1="2237.74" x2="2009.14" y2="2237.74" width="0.3" layer="91" />
                <label x="2009.14" y="2237.74" size="1.27" layer="95" />
                <pinref part="R509" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$61">
              <segment>
                <wire x1="965.20" y1="1361.44" x2="965.20" y2="1358.90" width="0.3" layer="91" />
                <label x="965.20" y="1358.90" size="1.27" layer="95" />
                <pinref part="C321" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1125.22" y1="1531.62" x2="1122.68" y2="1531.62" width="0.3" layer="91" />
                <label x="1122.68" y="1531.62" size="1.27" layer="95" />
                <pinref part="R513" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="1511.30" x2="1158.24" y2="1511.30" width="0.3" layer="91" />
                <label x="1158.24" y="1511.30" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$62">
              <segment>
                <wire x1="965.20" y1="1369.06" x2="965.20" y2="1371.60" width="0.3" layer="91" />
                <label x="965.20" y="1371.60" size="1.27" layer="95" />
                <pinref part="C321" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="825.50" y1="977.90" x2="828.04" y2="977.90" width="0.3" layer="91" />
                <label x="828.04" y="977.90" size="1.27" layer="95" />
                <pinref part="R511" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="957.58" y1="1369.06" x2="957.58" y2="1371.60" width="0.3" layer="91" />
                <label x="957.58" y="1371.60" size="1.27" layer="95" />
                <pinref part="C322" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="965.20" y1="1381.76" x2="962.66" y2="1381.76" width="0.3" layer="91" />
                <label x="962.66" y="1381.76" size="1.27" layer="95" />
                <pinref part="R512" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$63">
              <segment>
                <wire x1="957.58" y1="1361.44" x2="957.58" y2="1358.90" width="0.3" layer="91" />
                <label x="957.58" y="1358.90" size="1.27" layer="95" />
                <pinref part="C322" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="1516.38" x2="1158.24" y2="1516.38" width="0.3" layer="91" />
                <label x="1158.24" y="1516.38" size="1.27" layer="95" />
                <pinref part="U30" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1135.38" y1="1531.62" x2="1137.92" y2="1531.62" width="0.3" layer="91" />
                <label x="1137.92" y="1531.62" size="1.27" layer="95" />
                <pinref part="R513" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="1651.00" x2="1290.32" y2="1651.00" width="0.3" layer="91" />
                <label x="1290.32" y="1651.00" size="1.27" layer="95" />
                <pinref part="R514" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$64">
              <segment>
                <wire x1="1828.80" y1="2087.88" x2="1828.80" y2="2085.34" width="0.3" layer="91" />
                <label x="1828.80" y="2085.34" size="1.27" layer="95" />
                <pinref part="C323" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1905.00" y1="2194.56" x2="1902.46" y2="2194.56" width="0.3" layer="91" />
                <label x="1902.46" y="2194.56" size="1.27" layer="95" />
                <pinref part="R519" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2026.92" y1="2252.98" x2="2029.46" y2="2252.98" width="0.3" layer="91" />
                <label x="2029.46" y="2252.98" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$65">
              <segment>
                <wire x1="1828.80" y1="2095.50" x2="1828.80" y2="2098.04" width="0.3" layer="91" />
                <label x="1828.80" y="2098.04" size="1.27" layer="95" />
                <pinref part="C323" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1836.42" y1="2108.20" x2="1833.88" y2="2108.20" width="0.3" layer="91" />
                <label x="1833.88" y="2108.20" size="1.27" layer="95" />
                <pinref part="R518" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1643.38" y1="1902.46" x2="1645.92" y2="1902.46" width="0.3" layer="91" />
                <label x="1645.92" y="1902.46" size="1.27" layer="95" />
                <pinref part="R517" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1828.80" y1="2113.28" x2="1828.80" y2="2115.82" width="0.3" layer="91" />
                <label x="1828.80" y="2115.82" size="1.27" layer="95" />
                <pinref part="C324" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$66">
              <segment>
                <wire x1="1828.80" y1="2105.66" x2="1828.80" y2="2103.12" width="0.3" layer="91" />
                <label x="1828.80" y="2103.12" size="1.27" layer="95" />
                <pinref part="C324" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="66.04" x2="30.48" y2="66.04" width="0.3" layer="91" />
                <label x="30.48" y="66.04" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-9" />
              </segment>
              <segment>
                <wire x1="2026.92" y1="2258.06" x2="2029.46" y2="2258.06" width="0.3" layer="91" />
                <label x="2029.46" y="2258.06" size="1.27" layer="95" />
                <pinref part="U42" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1915.16" y1="2194.56" x2="1917.70" y2="2194.56" width="0.3" layer="91" />
                <label x="1917.70" y="2194.56" size="1.27" layer="95" />
                <pinref part="R519" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$67">
              <segment>
                <wire x1="982.98" y1="1404.62" x2="982.98" y2="1407.16" width="0.3" layer="91" />
                <label x="982.98" y="1407.16" size="1.27" layer="95" />
                <pinref part="C331" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="825.50" y1="1000.76" x2="828.04" y2="1000.76" width="0.3" layer="91" />
                <label x="828.04" y="1000.76" size="1.27" layer="95" />
                <pinref part="R521" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="990.60" y1="1399.54" x2="988.06" y2="1399.54" width="0.3" layer="91" />
                <label x="988.06" y="1399.54" size="1.27" layer="95" />
                <pinref part="R522" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="982.98" y1="1386.84" x2="982.98" y2="1389.38" width="0.3" layer="91" />
                <label x="982.98" y="1389.38" size="1.27" layer="95" />
                <pinref part="C332" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$68">
              <segment>
                <wire x1="982.98" y1="1397.00" x2="982.98" y2="1394.46" width="0.3" layer="91" />
                <label x="982.98" y="1394.46" size="1.27" layer="95" />
                <pinref part="C331" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1143.00" y1="1531.62" x2="1140.46" y2="1531.62" width="0.3" layer="91" />
                <label x="1140.46" y="1531.62" size="1.27" layer="95" />
                <pinref part="R523" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1143.00" y1="1551.94" x2="1140.46" y2="1551.94" width="0.3" layer="91" />
                <label x="1140.46" y="1551.94" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$69">
              <segment>
                <wire x1="982.98" y1="1379.22" x2="982.98" y2="1376.68" width="0.3" layer="91" />
                <label x="982.98" y="1376.68" size="1.27" layer="95" />
                <pinref part="C332" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1143.00" y1="1557.02" x2="1140.46" y2="1557.02" width="0.3" layer="91" />
                <label x="1140.46" y="1557.02" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1153.16" y1="1531.62" x2="1155.70" y2="1531.62" width="0.3" layer="91" />
                <label x="1155.70" y="1531.62" size="1.27" layer="95" />
                <pinref part="R523" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="1658.62" x2="1290.32" y2="1658.62" width="0.3" layer="91" />
                <label x="1290.32" y="1658.62" size="1.27" layer="95" />
                <pinref part="R524" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$70">
              <segment>
                <wire x1="1844.04" y1="2113.28" x2="1844.04" y2="2110.74" width="0.3" layer="91" />
                <label x="1844.04" y="2110.74" size="1.27" layer="95" />
                <pinref part="C333" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2034.54" y1="2258.06" x2="2032.00" y2="2258.06" width="0.3" layer="91" />
                <label x="2032.00" y="2258.06" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1922.78" y1="2194.56" x2="1920.24" y2="2194.56" width="0.3" layer="91" />
                <label x="1920.24" y="2194.56" size="1.27" layer="95" />
                <pinref part="R529" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$71">
              <segment>
                <wire x1="1844.04" y1="2120.90" x2="1844.04" y2="2123.44" width="0.3" layer="91" />
                <label x="1844.04" y="2123.44" size="1.27" layer="95" />
                <pinref part="C333" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1844.04" y1="2133.60" x2="1841.50" y2="2133.60" width="0.3" layer="91" />
                <label x="1841.50" y="2133.60" size="1.27" layer="95" />
                <pinref part="R528" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1661.16" y1="1902.46" x2="1663.70" y2="1902.46" width="0.3" layer="91" />
                <label x="1663.70" y="1902.46" size="1.27" layer="95" />
                <pinref part="R527" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1836.42" y1="2120.90" x2="1836.42" y2="2123.44" width="0.3" layer="91" />
                <label x="1836.42" y="2123.44" size="1.27" layer="95" />
                <pinref part="C334" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$72">
              <segment>
                <wire x1="1836.42" y1="2113.28" x2="1836.42" y2="2110.74" width="0.3" layer="91" />
                <label x="1836.42" y="2110.74" size="1.27" layer="95" />
                <pinref part="C334" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="63.50" x2="30.48" y2="63.50" width="0.3" layer="91" />
                <label x="30.48" y="63.50" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-10" />
              </segment>
              <segment>
                <wire x1="1932.94" y1="2194.56" x2="1935.48" y2="2194.56" width="0.3" layer="91" />
                <label x="1935.48" y="2194.56" size="1.27" layer="95" />
                <pinref part="R529" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2034.54" y1="2263.14" x2="2032.00" y2="2263.14" width="0.3" layer="91" />
                <label x="2032.00" y="2263.14" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$73">
              <segment>
                <wire x1="998.22" y1="1404.62" x2="998.22" y2="1402.08" width="0.3" layer="91" />
                <label x="998.22" y="1402.08" size="1.27" layer="95" />
                <pinref part="C341" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1181.10" y1="1539.24" x2="1178.56" y2="1539.24" width="0.3" layer="91" />
                <label x="1178.56" y="1539.24" size="1.27" layer="95" />
                <pinref part="R533" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1173.48" y1="1546.86" x2="1176.02" y2="1546.86" width="0.3" layer="91" />
                <label x="1176.02" y="1546.86" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$74">
              <segment>
                <wire x1="998.22" y1="1412.24" x2="998.22" y2="1414.78" width="0.3" layer="91" />
                <label x="998.22" y="1414.78" size="1.27" layer="95" />
                <pinref part="C341" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="825.50" y1="1023.62" x2="828.04" y2="1023.62" width="0.3" layer="91" />
                <label x="828.04" y="1023.62" size="1.27" layer="95" />
                <pinref part="R531" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="998.22" y1="1424.94" x2="995.68" y2="1424.94" width="0.3" layer="91" />
                <label x="995.68" y="1424.94" size="1.27" layer="95" />
                <pinref part="R532" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="990.60" y1="1412.24" x2="990.60" y2="1414.78" width="0.3" layer="91" />
                <label x="990.60" y="1414.78" size="1.27" layer="95" />
                <pinref part="C342" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$75">
              <segment>
                <wire x1="990.60" y1="1404.62" x2="990.60" y2="1402.08" width="0.3" layer="91" />
                <label x="990.60" y="1402.08" size="1.27" layer="95" />
                <pinref part="C342" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1173.48" y1="1551.94" x2="1176.02" y2="1551.94" width="0.3" layer="91" />
                <label x="1176.02" y="1551.94" size="1.27" layer="95" />
                <pinref part="U31" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1191.26" y1="1539.24" x2="1193.80" y2="1539.24" width="0.3" layer="91" />
                <label x="1193.80" y="1539.24" size="1.27" layer="95" />
                <pinref part="R533" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1310.64" y1="1658.62" x2="1308.10" y2="1658.62" width="0.3" layer="91" />
                <label x="1308.10" y="1658.62" size="1.27" layer="95" />
                <pinref part="R534" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$76">
              <segment>
                <wire x1="1861.82" y1="2148.84" x2="1861.82" y2="2146.30" width="0.3" layer="91" />
                <label x="1861.82" y="2146.30" size="1.27" layer="95" />
                <pinref part="C343" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2034.54" y1="2273.30" x2="2032.00" y2="2273.30" width="0.3" layer="91" />
                <label x="2032.00" y="2273.30" size="1.27" layer="95" />
                <pinref part="R539" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2065.02" y1="2252.98" x2="2067.56" y2="2252.98" width="0.3" layer="91" />
                <label x="2067.56" y="2252.98" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$77">
              <segment>
                <wire x1="1861.82" y1="2156.46" x2="1861.82" y2="2159.00" width="0.3" layer="91" />
                <label x="1861.82" y="2159.00" size="1.27" layer="95" />
                <pinref part="C343" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1861.82" y1="2138.68" x2="1861.82" y2="2141.22" width="0.3" layer="91" />
                <label x="1861.82" y="2141.22" size="1.27" layer="95" />
                <pinref part="C344" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1869.44" y1="2151.38" x2="1866.90" y2="2151.38" width="0.3" layer="91" />
                <label x="1866.90" y="2151.38" size="1.27" layer="95" />
                <pinref part="R538" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1696.72" y1="1925.32" x2="1699.26" y2="1925.32" width="0.3" layer="91" />
                <label x="1699.26" y="1925.32" size="1.27" layer="95" />
                <pinref part="R537" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$78">
              <segment>
                <wire x1="1861.82" y1="2131.06" x2="1861.82" y2="2128.52" width="0.3" layer="91" />
                <label x="1861.82" y="2128.52" size="1.27" layer="95" />
                <pinref part="C344" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="33.02" y1="60.96" x2="30.48" y2="60.96" width="0.3" layer="91" />
                <label x="30.48" y="60.96" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="1-11" />
              </segment>
              <segment>
                <wire x1="2065.02" y1="2258.06" x2="2067.56" y2="2258.06" width="0.3" layer="91" />
                <label x="2067.56" y="2258.06" size="1.27" layer="95" />
                <pinref part="U43" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="2044.70" y1="2273.30" x2="2047.24" y2="2273.30" width="0.3" layer="91" />
                <label x="2047.24" y="2273.30" size="1.27" layer="95" />
                <pinref part="R539" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$79">
              <segment>
                <wire x1="1686.56" y1="1917.70" x2="1684.02" y2="1917.70" width="0.3" layer="91" />
                <label x="1684.02" y="1917.70" size="1.27" layer="95" />
                <pinref part="R420" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1381.76" y1="1732.28" x2="1384.30" y2="1732.28" width="0.3" layer="91" />
                <label x="1384.30" y="1732.28" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1826.26" x2="1452.88" y2="1826.26" width="0.3" layer="91" />
                <label x="1452.88" y="1826.26" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="CTRLA" />
              </segment>
            </net>
            <net name="N$80">
              <segment>
                <wire x1="1696.72" y1="1917.70" x2="1699.26" y2="1917.70" width="0.3" layer="91" />
                <label x="1699.26" y="1917.70" size="1.27" layer="95" />
                <pinref part="R420" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="58.42" y1="78.74" x2="55.88" y2="78.74" width="0.3" layer="91" />
                <label x="55.88" y="78.74" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-4" />
              </segment>
              <segment>
                <wire x1="1536.70" y1="1849.12" x2="1539.24" y2="1849.12" width="0.3" layer="91" />
                <label x="1539.24" y="1849.12" size="1.27" layer="95" />
                <pinref part="R445" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1590.04" y1="1871.98" x2="1592.58" y2="1871.98" width="0.3" layer="91" />
                <label x="1592.58" y="1871.98" size="1.27" layer="95" />
                <pinref part="R455" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1678.94" y1="1917.70" x2="1681.48" y2="1917.70" width="0.3" layer="91" />
                <label x="1681.48" y="1917.70" size="1.27" layer="95" />
                <pinref part="R535" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1678.94" y1="1910.08" x2="1681.48" y2="1910.08" width="0.3" layer="91" />
                <label x="1681.48" y="1910.08" size="1.27" layer="95" />
                <pinref part="R525" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1661.16" y1="1910.08" x2="1663.70" y2="1910.08" width="0.3" layer="91" />
                <label x="1663.70" y="1910.08" size="1.27" layer="95" />
                <pinref part="R515" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1643.38" y1="1894.84" x2="1645.92" y2="1894.84" width="0.3" layer="91" />
                <label x="1645.92" y="1894.84" size="1.27" layer="95" />
                <pinref part="R505" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1625.60" y1="1887.22" x2="1628.14" y2="1887.22" width="0.3" layer="91" />
                <label x="1628.14" y="1887.22" size="1.27" layer="95" />
                <pinref part="R495" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1607.82" y1="1879.60" x2="1610.36" y2="1879.60" width="0.3" layer="91" />
                <label x="1610.36" y="1879.60" size="1.27" layer="95" />
                <pinref part="R485" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1572.26" y1="1871.98" x2="1574.80" y2="1871.98" width="0.3" layer="91" />
                <label x="1574.80" y="1871.98" size="1.27" layer="95" />
                <pinref part="R475" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1554.48" y1="1864.36" x2="1557.02" y2="1864.36" width="0.3" layer="91" />
                <label x="1557.02" y="1864.36" size="1.27" layer="95" />
                <pinref part="R465" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1684.02" x2="1361.44" y2="1684.02" width="0.3" layer="91" />
                <label x="1361.44" y="1684.02" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1711.96" x2="1361.44" y2="1711.96" width="0.3" layer="91" />
                <label x="1361.44" y="1711.96" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1800.86" x2="1452.88" y2="1800.86" width="0.3" layer="91" />
                <label x="1452.88" y="1800.86" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1828.80" x2="1452.88" y2="1828.80" width="0.3" layer="91" />
                <label x="1452.88" y="1828.80" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="VDD" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1681.48" x2="1361.44" y2="1681.48" width="0.3" layer="91" />
                <label x="1361.44" y="1681.48" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="CTRLA" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1678.94" x2="1361.44" y2="1678.94" width="0.3" layer="91" />
                <label x="1361.44" y="1678.94" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1681.48" x2="1325.88" y2="1681.48" width="0.3" layer="91" />
                <label x="1325.88" y="1681.48" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="AOUT" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1684.02" x2="1325.88" y2="1684.02" width="0.3" layer="91" />
                <label x="1325.88" y="1684.02" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="AIN" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1673.86" x2="1361.44" y2="1673.86" width="0.3" layer="91" />
                <label x="1361.44" y="1673.86" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="DOUT" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1676.40" x2="1361.44" y2="1676.40" width="0.3" layer="91" />
                <label x="1361.44" y="1676.40" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="DIN" />
              </segment>
            </net>
            <net name="N$81">
              <segment>
                <wire x1="1191.26" y1="1546.86" x2="1193.80" y2="1546.86" width="0.3" layer="91" />
                <label x="1193.80" y="1546.86" size="1.27" layer="95" />
                <pinref part="R421" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1198.88" y1="1546.86" x2="1196.34" y2="1546.86" width="0.3" layer="91" />
                <label x="1196.34" y="1546.86" size="1.27" layer="95" />
                <pinref part="R422" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1366.52" y1="1747.52" x2="1363.98" y2="1747.52" width="0.3" layer="91" />
                <label x="1363.98" y="1747.52" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="+IN1" />
              </segment>
            </net>
            <net name="N$82">
              <segment>
                <wire x1="1457.96" y1="1811.02" x2="1455.42" y2="1811.02" width="0.3" layer="91" />
                <label x="1455.42" y="1811.02" size="1.27" layer="95" />
                <pinref part="R434" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1826.26" x2="1417.32" y2="1826.26" width="0.3" layer="91" />
                <label x="1417.32" y="1826.26" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="AOUT" />
              </segment>
            </net>
            <net name="N$83">
              <segment>
                <wire x1="810.26" y1="789.94" x2="807.72" y2="789.94" width="0.3" layer="91" />
                <label x="807.72" y="789.94" size="1.27" layer="95" />
                <pinref part="R441" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="810.26" y1="919.48" x2="807.72" y2="919.48" width="0.3" layer="91" />
                <label x="807.72" y="919.48" size="1.27" layer="95" />
                <pinref part="R481" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="810.26" y1="863.60" x2="807.72" y2="863.60" width="0.3" layer="91" />
                <label x="807.72" y="863.60" size="1.27" layer="95" />
                <pinref part="R461" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="815.34" y1="1023.62" x2="812.80" y2="1023.62" width="0.3" layer="91" />
                <label x="812.80" y="1023.62" size="1.27" layer="95" />
                <pinref part="R531" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="812.80" y1="939.80" x2="810.26" y2="939.80" width="0.3" layer="91" />
                <label x="810.26" y="939.80" size="1.27" layer="95" />
                <pinref part="R491" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="815.34" y1="977.90" x2="812.80" y2="977.90" width="0.3" layer="91" />
                <label x="812.80" y="977.90" size="1.27" layer="95" />
                <pinref part="R511" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="815.34" y1="1000.76" x2="812.80" y2="1000.76" width="0.3" layer="91" />
                <label x="812.80" y="1000.76" size="1.27" layer="95" />
                <pinref part="R521" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="810.26" y1="830.58" x2="807.72" y2="830.58" width="0.3" layer="91" />
                <label x="807.72" y="830.58" size="1.27" layer="95" />
                <pinref part="R451" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="810.26" y1="896.62" x2="807.72" y2="896.62" width="0.3" layer="91" />
                <label x="807.72" y="896.62" size="1.27" layer="95" />
                <pinref part="R471" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="815.34" y1="960.12" x2="812.80" y2="960.12" width="0.3" layer="91" />
                <label x="812.80" y="960.12" size="1.27" layer="95" />
                <pinref part="R501" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="259.08" y1="2402.84" x2="261.62" y2="2402.84" width="0.3" layer="91" />
                <label x="261.62" y="2402.84" size="1.27" layer="95" />
                <pinref part="R720" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="210.82" y1="2420.62" x2="208.28" y2="2420.62" width="0.3" layer="91" />
                <label x="208.28" y="2420.62" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="2077.72" y1="104.14" x2="2075.18" y2="104.14" width="0.3" layer="91" />
                <label x="2075.18" y="104.14" size="1.27" layer="95" />
                <pinref part="R427" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2133.60" y1="182.88" x2="2131.06" y2="182.88" width="0.3" layer="91" />
                <label x="2131.06" y="182.88" size="1.27" layer="95" />
                <pinref part="R432" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$84">
              <segment>
                <wire x1="1209.04" y1="1554.48" x2="1211.58" y2="1554.48" width="0.3" layer="91" />
                <label x="1211.58" y="1554.48" size="1.27" layer="95" />
                <pinref part="R444" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1671.32" x2="1361.44" y2="1671.32" width="0.3" layer="91" />
                <label x="1361.44" y="1671.32" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="COUT" />
              </segment>
            </net>
            <net name="N$85">
              <segment>
                <wire x1="1526.54" y1="1849.12" x2="1524.00" y2="1849.12" width="0.3" layer="91" />
                <label x="1524.00" y="1849.12" size="1.27" layer="95" />
                <pinref part="R445" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1590.04" x2="1287.78" y2="1590.04" width="0.3" layer="91" />
                <label x="1287.78" y="1590.04" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="OUT4" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1671.32" x2="1325.88" y2="1671.32" width="0.3" layer="91" />
                <label x="1325.88" y="1671.32" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="CTRLC" />
              </segment>
            </net>
            <net name="N$86">
              <segment>
                <wire x1="1376.68" y1="1767.84" x2="1379.22" y2="1767.84" width="0.3" layer="91" />
                <label x="1379.22" y="1767.84" size="1.27" layer="95" />
                <pinref part="R446" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1668.78" x2="1361.44" y2="1668.78" width="0.3" layer="91" />
                <label x="1361.44" y="1668.78" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="CIN" />
              </segment>
              <segment>
                <wire x1="1508.76" y1="1849.12" x2="1506.22" y2="1849.12" width="0.3" layer="91" />
                <label x="1506.22" y="1849.12" size="1.27" layer="95" />
                <pinref part="R447" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$87">
              <segment>
                <wire x1="1226.82" y1="1554.48" x2="1229.36" y2="1554.48" width="0.3" layer="91" />
                <label x="1229.36" y="1554.48" size="1.27" layer="95" />
                <pinref part="R454" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1678.94" x2="1325.88" y2="1678.94" width="0.3" layer="91" />
                <label x="1325.88" y="1678.94" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="BOUT" />
              </segment>
            </net>
            <net name="N$88">
              <segment>
                <wire x1="1579.88" y1="1871.98" x2="1577.34" y2="1871.98" width="0.3" layer="91" />
                <label x="1577.34" y="1871.98" size="1.27" layer="95" />
                <pinref part="R455" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1673.86" x2="1325.88" y2="1673.86" width="0.3" layer="91" />
                <label x="1325.88" y="1673.86" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1602.74" x2="1287.78" y2="1602.74" width="0.3" layer="91" />
                <label x="1287.78" y="1602.74" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="OUT3" />
              </segment>
            </net>
            <net name="N$89">
              <segment>
                <wire x1="1394.46" y1="1767.84" x2="1397.00" y2="1767.84" width="0.3" layer="91" />
                <label x="1397.00" y="1767.84" size="1.27" layer="95" />
                <pinref part="R456" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1676.40" x2="1325.88" y2="1676.40" width="0.3" layer="91" />
                <label x="1325.88" y="1676.40" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="BIN" />
              </segment>
              <segment>
                <wire x1="1526.54" y1="1856.74" x2="1524.00" y2="1856.74" width="0.3" layer="91" />
                <label x="1524.00" y="1856.74" size="1.27" layer="95" />
                <pinref part="R457" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$90">
              <segment>
                <wire x1="1244.60" y1="1562.10" x2="1247.14" y2="1562.10" width="0.3" layer="91" />
                <label x="1247.14" y="1562.10" size="1.27" layer="95" />
                <pinref part="R464" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1704.34" x2="1361.44" y2="1704.34" width="0.3" layer="91" />
                <label x="1361.44" y="1704.34" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="DIN" />
              </segment>
            </net>
            <net name="N$91">
              <segment>
                <wire x1="1544.32" y1="1864.36" x2="1541.78" y2="1864.36" width="0.3" layer="91" />
                <label x="1541.78" y="1864.36" size="1.27" layer="95" />
                <pinref part="R465" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1706.88" x2="1361.44" y2="1706.88" width="0.3" layer="91" />
                <label x="1361.44" y="1706.88" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1628.14" x2="1287.78" y2="1628.14" width="0.3" layer="91" />
                <label x="1287.78" y="1628.14" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$92">
              <segment>
                <wire x1="1394.46" y1="1775.46" x2="1397.00" y2="1775.46" width="0.3" layer="91" />
                <label x="1397.00" y="1775.46" size="1.27" layer="95" />
                <pinref part="R466" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1544.32" y1="1856.74" x2="1541.78" y2="1856.74" width="0.3" layer="91" />
                <label x="1541.78" y="1856.74" size="1.27" layer="95" />
                <pinref part="R467" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1701.80" x2="1361.44" y2="1701.80" width="0.3" layer="91" />
                <label x="1361.44" y="1701.80" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="DOUT" />
              </segment>
            </net>
            <net name="N$93">
              <segment>
                <wire x1="1226.82" y1="1562.10" x2="1229.36" y2="1562.10" width="0.3" layer="91" />
                <label x="1229.36" y="1562.10" size="1.27" layer="95" />
                <pinref part="R474" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1699.26" x2="1361.44" y2="1699.26" width="0.3" layer="91" />
                <label x="1361.44" y="1699.26" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="COUT" />
              </segment>
            </net>
            <net name="N$94">
              <segment>
                <wire x1="1562.10" y1="1871.98" x2="1559.56" y2="1871.98" width="0.3" layer="91" />
                <label x="1559.56" y="1871.98" size="1.27" layer="95" />
                <pinref part="R475" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1699.26" x2="1325.88" y2="1699.26" width="0.3" layer="91" />
                <label x="1325.88" y="1699.26" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="CTRLC" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1663.70" x2="1287.78" y2="1663.70" width="0.3" layer="91" />
                <label x="1287.78" y="1663.70" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="OUT4" />
              </segment>
            </net>
            <net name="N$95">
              <segment>
                <wire x1="1412.24" y1="1775.46" x2="1414.78" y2="1775.46" width="0.3" layer="91" />
                <label x="1414.78" y="1775.46" size="1.27" layer="95" />
                <pinref part="R476" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1562.10" y1="1864.36" x2="1559.56" y2="1864.36" width="0.3" layer="91" />
                <label x="1559.56" y="1864.36" size="1.27" layer="95" />
                <pinref part="R477" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1696.72" x2="1361.44" y2="1696.72" width="0.3" layer="91" />
                <label x="1361.44" y="1696.72" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="CIN" />
              </segment>
            </net>
            <net name="N$96">
              <segment>
                <wire x1="1244.60" y1="1569.72" x2="1247.14" y2="1569.72" width="0.3" layer="91" />
                <label x="1247.14" y="1569.72" size="1.27" layer="95" />
                <pinref part="R484" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1711.96" x2="1325.88" y2="1711.96" width="0.3" layer="91" />
                <label x="1325.88" y="1711.96" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="AIN" />
              </segment>
            </net>
            <net name="N$97">
              <segment>
                <wire x1="1597.66" y1="1879.60" x2="1595.12" y2="1879.60" width="0.3" layer="91" />
                <label x="1595.12" y="1879.60" size="1.27" layer="95" />
                <pinref part="R485" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1358.90" y1="1709.42" x2="1361.44" y2="1709.42" width="0.3" layer="91" />
                <label x="1361.44" y="1709.42" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="CTRLA" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1615.44" x2="1287.78" y2="1615.44" width="0.3" layer="91" />
                <label x="1287.78" y="1615.44" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$98">
              <segment>
                <wire x1="1475.74" y1="1818.64" x2="1478.28" y2="1818.64" width="0.3" layer="91" />
                <label x="1478.28" y="1818.64" size="1.27" layer="95" />
                <pinref part="R486" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1579.88" y1="1879.60" x2="1577.34" y2="1879.60" width="0.3" layer="91" />
                <label x="1577.34" y="1879.60" size="1.27" layer="95" />
                <pinref part="R487" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1709.42" x2="1325.88" y2="1709.42" width="0.3" layer="91" />
                <label x="1325.88" y="1709.42" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="AOUT" />
              </segment>
            </net>
            <net name="N$99">
              <segment>
                <wire x1="1262.38" y1="1569.72" x2="1264.92" y2="1569.72" width="0.3" layer="91" />
                <label x="1264.92" y="1569.72" size="1.27" layer="95" />
                <pinref part="R494" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1706.88" x2="1325.88" y2="1706.88" width="0.3" layer="91" />
                <label x="1325.88" y="1706.88" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="BOUT" />
              </segment>
            </net>
            <net name="N$100">
              <segment>
                <wire x1="1615.44" y1="1887.22" x2="1612.90" y2="1887.22" width="0.3" layer="91" />
                <label x="1612.90" y="1887.22" size="1.27" layer="95" />
                <pinref part="R495" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1701.80" x2="1325.88" y2="1701.80" width="0.3" layer="91" />
                <label x="1325.88" y="1701.80" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1676.40" x2="1287.78" y2="1676.40" width="0.3" layer="91" />
                <label x="1287.78" y="1676.40" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="OUT3" />
              </segment>
            </net>
            <net name="N$101">
              <segment>
                <wire x1="1412.24" y1="1783.08" x2="1414.78" y2="1783.08" width="0.3" layer="91" />
                <label x="1414.78" y="1783.08" size="1.27" layer="95" />
                <pinref part="R496" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1597.66" y1="1887.22" x2="1595.12" y2="1887.22" width="0.3" layer="91" />
                <label x="1595.12" y="1887.22" size="1.27" layer="95" />
                <pinref part="R497" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1704.34" x2="1325.88" y2="1704.34" width="0.3" layer="91" />
                <label x="1325.88" y="1704.34" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="BIN" />
              </segment>
            </net>
            <net name="N$102">
              <segment>
                <wire x1="1262.38" y1="1577.34" x2="1264.92" y2="1577.34" width="0.3" layer="91" />
                <label x="1264.92" y="1577.34" size="1.27" layer="95" />
                <pinref part="R504" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1793.24" x2="1452.88" y2="1793.24" width="0.3" layer="91" />
                <label x="1452.88" y="1793.24" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="DIN" />
              </segment>
            </net>
            <net name="N$103">
              <segment>
                <wire x1="1633.22" y1="1894.84" x2="1630.68" y2="1894.84" width="0.3" layer="91" />
                <label x="1630.68" y="1894.84" size="1.27" layer="95" />
                <pinref part="R505" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1795.78" x2="1452.88" y2="1795.78" width="0.3" layer="91" />
                <label x="1452.88" y="1795.78" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1701.80" x2="1287.78" y2="1701.80" width="0.3" layer="91" />
                <label x="1287.78" y="1701.80" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$104">
              <segment>
                <wire x1="1475.74" y1="1826.26" x2="1478.28" y2="1826.26" width="0.3" layer="91" />
                <label x="1478.28" y="1826.26" size="1.27" layer="95" />
                <pinref part="R506" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1615.44" y1="1894.84" x2="1612.90" y2="1894.84" width="0.3" layer="91" />
                <label x="1612.90" y="1894.84" size="1.27" layer="95" />
                <pinref part="R507" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1790.70" x2="1452.88" y2="1790.70" width="0.3" layer="91" />
                <label x="1452.88" y="1790.70" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="DOUT" />
              </segment>
            </net>
            <net name="N$105">
              <segment>
                <wire x1="1303.02" y1="1651.00" x2="1305.56" y2="1651.00" width="0.3" layer="91" />
                <label x="1305.56" y="1651.00" size="1.27" layer="95" />
                <pinref part="R514" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1788.16" x2="1452.88" y2="1788.16" width="0.3" layer="91" />
                <label x="1452.88" y="1788.16" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="COUT" />
              </segment>
            </net>
            <net name="N$106">
              <segment>
                <wire x1="1651.00" y1="1910.08" x2="1648.46" y2="1910.08" width="0.3" layer="91" />
                <label x="1648.46" y="1910.08" size="1.27" layer="95" />
                <pinref part="R515" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1788.16" x2="1417.32" y2="1788.16" width="0.3" layer="91" />
                <label x="1417.32" y="1788.16" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="CTRLC" />
              </segment>
              <segment>
                <wire x1="1381.76" y1="1706.88" x2="1384.30" y2="1706.88" width="0.3" layer="91" />
                <label x="1384.30" y="1706.88" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="OUT4" />
              </segment>
            </net>
            <net name="N$107">
              <segment>
                <wire x1="1511.30" y1="1833.88" x2="1513.84" y2="1833.88" width="0.3" layer="91" />
                <label x="1513.84" y="1833.88" size="1.27" layer="95" />
                <pinref part="R516" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1633.22" y1="1902.46" x2="1630.68" y2="1902.46" width="0.3" layer="91" />
                <label x="1630.68" y="1902.46" size="1.27" layer="95" />
                <pinref part="R517" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1785.62" x2="1452.88" y2="1785.62" width="0.3" layer="91" />
                <label x="1452.88" y="1785.62" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="CIN" />
              </segment>
            </net>
            <net name="N$108">
              <segment>
                <wire x1="1303.02" y1="1658.62" x2="1305.56" y2="1658.62" width="0.3" layer="91" />
                <label x="1305.56" y="1658.62" size="1.27" layer="95" />
                <pinref part="R524" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1800.86" x2="1417.32" y2="1800.86" width="0.3" layer="91" />
                <label x="1417.32" y="1800.86" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="AIN" />
              </segment>
            </net>
            <net name="N$109">
              <segment>
                <wire x1="1668.78" y1="1910.08" x2="1666.24" y2="1910.08" width="0.3" layer="91" />
                <label x="1666.24" y="1910.08" size="1.27" layer="95" />
                <pinref part="R525" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1798.32" x2="1452.88" y2="1798.32" width="0.3" layer="91" />
                <label x="1452.88" y="1798.32" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="CTRLA" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="1689.10" x2="1287.78" y2="1689.10" width="0.3" layer="91" />
                <label x="1287.78" y="1689.10" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$110">
              <segment>
                <wire x1="1493.52" y1="1826.26" x2="1496.06" y2="1826.26" width="0.3" layer="91" />
                <label x="1496.06" y="1826.26" size="1.27" layer="95" />
                <pinref part="R526" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1651.00" y1="1902.46" x2="1648.46" y2="1902.46" width="0.3" layer="91" />
                <label x="1648.46" y="1902.46" size="1.27" layer="95" />
                <pinref part="R527" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1798.32" x2="1417.32" y2="1798.32" width="0.3" layer="91" />
                <label x="1417.32" y="1798.32" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="AOUT" />
              </segment>
            </net>
            <net name="N$111">
              <segment>
                <wire x1="1320.80" y1="1658.62" x2="1323.34" y2="1658.62" width="0.3" layer="91" />
                <label x="1323.34" y="1658.62" size="1.27" layer="95" />
                <pinref part="R534" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1795.78" x2="1417.32" y2="1795.78" width="0.3" layer="91" />
                <label x="1417.32" y="1795.78" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="BOUT" />
              </segment>
            </net>
            <net name="N$112">
              <segment>
                <wire x1="1668.78" y1="1917.70" x2="1666.24" y2="1917.70" width="0.3" layer="91" />
                <label x="1666.24" y="1917.70" size="1.27" layer="95" />
                <pinref part="R535" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1790.70" x2="1417.32" y2="1790.70" width="0.3" layer="91" />
                <label x="1417.32" y="1790.70" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1381.76" y1="1719.58" x2="1384.30" y2="1719.58" width="0.3" layer="91" />
                <label x="1384.30" y="1719.58" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="OUT3" />
              </segment>
            </net>
            <net name="N$113">
              <segment>
                <wire x1="1493.52" y1="1833.88" x2="1496.06" y2="1833.88" width="0.3" layer="91" />
                <label x="1496.06" y="1833.88" size="1.27" layer="95" />
                <pinref part="R536" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1686.56" y1="1925.32" x2="1684.02" y2="1925.32" width="0.3" layer="91" />
                <label x="1684.02" y="1925.32" size="1.27" layer="95" />
                <pinref part="R537" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1793.24" x2="1417.32" y2="1793.24" width="0.3" layer="91" />
                <label x="1417.32" y="1793.24" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="BIN" />
              </segment>
            </net>
            <net name="N$114">
              <segment>
                <wire x1="1328.42" y1="1668.78" x2="1325.88" y2="1668.78" width="0.3" layer="91" />
                <label x="1325.88" y="1668.78" size="1.27" layer="95" />
                <pinref part="U32" gate="G$1" pin="VSS" />
              </segment>
              <segment>
                <wire x1="58.42" y1="66.04" x2="55.88" y2="66.04" width="0.3" layer="91" />
                <label x="55.88" y="66.04" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-9" />
              </segment>
              <segment>
                <wire x1="1374.14" y1="1694.18" x2="1374.14" y2="1691.64" width="0.3" layer="91" />
                <label x="1374.14" y="1691.64" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1277.62" y1="1651.00" x2="1277.62" y2="1648.46" width="0.3" layer="91" />
                <label x="1277.62" y="1648.46" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1277.62" y1="1577.34" x2="1277.62" y2="1574.80" width="0.3" layer="91" />
                <label x="1277.62" y="1574.80" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="V-" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1813.56" x2="1417.32" y2="1813.56" width="0.3" layer="91" />
                <label x="1417.32" y="1813.56" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="VSS" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1821.18" x2="1417.32" y2="1821.18" width="0.3" layer="91" />
                <label x="1417.32" y="1821.18" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="BIN" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1818.64" x2="1417.32" y2="1818.64" width="0.3" layer="91" />
                <label x="1417.32" y="1818.64" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1823.72" x2="1417.32" y2="1823.72" width="0.3" layer="91" />
                <label x="1417.32" y="1823.72" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="BOUT" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1818.64" x2="1452.88" y2="1818.64" width="0.3" layer="91" />
                <label x="1452.88" y="1818.64" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="DOUT" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1821.18" x2="1452.88" y2="1821.18" width="0.3" layer="91" />
                <label x="1452.88" y="1821.18" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="DIN" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1823.72" x2="1452.88" y2="1823.72" width="0.3" layer="91" />
                <label x="1452.88" y="1823.72" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1816.10" x2="1452.88" y2="1816.10" width="0.3" layer="91" />
                <label x="1452.88" y="1816.10" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="COUT" />
              </segment>
              <segment>
                <wire x1="1450.34" y1="1813.56" x2="1452.88" y2="1813.56" width="0.3" layer="91" />
                <label x="1452.88" y="1813.56" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="CIN" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1816.10" x2="1417.32" y2="1816.10" width="0.3" layer="91" />
                <label x="1417.32" y="1816.10" size="1.27" layer="95" />
                <pinref part="U38" gate="G$1" pin="CTRLC" />
              </segment>
              <segment>
                <wire x1="1419.86" y1="1785.62" x2="1417.32" y2="1785.62" width="0.3" layer="91" />
                <label x="1417.32" y="1785.62" size="1.27" layer="95" />
                <pinref part="U36" gate="G$1" pin="VSS" />
              </segment>
              <segment>
                <wire x1="1328.42" y1="1696.72" x2="1325.88" y2="1696.72" width="0.3" layer="91" />
                <label x="1325.88" y="1696.72" size="1.27" layer="95" />
                <pinref part="U34" gate="G$1" pin="VSS" />
              </segment>
            </net>
            <net name="N$115">
              <segment>
                <wire x1="1270.00" y1="1592.58" x2="1267.46" y2="1592.58" width="0.3" layer="91" />
                <label x="1267.46" y="1592.58" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="+IN4" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="647.70" x2="1249.68" y2="647.70" width="0.3" layer="91" />
                <label x="1249.68" y="647.70" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1087.12" y1="431.80" x2="1087.12" y2="429.26" width="0.3" layer="91" />
                <label x="1087.12" y="429.26" size="1.27" layer="95" />
                <pinref part="C136" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="642.62" x2="1249.68" y2="642.62" width="0.3" layer="91" />
                <label x="1249.68" y="642.62" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$116">
              <segment>
                <wire x1="1270.00" y1="1605.28" x2="1267.46" y2="1605.28" width="0.3" layer="91" />
                <label x="1267.46" y="1605.28" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="+IN3" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="652.78" x2="1214.12" y2="652.78" width="0.3" layer="91" />
                <label x="1214.12" y="652.78" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="647.70" x2="1214.12" y2="647.70" width="0.3" layer="91" />
                <label x="1214.12" y="647.70" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1087.12" y1="449.58" x2="1087.12" y2="447.04" width="0.3" layer="91" />
                <label x="1087.12" y="447.04" size="1.27" layer="95" />
                <pinref part="C146" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$117">
              <segment>
                <wire x1="1270.00" y1="1630.68" x2="1267.46" y2="1630.68" width="0.3" layer="91" />
                <label x="1267.46" y="1630.68" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="+IN1" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="675.64" x2="1287.78" y2="675.64" width="0.3" layer="91" />
                <label x="1287.78" y="675.64" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1094.74" y1="449.58" x2="1094.74" y2="447.04" width="0.3" layer="91" />
                <label x="1094.74" y="447.04" size="1.27" layer="95" />
                <pinref part="C156" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="670.56" x2="1287.78" y2="670.56" width="0.3" layer="91" />
                <label x="1287.78" y="670.56" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$118">
              <segment>
                <wire x1="1270.00" y1="1617.98" x2="1267.46" y2="1617.98" width="0.3" layer="91" />
                <label x="1267.46" y="1617.98" size="1.27" layer="95" />
                <pinref part="U33" gate="G$1" pin="+IN2" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="759.46" x2="1363.98" y2="759.46" width="0.3" layer="91" />
                <label x="1363.98" y="759.46" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="754.38" x2="1363.98" y2="754.38" width="0.3" layer="91" />
                <label x="1363.98" y="754.38" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1148.08" y1="500.38" x2="1148.08" y2="497.84" width="0.3" layer="91" />
                <label x="1148.08" y="497.84" size="1.27" layer="95" />
                <pinref part="C176" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$119">
              <segment>
                <wire x1="1270.00" y1="1678.94" x2="1267.46" y2="1678.94" width="0.3" layer="91" />
                <label x="1267.46" y="1678.94" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="+IN3" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="764.54" x2="1328.42" y2="764.54" width="0.3" layer="91" />
                <label x="1328.42" y="764.54" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1155.70" y1="508.00" x2="1155.70" y2="505.46" width="0.3" layer="91" />
                <label x="1155.70" y="505.46" size="1.27" layer="95" />
                <pinref part="C186" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="759.46" x2="1328.42" y2="759.46" width="0.3" layer="91" />
                <label x="1328.42" y="759.46" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$120">
              <segment>
                <wire x1="1270.00" y1="1691.64" x2="1267.46" y2="1691.64" width="0.3" layer="91" />
                <label x="1267.46" y="1691.64" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="+IN2" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="731.52" x2="1363.98" y2="731.52" width="0.3" layer="91" />
                <label x="1363.98" y="731.52" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="726.44" x2="1363.98" y2="726.44" width="0.3" layer="91" />
                <label x="1363.98" y="726.44" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="1170.94" y1="525.78" x2="1170.94" y2="523.24" width="0.3" layer="91" />
                <label x="1170.94" y="523.24" size="1.27" layer="95" />
                <pinref part="C216" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$121">
              <segment>
                <wire x1="1270.00" y1="1666.24" x2="1267.46" y2="1666.24" width="0.3" layer="91" />
                <label x="1267.46" y="1666.24" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="+IN4" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="680.72" x2="1252.22" y2="680.72" width="0.3" layer="91" />
                <label x="1252.22" y="680.72" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1148.08" y1="482.60" x2="1148.08" y2="480.06" width="0.3" layer="91" />
                <label x="1148.08" y="480.06" size="1.27" layer="95" />
                <pinref part="C166" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="675.64" x2="1252.22" y2="675.64" width="0.3" layer="91" />
                <label x="1252.22" y="675.64" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$122">
              <segment>
                <wire x1="1270.00" y1="1704.34" x2="1267.46" y2="1704.34" width="0.3" layer="91" />
                <label x="1267.46" y="1704.34" size="1.27" layer="95" />
                <pinref part="U35" gate="G$1" pin="+IN1" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="703.58" x2="1325.88" y2="703.58" width="0.3" layer="91" />
                <label x="1325.88" y="703.58" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="1163.32" y1="508.00" x2="1163.32" y2="505.46" width="0.3" layer="91" />
                <label x="1163.32" y="505.46" size="1.27" layer="95" />
                <pinref part="C196" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="698.50" x2="1325.88" y2="698.50" width="0.3" layer="91" />
                <label x="1325.88" y="698.50" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$123">
              <segment>
                <wire x1="1366.52" y1="1709.42" x2="1363.98" y2="1709.42" width="0.3" layer="91" />
                <label x="1363.98" y="1709.42" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="+IN4" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="708.66" x2="1290.32" y2="708.66" width="0.3" layer="91" />
                <label x="1290.32" y="708.66" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1163.32" y1="525.78" x2="1163.32" y2="523.24" width="0.3" layer="91" />
                <label x="1163.32" y="523.24" size="1.27" layer="95" />
                <pinref part="C206" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="703.58" x2="1290.32" y2="703.58" width="0.3" layer="91" />
                <label x="1290.32" y="703.58" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$124">
              <segment>
                <wire x1="1366.52" y1="1722.12" x2="1363.98" y2="1722.12" width="0.3" layer="91" />
                <label x="1363.98" y="1722.12" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="+IN3" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="736.60" x2="1328.42" y2="736.60" width="0.3" layer="91" />
                <label x="1328.42" y="736.60" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="731.52" x2="1328.42" y2="731.52" width="0.3" layer="91" />
                <label x="1328.42" y="731.52" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="1170.94" y1="543.56" x2="1170.94" y2="541.02" width="0.3" layer="91" />
                <label x="1170.94" y="541.02" size="1.27" layer="95" />
                <pinref part="C226" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$125">
              <segment>
                <wire x1="1366.52" y1="1742.44" x2="1363.98" y2="1742.44" width="0.3" layer="91" />
                <label x="1363.98" y="1742.44" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="-IN1" />
              </segment>
              <segment>
                <wire x1="1366.52" y1="1734.82" x2="1363.98" y2="1734.82" width="0.3" layer="91" />
                <label x="1363.98" y="1734.82" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="+IN2" />
              </segment>
              <segment>
                <wire x1="2133.60" y1="162.56" x2="2133.60" y2="160.02" width="0.3" layer="91" />
                <label x="2133.60" y="160.02" size="1.27" layer="95" />
                <pinref part="C242" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2125.98" y1="165.10" x2="2128.52" y2="165.10" width="0.3" layer="91" />
                <label x="2128.52" y="165.10" size="1.27" layer="95" />
                <pinref part="R433" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2115.82" y1="154.94" x2="2113.28" y2="154.94" width="0.3" layer="91" />
                <label x="2113.28" y="154.94" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="45.72" y1="73.66" x2="43.18" y2="73.66" width="0.3" layer="91" />
                <label x="43.18" y="73.66" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-6" />
              </segment>
            </net>
            <net name="N$126">
              <segment>
                <wire x1="1381.76" y1="1744.98" x2="1384.30" y2="1744.98" width="0.3" layer="91" />
                <label x="1384.30" y="1744.98" size="1.27" layer="95" />
                <pinref part="U37" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="289.56" y1="2405.38" x2="287.02" y2="2405.38" width="0.3" layer="91" />
                <label x="287.02" y="2405.38" size="1.27" layer="95" />
                <pinref part="R177" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$127">
              <segment>
                <wire x1="830.58" y1="109.22" x2="830.58" y2="106.68" width="0.3" layer="91" />
                <label x="830.58" y="106.68" size="1.27" layer="95" />
                <pinref part="C135" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="838.20" y1="104.14" x2="840.74" y2="104.14" width="0.3" layer="91" />
                <label x="840.74" y="104.14" size="1.27" layer="95" />
                <pinref part="D37" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="944.88" y1="381.00" x2="942.34" y2="381.00" width="0.3" layer="91" />
                <label x="942.34" y="381.00" size="1.27" layer="95" />
                <pinref part="R308" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="855.98" y1="342.90" x2="853.44" y2="342.90" width="0.3" layer="91" />
                <label x="853.44" y="342.90" size="1.27" layer="95" />
                <pinref part="R307" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$128">
              <segment>
                <wire x1="1087.12" y1="439.42" x2="1087.12" y2="441.96" width="0.3" layer="91" />
                <label x="1087.12" y="441.96" size="1.27" layer="95" />
                <pinref part="C136" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1033.78" y1="419.10" x2="1031.24" y2="419.10" width="0.3" layer="91" />
                <label x="1031.24" y="419.10" size="1.27" layer="95" />
                <pinref part="R309" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="955.04" y1="381.00" x2="957.58" y2="381.00" width="0.3" layer="91" />
                <label x="957.58" y="381.00" size="1.27" layer="95" />
                <pinref part="R308" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$129">
              <segment>
                <wire x1="1178.56" y1="543.56" x2="1178.56" y2="541.02" width="0.3" layer="91" />
                <label x="1178.56" y="541.02" size="1.27" layer="95" />
                <pinref part="C137" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="637.54" x2="1249.68" y2="637.54" width="0.3" layer="91" />
                <label x="1249.68" y="637.54" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="419.10" x2="1046.48" y2="419.10" width="0.3" layer="91" />
                <label x="1046.48" y="419.10" size="1.27" layer="95" />
                <pinref part="R309" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$130">
              <segment>
                <wire x1="830.58" y1="129.54" x2="830.58" y2="127.00" width="0.3" layer="91" />
                <label x="830.58" y="127.00" size="1.27" layer="95" />
                <pinref part="C145" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="820.42" y1="124.46" x2="822.96" y2="124.46" width="0.3" layer="91" />
                <label x="822.96" y="124.46" size="1.27" layer="95" />
                <pinref part="D38" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="962.66" y1="381.00" x2="960.12" y2="381.00" width="0.3" layer="91" />
                <label x="960.12" y="381.00" size="1.27" layer="95" />
                <pinref part="R318" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="873.76" y1="342.90" x2="871.22" y2="342.90" width="0.3" layer="91" />
                <label x="871.22" y="342.90" size="1.27" layer="95" />
                <pinref part="R317" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$131">
              <segment>
                <wire x1="1087.12" y1="457.20" x2="1087.12" y2="459.74" width="0.3" layer="91" />
                <label x="1087.12" y="459.74" size="1.27" layer="95" />
                <pinref part="C146" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1051.56" y1="419.10" x2="1049.02" y2="419.10" width="0.3" layer="91" />
                <label x="1049.02" y="419.10" size="1.27" layer="95" />
                <pinref part="R319" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="972.82" y1="381.00" x2="975.36" y2="381.00" width="0.3" layer="91" />
                <label x="975.36" y="381.00" size="1.27" layer="95" />
                <pinref part="R318" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$132">
              <segment>
                <wire x1="1178.56" y1="561.34" x2="1178.56" y2="558.80" width="0.3" layer="91" />
                <label x="1178.56" y="558.80" size="1.27" layer="95" />
                <pinref part="C147" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1061.72" y1="419.10" x2="1064.26" y2="419.10" width="0.3" layer="91" />
                <label x="1064.26" y="419.10" size="1.27" layer="95" />
                <pinref part="R319" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="642.62" x2="1214.12" y2="642.62" width="0.3" layer="91" />
                <label x="1214.12" y="642.62" size="1.27" layer="95" />
                <pinref part="U63" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$133">
              <segment>
                <wire x1="830.58" y1="149.86" x2="830.58" y2="147.32" width="0.3" layer="91" />
                <label x="830.58" y="147.32" size="1.27" layer="95" />
                <pinref part="C155" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="820.42" y1="144.78" x2="822.96" y2="144.78" width="0.3" layer="91" />
                <label x="822.96" y="144.78" size="1.27" layer="95" />
                <pinref part="D39" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="962.66" y1="388.62" x2="960.12" y2="388.62" width="0.3" layer="91" />
                <label x="960.12" y="388.62" size="1.27" layer="95" />
                <pinref part="R328" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="873.76" y1="350.52" x2="871.22" y2="350.52" width="0.3" layer="91" />
                <label x="871.22" y="350.52" size="1.27" layer="95" />
                <pinref part="R327" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$134">
              <segment>
                <wire x1="1094.74" y1="457.20" x2="1094.74" y2="459.74" width="0.3" layer="91" />
                <label x="1094.74" y="459.74" size="1.27" layer="95" />
                <pinref part="C156" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1094.74" y1="469.90" x2="1092.20" y2="469.90" width="0.3" layer="91" />
                <label x="1092.20" y="469.90" size="1.27" layer="95" />
                <pinref part="R329" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="972.82" y1="388.62" x2="975.36" y2="388.62" width="0.3" layer="91" />
                <label x="975.36" y="388.62" size="1.27" layer="95" />
                <pinref part="R328" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$135">
              <segment>
                <wire x1="1186.18" y1="561.34" x2="1186.18" y2="558.80" width="0.3" layer="91" />
                <label x="1186.18" y="558.80" size="1.27" layer="95" />
                <pinref part="C157" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="665.48" x2="1287.78" y2="665.48" width="0.3" layer="91" />
                <label x="1287.78" y="665.48" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1104.90" y1="469.90" x2="1107.44" y2="469.90" width="0.3" layer="91" />
                <label x="1107.44" y="469.90" size="1.27" layer="95" />
                <pinref part="R329" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$136">
              <segment>
                <wire x1="820.42" y1="170.18" x2="820.42" y2="167.64" width="0.3" layer="91" />
                <label x="820.42" y="167.64" size="1.27" layer="95" />
                <pinref part="C165" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="822.96" y1="165.10" x2="825.50" y2="165.10" width="0.3" layer="91" />
                <label x="825.50" y="165.10" size="1.27" layer="95" />
                <pinref part="D40" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="980.44" y1="388.62" x2="977.90" y2="388.62" width="0.3" layer="91" />
                <label x="977.90" y="388.62" size="1.27" layer="95" />
                <pinref part="R338" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="891.54" y1="350.52" x2="889.00" y2="350.52" width="0.3" layer="91" />
                <label x="889.00" y="350.52" size="1.27" layer="95" />
                <pinref part="R337" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$137">
              <segment>
                <wire x1="1148.08" y1="490.22" x2="1148.08" y2="492.76" width="0.3" layer="91" />
                <label x="1148.08" y="492.76" size="1.27" layer="95" />
                <pinref part="C166" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1112.52" y1="469.90" x2="1109.98" y2="469.90" width="0.3" layer="91" />
                <label x="1109.98" y="469.90" size="1.27" layer="95" />
                <pinref part="R339" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="990.60" y1="388.62" x2="993.14" y2="388.62" width="0.3" layer="91" />
                <label x="993.14" y="388.62" size="1.27" layer="95" />
                <pinref part="R338" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$138">
              <segment>
                <wire x1="1186.18" y1="579.12" x2="1186.18" y2="576.58" width="0.3" layer="91" />
                <label x="1186.18" y="576.58" size="1.27" layer="95" />
                <pinref part="C167" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="670.56" x2="1252.22" y2="670.56" width="0.3" layer="91" />
                <label x="1252.22" y="670.56" size="1.27" layer="95" />
                <pinref part="U62" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1122.68" y1="469.90" x2="1125.22" y2="469.90" width="0.3" layer="91" />
                <label x="1125.22" y="469.90" size="1.27" layer="95" />
                <pinref part="R339" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$139">
              <segment>
                <wire x1="833.12" y1="279.40" x2="833.12" y2="276.86" width="0.3" layer="91" />
                <label x="833.12" y="276.86" size="1.27" layer="95" />
                <pinref part="C175" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="833.12" y1="185.42" x2="835.66" y2="185.42" width="0.3" layer="91" />
                <label x="835.66" y="185.42" size="1.27" layer="95" />
                <pinref part="D41" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="980.44" y1="396.24" x2="977.90" y2="396.24" width="0.3" layer="91" />
                <label x="977.90" y="396.24" size="1.27" layer="95" />
                <pinref part="R348" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="891.54" y1="358.14" x2="889.00" y2="358.14" width="0.3" layer="91" />
                <label x="889.00" y="358.14" size="1.27" layer="95" />
                <pinref part="R347" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$140">
              <segment>
                <wire x1="1148.08" y1="508.00" x2="1148.08" y2="510.54" width="0.3" layer="91" />
                <label x="1148.08" y="510.54" size="1.27" layer="95" />
                <pinref part="C176" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1112.52" y1="477.52" x2="1109.98" y2="477.52" width="0.3" layer="91" />
                <label x="1109.98" y="477.52" size="1.27" layer="95" />
                <pinref part="R349" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="990.60" y1="396.24" x2="993.14" y2="396.24" width="0.3" layer="91" />
                <label x="993.14" y="396.24" size="1.27" layer="95" />
                <pinref part="R348" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$141">
              <segment>
                <wire x1="1193.80" y1="579.12" x2="1193.80" y2="576.58" width="0.3" layer="91" />
                <label x="1193.80" y="576.58" size="1.27" layer="95" />
                <pinref part="C177" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1122.68" y1="477.52" x2="1125.22" y2="477.52" width="0.3" layer="91" />
                <label x="1125.22" y="477.52" size="1.27" layer="95" />
                <pinref part="R349" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="749.30" x2="1363.98" y2="749.30" width="0.3" layer="91" />
                <label x="1363.98" y="749.30" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$142">
              <segment>
                <wire x1="833.12" y1="297.18" x2="833.12" y2="294.64" width="0.3" layer="91" />
                <label x="833.12" y="294.64" size="1.27" layer="95" />
                <pinref part="C185" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="825.50" y1="205.74" x2="828.04" y2="205.74" width="0.3" layer="91" />
                <label x="828.04" y="205.74" size="1.27" layer="95" />
                <pinref part="D42" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="998.22" y1="396.24" x2="995.68" y2="396.24" width="0.3" layer="91" />
                <label x="995.68" y="396.24" size="1.27" layer="95" />
                <pinref part="R358" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="909.32" y1="358.14" x2="906.78" y2="358.14" width="0.3" layer="91" />
                <label x="906.78" y="358.14" size="1.27" layer="95" />
                <pinref part="R357" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$143">
              <segment>
                <wire x1="1155.70" y1="515.62" x2="1155.70" y2="518.16" width="0.3" layer="91" />
                <label x="1155.70" y="518.16" size="1.27" layer="95" />
                <pinref part="C186" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1130.30" y1="477.52" x2="1127.76" y2="477.52" width="0.3" layer="91" />
                <label x="1127.76" y="477.52" size="1.27" layer="95" />
                <pinref part="R359" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1008.38" y1="396.24" x2="1010.92" y2="396.24" width="0.3" layer="91" />
                <label x="1010.92" y="396.24" size="1.27" layer="95" />
                <pinref part="R358" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$144">
              <segment>
                <wire x1="1193.80" y1="596.90" x2="1193.80" y2="594.36" width="0.3" layer="91" />
                <label x="1193.80" y="594.36" size="1.27" layer="95" />
                <pinref part="C187" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="754.38" x2="1328.42" y2="754.38" width="0.3" layer="91" />
                <label x="1328.42" y="754.38" size="1.27" layer="95" />
                <pinref part="U61" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1140.46" y1="477.52" x2="1143.00" y2="477.52" width="0.3" layer="91" />
                <label x="1143.00" y="477.52" size="1.27" layer="95" />
                <pinref part="R359" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$145">
              <segment>
                <wire x1="840.74" y1="297.18" x2="840.74" y2="294.64" width="0.3" layer="91" />
                <label x="840.74" y="294.64" size="1.27" layer="95" />
                <pinref part="C195" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="825.50" y1="228.60" x2="828.04" y2="228.60" width="0.3" layer="91" />
                <label x="828.04" y="228.60" size="1.27" layer="95" />
                <pinref part="D43" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="998.22" y1="403.86" x2="995.68" y2="403.86" width="0.3" layer="91" />
                <label x="995.68" y="403.86" size="1.27" layer="95" />
                <pinref part="R368" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="909.32" y1="365.76" x2="906.78" y2="365.76" width="0.3" layer="91" />
                <label x="906.78" y="365.76" size="1.27" layer="95" />
                <pinref part="R367" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$146">
              <segment>
                <wire x1="1163.32" y1="515.62" x2="1163.32" y2="518.16" width="0.3" layer="91" />
                <label x="1163.32" y="518.16" size="1.27" layer="95" />
                <pinref part="C196" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1051.56" y1="426.72" x2="1049.02" y2="426.72" width="0.3" layer="91" />
                <label x="1049.02" y="426.72" size="1.27" layer="95" />
                <pinref part="R369" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1008.38" y1="403.86" x2="1010.92" y2="403.86" width="0.3" layer="91" />
                <label x="1010.92" y="403.86" size="1.27" layer="95" />
                <pinref part="R368" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$147">
              <segment>
                <wire x1="1201.42" y1="596.90" x2="1201.42" y2="594.36" width="0.3" layer="91" />
                <label x="1201.42" y="594.36" size="1.27" layer="95" />
                <pinref part="C197" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="693.42" x2="1325.88" y2="693.42" width="0.3" layer="91" />
                <label x="1325.88" y="693.42" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="1061.72" y1="426.72" x2="1064.26" y2="426.72" width="0.3" layer="91" />
                <label x="1064.26" y="426.72" size="1.27" layer="95" />
                <pinref part="R369" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$148">
              <segment>
                <wire x1="840.74" y1="314.96" x2="840.74" y2="312.42" width="0.3" layer="91" />
                <label x="840.74" y="312.42" size="1.27" layer="95" />
                <pinref part="C205" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="825.50" y1="248.92" x2="828.04" y2="248.92" width="0.3" layer="91" />
                <label x="828.04" y="248.92" size="1.27" layer="95" />
                <pinref part="D44" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="403.86" x2="1013.46" y2="403.86" width="0.3" layer="91" />
                <label x="1013.46" y="403.86" size="1.27" layer="95" />
                <pinref part="R378" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="927.10" y1="365.76" x2="924.56" y2="365.76" width="0.3" layer="91" />
                <label x="924.56" y="365.76" size="1.27" layer="95" />
                <pinref part="R377" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$149">
              <segment>
                <wire x1="1163.32" y1="533.40" x2="1163.32" y2="535.94" width="0.3" layer="91" />
                <label x="1163.32" y="535.94" size="1.27" layer="95" />
                <pinref part="C206" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1069.34" y1="426.72" x2="1066.80" y2="426.72" width="0.3" layer="91" />
                <label x="1066.80" y="426.72" size="1.27" layer="95" />
                <pinref part="R379" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="403.86" x2="1028.70" y2="403.86" width="0.3" layer="91" />
                <label x="1028.70" y="403.86" size="1.27" layer="95" />
                <pinref part="R378" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$150">
              <segment>
                <wire x1="1201.42" y1="614.68" x2="1201.42" y2="612.14" width="0.3" layer="91" />
                <label x="1201.42" y="612.14" size="1.27" layer="95" />
                <pinref part="C207" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="698.50" x2="1290.32" y2="698.50" width="0.3" layer="91" />
                <label x="1290.32" y="698.50" size="1.27" layer="95" />
                <pinref part="U60" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="1079.50" y1="426.72" x2="1082.04" y2="426.72" width="0.3" layer="91" />
                <label x="1082.04" y="426.72" size="1.27" layer="95" />
                <pinref part="R379" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$151">
              <segment>
                <wire x1="848.36" y1="314.96" x2="848.36" y2="312.42" width="0.3" layer="91" />
                <label x="848.36" y="312.42" size="1.27" layer="95" />
                <pinref part="C215" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="825.50" y1="274.32" x2="828.04" y2="274.32" width="0.3" layer="91" />
                <label x="828.04" y="274.32" size="1.27" layer="95" />
                <pinref part="D45" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1033.78" y1="411.48" x2="1031.24" y2="411.48" width="0.3" layer="91" />
                <label x="1031.24" y="411.48" size="1.27" layer="95" />
                <pinref part="R388" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="927.10" y1="373.38" x2="924.56" y2="373.38" width="0.3" layer="91" />
                <label x="924.56" y="373.38" size="1.27" layer="95" />
                <pinref part="R387" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$152">
              <segment>
                <wire x1="1170.94" y1="533.40" x2="1170.94" y2="535.94" width="0.3" layer="91" />
                <label x="1170.94" y="535.94" size="1.27" layer="95" />
                <pinref part="C216" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1130.30" y1="485.14" x2="1127.76" y2="485.14" width="0.3" layer="91" />
                <label x="1127.76" y="485.14" size="1.27" layer="95" />
                <pinref part="R389" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1043.94" y1="411.48" x2="1046.48" y2="411.48" width="0.3" layer="91" />
                <label x="1046.48" y="411.48" size="1.27" layer="95" />
                <pinref part="R388" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$153">
              <segment>
                <wire x1="1209.04" y1="614.68" x2="1209.04" y2="612.14" width="0.3" layer="91" />
                <label x="1209.04" y="612.14" size="1.27" layer="95" />
                <pinref part="C217" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1140.46" y1="485.14" x2="1143.00" y2="485.14" width="0.3" layer="91" />
                <label x="1143.00" y="485.14" size="1.27" layer="95" />
                <pinref part="R389" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1361.44" y1="721.36" x2="1363.98" y2="721.36" width="0.3" layer="91" />
                <label x="1363.98" y="721.36" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$154">
              <segment>
                <wire x1="848.36" y1="332.74" x2="848.36" y2="330.20" width="0.3" layer="91" />
                <label x="848.36" y="330.20" size="1.27" layer="95" />
                <pinref part="C225" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="825.50" y1="297.18" x2="828.04" y2="297.18" width="0.3" layer="91" />
                <label x="828.04" y="297.18" size="1.27" layer="95" />
                <pinref part="D46" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="1016.00" y1="411.48" x2="1013.46" y2="411.48" width="0.3" layer="91" />
                <label x="1013.46" y="411.48" size="1.27" layer="95" />
                <pinref part="R398" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="944.88" y1="373.38" x2="942.34" y2="373.38" width="0.3" layer="91" />
                <label x="942.34" y="373.38" size="1.27" layer="95" />
                <pinref part="R397" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$155">
              <segment>
                <wire x1="1170.94" y1="551.18" x2="1170.94" y2="553.72" width="0.3" layer="91" />
                <label x="1170.94" y="553.72" size="1.27" layer="95" />
                <pinref part="C226" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="1069.34" y1="434.34" x2="1066.80" y2="434.34" width="0.3" layer="91" />
                <label x="1066.80" y="434.34" size="1.27" layer="95" />
                <pinref part="R399" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1026.16" y1="411.48" x2="1028.70" y2="411.48" width="0.3" layer="91" />
                <label x="1028.70" y="411.48" size="1.27" layer="95" />
                <pinref part="R398" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$156">
              <segment>
                <wire x1="1209.04" y1="632.46" x2="1209.04" y2="629.92" width="0.3" layer="91" />
                <label x="1209.04" y="629.92" size="1.27" layer="95" />
                <pinref part="C227" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="1079.50" y1="434.34" x2="1082.04" y2="434.34" width="0.3" layer="91" />
                <label x="1082.04" y="434.34" size="1.27" layer="95" />
                <pinref part="R399" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1330.96" y1="726.44" x2="1328.42" y2="726.44" width="0.3" layer="91" />
                <label x="1328.42" y="726.44" size="1.27" layer="95" />
                <pinref part="U59" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$157">
              <segment>
                <wire x1="833.12" y1="104.14" x2="830.58" y2="104.14" width="0.3" layer="91" />
                <label x="830.58" y="104.14" size="1.27" layer="95" />
                <pinref part="D37" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="292.10" y1="254.00" x2="292.10" y2="251.46" width="0.3" layer="91" />
                <label x="292.10" y="251.46" size="1.27" layer="95" />
                <pinref part="C134" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="309.88" y1="292.10" x2="312.42" y2="292.10" width="0.3" layer="91" />
                <label x="312.42" y="292.10" size="1.27" layer="95" />
                <pinref part="R306" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="317.50" y1="309.88" x2="314.96" y2="309.88" width="0.3" layer="91" />
                <label x="314.96" y="309.88" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$158">
              <segment>
                <wire x1="815.34" y1="124.46" x2="812.80" y2="124.46" width="0.3" layer="91" />
                <label x="812.80" y="124.46" size="1.27" layer="95" />
                <pinref part="D38" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="327.66" y1="320.04" x2="330.20" y2="320.04" width="0.3" layer="91" />
                <label x="330.20" y="320.04" size="1.27" layer="95" />
                <pinref part="R316" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="347.98" y1="304.80" x2="350.52" y2="304.80" width="0.3" layer="91" />
                <label x="350.52" y="304.80" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="292.10" y1="271.78" x2="292.10" y2="269.24" width="0.3" layer="91" />
                <label x="292.10" y="269.24" size="1.27" layer="95" />
                <pinref part="C144" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$159">
              <segment>
                <wire x1="815.34" y1="144.78" x2="812.80" y2="144.78" width="0.3" layer="91" />
                <label x="812.80" y="144.78" size="1.27" layer="95" />
                <pinref part="D39" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="480.06" y1="238.76" x2="480.06" y2="236.22" width="0.3" layer="91" />
                <label x="480.06" y="236.22" size="1.27" layer="95" />
                <pinref part="C154" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="505.46" y1="284.48" x2="508.00" y2="284.48" width="0.3" layer="91" />
                <label x="508.00" y="284.48" size="1.27" layer="95" />
                <pinref part="R326" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="513.08" y1="302.26" x2="510.54" y2="302.26" width="0.3" layer="91" />
                <label x="510.54" y="302.26" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$160">
              <segment>
                <wire x1="817.88" y1="165.10" x2="815.34" y2="165.10" width="0.3" layer="91" />
                <label x="815.34" y="165.10" size="1.27" layer="95" />
                <pinref part="D40" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="543.56" y1="297.18" x2="546.10" y2="297.18" width="0.3" layer="91" />
                <label x="546.10" y="297.18" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="264.16" x2="487.68" y2="261.62" width="0.3" layer="91" />
                <label x="487.68" y="261.62" size="1.27" layer="95" />
                <pinref part="C164" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="523.24" y1="312.42" x2="525.78" y2="312.42" width="0.3" layer="91" />
                <label x="525.78" y="312.42" size="1.27" layer="95" />
                <pinref part="R336" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$161">
              <segment>
                <wire x1="828.04" y1="185.42" x2="825.50" y2="185.42" width="0.3" layer="91" />
                <label x="825.50" y="185.42" size="1.27" layer="95" />
                <pinref part="D41" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="518.16" y1="518.16" x2="515.62" y2="518.16" width="0.3" layer="91" />
                <label x="515.62" y="518.16" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="457.20" y1="436.88" x2="457.20" y2="434.34" width="0.3" layer="91" />
                <label x="457.20" y="434.34" size="1.27" layer="95" />
                <pinref part="C174" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="510.54" y1="492.76" x2="513.08" y2="492.76" width="0.3" layer="91" />
                <label x="513.08" y="492.76" size="1.27" layer="95" />
                <pinref part="R346" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$162">
              <segment>
                <wire x1="820.42" y1="205.74" x2="817.88" y2="205.74" width="0.3" layer="91" />
                <label x="817.88" y="205.74" size="1.27" layer="95" />
                <pinref part="D42" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="528.32" y1="528.32" x2="530.86" y2="528.32" width="0.3" layer="91" />
                <label x="530.86" y="528.32" size="1.27" layer="95" />
                <pinref part="R356" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="492.76" y1="472.44" x2="492.76" y2="469.90" width="0.3" layer="91" />
                <label x="492.76" y="469.90" size="1.27" layer="95" />
                <pinref part="C184" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="548.64" y1="513.08" x2="551.18" y2="513.08" width="0.3" layer="91" />
                <label x="551.18" y="513.08" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$163">
              <segment>
                <wire x1="820.42" y1="228.60" x2="817.88" y2="228.60" width="0.3" layer="91" />
                <label x="817.88" y="228.60" size="1.27" layer="95" />
                <pinref part="D43" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="713.74" y1="469.90" x2="716.28" y2="469.90" width="0.3" layer="91" />
                <label x="716.28" y="469.90" size="1.27" layer="95" />
                <pinref part="R366" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="721.36" y1="487.68" x2="718.82" y2="487.68" width="0.3" layer="91" />
                <label x="718.82" y="487.68" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="688.34" y1="424.18" x2="688.34" y2="421.64" width="0.3" layer="91" />
                <label x="688.34" y="421.64" size="1.27" layer="95" />
                <pinref part="C194" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$164">
              <segment>
                <wire x1="820.42" y1="248.92" x2="817.88" y2="248.92" width="0.3" layer="91" />
                <label x="817.88" y="248.92" size="1.27" layer="95" />
                <pinref part="D44" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="751.84" y1="482.60" x2="754.38" y2="482.60" width="0.3" layer="91" />
                <label x="754.38" y="482.60" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="695.96" y1="441.96" x2="695.96" y2="439.42" width="0.3" layer="91" />
                <label x="695.96" y="439.42" size="1.27" layer="95" />
                <pinref part="C204" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="731.52" y1="497.84" x2="734.06" y2="497.84" width="0.3" layer="91" />
                <label x="734.06" y="497.84" size="1.27" layer="95" />
                <pinref part="R376" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$165">
              <segment>
                <wire x1="820.42" y1="274.32" x2="817.88" y2="274.32" width="0.3" layer="91" />
                <label x="817.88" y="274.32" size="1.27" layer="95" />
                <pinref part="D45" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="680.72" y1="640.08" x2="680.72" y2="637.54" width="0.3" layer="91" />
                <label x="680.72" y="637.54" size="1.27" layer="95" />
                <pinref part="C214" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="706.12" y1="685.80" x2="708.66" y2="685.80" width="0.3" layer="91" />
                <label x="708.66" y="685.80" size="1.27" layer="95" />
                <pinref part="R386" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="713.74" y1="703.58" x2="711.20" y2="703.58" width="0.3" layer="91" />
                <label x="711.20" y="703.58" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$166">
              <segment>
                <wire x1="820.42" y1="297.18" x2="817.88" y2="297.18" width="0.3" layer="91" />
                <label x="817.88" y="297.18" size="1.27" layer="95" />
                <pinref part="D46" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="744.22" y1="698.50" x2="746.76" y2="698.50" width="0.3" layer="91" />
                <label x="746.76" y="698.50" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="723.90" y1="713.74" x2="726.44" y2="713.74" width="0.3" layer="91" />
                <label x="726.44" y="713.74" size="1.27" layer="95" />
                <pinref part="R396" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="688.34" y1="657.86" x2="688.34" y2="655.32" width="0.3" layer="91" />
                <label x="688.34" y="655.32" size="1.27" layer="95" />
                <pinref part="C224" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$167">
              <segment>
                <wire x1="1165.86" y1="502.92" x2="1168.40" y2="502.92" width="0.3" layer="91" />
                <label x="1168.40" y="502.92" size="1.27" layer="95" />
                <pinref part="R299" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="58.42" y1="73.66" x2="55.88" y2="73.66" width="0.3" layer="91" />
                <label x="55.88" y="73.66" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-6" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="665.48" x2="1214.12" y2="665.48" width="0.3" layer="91" />
                <label x="1214.12" y="665.48" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="VSS" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="693.42" x2="1252.22" y2="693.42" width="0.3" layer="91" />
                <label x="1252.22" y="693.42" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="VSS" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="721.36" x2="1290.32" y2="721.36" width="0.3" layer="91" />
                <label x="1290.32" y="721.36" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="VSS" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="670.56" x2="1249.68" y2="670.56" width="0.3" layer="91" />
                <label x="1249.68" y="670.56" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="DOUT" />
              </segment>
            </net>
            <net name="N$168">
              <segment>
                <wire x1="1155.70" y1="502.92" x2="1153.16" y2="502.92" width="0.3" layer="91" />
                <label x="1153.16" y="502.92" size="1.27" layer="95" />
                <pinref part="R299" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="675.64" x2="1249.68" y2="675.64" width="0.3" layer="91" />
                <label x="1249.68" y="675.64" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="58.42" y1="76.20" x2="55.88" y2="76.20" width="0.3" layer="91" />
                <label x="55.88" y="76.20" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-5" />
              </segment>
            </net>
            <net name="N$169">
              <segment>
                <wire x1="866.14" y1="335.28" x2="868.68" y2="335.28" width="0.3" layer="91" />
                <label x="868.68" y="335.28" size="1.27" layer="95" />
                <pinref part="R300" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="698.50" x2="1252.22" y2="698.50" width="0.3" layer="91" />
                <label x="1252.22" y="698.50" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="706.12" x2="1287.78" y2="706.12" width="0.3" layer="91" />
                <label x="1287.78" y="706.12" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="CTRLA" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="695.96" x2="1252.22" y2="695.96" width="0.3" layer="91" />
                <label x="1252.22" y="695.96" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="CTRLC" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="703.58" x2="1287.78" y2="703.58" width="0.3" layer="91" />
                <label x="1287.78" y="703.58" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="723.90" x2="1290.32" y2="723.90" width="0.3" layer="91" />
                <label x="1290.32" y="723.90" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="CTRLC" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="731.52" x2="1325.88" y2="731.52" width="0.3" layer="91" />
                <label x="1325.88" y="731.52" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="CTRLD" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="726.44" x2="1290.32" y2="726.44" width="0.3" layer="91" />
                <label x="1290.32" y="726.44" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="670.56" x2="1214.12" y2="670.56" width="0.3" layer="91" />
                <label x="1214.12" y="670.56" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="CTRLB" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="678.18" x2="1249.68" y2="678.18" width="0.3" layer="91" />
                <label x="1249.68" y="678.18" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="CTRLA" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="668.02" x2="1214.12" y2="668.02" width="0.3" layer="91" />
                <label x="1214.12" y="668.02" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="CTRLC" />
              </segment>
            </net>
            <net name="N$170">
              <segment>
                <wire x1="866.14" y1="342.90" x2="868.68" y2="342.90" width="0.3" layer="91" />
                <label x="868.68" y="342.90" size="1.27" layer="95" />
                <pinref part="R307" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1247.14" y1="668.02" x2="1249.68" y2="668.02" width="0.3" layer="91" />
                <label x="1249.68" y="668.02" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="COUT" />
              </segment>
            </net>
            <net name="N$171">
              <segment>
                <wire x1="883.92" y1="342.90" x2="886.46" y2="342.90" width="0.3" layer="91" />
                <label x="886.46" y="342.90" size="1.27" layer="95" />
                <pinref part="R317" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="675.64" x2="1214.12" y2="675.64" width="0.3" layer="91" />
                <label x="1214.12" y="675.64" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="BOUT" />
              </segment>
            </net>
            <net name="N$172">
              <segment>
                <wire x1="883.92" y1="350.52" x2="886.46" y2="350.52" width="0.3" layer="91" />
                <label x="886.46" y="350.52" size="1.27" layer="95" />
                <pinref part="R327" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1216.66" y1="678.18" x2="1214.12" y2="678.18" width="0.3" layer="91" />
                <label x="1214.12" y="678.18" size="1.27" layer="95" />
                <pinref part="U58" gate="G$1" pin="AOUT" />
              </segment>
            </net>
            <net name="N$173">
              <segment>
                <wire x1="901.70" y1="350.52" x2="904.24" y2="350.52" width="0.3" layer="91" />
                <label x="904.24" y="350.52" size="1.27" layer="95" />
                <pinref part="R337" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="695.96" x2="1287.78" y2="695.96" width="0.3" layer="91" />
                <label x="1287.78" y="695.96" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="COUT" />
              </segment>
            </net>
            <net name="N$174">
              <segment>
                <wire x1="901.70" y1="358.14" x2="904.24" y2="358.14" width="0.3" layer="91" />
                <label x="904.24" y="358.14" size="1.27" layer="95" />
                <pinref part="R347" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1285.24" y1="698.50" x2="1287.78" y2="698.50" width="0.3" layer="91" />
                <label x="1287.78" y="698.50" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="DOUT" />
              </segment>
            </net>
            <net name="N$175">
              <segment>
                <wire x1="919.48" y1="358.14" x2="922.02" y2="358.14" width="0.3" layer="91" />
                <label x="922.02" y="358.14" size="1.27" layer="95" />
                <pinref part="R357" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="703.58" x2="1252.22" y2="703.58" width="0.3" layer="91" />
                <label x="1252.22" y="703.58" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="BOUT" />
              </segment>
            </net>
            <net name="N$176">
              <segment>
                <wire x1="919.48" y1="365.76" x2="922.02" y2="365.76" width="0.3" layer="91" />
                <label x="922.02" y="365.76" size="1.27" layer="95" />
                <pinref part="R367" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1254.76" y1="706.12" x2="1252.22" y2="706.12" width="0.3" layer="91" />
                <label x="1252.22" y="706.12" size="1.27" layer="95" />
                <pinref part="U57" gate="G$1" pin="AOUT" />
              </segment>
            </net>
            <net name="N$177">
              <segment>
                <wire x1="937.26" y1="365.76" x2="939.80" y2="365.76" width="0.3" layer="91" />
                <label x="939.80" y="365.76" size="1.27" layer="95" />
                <pinref part="R377" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="723.90" x2="1325.88" y2="723.90" width="0.3" layer="91" />
                <label x="1325.88" y="723.90" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="COUT" />
              </segment>
            </net>
            <net name="N$178">
              <segment>
                <wire x1="937.26" y1="373.38" x2="939.80" y2="373.38" width="0.3" layer="91" />
                <label x="939.80" y="373.38" size="1.27" layer="95" />
                <pinref part="R387" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1323.34" y1="726.44" x2="1325.88" y2="726.44" width="0.3" layer="91" />
                <label x="1325.88" y="726.44" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="DOUT" />
              </segment>
            </net>
            <net name="N$179">
              <segment>
                <wire x1="955.04" y1="373.38" x2="957.58" y2="373.38" width="0.3" layer="91" />
                <label x="957.58" y="373.38" size="1.27" layer="95" />
                <pinref part="R397" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="1292.86" y1="731.52" x2="1290.32" y2="731.52" width="0.3" layer="91" />
                <label x="1290.32" y="731.52" size="1.27" layer="95" />
                <pinref part="U56" gate="G$1" pin="BOUT" />
              </segment>
            </net>
            <net name="N$180">
              <segment>
                <wire x1="187.96" y1="157.48" x2="187.96" y2="154.94" width="0.3" layer="91" />
                <label x="187.96" y="154.94" size="1.27" layer="95" />
                <pinref part="C131" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="220.98" y1="198.12" x2="218.44" y2="198.12" width="0.3" layer="91" />
                <label x="218.44" y="198.12" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="203.20" y1="185.42" x2="200.66" y2="185.42" width="0.3" layer="91" />
                <label x="200.66" y="185.42" size="1.27" layer="95" />
                <pinref part="R303" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$181">
              <segment>
                <wire x1="187.96" y1="165.10" x2="187.96" y2="167.64" width="0.3" layer="91" />
                <label x="187.96" y="167.64" size="1.27" layer="95" />
                <pinref part="C131" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="170.18" y1="142.24" x2="167.64" y2="142.24" width="0.3" layer="91" />
                <label x="167.64" y="142.24" size="1.27" layer="95" />
                <pinref part="R302" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="167.64" y1="109.22" x2="170.18" y2="109.22" width="0.3" layer="91" />
                <label x="170.18" y="109.22" size="1.27" layer="95" />
                <pinref part="R301" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="187.96" y1="147.32" x2="187.96" y2="149.86" width="0.3" layer="91" />
                <label x="187.96" y="149.86" size="1.27" layer="95" />
                <pinref part="C132" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$182">
              <segment>
                <wire x1="187.96" y1="139.70" x2="187.96" y2="137.16" width="0.3" layer="91" />
                <label x="187.96" y="137.16" size="1.27" layer="95" />
                <pinref part="C132" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="238.76" y1="213.36" x2="236.22" y2="213.36" width="0.3" layer="91" />
                <label x="236.22" y="213.36" size="1.27" layer="95" />
                <pinref part="R304" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="220.98" y1="203.20" x2="218.44" y2="203.20" width="0.3" layer="91" />
                <label x="218.44" y="203.20" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="213.36" y1="185.42" x2="215.90" y2="185.42" width="0.3" layer="91" />
                <label x="215.90" y="185.42" size="1.27" layer="95" />
                <pinref part="R303" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$183">
              <segment>
                <wire x1="274.32" y1="236.22" x2="274.32" y2="233.68" width="0.3" layer="91" />
                <label x="274.32" y="233.68" size="1.27" layer="95" />
                <pinref part="C133" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="317.50" y1="304.80" x2="314.96" y2="304.80" width="0.3" layer="91" />
                <label x="314.96" y="304.80" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="299.72" y1="292.10" x2="297.18" y2="292.10" width="0.3" layer="91" />
                <label x="297.18" y="292.10" size="1.27" layer="95" />
                <pinref part="R306" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$184">
              <segment>
                <wire x1="274.32" y1="243.84" x2="274.32" y2="246.38" width="0.3" layer="91" />
                <label x="274.32" y="246.38" size="1.27" layer="95" />
                <pinref part="C133" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="292.10" y1="261.62" x2="292.10" y2="264.16" width="0.3" layer="91" />
                <label x="292.10" y="264.16" size="1.27" layer="95" />
                <pinref part="C134" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="266.70" y1="238.76" x2="269.24" y2="238.76" width="0.3" layer="91" />
                <label x="269.24" y="238.76" size="1.27" layer="95" />
                <pinref part="R305" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="248.92" y1="213.36" x2="251.46" y2="213.36" width="0.3" layer="91" />
                <label x="251.46" y="213.36" size="1.27" layer="95" />
                <pinref part="R304" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$185">
              <segment>
                <wire x1="195.58" y1="182.88" x2="195.58" y2="185.42" width="0.3" layer="91" />
                <label x="195.58" y="185.42" size="1.27" layer="95" />
                <pinref part="C141" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="195.58" y1="165.10" x2="195.58" y2="167.64" width="0.3" layer="91" />
                <label x="195.58" y="167.64" size="1.27" layer="95" />
                <pinref part="C142" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="170.18" y1="134.62" x2="172.72" y2="134.62" width="0.3" layer="91" />
                <label x="172.72" y="134.62" size="1.27" layer="95" />
                <pinref part="R311" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="203.20" y1="177.80" x2="200.66" y2="177.80" width="0.3" layer="91" />
                <label x="200.66" y="177.80" size="1.27" layer="95" />
                <pinref part="R312" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$186">
              <segment>
                <wire x1="195.58" y1="175.26" x2="195.58" y2="172.72" width="0.3" layer="91" />
                <label x="195.58" y="172.72" size="1.27" layer="95" />
                <pinref part="C141" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="251.46" y1="193.04" x2="254.00" y2="193.04" width="0.3" layer="91" />
                <label x="254.00" y="193.04" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="220.98" y1="213.36" x2="218.44" y2="213.36" width="0.3" layer="91" />
                <label x="218.44" y="213.36" size="1.27" layer="95" />
                <pinref part="R313" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$187">
              <segment>
                <wire x1="195.58" y1="157.48" x2="195.58" y2="154.94" width="0.3" layer="91" />
                <label x="195.58" y="154.94" size="1.27" layer="95" />
                <pinref part="C142" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="238.76" y1="220.98" x2="236.22" y2="220.98" width="0.3" layer="91" />
                <label x="236.22" y="220.98" size="1.27" layer="95" />
                <pinref part="R314" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="251.46" y1="198.12" x2="254.00" y2="198.12" width="0.3" layer="91" />
                <label x="254.00" y="198.12" size="1.27" layer="95" />
                <pinref part="U50" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="231.14" y1="213.36" x2="233.68" y2="213.36" width="0.3" layer="91" />
                <label x="233.68" y="213.36" size="1.27" layer="95" />
                <pinref part="R313" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$188">
              <segment>
                <wire x1="299.72" y1="279.40" x2="299.72" y2="281.94" width="0.3" layer="91" />
                <label x="299.72" y="281.94" size="1.27" layer="95" />
                <pinref part="C143" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="292.10" y1="279.40" x2="292.10" y2="281.94" width="0.3" layer="91" />
                <label x="292.10" y="281.94" size="1.27" layer="95" />
                <pinref part="C144" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="274.32" y1="256.54" x2="271.78" y2="256.54" width="0.3" layer="91" />
                <label x="271.78" y="256.54" size="1.27" layer="95" />
                <pinref part="R315" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="248.92" y1="220.98" x2="251.46" y2="220.98" width="0.3" layer="91" />
                <label x="251.46" y="220.98" size="1.27" layer="95" />
                <pinref part="R314" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$189">
              <segment>
                <wire x1="299.72" y1="271.78" x2="299.72" y2="269.24" width="0.3" layer="91" />
                <label x="299.72" y="269.24" size="1.27" layer="95" />
                <pinref part="C143" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="347.98" y1="299.72" x2="350.52" y2="299.72" width="0.3" layer="91" />
                <label x="350.52" y="299.72" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="317.50" y1="320.04" x2="314.96" y2="320.04" width="0.3" layer="91" />
                <label x="314.96" y="320.04" size="1.27" layer="95" />
                <pinref part="R316" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$190">
              <segment>
                <wire x1="317.50" y1="299.72" x2="314.96" y2="299.72" width="0.3" layer="91" />
                <label x="314.96" y="299.72" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="513.08" y1="292.10" x2="510.54" y2="292.10" width="0.3" layer="91" />
                <label x="510.54" y="292.10" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="543.56" y1="287.02" x2="546.10" y2="287.02" width="0.3" layer="91" />
                <label x="546.10" y="287.02" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="548.64" y1="502.92" x2="551.18" y2="502.92" width="0.3" layer="91" />
                <label x="551.18" y="502.92" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="518.16" y1="508.00" x2="515.62" y2="508.00" width="0.3" layer="91" />
                <label x="515.62" y="508.00" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="721.36" y1="477.52" x2="718.82" y2="477.52" width="0.3" layer="91" />
                <label x="718.82" y="477.52" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="751.84" y1="472.44" x2="754.38" y2="472.44" width="0.3" layer="91" />
                <label x="754.38" y="472.44" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="713.74" y1="693.42" x2="711.20" y2="693.42" width="0.3" layer="91" />
                <label x="711.20" y="693.42" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="744.22" y1="688.34" x2="746.76" y2="688.34" width="0.3" layer="91" />
                <label x="746.76" y="688.34" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="347.98" y1="294.64" x2="350.52" y2="294.64" width="0.3" layer="91" />
                <label x="350.52" y="294.64" size="1.27" layer="95" />
                <pinref part="U55" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="772.16" y1="523.24" x2="772.16" y2="520.70" width="0.3" layer="91" />
                <label x="772.16" y="520.70" size="1.27" layer="95" />
                <pinref part="49" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="782.32" y1="510.54" x2="784.86" y2="510.54" width="0.3" layer="91" />
                <label x="784.86" y="510.54" size="1.27" layer="95" />
                <pinref part="R298" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="782.32" y1="518.16" x2="779.78" y2="518.16" width="0.3" layer="91" />
                <label x="779.78" y="518.16" size="1.27" layer="95" />
                <pinref part="D36" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="795.02" y1="518.16" x2="795.02" y2="520.70" width="0.3" layer="91" />
                <label x="795.02" y="520.70" size="1.27" layer="95" />
                <pinref part="C130" gate="G$1" pin="+" />
              </segment>
            </net>
            <net name="N$191">
              <segment>
                <wire x1="601.98" y1="355.60" x2="601.98" y2="353.06" width="0.3" layer="91" />
                <label x="601.98" y="353.06" size="1.27" layer="95" />
                <pinref part="C191" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="635.00" y1="396.24" x2="632.46" y2="396.24" width="0.3" layer="91" />
                <label x="632.46" y="396.24" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="617.22" y1="383.54" x2="614.68" y2="383.54" width="0.3" layer="91" />
                <label x="614.68" y="383.54" size="1.27" layer="95" />
                <pinref part="R363" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$192">
              <segment>
                <wire x1="601.98" y1="363.22" x2="601.98" y2="365.76" width="0.3" layer="91" />
                <label x="601.98" y="365.76" size="1.27" layer="95" />
                <pinref part="C191" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="584.20" y1="342.90" x2="581.66" y2="342.90" width="0.3" layer="91" />
                <label x="581.66" y="342.90" size="1.27" layer="95" />
                <pinref part="R362" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="594.36" y1="332.74" x2="596.90" y2="332.74" width="0.3" layer="91" />
                <label x="596.90" y="332.74" size="1.27" layer="95" />
                <pinref part="R361" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="601.98" y1="345.44" x2="601.98" y2="347.98" width="0.3" layer="91" />
                <label x="601.98" y="347.98" size="1.27" layer="95" />
                <pinref part="C192" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$193">
              <segment>
                <wire x1="601.98" y1="337.82" x2="601.98" y2="335.28" width="0.3" layer="91" />
                <label x="601.98" y="335.28" size="1.27" layer="95" />
                <pinref part="C192" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="635.00" y1="401.32" x2="632.46" y2="401.32" width="0.3" layer="91" />
                <label x="632.46" y="401.32" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="652.78" y1="411.48" x2="650.24" y2="411.48" width="0.3" layer="91" />
                <label x="650.24" y="411.48" size="1.27" layer="95" />
                <pinref part="R364" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="627.38" y1="383.54" x2="629.92" y2="383.54" width="0.3" layer="91" />
                <label x="629.92" y="383.54" size="1.27" layer="95" />
                <pinref part="R363" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$194">
              <segment>
                <wire x1="688.34" y1="441.96" x2="688.34" y2="439.42" width="0.3" layer="91" />
                <label x="688.34" y="439.42" size="1.27" layer="95" />
                <pinref part="C193" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="703.58" y1="469.90" x2="701.04" y2="469.90" width="0.3" layer="91" />
                <label x="701.04" y="469.90" size="1.27" layer="95" />
                <pinref part="R366" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="721.36" y1="482.60" x2="718.82" y2="482.60" width="0.3" layer="91" />
                <label x="718.82" y="482.60" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$195">
              <segment>
                <wire x1="688.34" y1="449.58" x2="688.34" y2="452.12" width="0.3" layer="91" />
                <label x="688.34" y="452.12" size="1.27" layer="95" />
                <pinref part="C193" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="688.34" y1="431.80" x2="688.34" y2="434.34" width="0.3" layer="91" />
                <label x="688.34" y="434.34" size="1.27" layer="95" />
                <pinref part="C194" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="662.94" y1="411.48" x2="665.48" y2="411.48" width="0.3" layer="91" />
                <label x="665.48" y="411.48" size="1.27" layer="95" />
                <pinref part="R364" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="662.94" y1="419.10" x2="665.48" y2="419.10" width="0.3" layer="91" />
                <label x="665.48" y="419.10" size="1.27" layer="95" />
                <pinref part="R365" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$196">
              <segment>
                <wire x1="609.60" y1="373.38" x2="609.60" y2="370.84" width="0.3" layer="91" />
                <label x="609.60" y="370.84" size="1.27" layer="95" />
                <pinref part="C201" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="665.48" y1="391.16" x2="668.02" y2="391.16" width="0.3" layer="91" />
                <label x="668.02" y="391.16" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="635.00" y1="411.48" x2="632.46" y2="411.48" width="0.3" layer="91" />
                <label x="632.46" y="411.48" size="1.27" layer="95" />
                <pinref part="R373" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$197">
              <segment>
                <wire x1="609.60" y1="381.00" x2="609.60" y2="383.54" width="0.3" layer="91" />
                <label x="609.60" y="383.54" size="1.27" layer="95" />
                <pinref part="C201" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="576.58" y1="355.60" x2="579.12" y2="355.60" width="0.3" layer="91" />
                <label x="579.12" y="355.60" size="1.27" layer="95" />
                <pinref part="R371" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="617.22" y1="375.92" x2="614.68" y2="375.92" width="0.3" layer="91" />
                <label x="614.68" y="375.92" size="1.27" layer="95" />
                <pinref part="R372" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="609.60" y1="363.22" x2="609.60" y2="365.76" width="0.3" layer="91" />
                <label x="609.60" y="365.76" size="1.27" layer="95" />
                <pinref part="C202" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$198">
              <segment>
                <wire x1="609.60" y1="355.60" x2="609.60" y2="353.06" width="0.3" layer="91" />
                <label x="609.60" y="353.06" size="1.27" layer="95" />
                <pinref part="C202" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="645.16" y1="411.48" x2="647.70" y2="411.48" width="0.3" layer="91" />
                <label x="647.70" y="411.48" size="1.27" layer="95" />
                <pinref part="R373" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="670.56" y1="419.10" x2="668.02" y2="419.10" width="0.3" layer="91" />
                <label x="668.02" y="419.10" size="1.27" layer="95" />
                <pinref part="R374" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="665.48" y1="396.24" x2="668.02" y2="396.24" width="0.3" layer="91" />
                <label x="668.02" y="396.24" size="1.27" layer="95" />
                <pinref part="U47" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$199">
              <segment>
                <wire x1="695.96" y1="459.74" x2="695.96" y2="457.20" width="0.3" layer="91" />
                <label x="695.96" y="457.20" size="1.27" layer="95" />
                <pinref part="C203" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="721.36" y1="497.84" x2="718.82" y2="497.84" width="0.3" layer="91" />
                <label x="718.82" y="497.84" size="1.27" layer="95" />
                <pinref part="R376" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="751.84" y1="477.52" x2="754.38" y2="477.52" width="0.3" layer="91" />
                <label x="754.38" y="477.52" size="1.27" layer="95" />
                <pinref part="U52" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$200">
              <segment>
                <wire x1="695.96" y1="467.36" x2="695.96" y2="469.90" width="0.3" layer="91" />
                <label x="695.96" y="469.90" size="1.27" layer="95" />
                <pinref part="C203" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="680.72" y1="419.10" x2="683.26" y2="419.10" width="0.3" layer="91" />
                <label x="683.26" y="419.10" size="1.27" layer="95" />
                <pinref part="R374" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="703.58" y1="462.28" x2="701.04" y2="462.28" width="0.3" layer="91" />
                <label x="701.04" y="462.28" size="1.27" layer="95" />
                <pinref part="R375" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="695.96" y1="449.58" x2="695.96" y2="452.12" width="0.3" layer="91" />
                <label x="695.96" y="452.12" size="1.27" layer="95" />
                <pinref part="C204" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$201">
              <segment>
                <wire x1="393.70" y1="167.64" x2="393.70" y2="170.18" width="0.3" layer="91" />
                <label x="393.70" y="170.18" size="1.27" layer="95" />
                <pinref part="C151" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="393.70" y1="149.86" x2="393.70" y2="152.40" width="0.3" layer="91" />
                <label x="393.70" y="152.40" size="1.27" layer="95" />
                <pinref part="C152" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="375.92" y1="144.78" x2="373.38" y2="144.78" width="0.3" layer="91" />
                <label x="373.38" y="144.78" size="1.27" layer="95" />
                <pinref part="R322" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="373.38" y1="114.30" x2="375.92" y2="114.30" width="0.3" layer="91" />
                <label x="375.92" y="114.30" size="1.27" layer="95" />
                <pinref part="R321" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$202">
              <segment>
                <wire x1="393.70" y1="160.02" x2="393.70" y2="157.48" width="0.3" layer="91" />
                <label x="393.70" y="157.48" size="1.27" layer="95" />
                <pinref part="C151" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="426.72" y1="200.66" x2="424.18" y2="200.66" width="0.3" layer="91" />
                <label x="424.18" y="200.66" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="408.94" y1="187.96" x2="406.40" y2="187.96" width="0.3" layer="91" />
                <label x="406.40" y="187.96" size="1.27" layer="95" />
                <pinref part="R323" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$203">
              <segment>
                <wire x1="393.70" y1="142.24" x2="393.70" y2="139.70" width="0.3" layer="91" />
                <label x="393.70" y="139.70" size="1.27" layer="95" />
                <pinref part="C152" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="444.50" y1="215.90" x2="441.96" y2="215.90" width="0.3" layer="91" />
                <label x="441.96" y="215.90" size="1.27" layer="95" />
                <pinref part="R324" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="426.72" y1="205.74" x2="424.18" y2="205.74" width="0.3" layer="91" />
                <label x="424.18" y="205.74" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="419.10" y1="187.96" x2="421.64" y2="187.96" width="0.3" layer="91" />
                <label x="421.64" y="187.96" size="1.27" layer="95" />
                <pinref part="R323" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$204">
              <segment>
                <wire x1="480.06" y1="256.54" x2="480.06" y2="254.00" width="0.3" layer="91" />
                <label x="480.06" y="254.00" size="1.27" layer="95" />
                <pinref part="C153" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="513.08" y1="297.18" x2="510.54" y2="297.18" width="0.3" layer="91" />
                <label x="510.54" y="297.18" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="495.30" y1="284.48" x2="492.76" y2="284.48" width="0.3" layer="91" />
                <label x="492.76" y="284.48" size="1.27" layer="95" />
                <pinref part="R326" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$205">
              <segment>
                <wire x1="480.06" y1="264.16" x2="480.06" y2="266.70" width="0.3" layer="91" />
                <label x="480.06" y="266.70" size="1.27" layer="95" />
                <pinref part="C153" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="472.44" y1="241.30" x2="474.98" y2="241.30" width="0.3" layer="91" />
                <label x="474.98" y="241.30" size="1.27" layer="95" />
                <pinref part="R325" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="454.66" y1="215.90" x2="457.20" y2="215.90" width="0.3" layer="91" />
                <label x="457.20" y="215.90" size="1.27" layer="95" />
                <pinref part="R324" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="480.06" y1="246.38" x2="480.06" y2="248.92" width="0.3" layer="91" />
                <label x="480.06" y="248.92" size="1.27" layer="95" />
                <pinref part="C154" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$206">
              <segment>
                <wire x1="401.32" y1="185.42" x2="401.32" y2="187.96" width="0.3" layer="91" />
                <label x="401.32" y="187.96" size="1.27" layer="95" />
                <pinref part="C161" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="408.94" y1="180.34" x2="406.40" y2="180.34" width="0.3" layer="91" />
                <label x="406.40" y="180.34" size="1.27" layer="95" />
                <pinref part="R332" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="375.92" y1="137.16" x2="378.46" y2="137.16" width="0.3" layer="91" />
                <label x="378.46" y="137.16" size="1.27" layer="95" />
                <pinref part="R331" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="401.32" y1="167.64" x2="401.32" y2="170.18" width="0.3" layer="91" />
                <label x="401.32" y="170.18" size="1.27" layer="95" />
                <pinref part="C162" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$207">
              <segment>
                <wire x1="401.32" y1="177.80" x2="401.32" y2="175.26" width="0.3" layer="91" />
                <label x="401.32" y="175.26" size="1.27" layer="95" />
                <pinref part="C161" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="457.20" y1="195.58" x2="459.74" y2="195.58" width="0.3" layer="91" />
                <label x="459.74" y="195.58" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="426.72" y1="215.90" x2="424.18" y2="215.90" width="0.3" layer="91" />
                <label x="424.18" y="215.90" size="1.27" layer="95" />
                <pinref part="R333" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$208">
              <segment>
                <wire x1="401.32" y1="160.02" x2="401.32" y2="157.48" width="0.3" layer="91" />
                <label x="401.32" y="157.48" size="1.27" layer="95" />
                <pinref part="C162" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="436.88" y1="215.90" x2="439.42" y2="215.90" width="0.3" layer="91" />
                <label x="439.42" y="215.90" size="1.27" layer="95" />
                <pinref part="R333" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="444.50" y1="223.52" x2="441.96" y2="223.52" width="0.3" layer="91" />
                <label x="441.96" y="223.52" size="1.27" layer="95" />
                <pinref part="R334" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="457.20" y1="200.66" x2="459.74" y2="200.66" width="0.3" layer="91" />
                <label x="459.74" y="200.66" size="1.27" layer="95" />
                <pinref part="U49" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$209">
              <segment>
                <wire x1="495.30" y1="264.16" x2="495.30" y2="261.62" width="0.3" layer="91" />
                <label x="495.30" y="261.62" size="1.27" layer="95" />
                <pinref part="C163" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="513.08" y1="312.42" x2="510.54" y2="312.42" width="0.3" layer="91" />
                <label x="510.54" y="312.42" size="1.27" layer="95" />
                <pinref part="R336" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="543.56" y1="292.10" x2="546.10" y2="292.10" width="0.3" layer="91" />
                <label x="546.10" y="292.10" size="1.27" layer="95" />
                <pinref part="U54" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$210">
              <segment>
                <wire x1="495.30" y1="271.78" x2="495.30" y2="274.32" width="0.3" layer="91" />
                <label x="495.30" y="274.32" size="1.27" layer="95" />
                <pinref part="C163" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="497.84" y1="259.08" x2="500.38" y2="259.08" width="0.3" layer="91" />
                <label x="500.38" y="259.08" size="1.27" layer="95" />
                <pinref part="R335" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="454.66" y1="223.52" x2="457.20" y2="223.52" width="0.3" layer="91" />
                <label x="457.20" y="223.52" size="1.27" layer="95" />
                <pinref part="R334" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="271.78" x2="487.68" y2="274.32" width="0.3" layer="91" />
                <label x="487.68" y="274.32" size="1.27" layer="95" />
                <pinref part="C164" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$211">
              <segment>
                <wire x1="594.36" y1="561.34" x2="594.36" y2="558.80" width="0.3" layer="91" />
                <label x="594.36" y="558.80" size="1.27" layer="95" />
                <pinref part="C211" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="627.38" y1="601.98" x2="624.84" y2="601.98" width="0.3" layer="91" />
                <label x="624.84" y="601.98" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="609.60" y1="589.28" x2="607.06" y2="589.28" width="0.3" layer="91" />
                <label x="607.06" y="589.28" size="1.27" layer="95" />
                <pinref part="R383" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$212">
              <segment>
                <wire x1="594.36" y1="568.96" x2="594.36" y2="571.50" width="0.3" layer="91" />
                <label x="594.36" y="571.50" size="1.27" layer="95" />
                <pinref part="C211" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="576.58" y1="546.10" x2="574.04" y2="546.10" width="0.3" layer="91" />
                <label x="574.04" y="546.10" size="1.27" layer="95" />
                <pinref part="R382" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="574.04" y1="515.62" x2="576.58" y2="515.62" width="0.3" layer="91" />
                <label x="576.58" y="515.62" size="1.27" layer="95" />
                <pinref part="R381" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="594.36" y1="551.18" x2="594.36" y2="553.72" width="0.3" layer="91" />
                <label x="594.36" y="553.72" size="1.27" layer="95" />
                <pinref part="C212" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$213">
              <segment>
                <wire x1="594.36" y1="543.56" x2="594.36" y2="541.02" width="0.3" layer="91" />
                <label x="594.36" y="541.02" size="1.27" layer="95" />
                <pinref part="C212" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="645.16" y1="617.22" x2="642.62" y2="617.22" width="0.3" layer="91" />
                <label x="642.62" y="617.22" size="1.27" layer="95" />
                <pinref part="R384" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="627.38" y1="607.06" x2="624.84" y2="607.06" width="0.3" layer="91" />
                <label x="624.84" y="607.06" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="619.76" y1="589.28" x2="622.30" y2="589.28" width="0.3" layer="91" />
                <label x="622.30" y="589.28" size="1.27" layer="95" />
                <pinref part="R383" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$214">
              <segment>
                <wire x1="680.72" y1="657.86" x2="680.72" y2="655.32" width="0.3" layer="91" />
                <label x="680.72" y="655.32" size="1.27" layer="95" />
                <pinref part="C213" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="713.74" y1="698.50" x2="711.20" y2="698.50" width="0.3" layer="91" />
                <label x="711.20" y="698.50" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="695.96" y1="685.80" x2="693.42" y2="685.80" width="0.3" layer="91" />
                <label x="693.42" y="685.80" size="1.27" layer="95" />
                <pinref part="R386" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$215">
              <segment>
                <wire x1="680.72" y1="665.48" x2="680.72" y2="668.02" width="0.3" layer="91" />
                <label x="680.72" y="668.02" size="1.27" layer="95" />
                <pinref part="C213" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="673.10" y1="642.62" x2="675.64" y2="642.62" width="0.3" layer="91" />
                <label x="675.64" y="642.62" size="1.27" layer="95" />
                <pinref part="R385" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="655.32" y1="617.22" x2="657.86" y2="617.22" width="0.3" layer="91" />
                <label x="657.86" y="617.22" size="1.27" layer="95" />
                <pinref part="R384" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="680.72" y1="647.70" x2="680.72" y2="650.24" width="0.3" layer="91" />
                <label x="680.72" y="650.24" size="1.27" layer="95" />
                <pinref part="C214" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$216">
              <segment>
                <wire x1="601.98" y1="586.74" x2="601.98" y2="589.28" width="0.3" layer="91" />
                <label x="601.98" y="589.28" size="1.27" layer="95" />
                <pinref part="C221" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="609.60" y1="581.66" x2="607.06" y2="581.66" width="0.3" layer="91" />
                <label x="607.06" y="581.66" size="1.27" layer="95" />
                <pinref part="R392" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="576.58" y1="538.48" x2="579.12" y2="538.48" width="0.3" layer="91" />
                <label x="579.12" y="538.48" size="1.27" layer="95" />
                <pinref part="R391" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="601.98" y1="568.96" x2="601.98" y2="571.50" width="0.3" layer="91" />
                <label x="601.98" y="571.50" size="1.27" layer="95" />
                <pinref part="C222" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$217">
              <segment>
                <wire x1="601.98" y1="579.12" x2="601.98" y2="576.58" width="0.3" layer="91" />
                <label x="601.98" y="576.58" size="1.27" layer="95" />
                <pinref part="C221" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="657.86" y1="596.90" x2="660.40" y2="596.90" width="0.3" layer="91" />
                <label x="660.40" y="596.90" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="627.38" y1="617.22" x2="624.84" y2="617.22" width="0.3" layer="91" />
                <label x="624.84" y="617.22" size="1.27" layer="95" />
                <pinref part="R393" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$218">
              <segment>
                <wire x1="688.34" y1="675.64" x2="688.34" y2="673.10" width="0.3" layer="91" />
                <label x="688.34" y="673.10" size="1.27" layer="95" />
                <pinref part="C221$1" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="713.74" y1="713.74" x2="711.20" y2="713.74" width="0.3" layer="91" />
                <label x="711.20" y="713.74" size="1.27" layer="95" />
                <pinref part="R396" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="744.22" y1="693.42" x2="746.76" y2="693.42" width="0.3" layer="91" />
                <label x="746.76" y="693.42" size="1.27" layer="95" />
                <pinref part="U51" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$219">
              <segment>
                <wire x1="688.34" y1="683.26" x2="688.34" y2="685.80" width="0.3" layer="91" />
                <label x="688.34" y="685.80" size="1.27" layer="95" />
                <pinref part="C221$1" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="695.96" y1="678.18" x2="693.42" y2="678.18" width="0.3" layer="91" />
                <label x="693.42" y="678.18" size="1.27" layer="95" />
                <pinref part="R395" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="655.32" y1="624.84" x2="657.86" y2="624.84" width="0.3" layer="91" />
                <label x="657.86" y="624.84" size="1.27" layer="95" />
                <pinref part="R394" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="688.34" y1="665.48" x2="688.34" y2="668.02" width="0.3" layer="91" />
                <label x="688.34" y="668.02" size="1.27" layer="95" />
                <pinref part="C224" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$220">
              <segment>
                <wire x1="601.98" y1="561.34" x2="601.98" y2="558.80" width="0.3" layer="91" />
                <label x="601.98" y="558.80" size="1.27" layer="95" />
                <pinref part="C222" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="637.54" y1="617.22" x2="640.08" y2="617.22" width="0.3" layer="91" />
                <label x="640.08" y="617.22" size="1.27" layer="95" />
                <pinref part="R393" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="645.16" y1="624.84" x2="642.62" y2="624.84" width="0.3" layer="91" />
                <label x="642.62" y="624.84" size="1.27" layer="95" />
                <pinref part="R394" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="657.86" y1="601.98" x2="660.40" y2="601.98" width="0.3" layer="91" />
                <label x="660.40" y="601.98" size="1.27" layer="95" />
                <pinref part="U46" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$221">
              <segment>
                <wire x1="388.62" y1="375.92" x2="388.62" y2="373.38" width="0.3" layer="91" />
                <label x="388.62" y="373.38" size="1.27" layer="95" />
                <pinref part="C171" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="403.86" y1="403.86" x2="401.32" y2="403.86" width="0.3" layer="91" />
                <label x="401.32" y="403.86" size="1.27" layer="95" />
                <pinref part="R343" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="421.64" y1="416.56" x2="419.10" y2="416.56" width="0.3" layer="91" />
                <label x="419.10" y="416.56" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="I1-" />
              </segment>
            </net>
            <net name="N$222">
              <segment>
                <wire x1="388.62" y1="383.54" x2="388.62" y2="386.08" width="0.3" layer="91" />
                <label x="388.62" y="386.08" size="1.27" layer="95" />
                <pinref part="C171" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="388.62" y1="370.84" x2="386.08" y2="370.84" width="0.3" layer="91" />
                <label x="386.08" y="370.84" size="1.27" layer="95" />
                <pinref part="R342" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="381.00" y1="375.92" x2="381.00" y2="378.46" width="0.3" layer="91" />
                <label x="381.00" y="378.46" size="1.27" layer="95" />
                <pinref part="C172" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="373.38" y1="330.20" x2="375.92" y2="330.20" width="0.3" layer="91" />
                <label x="375.92" y="330.20" size="1.27" layer="95" />
                <pinref part="R341" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$223">
              <segment>
                <wire x1="381.00" y1="368.30" x2="381.00" y2="365.76" width="0.3" layer="91" />
                <label x="381.00" y="365.76" size="1.27" layer="95" />
                <pinref part="C172" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="431.80" x2="436.88" y2="431.80" width="0.3" layer="91" />
                <label x="436.88" y="431.80" size="1.27" layer="95" />
                <pinref part="R344" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="421.64" y1="421.64" x2="419.10" y2="421.64" width="0.3" layer="91" />
                <label x="419.10" y="421.64" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="OUT1" />
              </segment>
              <segment>
                <wire x1="414.02" y1="403.86" x2="416.56" y2="403.86" width="0.3" layer="91" />
                <label x="416.56" y="403.86" size="1.27" layer="95" />
                <pinref part="R343" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$224">
              <segment>
                <wire x1="474.98" y1="462.28" x2="474.98" y2="464.82" width="0.3" layer="91" />
                <label x="474.98" y="464.82" size="1.27" layer="95" />
                <pinref part="C173" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="474.98" y1="474.98" x2="472.44" y2="474.98" width="0.3" layer="91" />
                <label x="472.44" y="474.98" size="1.27" layer="95" />
                <pinref part="R345" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="449.58" y1="431.80" x2="452.12" y2="431.80" width="0.3" layer="91" />
                <label x="452.12" y="431.80" size="1.27" layer="95" />
                <pinref part="R344" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="457.20" y1="444.50" x2="457.20" y2="447.04" width="0.3" layer="91" />
                <label x="457.20" y="447.04" size="1.27" layer="95" />
                <pinref part="C174" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$225">
              <segment>
                <wire x1="474.98" y1="454.66" x2="474.98" y2="452.12" width="0.3" layer="91" />
                <label x="474.98" y="452.12" size="1.27" layer="95" />
                <pinref part="C173" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="518.16" y1="513.08" x2="515.62" y2="513.08" width="0.3" layer="91" />
                <label x="515.62" y="513.08" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="500.38" y1="492.76" x2="497.84" y2="492.76" width="0.3" layer="91" />
                <label x="497.84" y="492.76" size="1.27" layer="95" />
                <pinref part="R346" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$226">
              <segment>
                <wire x1="396.24" y1="401.32" x2="396.24" y2="403.86" width="0.3" layer="91" />
                <label x="396.24" y="403.86" size="1.27" layer="95" />
                <pinref part="C181" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="403.86" y1="396.24" x2="401.32" y2="396.24" width="0.3" layer="91" />
                <label x="401.32" y="396.24" size="1.27" layer="95" />
                <pinref part="R352" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="396.24" y1="383.54" x2="396.24" y2="386.08" width="0.3" layer="91" />
                <label x="396.24" y="386.08" size="1.27" layer="95" />
                <pinref part="C182" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="373.38" y1="363.22" x2="375.92" y2="363.22" width="0.3" layer="91" />
                <label x="375.92" y="363.22" size="1.27" layer="95" />
                <pinref part="R351" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$227">
              <segment>
                <wire x1="396.24" y1="393.70" x2="396.24" y2="391.16" width="0.3" layer="91" />
                <label x="396.24" y="391.16" size="1.27" layer="95" />
                <pinref part="C181" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="452.12" y1="411.48" x2="454.66" y2="411.48" width="0.3" layer="91" />
                <label x="454.66" y="411.48" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="421.64" y1="431.80" x2="419.10" y2="431.80" width="0.3" layer="91" />
                <label x="419.10" y="431.80" size="1.27" layer="95" />
                <pinref part="R353" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$228">
              <segment>
                <wire x1="396.24" y1="375.92" x2="396.24" y2="373.38" width="0.3" layer="91" />
                <label x="396.24" y="373.38" size="1.27" layer="95" />
                <pinref part="C182" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="431.80" y1="431.80" x2="434.34" y2="431.80" width="0.3" layer="91" />
                <label x="434.34" y="431.80" size="1.27" layer="95" />
                <pinref part="R353" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="439.42" x2="436.88" y2="439.42" width="0.3" layer="91" />
                <label x="436.88" y="439.42" size="1.27" layer="95" />
                <pinref part="R354" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="452.12" y1="416.56" x2="454.66" y2="416.56" width="0.3" layer="91" />
                <label x="454.66" y="416.56" size="1.27" layer="95" />
                <pinref part="U48" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$229">
              <segment>
                <wire x1="492.76" y1="490.22" x2="492.76" y2="487.68" width="0.3" layer="91" />
                <label x="492.76" y="487.68" size="1.27" layer="95" />
                <pinref part="C183" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="548.64" y1="508.00" x2="551.18" y2="508.00" width="0.3" layer="91" />
                <label x="551.18" y="508.00" size="1.27" layer="95" />
                <pinref part="U53" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="518.16" y1="528.32" x2="515.62" y2="528.32" width="0.3" layer="91" />
                <label x="515.62" y="528.32" size="1.27" layer="95" />
                <pinref part="R356" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$230">
              <segment>
                <wire x1="492.76" y1="497.84" x2="492.76" y2="500.38" width="0.3" layer="91" />
                <label x="492.76" y="500.38" size="1.27" layer="95" />
                <pinref part="C183" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="500.38" y1="500.38" x2="497.84" y2="500.38" width="0.3" layer="91" />
                <label x="497.84" y="500.38" size="1.27" layer="95" />
                <pinref part="R355" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="492.76" y1="480.06" x2="492.76" y2="482.60" width="0.3" layer="91" />
                <label x="492.76" y="482.60" size="1.27" layer="95" />
                <pinref part="C184" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="449.58" y1="439.42" x2="452.12" y2="439.42" width="0.3" layer="91" />
                <label x="452.12" y="439.42" size="1.27" layer="95" />
                <pinref part="R354" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$231">
              <segment>
                <wire x1="2273.30" y1="297.18" x2="2273.30" y2="294.64" width="0.3" layer="91" />
                <label x="2273.30" y="294.64" size="1.27" layer="95" />
                <pinref part="36" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2225.04" y1="261.62" x2="2222.50" y2="261.62" width="0.3" layer="91" />
                <label x="2222.50" y="261.62" size="1.27" layer="95" />
                <pinref part="R417" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2199.64" y1="233.68" x2="2202.18" y2="233.68" width="0.3" layer="91" />
                <label x="2202.18" y="233.68" size="1.27" layer="95" />
                <pinref part="R416" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2225.04" y1="269.24" x2="2222.50" y2="269.24" width="0.3" layer="91" />
                <label x="2222.50" y="269.24" size="1.27" layer="95" />
                <pinref part="R418" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$232">
              <segment>
                <wire x1="2164.08" y1="195.58" x2="2164.08" y2="193.04" width="0.3" layer="91" />
                <label x="2164.08" y="193.04" size="1.27" layer="95" />
                <pinref part="C239" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2189.48" y1="233.68" x2="2186.94" y2="233.68" width="0.3" layer="91" />
                <label x="2186.94" y="233.68" size="1.27" layer="95" />
                <pinref part="R416" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2207.26" y1="241.30" x2="2204.72" y2="241.30" width="0.3" layer="91" />
                <label x="2204.72" y="241.30" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$233">
              <segment>
                <wire x1="2164.08" y1="203.20" x2="2164.08" y2="205.74" width="0.3" layer="91" />
                <label x="2164.08" y="205.74" size="1.27" layer="95" />
                <pinref part="C239" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="2308.86" y1="330.20" x2="2311.40" y2="330.20" width="0.3" layer="91" />
                <label x="2311.40" y="330.20" size="1.27" layer="95" />
                <pinref part="D48" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="2291.08" y1="330.20" x2="2288.54" y2="330.20" width="0.3" layer="91" />
                <label x="2288.54" y="330.20" size="1.27" layer="95" />
                <pinref part="D47" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="2171.70" y1="195.58" x2="2171.70" y2="193.04" width="0.3" layer="91" />
                <label x="2171.70" y="193.04" size="1.27" layer="95" />
                <pinref part="C240" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="2237.74" y1="241.30" x2="2240.28" y2="241.30" width="0.3" layer="91" />
                <label x="2240.28" y="241.30" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$234">
              <segment>
                <wire x1="2133.60" y1="170.18" x2="2133.60" y2="172.72" width="0.3" layer="91" />
                <label x="2133.60" y="172.72" size="1.27" layer="95" />
                <pinref part="C242" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="2161.54" y1="182.88" x2="2164.08" y2="182.88" width="0.3" layer="91" />
                <label x="2164.08" y="182.88" size="1.27" layer="95" />
                <pinref part="R431" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2143.76" y1="182.88" x2="2146.30" y2="182.88" width="0.3" layer="91" />
                <label x="2146.30" y="182.88" size="1.27" layer="95" />
                <pinref part="R432" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2115.82" y1="149.86" x2="2113.28" y2="149.86" width="0.3" layer="91" />
                <label x="2113.28" y="149.86" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="2115.82" y1="165.10" x2="2113.28" y2="165.10" width="0.3" layer="91" />
                <label x="2113.28" y="165.10" size="1.27" layer="95" />
                <pinref part="R433" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$235">
              <segment>
                <wire x1="2296.16" y1="330.20" x2="2298.70" y2="330.20" width="0.3" layer="91" />
                <label x="2298.70" y="330.20" size="1.27" layer="95" />
                <pinref part="D47" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="2265.68" y1="292.10" x2="2268.22" y2="292.10" width="0.3" layer="91" />
                <label x="2268.22" y="292.10" size="1.27" layer="95" />
                <pinref part="D50" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="2280.92" y1="332.74" x2="2280.92" y2="335.28" width="0.3" layer="91" />
                <label x="2280.92" y="335.28" size="1.27" layer="95" />
                <pinref part="Q52" gate="G$1" pin="D" />
              </segment>
            </net>
            <net name="N$236">
              <segment>
                <wire x1="2303.78" y1="330.20" x2="2301.24" y2="330.20" width="0.3" layer="91" />
                <label x="2301.24" y="330.20" size="1.27" layer="95" />
                <pinref part="D48" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="2260.60" y1="299.72" x2="2258.06" y2="299.72" width="0.3" layer="91" />
                <label x="2258.06" y="299.72" size="1.27" layer="95" />
                <pinref part="D49" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="2242.82" y1="292.10" x2="2240.28" y2="292.10" width="0.3" layer="91" />
                <label x="2240.28" y="292.10" size="1.27" layer="95" />
                <pinref part="RVR9Ext" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2270.76" y1="320.04" x2="2268.22" y2="320.04" width="0.3" layer="91" />
                <label x="2268.22" y="320.04" size="1.27" layer="95" />
                <pinref part="Q52" gate="G$1" pin="G" />
              </segment>
            </net>
            <net name="N$237">
              <segment>
                <wire x1="2265.68" y1="299.72" x2="2268.22" y2="299.72" width="0.3" layer="91" />
                <label x="2268.22" y="299.72" size="1.27" layer="95" />
                <pinref part="D49" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="2237.74" y1="246.38" x2="2240.28" y2="246.38" width="0.3" layer="91" />
                <label x="2240.28" y="246.38" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="2260.60" y1="292.10" x2="2258.06" y2="292.10" width="0.3" layer="91" />
                <label x="2258.06" y="292.10" size="1.27" layer="95" />
                <pinref part="D50" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="2217.42" y1="261.62" x2="2219.96" y2="261.62" width="0.3" layer="91" />
                <label x="2219.96" y="261.62" size="1.27" layer="95" />
                <pinref part="R414" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$238">
              <segment>
                <wire x1="2151.38" y1="190.50" x2="2148.84" y2="190.50" width="0.3" layer="91" />
                <label x="2148.84" y="190.50" size="1.27" layer="95" />
                <pinref part="D51" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="2169.16" y1="190.50" x2="2171.70" y2="190.50" width="0.3" layer="91" />
                <label x="2171.70" y="190.50" size="1.27" layer="95" />
                <pinref part="D52" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="2146.30" y1="149.86" x2="2148.84" y2="149.86" width="0.3" layer="91" />
                <label x="2148.84" y="149.86" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$239">
              <segment>
                <wire x1="2156.46" y1="190.50" x2="2159.00" y2="190.50" width="0.3" layer="91" />
                <label x="2159.00" y="190.50" size="1.27" layer="95" />
                <pinref part="D51" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="2108.20" y1="137.16" x2="2110.74" y2="137.16" width="0.3" layer="91" />
                <label x="2110.74" y="137.16" size="1.27" layer="95" />
                <pinref part="R426" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$240">
              <segment>
                <wire x1="2164.08" y1="190.50" x2="2161.54" y2="190.50" width="0.3" layer="91" />
                <label x="2161.54" y="190.50" size="1.27" layer="95" />
                <pinref part="D52" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="2108.20" y1="129.54" x2="2110.74" y2="129.54" width="0.3" layer="91" />
                <label x="2110.74" y="129.54" size="1.27" layer="95" />
                <pinref part="R428" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2151.38" y1="182.88" x2="2148.84" y2="182.88" width="0.3" layer="91" />
                <label x="2148.84" y="182.88" size="1.27" layer="95" />
                <pinref part="R431" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$241">
              <segment>
                <wire x1="2280.92" y1="307.34" x2="2280.92" y2="304.80" width="0.3" layer="91" />
                <label x="2280.92" y="304.80" size="1.27" layer="95" />
                <pinref part="Q52" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="2302.51" y1="327.66" x2="2302.51" y2="330.20" width="0.3" layer="91" />
                <label x="2302.51" y="330.20" size="1.27" layer="95" />
                <pinref part="VR9" gate="G$1" pin="W" />
              </segment>
              <segment>
                <wire x1="2313.94" y1="314.96" x2="2316.48" y2="314.96" width="0.3" layer="91" />
                <label x="2316.48" y="314.96" size="1.27" layer="95" />
                <pinref part="VR9" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$242">
              <segment>
                <wire x1="2207.26" y1="261.62" x2="2204.72" y2="261.62" width="0.3" layer="91" />
                <label x="2204.72" y="261.62" size="1.27" layer="95" />
                <pinref part="R414" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2237.74" y1="236.22" x2="2240.28" y2="236.22" width="0.3" layer="91" />
                <label x="2240.28" y="236.22" size="1.27" layer="95" />
                <pinref part="U44" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="2181.86" y1="215.90" x2="2184.40" y2="215.90" width="0.3" layer="91" />
                <label x="2184.40" y="215.90" size="1.27" layer="95" />
                <pinref part="R415" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$243">
              <segment>
                <wire x1="2235.20" y1="261.62" x2="2237.74" y2="261.62" width="0.3" layer="91" />
                <label x="2237.74" y="261.62" size="1.27" layer="95" />
                <pinref part="R417" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2240.28" y1="276.86" x2="2237.74" y2="276.86" width="0.3" layer="91" />
                <label x="2237.74" y="276.86" size="1.27" layer="95" />
                <pinref part="VR8" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$244">
              <segment>
                <wire x1="2098.04" y1="137.16" x2="2095.50" y2="137.16" width="0.3" layer="91" />
                <label x="2095.50" y="137.16" size="1.27" layer="95" />
                <pinref part="R426" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2146.30" y1="144.78" x2="2148.84" y2="144.78" width="0.3" layer="91" />
                <label x="2148.84" y="144.78" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="2098.04" y1="129.54" x2="2095.50" y2="129.54" width="0.3" layer="91" />
                <label x="2095.50" y="129.54" size="1.27" layer="95" />
                <pinref part="R428" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="2087.88" y1="104.14" x2="2090.42" y2="104.14" width="0.3" layer="91" />
                <label x="2090.42" y="104.14" size="1.27" layer="95" />
                <pinref part="R427" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$245">
              <segment>
                <wire x1="2090.42" y1="121.92" x2="2092.96" y2="121.92" width="0.3" layer="91" />
                <label x="2092.96" y="121.92" size="1.27" layer="95" />
                <pinref part="R429" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2146.30" y1="139.70" x2="2148.84" y2="139.70" width="0.3" layer="91" />
                <label x="2148.84" y="139.70" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$246">
              <segment>
                <wire x1="2090.42" y1="129.54" x2="2092.96" y2="129.54" width="0.3" layer="91" />
                <label x="2092.96" y="129.54" size="1.27" layer="95" />
                <pinref part="R430" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2115.82" y1="144.78" x2="2113.28" y2="144.78" width="0.3" layer="91" />
                <label x="2113.28" y="144.78" size="1.27" layer="95" />
                <pinref part="U45" gate="G$1" pin="I1+" />
              </segment>
            </net>
            <net name="N$247">
              <segment>
                <wire x1="2252.98" y1="292.10" x2="2255.52" y2="292.10" width="0.3" layer="91" />
                <label x="2255.52" y="292.10" size="1.27" layer="95" />
                <pinref part="RVR9Ext" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="2288.54" y1="314.96" x2="2286.00" y2="314.96" width="0.3" layer="91" />
                <label x="2286.00" y="314.96" size="1.27" layer="95" />
                <pinref part="VR9" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$248">
              <segment>
                <wire x1="514.35" y1="2684.78" x2="511.81" y2="2684.78" width="0.3" layer="91" />
                <label x="511.81" y="2684.78" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="593.09" y1="2674.62" x2="595.63" y2="2674.62" width="0.3" layer="91" />
                <label x="595.63" y="2674.62" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="600.71" y1="2689.86" x2="598.17" y2="2689.86" width="0.3" layer="91" />
                <label x="598.17" y="2689.86" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="E1" />
              </segment>
            </net>
            <net name="N$249">
              <segment>
                <wire x1="529.59" y1="2692.40" x2="532.13" y2="2692.40" width="0.3" layer="91" />
                <label x="532.13" y="2692.40" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="491.49" y1="2692.40" x2="488.95" y2="2692.40" width="0.3" layer="91" />
                <label x="488.95" y="2692.40" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2700.02" x2="509.27" y2="2700.02" width="0.3" layer="91" />
                <label x="509.27" y="2700.02" size="1.27" layer="95" />
                <pinref part="Q1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="469.90" y1="2517.14" x2="472.44" y2="2517.14" width="0.3" layer="91" />
                <label x="472.44" y="2517.14" size="1.27" layer="95" />
                <pinref part="R152" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$250">
              <segment>
                <wire x1="600.71" y1="2674.62" x2="598.17" y2="2674.62" width="0.3" layer="91" />
                <label x="598.17" y="2674.62" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="514.35" y1="2659.38" x2="511.81" y2="2659.38" width="0.3" layer="91" />
                <label x="511.81" y="2659.38" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="572.77" y1="2677.16" x2="575.31" y2="2677.16" width="0.3" layer="91" />
                <label x="575.31" y="2677.16" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$251">
              <segment>
                <wire x1="577.85" y1="2682.24" x2="575.31" y2="2682.24" width="0.3" layer="91" />
                <label x="575.31" y="2682.24" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="441.96" y1="2509.52" x2="439.42" y2="2509.52" width="0.3" layer="91" />
                <label x="439.42" y="2509.52" size="1.27" layer="95" />
                <pinref part="R150" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="469.90" y1="2509.52" x2="472.44" y2="2509.52" width="0.3" layer="91" />
                <label x="472.44" y="2509.52" size="1.27" layer="95" />
                <pinref part="R153" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$252">
              <segment>
                <wire x1="593.09" y1="2689.86" x2="595.63" y2="2689.86" width="0.3" layer="91" />
                <label x="595.63" y="2689.86" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="600.71" y1="2697.48" x2="598.17" y2="2697.48" width="0.3" layer="91" />
                <label x="598.17" y="2697.48" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="534.67" y1="2677.16" x2="532.13" y2="2677.16" width="0.3" layer="91" />
                <label x="532.13" y="2677.16" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$253">
              <segment>
                <wire x1="615.95" y1="2682.24" x2="618.49" y2="2682.24" width="0.3" layer="91" />
                <label x="618.49" y="2682.24" size="1.27" layer="95" />
                <pinref part="Q2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="416.56" y1="2494.28" x2="419.10" y2="2494.28" width="0.3" layer="91" />
                <label x="419.10" y="2494.28" size="1.27" layer="95" />
                <pinref part="R151" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="416.56" y1="2486.66" x2="419.10" y2="2486.66" width="0.3" layer="91" />
                <label x="419.10" y="2486.66" size="1.27" layer="95" />
                <pinref part="R160" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$254">
              <segment>
                <wire x1="491.49" y1="2667.00" x2="488.95" y2="2667.00" width="0.3" layer="91" />
                <label x="488.95" y="2667.00" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="557.53" y1="2684.78" x2="554.99" y2="2684.78" width="0.3" layer="91" />
                <label x="554.99" y="2684.78" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2674.62" x2="509.27" y2="2674.62" width="0.3" layer="91" />
                <label x="509.27" y="2674.62" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="529.59" y1="2667.00" x2="532.13" y2="2667.00" width="0.3" layer="91" />
                <label x="532.13" y="2667.00" size="1.27" layer="95" />
                <pinref part="Q3" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$255">
              <segment>
                <wire x1="615.95" y1="2705.10" x2="618.49" y2="2705.10" width="0.3" layer="91" />
                <label x="618.49" y="2705.10" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="549.91" y1="2669.54" x2="552.45" y2="2669.54" width="0.3" layer="91" />
                <label x="552.45" y="2669.54" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="577.85" y1="2705.10" x2="575.31" y2="2705.10" width="0.3" layer="91" />
                <label x="575.31" y="2705.10" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="593.09" y1="2712.72" x2="595.63" y2="2712.72" width="0.3" layer="91" />
                <label x="595.63" y="2712.72" size="1.27" layer="95" />
                <pinref part="Q4" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$256">
              <segment>
                <wire x1="557.53" y1="2669.54" x2="554.99" y2="2669.54" width="0.3" layer="91" />
                <label x="554.99" y="2669.54" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="643.89" y1="2707.64" x2="641.35" y2="2707.64" width="0.3" layer="91" />
                <label x="641.35" y="2707.64" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="638.81" y1="2738.12" x2="636.27" y2="2738.12" width="0.3" layer="91" />
                <label x="636.27" y="2738.12" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$257">
              <segment>
                <wire x1="549.91" y1="2684.78" x2="552.45" y2="2684.78" width="0.3" layer="91" />
                <label x="552.45" y="2684.78" size="1.27" layer="95" />
                <pinref part="Q5" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2745.74" x2="656.59" y2="2745.74" width="0.3" layer="91" />
                <label x="656.59" y="2745.74" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="812.80" y1="2689.86" x2="810.26" y2="2689.86" width="0.3" layer="91" />
                <label x="810.26" y="2689.86" size="1.27" layer="95" />
                <pinref part="R144" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="812.80" y1="2697.48" x2="810.26" y2="2697.48" width="0.3" layer="91" />
                <label x="810.26" y="2697.48" size="1.27" layer="95" />
                <pinref part="R145" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="861.06" y1="2705.10" x2="863.60" y2="2705.10" width="0.3" layer="91" />
                <label x="863.60" y="2705.10" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$258">
              <segment>
                <wire x1="621.03" y1="2715.26" x2="618.49" y2="2715.26" width="0.3" layer="91" />
                <label x="618.49" y="2715.26" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2730.50" x2="656.59" y2="2730.50" width="0.3" layer="91" />
                <label x="656.59" y="2730.50" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="659.13" y1="2715.26" x2="661.67" y2="2715.26" width="0.3" layer="91" />
                <label x="661.67" y="2715.26" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="636.27" y1="2722.88" x2="638.81" y2="2722.88" width="0.3" layer="91" />
                <label x="638.81" y="2722.88" size="1.27" layer="95" />
                <pinref part="Q6" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$259">
              <segment>
                <wire x1="661.67" y1="2745.74" x2="659.13" y2="2745.74" width="0.3" layer="91" />
                <label x="659.13" y="2745.74" size="1.27" layer="95" />
                <pinref part="Q7" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="661.67" y1="2753.36" x2="659.13" y2="2753.36" width="0.3" layer="91" />
                <label x="659.13" y="2753.36" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="681.99" y1="2760.98" x2="679.45" y2="2760.98" width="0.3" layer="91" />
                <label x="679.45" y="2760.98" size="1.27" layer="95" />
                <pinref part="Q8" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$260">
              <segment>
                <wire x1="704.85" y1="2768.60" x2="702.31" y2="2768.60" width="0.3" layer="91" />
                <label x="702.31" y="2768.60" size="1.27" layer="95" />
                <pinref part="Q8" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="693.42" y1="2776.22" x2="695.96" y2="2776.22" width="0.3" layer="91" />
                <label x="695.96" y="2776.22" size="1.27" layer="95" />
                <pinref part="R2" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$261">
              <segment>
                <wire x1="638.81" y1="2760.98" x2="636.27" y2="2760.98" width="0.3" layer="91" />
                <label x="636.27" y="2760.98" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="676.91" y1="2760.98" x2="679.45" y2="2760.98" width="0.3" layer="91" />
                <label x="679.45" y="2760.98" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="632.46" y1="2730.50" x2="635.00" y2="2730.50" width="0.3" layer="91" />
                <label x="635.00" y="2730.50" size="1.27" layer="95" />
                <pinref part="R1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2768.60" x2="656.59" y2="2768.60" width="0.3" layer="91" />
                <label x="656.59" y="2768.60" size="1.27" layer="95" />
                <pinref part="Q9" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$262">
              <segment>
                <wire x1="506.73" y1="2565.40" x2="509.27" y2="2565.40" width="0.3" layer="91" />
                <label x="509.27" y="2565.40" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="491.49" y1="2557.78" x2="488.95" y2="2557.78" width="0.3" layer="91" />
                <label x="488.95" y="2557.78" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="529.59" y1="2557.78" x2="532.13" y2="2557.78" width="0.3" layer="91" />
                <label x="532.13" y="2557.78" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="914.40" y1="2806.70" x2="916.94" y2="2806.70" width="0.3" layer="91" />
                <label x="916.94" y="2806.70" size="1.27" layer="95" />
                <pinref part="R158" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$263">
              <segment>
                <wire x1="514.35" y1="2550.16" x2="511.81" y2="2550.16" width="0.3" layer="91" />
                <label x="511.81" y="2550.16" size="1.27" layer="95" />
                <pinref part="Q1$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="593.09" y1="2540.00" x2="595.63" y2="2540.00" width="0.3" layer="91" />
                <label x="595.63" y="2540.00" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="600.71" y1="2555.24" x2="598.17" y2="2555.24" width="0.3" layer="91" />
                <label x="598.17" y="2555.24" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="E1" />
              </segment>
            </net>
            <net name="N$264">
              <segment>
                <wire x1="600.71" y1="2540.00" x2="598.17" y2="2540.00" width="0.3" layer="91" />
                <label x="598.17" y="2540.00" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="514.35" y1="2524.76" x2="511.81" y2="2524.76" width="0.3" layer="91" />
                <label x="511.81" y="2524.76" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="572.77" y1="2542.54" x2="575.31" y2="2542.54" width="0.3" layer="91" />
                <label x="575.31" y="2542.54" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$265">
              <segment>
                <wire x1="577.85" y1="2547.62" x2="575.31" y2="2547.62" width="0.3" layer="91" />
                <label x="575.31" y="2547.62" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="441.96" y1="2501.90" x2="439.42" y2="2501.90" width="0.3" layer="91" />
                <label x="439.42" y="2501.90" size="1.27" layer="95" />
                <pinref part="R155" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="434.34" y1="2501.90" x2="436.88" y2="2501.90" width="0.3" layer="91" />
                <label x="436.88" y="2501.90" size="1.27" layer="95" />
                <pinref part="R156" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$266">
              <segment>
                <wire x1="593.09" y1="2555.24" x2="595.63" y2="2555.24" width="0.3" layer="91" />
                <label x="595.63" y="2555.24" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="600.71" y1="2562.86" x2="598.17" y2="2562.86" width="0.3" layer="91" />
                <label x="598.17" y="2562.86" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="534.67" y1="2542.54" x2="532.13" y2="2542.54" width="0.3" layer="91" />
                <label x="532.13" y="2542.54" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$267">
              <segment>
                <wire x1="615.95" y1="2547.62" x2="618.49" y2="2547.62" width="0.3" layer="91" />
                <label x="618.49" y="2547.62" size="1.27" layer="95" />
                <pinref part="Q2$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="398.78" y1="2486.66" x2="401.32" y2="2486.66" width="0.3" layer="91" />
                <label x="401.32" y="2486.66" size="1.27" layer="95" />
                <pinref part="R157" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="398.78" y1="2479.04" x2="401.32" y2="2479.04" width="0.3" layer="91" />
                <label x="401.32" y="2479.04" size="1.27" layer="95" />
                <pinref part="R161" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$268">
              <segment>
                <wire x1="491.49" y1="2532.38" x2="488.95" y2="2532.38" width="0.3" layer="91" />
                <label x="488.95" y="2532.38" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="557.53" y1="2550.16" x2="554.99" y2="2550.16" width="0.3" layer="91" />
                <label x="554.99" y="2550.16" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="506.73" y1="2540.00" x2="509.27" y2="2540.00" width="0.3" layer="91" />
                <label x="509.27" y="2540.00" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="529.59" y1="2532.38" x2="532.13" y2="2532.38" width="0.3" layer="91" />
                <label x="532.13" y="2532.38" size="1.27" layer="95" />
                <pinref part="Q3$1" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$269">
              <segment>
                <wire x1="615.95" y1="2570.48" x2="618.49" y2="2570.48" width="0.3" layer="91" />
                <label x="618.49" y="2570.48" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="549.91" y1="2534.92" x2="552.45" y2="2534.92" width="0.3" layer="91" />
                <label x="552.45" y="2534.92" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="577.85" y1="2570.48" x2="575.31" y2="2570.48" width="0.3" layer="91" />
                <label x="575.31" y="2570.48" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="593.09" y1="2578.10" x2="595.63" y2="2578.10" width="0.3" layer="91" />
                <label x="595.63" y="2578.10" size="1.27" layer="95" />
                <pinref part="Q4$1" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$270">
              <segment>
                <wire x1="557.53" y1="2534.92" x2="554.99" y2="2534.92" width="0.3" layer="91" />
                <label x="554.99" y="2534.92" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="643.89" y1="2573.02" x2="641.35" y2="2573.02" width="0.3" layer="91" />
                <label x="641.35" y="2573.02" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="638.81" y1="2603.50" x2="636.27" y2="2603.50" width="0.3" layer="91" />
                <label x="636.27" y="2603.50" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$271">
              <segment>
                <wire x1="549.91" y1="2550.16" x2="552.45" y2="2550.16" width="0.3" layer="91" />
                <label x="552.45" y="2550.16" size="1.27" layer="95" />
                <pinref part="Q5$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2611.12" x2="656.59" y2="2611.12" width="0.3" layer="91" />
                <label x="656.59" y="2611.12" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="795.02" y1="2682.24" x2="792.48" y2="2682.24" width="0.3" layer="91" />
                <label x="792.48" y="2682.24" size="1.27" layer="95" />
                <pinref part="R146" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="795.02" y1="2689.86" x2="792.48" y2="2689.86" width="0.3" layer="91" />
                <label x="792.48" y="2689.86" size="1.27" layer="95" />
                <pinref part="R148" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="787.40" y1="2689.86" x2="789.94" y2="2689.86" width="0.3" layer="91" />
                <label x="789.94" y="2689.86" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="I2-" />
              </segment>
            </net>
            <net name="N$272">
              <segment>
                <wire x1="659.13" y1="2580.64" x2="661.67" y2="2580.64" width="0.3" layer="91" />
                <label x="661.67" y="2580.64" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2595.88" x2="656.59" y2="2595.88" width="0.3" layer="91" />
                <label x="656.59" y="2595.88" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="621.03" y1="2580.64" x2="618.49" y2="2580.64" width="0.3" layer="91" />
                <label x="618.49" y="2580.64" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="636.27" y1="2588.26" x2="638.81" y2="2588.26" width="0.3" layer="91" />
                <label x="638.81" y="2588.26" size="1.27" layer="95" />
                <pinref part="Q6$1" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$273">
              <segment>
                <wire x1="661.67" y1="2611.12" x2="659.13" y2="2611.12" width="0.3" layer="91" />
                <label x="659.13" y="2611.12" size="1.27" layer="95" />
                <pinref part="Q7$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="661.67" y1="2618.74" x2="659.13" y2="2618.74" width="0.3" layer="91" />
                <label x="659.13" y="2618.74" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="681.99" y1="2626.36" x2="679.45" y2="2626.36" width="0.3" layer="91" />
                <label x="679.45" y="2626.36" size="1.27" layer="95" />
                <pinref part="Q8$1" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$274">
              <segment>
                <wire x1="704.85" y1="2633.98" x2="702.31" y2="2633.98" width="0.3" layer="91" />
                <label x="702.31" y="2633.98" size="1.27" layer="95" />
                <pinref part="Q8$1" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="693.42" y1="2641.60" x2="695.96" y2="2641.60" width="0.3" layer="91" />
                <label x="695.96" y="2641.60" size="1.27" layer="95" />
                <pinref part="R2$1" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$275">
              <segment>
                <wire x1="638.81" y1="2626.36" x2="636.27" y2="2626.36" width="0.3" layer="91" />
                <label x="636.27" y="2626.36" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="676.91" y1="2626.36" x2="679.45" y2="2626.36" width="0.3" layer="91" />
                <label x="679.45" y="2626.36" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="632.46" y1="2595.88" x2="635.00" y2="2595.88" width="0.3" layer="91" />
                <label x="635.00" y="2595.88" size="1.27" layer="95" />
                <pinref part="R1$1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="654.05" y1="2633.98" x2="656.59" y2="2633.98" width="0.3" layer="91" />
                <label x="656.59" y="2633.98" size="1.27" layer="95" />
                <pinref part="Q9$1" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$276">
              <segment>
                <wire x1="347.98" y1="2461.26" x2="347.98" y2="2458.72" width="0.3" layer="91" />
                <label x="347.98" y="2458.72" size="1.27" layer="95" />
                <pinref part="15" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="406.40" y1="2486.66" x2="403.86" y2="2486.66" width="0.3" layer="91" />
                <label x="403.86" y="2486.66" size="1.27" layer="95" />
                <pinref part="R160" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="388.62" y1="2479.04" x2="386.08" y2="2479.04" width="0.3" layer="91" />
                <label x="386.08" y="2479.04" size="1.27" layer="95" />
                <pinref part="R161" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="332.74" y1="2430.78" x2="332.74" y2="2428.24" width="0.3" layer="91" />
                <label x="332.74" y="2428.24" size="1.27" layer="95" />
                <pinref part="Q27" gate="G$1" pin="S" />
              </segment>
            </net>
            <net name="N$277">
              <segment>
                <wire x1="424.18" y1="2491.74" x2="424.18" y2="2489.20" width="0.3" layer="91" />
                <label x="424.18" y="2489.20" size="1.27" layer="95" />
                <pinref part="16" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="459.74" y1="2517.14" x2="457.20" y2="2517.14" width="0.3" layer="91" />
                <label x="457.20" y="2517.14" size="1.27" layer="95" />
                <pinref part="R152" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="378.46" y1="2496.82" x2="381.00" y2="2496.82" width="0.3" layer="91" />
                <label x="381.00" y="2496.82" size="1.27" layer="95" />
                <pinref part="Q22" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$278">
              <segment>
                <wire x1="889.00" y1="2781.30" x2="889.00" y2="2778.76" width="0.3" layer="91" />
                <label x="889.00" y="2778.76" size="1.27" layer="95" />
                <pinref part="17" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="911.86" y1="2768.60" x2="914.40" y2="2768.60" width="0.3" layer="91" />
                <label x="914.40" y="2768.60" size="1.27" layer="95" />
                <pinref part="VR6" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="900.43" y1="2781.30" x2="900.43" y2="2783.84" width="0.3" layer="91" />
                <label x="900.43" y="2783.84" size="1.27" layer="95" />
                <pinref part="VR6" gate="G$1" pin="W" />
              </segment>
              <segment>
                <wire x1="881.38" y1="2740.66" x2="881.38" y2="2738.12" width="0.3" layer="91" />
                <label x="881.38" y="2738.12" size="1.27" layer="95" />
                <pinref part="C53" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="45.72" y1="58.42" x2="43.18" y2="58.42" width="0.3" layer="91" />
                <label x="43.18" y="58.42" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-12" />
              </segment>
              <segment>
                <wire x1="861.06" y1="2710.18" x2="863.60" y2="2710.18" width="0.3" layer="91" />
                <label x="863.60" y="2710.18" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$279">
              <segment>
                <wire x1="881.38" y1="2758.44" x2="881.38" y2="2755.90" width="0.3" layer="91" />
                <label x="881.38" y="2755.90" size="1.27" layer="95" />
                <pinref part="18" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="45.72" y1="60.96" x2="43.18" y2="60.96" width="0.3" layer="91" />
                <label x="43.18" y="60.96" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-11" />
              </segment>
              <segment>
                <wire x1="871.22" y1="2750.82" x2="873.76" y2="2750.82" width="0.3" layer="91" />
                <label x="873.76" y="2750.82" size="1.27" layer="95" />
                <pinref part="VR7" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="848.36" y1="2722.88" x2="848.36" y2="2720.34" width="0.3" layer="91" />
                <label x="848.36" y="2720.34" size="1.27" layer="95" />
                <pinref part="C55" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="859.79" y1="2763.52" x2="859.79" y2="2766.06" width="0.3" layer="91" />
                <label x="859.79" y="2766.06" size="1.27" layer="95" />
                <pinref part="VR7" gate="G$1" pin="W" />
              </segment>
              <segment>
                <wire x1="787.40" y1="2694.94" x2="789.94" y2="2694.94" width="0.3" layer="91" />
                <label x="789.94" y="2694.94" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$280">
              <segment>
                <wire x1="896.62" y1="2804.16" x2="896.62" y2="2801.62" width="0.3" layer="91" />
                <label x="896.62" y="2801.62" size="1.27" layer="95" />
                <pinref part="19" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="904.24" y1="2806.70" x2="901.70" y2="2806.70" width="0.3" layer="91" />
                <label x="901.70" y="2806.70" size="1.27" layer="95" />
                <pinref part="R158" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="909.32" y1="2801.62" x2="911.86" y2="2801.62" width="0.3" layer="91" />
                <label x="911.86" y="2801.62" size="1.27" layer="95" />
                <pinref part="Q23" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$281">
              <segment>
                <wire x1="881.38" y1="2748.28" x2="881.38" y2="2750.82" width="0.3" layer="91" />
                <label x="881.38" y="2750.82" size="1.27" layer="95" />
                <pinref part="C53" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="822.96" y1="2689.86" x2="825.50" y2="2689.86" width="0.3" layer="91" />
                <label x="825.50" y="2689.86" size="1.27" layer="95" />
                <pinref part="R144" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$282">
              <segment>
                <wire x1="281.94" y1="2395.22" x2="281.94" y2="2397.76" width="0.3" layer="91" />
                <label x="281.94" y="2397.76" size="1.27" layer="95" />
                <pinref part="C54" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="58.42" y1="68.58" x2="55.88" y2="68.58" width="0.3" layer="91" />
                <label x="55.88" y="68.58" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-8" />
              </segment>
              <segment>
                <wire x1="723.90" y1="2651.76" x2="721.36" y2="2651.76" width="0.3" layer="91" />
                <label x="721.36" y="2651.76" size="1.27" layer="95" />
                <pinref part="R149" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="375.92" y1="2471.42" x2="378.46" y2="2471.42" width="0.3" layer="91" />
                <label x="378.46" y="2471.42" size="1.27" layer="95" />
                <pinref part="R176" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="307.34" y1="2430.78" x2="304.80" y2="2430.78" width="0.3" layer="91" />
                <label x="304.80" y="2430.78" size="1.27" layer="95" />
                <pinref part="R147" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$283">
              <segment>
                <wire x1="848.36" y1="2730.50" x2="848.36" y2="2733.04" width="0.3" layer="91" />
                <label x="848.36" y="2733.04" size="1.27" layer="95" />
                <pinref part="C55" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="805.18" y1="2682.24" x2="807.72" y2="2682.24" width="0.3" layer="91" />
                <label x="807.72" y="2682.24" size="1.27" layer="95" />
                <pinref part="R146" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$284">
              <segment>
                <wire x1="378.46" y1="2481.58" x2="381.00" y2="2481.58" width="0.3" layer="91" />
                <label x="381.00" y="2481.58" size="1.27" layer="95" />
                <pinref part="Q22" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="358.14" y1="2471.42" x2="360.68" y2="2471.42" width="0.3" layer="91" />
                <label x="360.68" y="2471.42" size="1.27" layer="95" />
                <pinref part="R154" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$285">
              <segment>
                <wire x1="363.22" y1="2489.20" x2="360.68" y2="2489.20" width="0.3" layer="91" />
                <label x="360.68" y="2489.20" size="1.27" layer="95" />
                <pinref part="Q22" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="830.58" y1="2715.26" x2="828.04" y2="2715.26" width="0.3" layer="91" />
                <label x="828.04" y="2715.26" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$286">
              <segment>
                <wire x1="909.32" y1="2786.38" x2="911.86" y2="2786.38" width="0.3" layer="91" />
                <label x="911.86" y="2786.38" size="1.27" layer="95" />
                <pinref part="Q23" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="840.74" y1="2725.42" x2="843.28" y2="2725.42" width="0.3" layer="91" />
                <label x="843.28" y="2725.42" size="1.27" layer="95" />
                <pinref part="R159" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$287">
              <segment>
                <wire x1="894.08" y1="2794.00" x2="891.54" y2="2794.00" width="0.3" layer="91" />
                <label x="891.54" y="2794.00" size="1.27" layer="95" />
                <pinref part="Q23" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="756.92" y1="2700.02" x2="754.38" y2="2700.02" width="0.3" layer="91" />
                <label x="754.38" y="2700.02" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$288">
              <segment>
                <wire x1="332.74" y1="2456.18" x2="332.74" y2="2458.72" width="0.3" layer="91" />
                <label x="332.74" y="2458.72" size="1.27" layer="95" />
                <pinref part="Q27" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="289.56" y1="2339.34" x2="292.10" y2="2339.34" width="0.3" layer="91" />
                <label x="292.10" y="2339.34" size="1.27" layer="95" />
                <pinref part="R172" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="289.56" y1="2331.72" x2="292.10" y2="2331.72" width="0.3" layer="91" />
                <label x="292.10" y="2331.72" size="1.27" layer="95" />
                <pinref part="R174" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$289">
              <segment>
                <wire x1="322.58" y1="2443.48" x2="320.04" y2="2443.48" width="0.3" layer="91" />
                <label x="320.04" y="2443.48" size="1.27" layer="95" />
                <pinref part="Q27" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="307.34" y1="2423.16" x2="304.80" y2="2423.16" width="0.3" layer="91" />
                <label x="304.80" y="2423.16" size="1.27" layer="95" />
                <pinref part="R163" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$290">
              <segment>
                <wire x1="337.82" y1="2479.04" x2="340.36" y2="2479.04" width="0.3" layer="91" />
                <label x="340.36" y="2479.04" size="1.27" layer="95" />
                <pinref part="Q29" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="365.76" y1="2471.42" x2="363.22" y2="2471.42" width="0.3" layer="91" />
                <label x="363.22" y="2471.42" size="1.27" layer="95" />
                <pinref part="R176" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$291">
              <segment>
                <wire x1="322.58" y1="2471.42" x2="320.04" y2="2471.42" width="0.3" layer="91" />
                <label x="320.04" y="2471.42" size="1.27" layer="95" />
                <pinref part="Q29" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="299.72" y1="2405.38" x2="302.26" y2="2405.38" width="0.3" layer="91" />
                <label x="302.26" y="2405.38" size="1.27" layer="95" />
                <pinref part="R177" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="299.72" y1="2397.76" x2="302.26" y2="2397.76" width="0.3" layer="91" />
                <label x="302.26" y="2397.76" size="1.27" layer="95" />
                <pinref part="R175" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$292">
              <segment>
                <wire x1="822.96" y1="2697.48" x2="825.50" y2="2697.48" width="0.3" layer="91" />
                <label x="825.50" y="2697.48" size="1.27" layer="95" />
                <pinref part="R145" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="886.46" y1="2768.60" x2="883.92" y2="2768.60" width="0.3" layer="91" />
                <label x="883.92" y="2768.60" size="1.27" layer="95" />
                <pinref part="VR6" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$293">
              <segment>
                <wire x1="317.50" y1="2430.78" x2="320.04" y2="2430.78" width="0.3" layer="91" />
                <label x="320.04" y="2430.78" size="1.27" layer="95" />
                <pinref part="R147" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="830.58" y1="2710.18" x2="828.04" y2="2710.18" width="0.3" layer="91" />
                <label x="828.04" y="2710.18" size="1.27" layer="95" />
                <pinref part="U17" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="347.98" y1="2471.42" x2="345.44" y2="2471.42" width="0.3" layer="91" />
                <label x="345.44" y="2471.42" size="1.27" layer="95" />
                <pinref part="R154" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$294">
              <segment>
                <wire x1="805.18" y1="2689.86" x2="807.72" y2="2689.86" width="0.3" layer="91" />
                <label x="807.72" y="2689.86" size="1.27" layer="95" />
                <pinref part="R148" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="845.82" y1="2750.82" x2="843.28" y2="2750.82" width="0.3" layer="91" />
                <label x="843.28" y="2750.82" size="1.27" layer="95" />
                <pinref part="VR7" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$295">
              <segment>
                <wire x1="734.06" y1="2651.76" x2="736.60" y2="2651.76" width="0.3" layer="91" />
                <label x="736.60" y="2651.76" size="1.27" layer="95" />
                <pinref part="R149" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="756.92" y1="2694.94" x2="754.38" y2="2694.94" width="0.3" layer="91" />
                <label x="754.38" y="2694.94" size="1.27" layer="95" />
                <pinref part="U19" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="830.58" y1="2725.42" x2="828.04" y2="2725.42" width="0.3" layer="91" />
                <label x="828.04" y="2725.42" size="1.27" layer="95" />
                <pinref part="R159" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$296">
              <segment>
                <wire x1="452.12" y1="2509.52" x2="454.66" y2="2509.52" width="0.3" layer="91" />
                <label x="454.66" y="2509.52" size="1.27" layer="95" />
                <pinref part="R150" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="768.35" y1="2679.70" x2="768.35" y2="2682.24" width="0.3" layer="91" />
                <label x="768.35" y="2682.24" size="1.27" layer="95" />
                <pinref part="VR4" gate="G$1" pin="W" />
              </segment>
            </net>
            <net name="N$297">
              <segment>
                <wire x1="452.12" y1="2501.90" x2="454.66" y2="2501.90" width="0.3" layer="91" />
                <label x="454.66" y="2501.90" size="1.27" layer="95" />
                <pinref part="R155" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="735.33" y1="2679.70" x2="735.33" y2="2682.24" width="0.3" layer="91" />
                <label x="735.33" y="2682.24" size="1.27" layer="95" />
                <pinref part="VR5" gate="G$1" pin="W" />
              </segment>
            </net>
            <net name="N$298">
              <segment>
                <wire x1="279.40" y1="2331.72" x2="276.86" y2="2331.72" width="0.3" layer="91" />
                <label x="276.86" y="2331.72" size="1.27" layer="95" />
                <pinref part="R174" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="881.38" y1="3086.10" x2="881.38" y2="3083.56" width="0.3" layer="91" />
                <label x="881.38" y="3083.56" size="1.27" layer="95" />
                <pinref part="C45" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$299">
              <segment>
                <wire x1="193.04" y1="2382.52" x2="193.04" y2="2385.06" width="0.3" layer="91" />
                <label x="193.04" y="2385.06" size="1.27" layer="95" />
                <pinref part="C701" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="45.72" y1="83.82" x2="43.18" y2="83.82" width="0.3" layer="91" />
                <label x="43.18" y="83.82" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-2" />
              </segment>
            </net>
            <net name="N$300">
              <segment>
                <wire x1="193.04" y1="2374.90" x2="193.04" y2="2372.36" width="0.3" layer="91" />
                <label x="193.04" y="2372.36" size="1.27" layer="95" />
                <pinref part="C701" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="210.82" y1="2395.22" x2="208.28" y2="2395.22" width="0.3" layer="91" />
                <label x="208.28" y="2395.22" size="1.27" layer="95" />
                <pinref part="R719" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$301">
              <segment>
                <wire x1="121.92" y1="2311.40" x2="121.92" y2="2308.86" width="0.3" layer="91" />
                <label x="121.92" y="2308.86" size="1.27" layer="95" />
                <pinref part="C702" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="129.54" y1="2316.48" x2="127.00" y2="2316.48" width="0.3" layer="91" />
                <label x="127.00" y="2316.48" size="1.27" layer="95" />
                <pinref part="R718" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$302">
              <segment>
                <wire x1="121.92" y1="2319.02" x2="121.92" y2="2321.56" width="0.3" layer="91" />
                <label x="121.92" y="2321.56" size="1.27" layer="95" />
                <pinref part="C702" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="45.72" y1="86.36" x2="43.18" y2="86.36" width="0.3" layer="91" />
                <label x="43.18" y="86.36" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-1" />
              </segment>
              <segment>
                <wire x1="58.42" y1="81.28" x2="55.88" y2="81.28" width="0.3" layer="91" />
                <label x="55.88" y="81.28" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-3" />
              </segment>
            </net>
            <net name="N$303">
              <segment>
                <wire x1="170.18" y1="2357.12" x2="170.18" y2="2354.58" width="0.3" layer="91" />
                <label x="170.18" y="2354.58" size="1.27" layer="95" />
                <pinref part="C703" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="182.88" y1="2392.68" x2="185.42" y2="2392.68" width="0.3" layer="91" />
                <label x="185.42" y="2392.68" size="1.27" layer="95" />
                <pinref part="Q76" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="45.72" y1="81.28" x2="43.18" y2="81.28" width="0.3" layer="91" />
                <label x="43.18" y="81.28" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-3" />
              </segment>
              <segment>
                <wire x1="134.62" y1="2341.88" x2="132.08" y2="2341.88" width="0.3" layer="91" />
                <label x="132.08" y="2341.88" size="1.27" layer="95" />
                <pinref part="R717" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$304">
              <segment>
                <wire x1="170.18" y1="2364.74" x2="170.18" y2="2367.28" width="0.3" layer="91" />
                <label x="170.18" y="2367.28" size="1.27" layer="95" />
                <pinref part="C703" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="167.64" y1="2385.06" x2="165.10" y2="2385.06" width="0.3" layer="91" />
                <label x="165.10" y="2385.06" size="1.27" layer="95" />
                <pinref part="Q76" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="144.78" y1="2341.88" x2="147.32" y2="2341.88" width="0.3" layer="91" />
                <label x="147.32" y="2341.88" size="1.27" layer="95" />
                <pinref part="R717" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="139.70" y1="2316.48" x2="142.24" y2="2316.48" width="0.3" layer="91" />
                <label x="142.24" y="2316.48" size="1.27" layer="95" />
                <pinref part="R718" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$305">
              <segment>
                <wire x1="152.40" y1="2349.50" x2="152.40" y2="2352.04" width="0.3" layer="91" />
                <label x="152.40" y="2352.04" size="1.27" layer="95" />
                <pinref part="C704" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="127.00" y1="2339.34" x2="129.54" y2="2339.34" width="0.3" layer="91" />
                <label x="129.54" y="2339.34" size="1.27" layer="95" />
                <pinref part="R713" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="152.40" y1="2359.66" x2="149.86" y2="2359.66" width="0.3" layer="91" />
                <label x="149.86" y="2359.66" size="1.27" layer="95" />
                <pinref part="R716" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$306">
              <segment>
                <wire x1="182.88" y1="2377.44" x2="185.42" y2="2377.44" width="0.3" layer="91" />
                <label x="185.42" y="2377.44" size="1.27" layer="95" />
                <pinref part="Q76" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="162.56" y1="2359.66" x2="165.10" y2="2359.66" width="0.3" layer="91" />
                <label x="165.10" y="2359.66" size="1.27" layer="95" />
                <pinref part="R716" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$307">
              <segment>
                <wire x1="220.98" y1="2395.22" x2="223.52" y2="2395.22" width="0.3" layer="91" />
                <label x="223.52" y="2395.22" size="1.27" layer="95" />
                <pinref part="R719" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="248.92" y1="2402.84" x2="246.38" y2="2402.84" width="0.3" layer="91" />
                <label x="246.38" y="2402.84" size="1.27" layer="95" />
                <pinref part="R720" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="210.82" y1="2415.54" x2="208.28" y2="2415.54" width="0.3" layer="91" />
                <label x="208.28" y="2415.54" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="203.20" y1="2395.22" x2="205.74" y2="2395.22" width="0.3" layer="91" />
                <label x="205.74" y="2395.22" size="1.27" layer="95" />
                <pinref part="R721" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$308">
              <segment>
                <wire x1="193.04" y1="2395.22" x2="190.50" y2="2395.22" width="0.3" layer="91" />
                <label x="190.50" y="2395.22" size="1.27" layer="95" />
                <pinref part="R721" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="58.42" y1="83.82" x2="55.88" y2="83.82" width="0.3" layer="91" />
                <label x="55.88" y="83.82" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="3-2" />
              </segment>
            </net>
            <net name="N$309">
              <segment>
                <wire x1="241.30" y1="2410.46" x2="243.84" y2="2410.46" width="0.3" layer="91" />
                <label x="243.84" y="2410.46" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="45.72" y1="68.58" x2="43.18" y2="68.58" width="0.3" layer="91" />
                <label x="43.18" y="68.58" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-8" />
              </segment>
            </net>
            <net name="N$310">
              <segment>
                <wire x1="241.30" y1="2405.38" x2="243.84" y2="2405.38" width="0.3" layer="91" />
                <label x="243.84" y="2405.38" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="45.72" y1="71.12" x2="43.18" y2="71.12" width="0.3" layer="91" />
                <label x="43.18" y="71.12" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-7" />
              </segment>
            </net>
            <net name="N$311">
              <segment>
                <wire x1="241.30" y1="2415.54" x2="243.84" y2="2415.54" width="0.3" layer="91" />
                <label x="243.84" y="2415.54" size="1.27" layer="95" />
                <pinref part="U71" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="444.50" y1="2938.78" x2="441.96" y2="2938.78" width="0.3" layer="91" />
                <label x="441.96" y="2938.78" size="1.27" layer="95" />
                <pinref part="VR3" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="458.47" y1="2951.48" x2="458.47" y2="2954.02" width="0.3" layer="91" />
                <label x="458.47" y="2954.02" size="1.27" layer="95" />
                <pinref part="VR3" gate="G$1" pin="W" />
              </segment>
              <segment>
                <wire x1="434.34" y1="2921.00" x2="434.34" y2="2918.46" width="0.3" layer="91" />
                <label x="434.34" y="2918.46" size="1.27" layer="95" />
                <pinref part="10" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="309.88" y1="2816.86" x2="312.42" y2="2816.86" width="0.3" layer="91" />
                <label x="312.42" y="2816.86" size="1.27" layer="95" />
                <pinref part="R118" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="317.50" y1="2824.48" x2="314.96" y2="2824.48" width="0.3" layer="91" />
                <label x="314.96" y="2824.48" size="1.27" layer="95" />
                <pinref part="R112" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="45.72" y1="66.04" x2="43.18" y2="66.04" width="0.3" layer="91" />
                <label x="43.18" y="66.04" size="1.27" layer="95" />
                <pinref part="JMiddle" gate="G$1" pin="2-9" />
              </segment>
            </net>
            <net name="N$312">
              <segment>
                <wire x1="570.23" y1="3070.86" x2="572.77" y2="3070.86" width="0.3" layer="91" />
                <label x="572.77" y="3070.86" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="554.99" y1="3063.24" x2="552.45" y2="3063.24" width="0.3" layer="91" />
                <label x="552.45" y="3063.24" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="593.09" y1="3063.24" x2="595.63" y2="3063.24" width="0.3" layer="91" />
                <label x="595.63" y="3063.24" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="515.62" y1="2989.58" x2="518.16" y2="2989.58" width="0.3" layer="91" />
                <label x="518.16" y="2989.58" size="1.27" layer="95" />
                <pinref part="R121" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="533.40" y1="2997.20" x2="535.94" y2="2997.20" width="0.3" layer="91" />
                <label x="535.94" y="2997.20" size="1.27" layer="95" />
                <pinref part="R124" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$313">
              <segment>
                <wire x1="577.85" y1="3055.62" x2="575.31" y2="3055.62" width="0.3" layer="91" />
                <label x="575.31" y="3055.62" size="1.27" layer="95" />
                <pinref part="Q1$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="656.59" y1="3045.46" x2="659.13" y2="3045.46" width="0.3" layer="91" />
                <label x="659.13" y="3045.46" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="664.21" y1="3060.70" x2="661.67" y2="3060.70" width="0.3" layer="91" />
                <label x="661.67" y="3060.70" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="E1" />
              </segment>
            </net>
            <net name="N$314">
              <segment>
                <wire x1="641.35" y1="3053.08" x2="638.81" y2="3053.08" width="0.3" layer="91" />
                <label x="638.81" y="3053.08" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="805.18" y1="3037.84" x2="802.64" y2="3037.84" width="0.3" layer="91" />
                <label x="802.64" y="3037.84" size="1.27" layer="95" />
                <pinref part="R126" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="805.18" y1="3030.22" x2="802.64" y2="3030.22" width="0.3" layer="91" />
                <label x="802.64" y="3030.22" size="1.27" layer="95" />
                <pinref part="R123" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$315">
              <segment>
                <wire x1="679.45" y1="3053.08" x2="681.99" y2="3053.08" width="0.3" layer="91" />
                <label x="681.99" y="3053.08" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="787.40" y1="3022.60" x2="784.86" y2="3022.60" width="0.3" layer="91" />
                <label x="784.86" y="3022.60" size="1.27" layer="95" />
                <pinref part="R125" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$316">
              <segment>
                <wire x1="656.59" y1="3060.70" x2="659.13" y2="3060.70" width="0.3" layer="91" />
                <label x="659.13" y="3060.70" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="664.21" y1="3068.32" x2="661.67" y2="3068.32" width="0.3" layer="91" />
                <label x="661.67" y="3068.32" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="598.17" y1="3048.00" x2="595.63" y2="3048.00" width="0.3" layer="91" />
                <label x="595.63" y="3048.00" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$317">
              <segment>
                <wire x1="664.21" y1="3045.46" x2="661.67" y2="3045.46" width="0.3" layer="91" />
                <label x="661.67" y="3045.46" size="1.27" layer="95" />
                <pinref part="Q2$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="577.85" y1="3030.22" x2="575.31" y2="3030.22" width="0.3" layer="91" />
                <label x="575.31" y="3030.22" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="636.27" y1="3048.00" x2="638.81" y2="3048.00" width="0.3" layer="91" />
                <label x="638.81" y="3048.00" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="B1" />
              </segment>
            </net>
            <net name="N$318">
              <segment>
                <wire x1="570.23" y1="3045.46" x2="572.77" y2="3045.46" width="0.3" layer="91" />
                <label x="572.77" y="3045.46" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="621.03" y1="3055.62" x2="618.49" y2="3055.62" width="0.3" layer="91" />
                <label x="618.49" y="3055.62" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="593.09" y1="3037.84" x2="595.63" y2="3037.84" width="0.3" layer="91" />
                <label x="595.63" y="3037.84" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="554.99" y1="3037.84" x2="552.45" y2="3037.84" width="0.3" layer="91" />
                <label x="552.45" y="3037.84" size="1.27" layer="95" />
                <pinref part="Q3$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$319">
              <segment>
                <wire x1="679.45" y1="3075.94" x2="681.99" y2="3075.94" width="0.3" layer="91" />
                <label x="681.99" y="3075.94" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="613.41" y1="3040.38" x2="615.95" y2="3040.38" width="0.3" layer="91" />
                <label x="615.95" y="3040.38" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="641.35" y1="3075.94" x2="638.81" y2="3075.94" width="0.3" layer="91" />
                <label x="638.81" y="3075.94" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="656.59" y1="3083.56" x2="659.13" y2="3083.56" width="0.3" layer="91" />
                <label x="659.13" y="3083.56" size="1.27" layer="95" />
                <pinref part="Q4$2" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$320">
              <segment>
                <wire x1="613.41" y1="3055.62" x2="615.95" y2="3055.62" width="0.3" layer="91" />
                <label x="615.95" y="3055.62" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="717.55" y1="3116.58" x2="720.09" y2="3116.58" width="0.3" layer="91" />
                <label x="720.09" y="3116.58" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="505.46" y1="2997.20" x2="502.92" y2="2997.20" width="0.3" layer="91" />
                <label x="502.92" y="2997.20" size="1.27" layer="95" />
                <pinref part="R122" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="513.08" y1="2969.26" x2="515.62" y2="2969.26" width="0.3" layer="91" />
                <label x="515.62" y="2969.26" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="474.98" y1="2954.02" x2="477.52" y2="2954.02" width="0.3" layer="91" />
                <label x="477.52" y="2954.02" size="1.27" layer="95" />
                <pinref part="R116" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$321">
              <segment>
                <wire x1="621.03" y1="3040.38" x2="618.49" y2="3040.38" width="0.3" layer="91" />
                <label x="618.49" y="3040.38" size="1.27" layer="95" />
                <pinref part="Q5$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="707.39" y1="3078.48" x2="704.85" y2="3078.48" width="0.3" layer="91" />
                <label x="704.85" y="3078.48" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="702.31" y1="3108.96" x2="699.77" y2="3108.96" width="0.3" layer="91" />
                <label x="699.77" y="3108.96" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$322">
              <segment>
                <wire x1="722.63" y1="3086.10" x2="725.17" y2="3086.10" width="0.3" layer="91" />
                <label x="725.17" y="3086.10" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="717.55" y1="3101.34" x2="720.09" y2="3101.34" width="0.3" layer="91" />
                <label x="720.09" y="3101.34" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="E2" />
              </segment>
              <segment>
                <wire x1="684.53" y1="3086.10" x2="681.99" y2="3086.10" width="0.3" layer="91" />
                <label x="681.99" y="3086.10" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="699.77" y1="3093.72" x2="702.31" y2="3093.72" width="0.3" layer="91" />
                <label x="702.31" y="3093.72" size="1.27" layer="95" />
                <pinref part="Q6$2" gate="G$1" pin="C2" />
              </segment>
            </net>
            <net name="N$323">
              <segment>
                <wire x1="725.17" y1="3116.58" x2="722.63" y2="3116.58" width="0.3" layer="91" />
                <label x="722.63" y="3116.58" size="1.27" layer="95" />
                <pinref part="Q7$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="725.17" y1="3124.20" x2="722.63" y2="3124.20" width="0.3" layer="91" />
                <label x="722.63" y="3124.20" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="C1" />
              </segment>
              <segment>
                <wire x1="745.49" y1="3131.82" x2="742.95" y2="3131.82" width="0.3" layer="91" />
                <label x="742.95" y="3131.82" size="1.27" layer="95" />
                <pinref part="Q8$2" gate="G$1" pin="B2" />
              </segment>
            </net>
            <net name="N$324">
              <segment>
                <wire x1="768.35" y1="3139.44" x2="765.81" y2="3139.44" width="0.3" layer="91" />
                <label x="765.81" y="3139.44" size="1.27" layer="95" />
                <pinref part="Q8$2" gate="G$1" pin="E1" />
              </segment>
              <segment>
                <wire x1="756.92" y1="3147.06" x2="759.46" y2="3147.06" width="0.3" layer="91" />
                <label x="759.46" y="3147.06" size="1.27" layer="95" />
                <pinref part="R2$2" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$325">
              <segment>
                <wire x1="717.55" y1="3139.44" x2="720.09" y2="3139.44" width="0.3" layer="91" />
                <label x="720.09" y="3139.44" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="C2" />
              </segment>
              <segment>
                <wire x1="702.31" y1="3131.82" x2="699.77" y2="3131.82" width="0.3" layer="91" />
                <label x="699.77" y="3131.82" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="B2" />
              </segment>
              <segment>
                <wire x1="740.41" y1="3131.82" x2="742.95" y2="3131.82" width="0.3" layer="91" />
                <label x="742.95" y="3131.82" size="1.27" layer="95" />
                <pinref part="Q9$2" gate="G$1" pin="B1" />
              </segment>
              <segment>
                <wire x1="695.96" y1="3101.34" x2="698.50" y2="3101.34" width="0.3" layer="91" />
                <label x="698.50" y="3101.34" size="1.27" layer="95" />
                <pinref part="R1$2" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$326">
              <segment>
                <wire x1="822.96" y1="3035.30" x2="822.96" y2="3032.76" width="0.3" layer="91" />
                <label x="822.96" y="3032.76" size="1.27" layer="95" />
                <pinref part="12" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="513.08" y1="2974.34" x2="515.62" y2="2974.34" width="0.3" layer="91" />
                <label x="515.62" y="2974.34" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="815.34" y1="3030.22" x2="817.88" y2="3030.22" width="0.3" layer="91" />
                <label x="817.88" y="3030.22" size="1.27" layer="95" />
                <pinref part="R123" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="797.56" y1="3030.22" x2="800.10" y2="3030.22" width="0.3" layer="91" />
                <label x="800.10" y="3030.22" size="1.27" layer="95" />
                <pinref part="R127" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="822.96" y1="3045.46" x2="820.42" y2="3045.46" width="0.3" layer="91" />
                <label x="820.42" y="3045.46" size="1.27" layer="95" />
                <pinref part="R128" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$327">
              <segment>
                <wire x1="541.02" y1="3002.28" x2="541.02" y2="2999.74" width="0.3" layer="91" />
                <label x="541.02" y="2999.74" size="1.27" layer="95" />
                <pinref part="C44" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="533.40" y1="3004.82" x2="535.94" y2="3004.82" width="0.3" layer="91" />
                <label x="535.94" y="3004.82" size="1.27" layer="95" />
                <pinref part="R131" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$328">
              <segment>
                <wire x1="881.38" y1="3093.72" x2="881.38" y2="3096.26" width="0.3" layer="91" />
                <label x="881.38" y="3096.26" size="1.27" layer="95" />
                <pinref part="C45" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="891.54" y1="3081.02" x2="894.08" y2="3081.02" width="0.3" layer="91" />
                <label x="894.08" y="3081.02" size="1.27" layer="95" />
                <pinref part="R132" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="871.22" y1="3081.02" x2="873.76" y2="3081.02" width="0.3" layer="91" />
                <label x="873.76" y="3081.02" size="1.27" layer="95" />
                <pinref part="Q18" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="840.74" y1="3042.92" x2="840.74" y2="3040.38" width="0.3" layer="91" />
                <label x="840.74" y="3040.38" size="1.27" layer="95" />
                <pinref part="C47" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$329">
              <segment>
                <wire x1="858.52" y1="3060.70" x2="858.52" y2="3058.16" width="0.3" layer="91" />
                <label x="858.52" y="3058.16" size="1.27" layer="95" />
                <pinref part="C46" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="850.90" y1="3063.24" x2="853.44" y2="3063.24" width="0.3" layer="91" />
                <label x="853.44" y="3063.24" size="1.27" layer="95" />
                <pinref part="R133" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="855.98" y1="3088.64" x2="853.44" y2="3088.64" width="0.3" layer="91" />
                <label x="853.44" y="3088.64" size="1.27" layer="95" />
                <pinref part="Q18" gate="G$1" pin="B" />
              </segment>
            </net>
            <net name="N$330">
              <segment>
                <wire x1="840.74" y1="3050.54" x2="840.74" y2="3053.08" width="0.3" layer="91" />
                <label x="840.74" y="3053.08" size="1.27" layer="95" />
                <pinref part="C47" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="833.12" y1="3045.46" x2="835.66" y2="3045.46" width="0.3" layer="91" />
                <label x="835.66" y="3045.46" size="1.27" layer="95" />
                <pinref part="R128" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="840.74" y1="3063.24" x2="838.20" y2="3063.24" width="0.3" layer="91" />
                <label x="838.20" y="3063.24" size="1.27" layer="95" />
                <pinref part="R133" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$331">
              <segment>
                <wire x1="426.72" y1="2921.00" x2="426.72" y2="2918.46" width="0.3" layer="91" />
                <label x="426.72" y="2918.46" size="1.27" layer="95" />
                <pinref part="C48" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="2931.16" x2="441.96" y2="2931.16" width="0.3" layer="91" />
                <label x="441.96" y="2931.16" size="1.27" layer="95" />
                <pinref part="D21" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="401.32" y1="2910.84" x2="403.86" y2="2910.84" width="0.3" layer="91" />
                <label x="403.86" y="2910.84" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="I2+" />
              </segment>
              <segment>
                <wire x1="436.88" y1="2915.92" x2="439.42" y2="2915.92" width="0.3" layer="91" />
                <label x="439.42" y="2915.92" size="1.27" layer="95" />
                <pinref part="R134" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$332">
              <segment>
                <wire x1="353.06" y1="2870.20" x2="353.06" y2="2867.66" width="0.3" layer="91" />
                <label x="353.06" y="2867.66" size="1.27" layer="95" />
                <pinref part="C49" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="408.94" y1="2915.92" x2="406.40" y2="2915.92" width="0.3" layer="91" />
                <label x="406.40" y="2915.92" size="1.27" layer="95" />
                <pinref part="R135" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$333">
              <segment>
                <wire x1="299.72" y1="2837.18" x2="302.26" y2="2837.18" width="0.3" layer="91" />
                <label x="302.26" y="2837.18" size="1.27" layer="95" />
                <pinref part="D19" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="309.88" y1="2829.56" x2="312.42" y2="2829.56" width="0.3" layer="91" />
                <label x="312.42" y="2829.56" size="1.27" layer="95" />
                <pinref part="R117" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$334">
              <segment>
                <wire x1="294.64" y1="2837.18" x2="292.10" y2="2837.18" width="0.3" layer="91" />
                <label x="292.10" y="2837.18" size="1.27" layer="95" />
                <pinref part="D19" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="299.72" y1="2844.80" x2="302.26" y2="2844.80" width="0.3" layer="91" />
                <label x="302.26" y="2844.80" size="1.27" layer="95" />
                <pinref part="D20" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="325.12" y1="2862.58" x2="322.58" y2="2862.58" width="0.3" layer="91" />
                <label x="322.58" y="2862.58" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$335">
              <segment>
                <wire x1="294.64" y1="2844.80" x2="292.10" y2="2844.80" width="0.3" layer="91" />
                <label x="292.10" y="2844.80" size="1.27" layer="95" />
                <pinref part="D20" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="335.28" y1="2832.10" x2="332.74" y2="2832.10" width="0.3" layer="91" />
                <label x="332.74" y="2832.10" size="1.27" layer="95" />
                <pinref part="R114" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="317.50" y1="2832.10" x2="314.96" y2="2832.10" width="0.3" layer="91" />
                <label x="314.96" y="2832.10" size="1.27" layer="95" />
                <pinref part="R113" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="292.10" y1="2824.48" x2="294.64" y2="2824.48" width="0.3" layer="91" />
                <label x="294.64" y="2824.48" size="1.27" layer="95" />
                <pinref part="R119" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$336">
              <segment>
                <wire x1="434.34" y1="2931.16" x2="431.80" y2="2931.16" width="0.3" layer="91" />
                <label x="431.80" y="2931.16" size="1.27" layer="95" />
                <pinref part="D21" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="414.02" y1="2908.30" x2="416.56" y2="2908.30" width="0.3" layer="91" />
                <label x="416.56" y="2908.30" size="1.27" layer="95" />
                <pinref part="D22" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="363.22" y1="2890.52" x2="365.76" y2="2890.52" width="0.3" layer="91" />
                <label x="365.76" y="2890.52" size="1.27" layer="95" />
                <pinref part="R136" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="419.10" y1="2915.92" x2="421.64" y2="2915.92" width="0.3" layer="91" />
                <label x="421.64" y="2915.92" size="1.27" layer="95" />
                <pinref part="R135" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="370.84" y1="2921.00" x2="368.30" y2="2921.00" width="0.3" layer="91" />
                <label x="368.30" y="2921.00" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="426.72" y1="2915.92" x2="424.18" y2="2915.92" width="0.3" layer="91" />
                <label x="424.18" y="2915.92" size="1.27" layer="95" />
                <pinref part="R134" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$337">
              <segment>
                <wire x1="408.94" y1="2908.30" x2="406.40" y2="2908.30" width="0.3" layer="91" />
                <label x="406.40" y="2908.30" size="1.27" layer="95" />
                <pinref part="D22" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="370.84" y1="2926.08" x2="368.30" y2="2926.08" width="0.3" layer="91" />
                <label x="368.30" y="2926.08" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$338">
              <segment>
                <wire x1="495.30" y1="2992.12" x2="497.84" y2="2992.12" width="0.3" layer="91" />
                <label x="497.84" y="2992.12" size="1.27" layer="95" />
                <pinref part="Q19" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="474.98" y1="2961.64" x2="477.52" y2="2961.64" width="0.3" layer="91" />
                <label x="477.52" y="2961.64" size="1.27" layer="95" />
                <pinref part="R129" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$339">
              <segment>
                <wire x1="480.06" y1="2999.74" x2="477.52" y2="2999.74" width="0.3" layer="91" />
                <label x="477.52" y="2999.74" size="1.27" layer="95" />
                <pinref part="Q19" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="482.60" y1="2979.42" x2="480.06" y2="2979.42" width="0.3" layer="91" />
                <label x="480.06" y="2979.42" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="OUT1" />
              </segment>
            </net>
            <net name="N$340">
              <segment>
                <wire x1="495.30" y1="3007.36" x2="497.84" y2="3007.36" width="0.3" layer="91" />
                <label x="497.84" y="3007.36" size="1.27" layer="95" />
                <pinref part="Q19" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="523.24" y1="2997.20" x2="520.70" y2="2997.20" width="0.3" layer="91" />
                <label x="520.70" y="2997.20" size="1.27" layer="95" />
                <pinref part="R124" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$341">
              <segment>
                <wire x1="327.66" y1="2824.48" x2="330.20" y2="2824.48" width="0.3" layer="91" />
                <label x="330.20" y="2824.48" size="1.27" layer="95" />
                <pinref part="R112" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="335.28" y1="2872.74" x2="332.74" y2="2872.74" width="0.3" layer="91" />
                <label x="332.74" y="2872.74" size="1.27" layer="95" />
                <pinref part="R120" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="355.60" y1="2852.42" x2="358.14" y2="2852.42" width="0.3" layer="91" />
                <label x="358.14" y="2852.42" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="I2-" />
              </segment>
              <segment>
                <wire x1="345.44" y1="2832.10" x2="347.98" y2="2832.10" width="0.3" layer="91" />
                <label x="347.98" y="2832.10" size="1.27" layer="95" />
                <pinref part="R114" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="327.66" y1="2832.10" x2="330.20" y2="2832.10" width="0.3" layer="91" />
                <label x="330.20" y="2832.10" size="1.27" layer="95" />
                <pinref part="R113" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$342">
              <segment>
                <wire x1="317.50" y1="2849.88" x2="320.04" y2="2849.88" width="0.3" layer="91" />
                <label x="320.04" y="2849.88" size="1.27" layer="95" />
                <pinref part="R115" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="355.60" y1="2847.34" x2="358.14" y2="2847.34" width="0.3" layer="91" />
                <label x="358.14" y="2847.34" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="I2+" />
              </segment>
            </net>
            <net name="N$343">
              <segment>
                <wire x1="464.82" y1="2954.02" x2="462.28" y2="2954.02" width="0.3" layer="91" />
                <label x="462.28" y="2954.02" size="1.27" layer="95" />
                <pinref part="R116" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="469.90" y1="2938.78" x2="472.44" y2="2938.78" width="0.3" layer="91" />
                <label x="472.44" y="2938.78" size="1.27" layer="95" />
                <pinref part="VR3" gate="G$1" pin="P$2" />
              </segment>
            </net>
            <net name="N$344">
              <segment>
                <wire x1="299.72" y1="2829.56" x2="297.18" y2="2829.56" width="0.3" layer="91" />
                <label x="297.18" y="2829.56" size="1.27" layer="95" />
                <pinref part="R117" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="325.12" y1="2857.50" x2="322.58" y2="2857.50" width="0.3" layer="91" />
                <label x="322.58" y="2857.50" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="281.94" y1="2824.48" x2="279.40" y2="2824.48" width="0.3" layer="91" />
                <label x="279.40" y="2824.48" size="1.27" layer="95" />
                <pinref part="R119" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="299.72" y1="2816.86" x2="297.18" y2="2816.86" width="0.3" layer="91" />
                <label x="297.18" y="2816.86" size="1.27" layer="95" />
                <pinref part="R118" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$345">
              <segment>
                <wire x1="345.44" y1="2872.74" x2="347.98" y2="2872.74" width="0.3" layer="91" />
                <label x="347.98" y="2872.74" size="1.27" layer="95" />
                <pinref part="R120" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="370.84" y1="2915.92" x2="368.30" y2="2915.92" width="0.3" layer="91" />
                <label x="368.30" y="2915.92" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="I1+" />
              </segment>
              <segment>
                <wire x1="355.60" y1="2857.50" x2="358.14" y2="2857.50" width="0.3" layer="91" />
                <label x="358.14" y="2857.50" size="1.27" layer="95" />
                <pinref part="U14" gate="G$1" pin="OUT2" />
              </segment>
            </net>
            <net name="N$346">
              <segment>
                <wire x1="515.62" y1="2997.20" x2="518.16" y2="2997.20" width="0.3" layer="91" />
                <label x="518.16" y="2997.20" size="1.27" layer="95" />
                <pinref part="R122" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="523.24" y1="3004.82" x2="520.70" y2="3004.82" width="0.3" layer="91" />
                <label x="520.70" y="3004.82" size="1.27" layer="95" />
                <pinref part="R131" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="787.40" y1="3030.22" x2="784.86" y2="3030.22" width="0.3" layer="91" />
                <label x="784.86" y="3030.22" size="1.27" layer="95" />
                <pinref part="R127" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$347">
              <segment>
                <wire x1="464.82" y1="2961.64" x2="462.28" y2="2961.64" width="0.3" layer="91" />
                <label x="462.28" y="2961.64" size="1.27" layer="95" />
                <pinref part="R129" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="482.60" y1="2974.34" x2="480.06" y2="2974.34" width="0.3" layer="91" />
                <label x="480.06" y="2974.34" size="1.27" layer="95" />
                <pinref part="U15" gate="G$1" pin="I1-" />
              </segment>
              <segment>
                <wire x1="457.20" y1="2954.02" x2="459.74" y2="2954.02" width="0.3" layer="91" />
                <label x="459.74" y="2954.02" size="1.27" layer="95" />
                <pinref part="R130" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$348">
              <segment>
                <wire x1="447.04" y1="2954.02" x2="444.50" y2="2954.02" width="0.3" layer="91" />
                <label x="444.50" y="2954.02" size="1.27" layer="95" />
                <pinref part="R130" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="401.32" y1="2921.00" x2="403.86" y2="2921.00" width="0.3" layer="91" />
                <label x="403.86" y="2921.00" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="OUT2" />
              </segment>
              <segment>
                <wire x1="401.32" y1="2915.92" x2="403.86" y2="2915.92" width="0.3" layer="91" />
                <label x="403.86" y="2915.92" size="1.27" layer="95" />
                <pinref part="U16" gate="G$1" pin="I2-" />
              </segment>
            </net>
          </nets>
        </sheet>
      </sheets>
      <errors />
    </schematic>
  </drawing>
  <compatibility />
</eagle>
