<?xml version="1.0"?>
<eagle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="6.5.0" xmlns="eagle">
  <compatibility />
  <drawing>
    <settings>
      <setting alwaysvectorfont="no" />
      <setting />
    </settings>
    <grid distance="0.01" unitdist="inch" unit="inch" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch" />
    <layers>
      <layer number="1" name="Top" color="4" fill="1" visible="no" active="no" />
      <layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no" />
      <layer number="17" name="Pads" color="2" fill="1" visible="no" active="no" />
      <layer number="18" name="Vias" color="2" fill="1" visible="no" active="no" />
      <layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no" />
      <layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no" />
      <layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="25" name="tNames" color="7" fill="1" visible="no" active="no" />
      <layer number="26" name="bNames" color="7" fill="1" visible="no" active="no" />
      <layer number="27" name="tValues" color="7" fill="1" visible="no" active="no" />
      <layer number="28" name="bValues" color="7" fill="1" visible="no" active="no" />
      <layer number="29" name="tStop" color="7" fill="3" visible="no" active="no" />
      <layer number="30" name="bStop" color="7" fill="6" visible="no" active="no" />
      <layer number="31" name="tCream" color="7" fill="4" visible="no" active="no" />
      <layer number="32" name="bCream" color="7" fill="5" visible="no" active="no" />
      <layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no" />
      <layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no" />
      <layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no" />
      <layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no" />
      <layer number="37" name="tTest" color="7" fill="1" visible="no" active="no" />
      <layer number="38" name="bTest" color="7" fill="1" visible="no" active="no" />
      <layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no" />
      <layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no" />
      <layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no" />
      <layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no" />
      <layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no" />
      <layer number="44" name="Drills" color="7" fill="1" visible="no" active="no" />
      <layer number="45" name="Holes" color="7" fill="1" visible="no" active="no" />
      <layer number="46" name="Milling" color="3" fill="1" visible="no" active="no" />
      <layer number="47" name="Measures" color="7" fill="1" visible="no" active="no" />
      <layer number="48" name="Document" color="7" fill="1" visible="no" active="no" />
      <layer number="49" name="Reference" color="7" fill="1" visible="no" active="no" />
      <layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="91" name="Nets" color="2" fill="1" />
      <layer number="92" name="Busses" color="1" fill="1" />
      <layer number="93" name="Pins" color="2" fill="1" visible="no" />
      <layer number="94" name="Symbols" color="4" fill="1" />
      <layer number="95" name="Names" color="7" fill="1" />
      <layer number="96" name="Values" color="7" fill="1" />
      <layer number="97" name="Info" color="7" fill="1" />
      <layer number="98" name="Guide" color="6" fill="1" />
    </layers>
    <schematic xrefpart="/%S.%C%R" xreflabel="%F%N/%S.%C%R">
      <description />
      <libraries>
        <library name="resistor">
          <description />
          <packages>
            <package name="R_0603">
              <description>&lt;B&gt;
0603
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-0.1" y1="0.3" x2="0.1" y2="0.3" width="0.1524" layer="21" />
              <wire x1="-0.1" y1="-0.3" x2="0.1" y2="-0.3" width="0.1524" layer="21" />
              <smd name="P$1" x="-0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <smd name="P$2" x="0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <text x="-1.3" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
            </package>
            <package name="R_2512">
              <description>&lt;B&gt;
2512
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.1524" layer="21" />
              <wire x1="-2" y1="-1.5" x2="2" y2="-1.5" width="0.1524" layer="21" />
              <smd name="P$1" x="-3" y="0" dx="1.4" dy="3" layer="1" />
              <smd name="P$2" x="3" y="0" dx="1.4" dy="3" layer="1" />
              <text x="-2.3" y="1.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-3.15" y1="1.6" x2="3.15" y2="1.6" width="0.0762" layer="51" />
              <wire x1="3.15" y1="1.6" x2="3.15" y2="-1.6" width="0.0762" layer="51" />
              <wire x1="3.15" y1="-1.6" x2="-3.15" y2="-1.6" width="0.0762" layer="51" />
              <wire x1="-3.15" y1="-1.6" x2="-3.15" y2="1.6" width="0.0762" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="R">
              <description>&lt;B&gt;Resistor&lt;/B&gt;</description>
              <wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94" />
              <wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94" />
              <wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94" />
              <pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" />
              <pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180" />
              <text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
              <text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="R" name="RESISTOR_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="0" y="0" />
              </gates>
              <devices>
                <device package="R_0603">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="R" name="RESISTOR_2512">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="0" y="0" />
              </gates>
              <devices>
                <device package="R_2512">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="General_Will">
          <description />
          <packages>
            <package name="PTA4543">
              <description />
              <pad name="P$1" x="-29.25" y="1.75" drill="1.5" />
              <pad name="P$2" x="-29.25" y="-1.75" drill="1.5" />
              <pad name="P$3" x="29.25" y="1.75" drill="1.5" />
              <hole x="-23.9" y="4.2" drill="2" />
              <hole x="23.9" y="4.2" drill="2" />
              <hole x="-25.1" y="-4.2" drill="2" />
              <hole x="25.1" y="-4.2" drill="2" />
              <wire x1="-30.7" y1="6" x2="-30.7" y2="3.2" width="0.127" layer="21" />
              <wire x1="-30.7" y1="3.2" x2="-30.7" y2="-6" width="0.127" layer="21" />
              <wire x1="-30.7" y1="-6" x2="30.7" y2="-6" width="0.127" layer="21" />
              <wire x1="30.7" y1="-6" x2="30.7" y2="6" width="0.127" layer="21" />
              <wire x1="30.7" y1="6" x2="-27.9" y2="6" width="0.127" layer="21" />
              <circle x="-31.1" y="2.8" radius="0.1542" width="0" layer="21" />
              <wire x1="-27.9" y1="6" x2="-30.7" y2="6" width="0.127" layer="21" />
              <wire x1="-30.7" y1="3.2" x2="-27.9" y2="6" width="0.127" layer="21" />
              <text x="-30.9" y="-6" size="1.27" layer="25" rot="R90">&gt;NAME</text>
            </package>
            <package name="PTV112-4">
              <description />
              <wire x1="-4" y1="12" x2="4" y2="12" width="0.127" layer="21" />
              <wire x1="-7" y1="0" x2="-6.2" y2="0" width="0.127" layer="21" />
              <wire x1="-7" y1="0" x2="-7" y2="8.865" width="0.127" layer="21" />
              <wire x1="6.2" y1="0" x2="7" y2="0" width="0.127" layer="21" />
              <wire x1="7" y1="0" x2="7" y2="8.865" width="0.127" layer="21" />
              <text x="-7.5" y="0.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
              <circle x="-6.2" y="-0.5" radius="0.1542" width="0" layer="21" />
              <pad name="P$1" x="-5" y="0" drill="1.15" diameter="1.6" />
              <pad name="P$2" x="-3" y="0" drill="1.15" diameter="1.6" />
              <pad name="P$3" x="-1" y="0" drill="1.15" diameter="1.6" />
              <pad name="P$4" x="1" y="0" drill="1.15" diameter="1.6" />
              <pad name="P$5" x="3" y="0" drill="1.15" diameter="1.6" />
              <pad name="P$6" x="5" y="0" drill="1.15" diameter="1.6" />
              <wire x1="6.8" y1="12.4" x2="4.8" y2="12.4" width="0.127" layer="20" />
              <wire x1="4.8" y1="12.4" x2="4.8" y2="9.6" width="0.127" layer="20" />
              <wire x1="4.8" y1="9.6" x2="6.8" y2="9.6" width="0.127" layer="20" />
              <wire x1="6.8" y1="9.6" x2="6.8" y2="12.4" width="0.127" layer="20" />
              <wire x1="-4.8" y1="12.4" x2="-6.8" y2="12.4" width="0.127" layer="20" />
              <wire x1="-6.8" y1="12.4" x2="-6.8" y2="9.6" width="0.127" layer="20" />
              <wire x1="-6.8" y1="9.6" x2="-4.8" y2="9.6" width="0.127" layer="20" />
              <wire x1="-4.8" y1="9.6" x2="-4.8" y2="12.4" width="0.127" layer="20" />
            </package>
            <package name="TO-92">
              <description>&lt;b&gt;TO 92&lt;/b&gt;</description>
              <wire x1="-1.0403" y1="2.4215" x2="-2.0946" y2="-1.651" width="0.1524" layer="21" curve="111.098962" />
              <wire x1="2.0945" y1="-1.651" x2="1.0403" y2="2.421396875" width="0.1524" layer="21" curve="111.099507" />
              <wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.1524" layer="21" />
              <wire x1="-2.6549" y1="-0.254" x2="-2.3807" y2="-0.254" width="0.1524" layer="21" />
              <wire x1="-0.1593" y1="-0.254" x2="0.1593" y2="-0.254" width="0.1524" layer="21" />
              <wire x1="2.3807" y1="-0.254" x2="2.6549" y2="-0.254" width="0.1524" layer="21" />
              <pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796" />
              <pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796" />
              <pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796" />
              <text x="-2.159" y="-2.794" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="2SB605">
              <description />
              <pad name="P$1" x="-1.7" y="0" drill="0.75" />
              <pad name="P$2" x="0" y="0" drill="0.75" />
              <pad name="P$3" x="1.7" y="0" drill="0.75" />
              <wire x1="-3.5" y1="2.5" x2="-3.5" y2="-0.6" width="0.127" layer="21" />
              <wire x1="-3.5" y1="-0.6" x2="-3.5" y2="-1.5" width="0.127" layer="21" />
              <wire x1="-3.5" y1="-1.5" x2="-2.8" y2="-1.5" width="0.127" layer="21" />
              <wire x1="-2.8" y1="-1.5" x2="2.8" y2="-1.5" width="0.127" layer="21" />
              <wire x1="2.8" y1="-1.5" x2="3.5" y2="-1.5" width="0.127" layer="21" />
              <wire x1="3.5" y1="-1.5" x2="3.5" y2="-0.6" width="0.127" layer="21" />
              <wire x1="3.5" y1="-0.6" x2="3.5" y2="2.5" width="0.127" layer="21" />
              <circle x="-3.8" y="0" radius="0.1542" width="0" layer="21" />
              <wire x1="-2.8" y1="-1.5" x2="-3.5" y2="-0.6" width="0.127" layer="21" />
              <wire x1="3.5" y1="-0.6" x2="2.8" y2="-1.5" width="0.127" layer="21" />
              <text x="-3.3" y="2.7" size="1.27" layer="25">&gt;NAME</text>
              <wire x1="-3.5" y1="2.5" x2="2.7" y2="2.5" width="0.127" layer="21" />
              <wire x1="3.5" y1="2.5" x2="2.8" y2="2.5" width="0.127" layer="21" />
              <wire x1="2.8" y1="2.5" x2="2.7" y2="2.5" width="0.127" layer="21" />
            </package>
            <package name="3X12TERMINAL">
              <description />
              <pad name="1-1" x="0" y="0" drill="1.8" shape="square" />
              <pad name="2-1" x="-1.9" y="10.16" drill="1.8" />
              <pad name="3-1" x="0" y="19.05" drill="1.8" />
              <wire x1="-1.9" y1="-3.6" x2="47.62" y2="-3.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="-3.6" x2="47.62" y2="3.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="3.6" x2="45.72" y2="3.6" width="0.1524" layer="21" />
              <wire x1="45.72" y1="3.6" x2="45.72" y2="14.4" width="0.1524" layer="21" />
              <wire x1="45.72" y1="14.4" x2="47.62" y2="14.4" width="0.1524" layer="21" />
              <wire x1="47.62" y1="14.4" x2="47.62" y2="21.6" width="0.1524" layer="21" />
              <wire x1="47.62" y1="21.6" x2="-1.9" y2="21.6" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="21.6" x2="-1.9" y2="14.4" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="14.4" x2="-3.8" y2="14.4" width="0.1524" layer="21" />
              <wire x1="-3.8" y1="14.4" x2="-3.8" y2="3.6" width="0.1524" layer="21" />
              <wire x1="-3.8" y1="3.6" x2="-1.9" y2="3.6" width="0.1524" layer="21" />
              <wire x1="-1.9" y1="3.6" x2="-1.9" y2="-3.6" width="0.1524" layer="21" />
              <text x="-5.08" y="4.445" size="1.016" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
              <pad name="1-2" x="3.81" y="0" drill="1.8" />
              <pad name="1-3" x="7.62" y="0" drill="1.8" />
              <pad name="1-4" x="11.43" y="0" drill="1.8" />
              <pad name="1-5" x="15.24" y="0" drill="1.8" />
              <pad name="1-6" x="19.05" y="0" drill="1.8" />
              <pad name="1-7" x="22.86" y="0" drill="1.8" />
              <pad name="1-8" x="26.67" y="0" drill="1.8" />
              <pad name="1-9" x="30.48" y="0" drill="1.8" />
              <pad name="1-10" x="34.29" y="0" drill="1.8" />
              <pad name="1-11" x="38.1" y="0" drill="1.8" />
              <pad name="1-12" x="41.91" y="0" drill="1.8" />
              <pad name="2-2" x="1.91" y="10.16" drill="1.8" />
              <pad name="2-3" x="5.72" y="10.16" drill="1.8" />
              <pad name="2-4" x="9.53" y="10.16" drill="1.8" />
              <pad name="2-5" x="13.34" y="10.16" drill="1.8" />
              <pad name="2-6" x="17.15" y="10.16" drill="1.8" />
              <pad name="2-7" x="20.96" y="10.16" drill="1.8" />
              <pad name="2-8" x="24.77" y="10.16" drill="1.8" />
              <pad name="2-9" x="28.58" y="10.16" drill="1.8" />
              <pad name="2-10" x="32.39" y="10.16" drill="1.8" />
              <pad name="2-11" x="36.2" y="10.16" drill="1.8" />
              <pad name="2-12" x="40.01" y="10.16" drill="1.8" />
              <pad name="3-2" x="3.81" y="19.05" drill="1.8" />
              <pad name="3-3" x="7.62" y="19.05" drill="1.8" />
              <pad name="3-4" x="11.43" y="19.05" drill="1.8" />
              <pad name="3-5" x="15.24" y="19.05" drill="1.8" />
              <pad name="3-6" x="19.05" y="19.05" drill="1.8" />
              <pad name="3-7" x="22.86" y="19.05" drill="1.8" />
              <pad name="3-8" x="26.67" y="19.05" drill="1.8" />
              <pad name="3-9" x="30.48" y="19.05" drill="1.8" />
              <pad name="3-10" x="34.29" y="19.05" drill="1.8" />
              <pad name="3-11" x="38.1" y="19.05" drill="1.8" />
              <pad name="3-12" x="41.91" y="19.05" drill="1.8" />
            </package>
            <package name="PTV111-3">
              <description />
              <pad name="P$1" x="-3.75" y="0" drill="1.2" />
              <pad name="P$2" x="-1.25" y="0" drill="1.2" />
              <pad name="P$3" x="1.25" y="0" drill="1.2" />
              <pad name="P$4" x="3.75" y="0" drill="1.2" />
              <wire x1="-4" y1="12" x2="4" y2="12" width="0.127" layer="21" />
              <wire x1="-6" y1="0" x2="-5" y2="0" width="0.127" layer="21" />
              <wire x1="-6" y1="0" x2="-6" y2="8.865" width="0.127" layer="21" />
              <wire x1="5" y1="0" x2="6" y2="0" width="0.127" layer="21" />
              <wire x1="6" y1="0" x2="6" y2="8.865" width="0.127" layer="21" />
              <text x="-6.5" y="0.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
              <circle x="-5" y="-0.5" radius="0.1542" width="0" layer="21" />
              <wire x1="6.8" y1="12.1" x2="4.8" y2="12.1" width="0.127" layer="20" />
              <wire x1="4.8" y1="12.1" x2="4.8" y2="9.9" width="0.127" layer="20" />
              <wire x1="4.8" y1="9.9" x2="6.8" y2="9.9" width="0.127" layer="20" />
              <wire x1="6.8" y1="9.9" x2="6.8" y2="12.1" width="0.127" layer="20" />
              <wire x1="-4.8" y1="12.1" x2="-6.8" y2="12.1" width="0.127" layer="20" />
              <wire x1="-6.8" y1="12.1" x2="-6.8" y2="9.9" width="0.127" layer="20" />
              <wire x1="-6.8" y1="9.9" x2="-4.8" y2="9.9" width="0.127" layer="20" />
              <wire x1="-4.8" y1="9.9" x2="-4.8" y2="12.1" width="0.127" layer="20" />
            </package>
          </packages>
          <symbols>
            <symbol name="POT">
              <description />
              <wire x1="-0.762" y1="2.54" x2="-0.762" y2="-2.54" width="0.254" layer="94" />
              <wire x1="0.762" y1="-2.54" x2="0.762" y2="2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94" />
              <wire x1="0.762" y1="2.54" x2="-0.762" y2="2.54" width="0.254" layer="94" />
              <wire x1="-0.762" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-3.683" x2="-2.54" y2="-1.143" width="0.1524" layer="94" />
              <wire x1="-2.54" y1="-1.143" x2="-3.175" y2="-2.413" width="0.1524" layer="94" />
              <wire x1="-3.175" y1="-2.413" x2="-1.905" y2="-2.413" width="0.1524" layer="94" />
              <wire x1="-1.905" y1="-2.413" x2="-2.54" y2="-1.143" width="0.1524" layer="94" />
              <wire x1="1.651" y1="0" x2="-2.286" y2="2.54" width="0.1524" layer="94" />
              <wire x1="-2.667" y1="1.905" x2="-2.286" y2="2.54" width="0.1524" layer="94" />
              <wire x1="-2.286" y1="2.54" x2="-1.905" y2="3.175" width="0.1524" layer="94" />
              <text x="-5.715" y="-3.81" size="1.778" layer="95" ratio="10" rot="R90">&gt;NAME</text>
              <text x="-3.81" y="-3.81" size="1.778" layer="96" ratio="10" rot="R90">&gt;VALUE</text>
              <text x="-1.905" y="-4.953" size="1.778" layer="94" rot="R90">1</text>
              <text x="-1.905" y="-0.508" size="1.778" layer="94" rot="R90">3</text>
              <pin name="1" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90" />
              <pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270" />
              <pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180" />
            </symbol>
            <symbol name="DUALPOT">
              <description />
              <wire x1="-5.842" y1="2.54" x2="-5.842" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-4.318" y1="-2.54" x2="-4.318" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="0" x2="-3.429" y2="0" width="0.1524" layer="94" />
              <wire x1="-4.318" y1="2.54" x2="-5.842" y2="2.54" width="0.254" layer="94" />
              <wire x1="-5.842" y1="-2.54" x2="-4.318" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-7.62" y1="-3.683" x2="-7.62" y2="-1.143" width="0.1524" layer="94" />
              <wire x1="-7.62" y1="-1.143" x2="-8.255" y2="-2.413" width="0.1524" layer="94" />
              <wire x1="-8.255" y1="-2.413" x2="-6.985" y2="-2.413" width="0.1524" layer="94" />
              <wire x1="-6.985" y1="-2.413" x2="-7.62" y2="-1.143" width="0.1524" layer="94" />
              <wire x1="-3.429" y1="0" x2="-7.366" y2="2.54" width="0.1524" layer="94" />
              <wire x1="-7.747" y1="1.905" x2="-7.366" y2="2.54" width="0.1524" layer="94" />
              <wire x1="-7.366" y1="2.54" x2="-6.985" y2="3.175" width="0.1524" layer="94" />
              <text x="-10.795" y="-3.81" size="1.778" layer="95" ratio="10" rot="R90">&gt;NAME</text>
              <text x="-8.89" y="-3.81" size="1.778" layer="96" ratio="10" rot="R90">&gt;VALUE</text>
              <text x="-6.985" y="-4.953" size="1.778" layer="94" rot="R90">1</text>
              <text x="-6.985" y="-0.508" size="1.778" layer="94" rot="R90">3</text>
              <pin name="X1" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90" />
              <pin name="X3" x="-5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270" />
              <pin name="X2" x="0" y="0" visible="pad" length="short" direction="pas" rot="R180" />
              <wire x1="6.858" y1="2.54" x2="6.858" y2="-2.54" width="0.254" layer="94" />
              <wire x1="8.382" y1="-2.54" x2="8.382" y2="2.54" width="0.254" layer="94" />
              <wire x1="10.16" y1="0" x2="9.271" y2="0" width="0.1524" layer="94" />
              <wire x1="8.382" y1="2.54" x2="6.858" y2="2.54" width="0.254" layer="94" />
              <wire x1="6.858" y1="-2.54" x2="8.382" y2="-2.54" width="0.254" layer="94" />
              <wire x1="5.08" y1="-3.683" x2="5.08" y2="-1.143" width="0.1524" layer="94" />
              <wire x1="5.08" y1="-1.143" x2="4.445" y2="-2.413" width="0.1524" layer="94" />
              <wire x1="4.445" y1="-2.413" x2="5.715" y2="-2.413" width="0.1524" layer="94" />
              <wire x1="5.715" y1="-2.413" x2="5.08" y2="-1.143" width="0.1524" layer="94" />
              <wire x1="9.271" y1="0" x2="5.334" y2="2.54" width="0.1524" layer="94" />
              <wire x1="4.953" y1="1.905" x2="5.334" y2="2.54" width="0.1524" layer="94" />
              <wire x1="5.334" y1="2.54" x2="5.715" y2="3.175" width="0.1524" layer="94" />
              <text x="5.715" y="-4.953" size="1.778" layer="94" rot="R90">1</text>
              <text x="5.715" y="-0.508" size="1.778" layer="94" rot="R90">3</text>
              <pin name="Y1" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R90" />
              <pin name="Y3" x="7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R270" />
              <pin name="Y2" x="12.7" y="0" visible="pad" length="short" direction="pas" rot="R180" />
            </symbol>
            <symbol name="NPN">
              <description />
              <pin name="B" x="-10.16" y="0" length="middle" direction="pas" />
              <wire x1="-5.08" y1="5.08" x2="-5.08" y2="2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="2.54" x2="0" y2="7.62" width="0.254" layer="94" />
              <wire x1="-5.08" y1="-2.54" x2="0" y2="-7.62" width="0.254" layer="94" />
              <wire x1="0" y1="-7.62" x2="-1.016" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-1.016" y1="-5.08" x2="-2.54" y2="-6.604" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-6.604" x2="0" y2="-7.62" width="0.254" layer="94" />
              <pin name="C" x="5.08" y="7.62" length="middle" direction="pas" rot="R180" />
              <pin name="E" x="5.08" y="-7.62" length="middle" direction="pas" rot="R180" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="PNP">
              <description />
              <pin name="B" x="-10.16" y="2.54" length="middle" direction="pas" />
              <wire x1="-5.08" y1="7.62" x2="-5.08" y2="5.08" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-5.08" y1="5.08" x2="0" y2="10.16" width="0.254" layer="94" />
              <wire x1="-5.08" y1="0" x2="0" y2="-5.08" width="0.254" layer="94" />
              <pin name="C" x="5.08" y="10.16" length="middle" direction="pas" rot="R180" />
              <pin name="E" x="5.08" y="-5.08" length="middle" direction="pas" rot="R180" />
              <text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="15.24" size="1.778" layer="96">&gt;VALUE</text>
              <wire x1="-5.08" y1="0" x2="-4.318" y2="-2.032" width="0.254" layer="94" />
              <wire x1="-4.318" y1="-2.032" x2="-3.048" y2="-0.762" width="0.254" layer="94" />
              <wire x1="-3.048" y1="-0.762" x2="-5.08" y2="0" width="0.254" layer="94" />
            </symbol>
            <symbol name="36TERMINALBLOCK">
              <description />
              <pin name="1-1" x="-12.7" y="10.16" length="middle" direction="pas" />
              <pin name="2-1" x="0" y="10.16" length="middle" direction="pas" />
              <pin name="3-1" x="12.7" y="10.16" length="middle" direction="pas" />
              <pin name="1-2" x="-12.7" y="7.62" length="middle" direction="pas" />
              <pin name="2-2" x="0" y="7.62" length="middle" direction="pas" />
              <pin name="3-2" x="12.7" y="7.62" length="middle" direction="pas" />
              <pin name="1-3" x="-12.7" y="5.08" length="middle" direction="pas" />
              <pin name="2-3" x="0" y="5.08" length="middle" direction="pas" />
              <pin name="3-3" x="12.7" y="5.08" length="middle" direction="pas" />
              <pin name="1-4" x="-12.7" y="2.54" length="middle" direction="pas" />
              <pin name="2-4" x="0" y="2.54" length="middle" direction="pas" />
              <pin name="3-4" x="12.7" y="2.54" length="middle" direction="pas" />
              <pin name="1-5" x="-12.7" y="0" length="middle" direction="pas" />
              <pin name="2-5" x="0" y="0" length="middle" direction="pas" />
              <pin name="3-5" x="12.7" y="0" length="middle" direction="pas" />
              <pin name="1-6" x="-12.7" y="-2.54" length="middle" direction="pas" />
              <pin name="2-6" x="0" y="-2.54" length="middle" direction="pas" />
              <pin name="3-6" x="12.7" y="-2.54" length="middle" direction="pas" />
              <pin name="1-7" x="-12.7" y="-5.08" length="middle" direction="pas" />
              <pin name="2-7" x="0" y="-5.08" length="middle" direction="pas" />
              <pin name="3-7" x="12.7" y="-5.08" length="middle" direction="pas" />
              <pin name="1-8" x="-12.7" y="-7.62" length="middle" direction="pas" />
              <pin name="2-8" x="0" y="-7.62" length="middle" direction="pas" />
              <pin name="3-8" x="12.7" y="-7.62" length="middle" direction="pas" />
              <pin name="1-9" x="-12.7" y="-10.16" length="middle" direction="pas" />
              <pin name="2-9" x="0" y="-10.16" length="middle" direction="pas" />
              <pin name="3-9" x="12.7" y="-10.16" length="middle" direction="pas" />
              <pin name="1-10" x="-12.7" y="-12.7" length="middle" direction="pas" />
              <pin name="2-10" x="0" y="-12.7" length="middle" direction="pas" />
              <pin name="3-10" x="12.7" y="-12.7" length="middle" direction="pas" />
              <pin name="1-11" x="-12.7" y="-15.24" length="middle" direction="pas" />
              <pin name="2-11" x="0" y="-15.24" length="middle" direction="pas" />
              <pin name="3-11" x="12.7" y="-15.24" length="middle" direction="pas" />
              <pin name="1-12" x="-12.7" y="-17.78" length="middle" direction="pas" />
              <pin name="2-12" x="0" y="-17.78" length="middle" direction="pas" />
              <pin name="3-12" x="12.7" y="-17.78" length="middle" direction="pas" />
              <wire x1="-10.16" y1="12.7" x2="-10.16" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-10.16" y1="-20.32" x2="-5.08" y2="-20.32" width="0.254" layer="94" />
              <wire x1="-10.16" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94" />
              <wire x1="2.54" y1="12.7" x2="2.54" y2="-20.32" width="0.254" layer="94" />
              <wire x1="2.54" y1="-20.32" x2="7.62" y2="-20.32" width="0.254" layer="94" />
              <wire x1="2.54" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94" />
              <wire x1="15.24" y1="12.7" x2="15.24" y2="-20.32" width="0.254" layer="94" />
              <wire x1="15.24" y1="-20.32" x2="20.32" y2="-20.32" width="0.254" layer="94" />
              <wire x1="15.24" y1="12.7" x2="20.32" y2="12.7" width="0.254" layer="94" />
              <text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
              <text x="-10.16" y="12.7" size="1.778" layer="95">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset name="PTA2043-2015CPB103">
              <description />
              <gates>
                <gate name="G$1" symbol="POT" x="0" y="0" />
              </gates>
              <devices>
                <device package="PTA4543">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                    <connect gate="G$1" pin="3" pad="P$3" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PTV112-4420A-A103">
              <description />
              <gates>
                <gate name="G$1" symbol="DUALPOT" x="0" y="0" />
              </gates>
              <devices>
                <device package="PTV112-4">
                  <connects>
                    <connect gate="G$1" pin="X1" pad="P$2" />
                    <connect gate="G$1" pin="X2" pad="P$1" />
                    <connect gate="G$1" pin="X3" pad="P$6" />
                    <connect gate="G$1" pin="Y1" pad="P$3" />
                    <connect gate="G$1" pin="Y2" pad="P$4" />
                    <connect gate="G$1" pin="Y3" pad="P$5" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="KSC1008YBU">
              <description />
              <gates>
                <gate name="G$1" symbol="NPN" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-92">
                  <connects>
                    <connect gate="G$1" pin="B" pad="2" />
                    <connect gate="G$1" pin="C" pad="3" />
                    <connect gate="G$1" pin="E" pad="1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="2SB605">
              <description />
              <gates>
                <gate name="G$1" symbol="PNP" x="0" y="-2.54" />
              </gates>
              <devices>
                <device package="2SB605">
                  <connects>
                    <connect gate="G$1" pin="B" pad="P$3" />
                    <connect gate="G$1" pin="C" pad="P$2" />
                    <connect gate="G$1" pin="E" pad="P$1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="1727832">
              <description />
              <gates>
                <gate name="G$1" symbol="36TERMINALBLOCK" x="0" y="0" />
              </gates>
              <devices>
                <device package="3X12TERMINAL">
                  <connects>
                    <connect gate="G$1" pin="1-1" pad="1-1" />
                    <connect gate="G$1" pin="1-10" pad="1-10" />
                    <connect gate="G$1" pin="1-11" pad="1-11" />
                    <connect gate="G$1" pin="1-12" pad="1-12" />
                    <connect gate="G$1" pin="1-2" pad="1-2" />
                    <connect gate="G$1" pin="1-3" pad="1-3" />
                    <connect gate="G$1" pin="1-4" pad="1-4" />
                    <connect gate="G$1" pin="1-5" pad="1-5" />
                    <connect gate="G$1" pin="1-6" pad="1-6" />
                    <connect gate="G$1" pin="1-7" pad="1-7" />
                    <connect gate="G$1" pin="1-8" pad="1-8" />
                    <connect gate="G$1" pin="1-9" pad="1-9" />
                    <connect gate="G$1" pin="2-1" pad="2-1" />
                    <connect gate="G$1" pin="2-10" pad="2-10" />
                    <connect gate="G$1" pin="2-11" pad="2-11" />
                    <connect gate="G$1" pin="2-12" pad="2-12" />
                    <connect gate="G$1" pin="2-2" pad="2-2" />
                    <connect gate="G$1" pin="2-3" pad="2-3" />
                    <connect gate="G$1" pin="2-4" pad="2-4" />
                    <connect gate="G$1" pin="2-5" pad="2-5" />
                    <connect gate="G$1" pin="2-6" pad="2-6" />
                    <connect gate="G$1" pin="2-7" pad="2-7" />
                    <connect gate="G$1" pin="2-8" pad="2-8" />
                    <connect gate="G$1" pin="2-9" pad="2-9" />
                    <connect gate="G$1" pin="3-1" pad="3-1" />
                    <connect gate="G$1" pin="3-10" pad="3-10" />
                    <connect gate="G$1" pin="3-11" pad="3-11" />
                    <connect gate="G$1" pin="3-12" pad="3-12" />
                    <connect gate="G$1" pin="3-2" pad="3-2" />
                    <connect gate="G$1" pin="3-3" pad="3-3" />
                    <connect gate="G$1" pin="3-4" pad="3-4" />
                    <connect gate="G$1" pin="3-5" pad="3-5" />
                    <connect gate="G$1" pin="3-6" pad="3-6" />
                    <connect gate="G$1" pin="3-7" pad="3-7" />
                    <connect gate="G$1" pin="3-8" pad="3-8" />
                    <connect gate="G$1" pin="3-9" pad="3-9" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="PTV111-3420A-A103">
              <description />
              <gates>
                <gate name="G$1" symbol="POT" x="0" y="0" />
              </gates>
              <devices>
                <device package="PTV111-3">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                    <connect gate="G$1" pin="3" pad="P$3" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="mlcc">
          <description />
          <packages>
            <package name="C_0805">
              <description>&lt;B&gt; 0805&lt;/B&gt; (2012 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.0762" layer="51" />
              <wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.0762" layer="51" />
              <wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.0762" layer="51" />
              <wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="0.7112" x2="0.1016" y2="0.7112" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="-0.7112" x2="0.1016" y2="-0.7112" width="0.1524" layer="21" />
              <smd name="1" x="-1" y="0" dx="1.35" dy="1.55" layer="1" />
              <smd name="2" x="1" y="0" dx="1.35" dy="1.55" layer="1" />
              <text x="-1.8" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0402">
              <description>&lt;B&gt; 0402&lt;/B&gt; (1005 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.0762" layer="51" />
              <wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.0762" layer="51" />
              <smd name="1" x="-0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <smd name="2" x="0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <text x="-1" y="0.6" size="0.508" layer="51" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0603">
              <description>&lt;B&gt; 0603&lt;/B&gt; (1608 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="-0.5334" x2="0.1016" y2="-0.5334" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="0.5334" x2="0.1016" y2="0.5334" width="0.1524" layer="21" />
              <smd name="1" x="-0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <smd name="2" x="0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <text x="-1.6" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="CAP_NP">
              <description>&lt;B&gt;Capacitor&lt;/B&gt; -- non-polarized</description>
              <wire x1="-1.905" y1="-3.175" x2="0" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="0" y1="-3.175" x2="1.905" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="-1.905" y1="-4.445" x2="0" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-4.445" x2="1.905" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-2.54" x2="0" y2="-3.175" width="0.254" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-4.445" width="0.254" layer="94" />
              <pin name="P$1" x="0" y="0" visible="off" length="short" direction="pas" rot="R270" />
              <pin name="P$2" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90" />
              <text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
              <text x="-5.08" y="-7.62" size="1.778" layer="95" rot="R90">&gt;NAME</text>
              <text x="0.508" y="-2.286" size="1.778" layer="95">1</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="C" name="C_0805">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0805">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0402">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0402">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0603">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="ecad">
          <description />
          <packages>
            <package name="TANT_2012-METRIC">
              <description />
              <smd name="P$1" x="-0.955" y="0" dx="1.14" dy="1.27" layer="1" />
              <smd name="P$2" x="0.955" y="0" dx="1.14" dy="1.27" layer="1" />
              <circle x="-1.96" y="0.34" radius="0.1542" width="0" layer="21" />
              <wire x1="-0.2" y1="0.8" x2="0.2" y2="0.8" width="0.127" layer="21" />
              <wire x1="-0.2" y1="-0.8" x2="0.2" y2="-0.8" width="0.127" layer="21" />
              <text x="-3.4" y="1" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-1.025" y1="0.65" x2="-1.025" y2="-0.65" width="0.127" layer="51" />
              <wire x1="-1.025" y1="-0.65" x2="1.025" y2="-0.65" width="0.127" layer="51" />
              <wire x1="1.025" y1="-0.65" x2="1.025" y2="0.65" width="0.127" layer="51" />
              <wire x1="1.025" y1="0.65" x2="-1.025" y2="0.65" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0.4" x2="-0.2" y2="0" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0" x2="-0.2" y2="-0.4" width="0.127" layer="51" />
              <wire x1="-0.2" y1="0" x2="-1" y2="0" width="0.127" layer="51" />
              <wire x1="0.4" y1="0.4" x2="0" y2="0" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0" y1="0" x2="0.4" y2="-0.4" width="0.127" layer="51" curve="90" cap="flat" />
              <wire x1="0" y1="0" x2="1" y2="0" width="0.127" layer="51" />
              <text x="-2.54" y="-0.635" size="1.27" layer="51">+</text>
            </package>
            <package name="DIODES_DO-35">
              <description>Diode, DO-35 package, 0.2in lead spacing, 0.7mm drill</description>
              <wire x1="-1.605" y1="0.9525" x2="1.605" y2="0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="-1.605" y1="-0.9525" x2="1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="-1.605" y1="0.9525" x2="-1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <wire x1="1.605" y1="0.9525" x2="1.605" y2="-0.9525" width="0.15239999999999998" layer="21" />
              <pad name="+" x="-2.54" y="0" drill="0.7" />
              <pad name="-" x="2.54" y="0" drill="0.7" />
              <text x="-3.505" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="LED">
              <description />
              <smd name="ANODE" x="0.75" y="0" dx="0.8" dy="0.8" layer="1" rot="R180" />
              <smd name="CATHODE" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1" rot="R180" />
              <wire x1="1.5" y1="-0.7" x2="-1.5" y2="-0.7" width="0.1542" layer="21" />
              <wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.1542" layer="21" />
              <wire x1="-1.5" y1="0.7" x2="1.5" y2="0.7" width="0.1542" layer="21" />
              <wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.1542" layer="21" />
              <circle x="-1.25" y="-1.05" radius="0.1542" width="0" layer="21" />
              <text x="-2" y="1.25" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="TANT_CAP">
              <description />
              <wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94" />
              <wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94" />
              <wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat" />
              <wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat" />
              <text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
              <text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
              <rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94" />
              <rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94" />
              <pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270" />
              <pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90" />
            </symbol>
            <symbol name="DIODES_DIODE">
              <description />
              <wire x1="-1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94" />
              <wire x1="1.27" y1="0" x2="-1.27" y2="1.905" width="0.254" layer="94" />
              <wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94" />
              <wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94" />
              <pin name="+" x="-2.54" y="0" visible="off" length="short" direction="pas" />
              <pin name="-" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180" />
              <text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
              <text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
            <symbol name="LED">
              <description />
              <wire x1="0" y1="7.62" x2="0" y2="5.08" width="0.254" layer="94" />
              <wire x1="0" y1="5.08" x2="0" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94" />
              <wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94" />
              <wire x1="-2.54" y1="0" x2="0" y2="5.08" width="0.254" layer="94" />
              <wire x1="0" y1="5.08" x2="2.54" y2="0" width="0.254" layer="94" />
              <wire x1="2.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" />
              <wire x1="5.08" y1="5.08" x2="7.62" y2="7.62" width="0.254" layer="94" />
              <wire x1="7.62" y1="5.08" x2="7.62" y2="7.62" width="0.254" layer="94" />
              <wire x1="7.62" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94" />
              <pin name="ANODE" x="0" y="-2.54" visible="pin" length="point" direction="pas" function="dot" />
              <pin name="CATHODE" x="0" y="7.62" length="point" direction="pas" function="dot" />
              <text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
              <text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset name="TANT_CAP">
              <description />
              <gates>
                <gate name="G$1" symbol="TANT_CAP" x="0" y="0" />
              </gates>
              <devices>
                <device package="TANT_2012-METRIC" name="2012-METRIC">
                  <connects>
                    <connect gate="G$1" pin="+" pad="P$1" />
                    <connect gate="G$1" pin="-" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="D" name="1N4148,113">
              <description />
              <gates>
                <gate name="A" symbol="DIODES_DIODE" x="0" y="0" />
              </gates>
              <devices>
                <device package="DIODES_DO-35" name="1N4148">
                  <connects>
                    <connect gate="A" pin="+" pad="+" />
                    <connect gate="A" pin="-" pad="-" />
                  </connects>
                  <technologies>
                    <technology name="">
                      <attribute name="SPICE" value="D{NAME} {+.NET} {-.NET} 1N4148" />
                      <attribute name="SPICEMOD" value=".model 1N4148 D (IS=0.1PA, RS=16 CJO=2PF TT=12N BV=100 IBV=1nA)" />
                    </technology>
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="LED">
              <description />
              <gates>
                <gate name="G$1" symbol="LED" x="0" y="2.54" />
              </gates>
              <devices>
                <device package="LED">
                  <connects>
                    <connect gate="G$1" pin="ANODE" pad="ANODE" />
                    <connect gate="G$1" pin="CATHODE" pad="CATHODE" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
      </libraries>
      <attributes />
      <variantdefs />
      <classes>
        <class number="0" name="default" />
      </classes>
      <parts>
        <part device="" value="ERJ-PA3J563V" name="R726" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R727" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR12" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="MF-RES-0603-100k" name="R744" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R745" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR21" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="MF-RES-0603-100k" name="R746" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R747" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR22" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="ERJ-PA3J563V" name="R728" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R729" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR13" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="ERJ-PA3J563V" name="R730" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R731" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR14" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="ERJ-PA3J563V" name="R732" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R733" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR15" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="ERJ-PA3J683V" name="R734" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R735" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR16" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="ERJ-PA3J823V" name="R736" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R737" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR17" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="MF-RES-0603-100k" name="R738" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R739" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR18" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="MF-RES-0603-100k" name="R740" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R741" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR19" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="MF-RES-0603-100k" name="R742" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R743" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTA4543-2015CPA103" name="VR20" library="General_Will" deviceset="PTA2043-2015CPB103" />
        <part device="" value="C2012JB1C106K085AC" name="C708" library="mlcc" deviceset="C_0805" />
        <part device="" value="C1005CH1H470J050BA" name="C709" library="mlcc" deviceset="C_0402" />
        <part device="" value="CL10B104JB8NNNC" name="C710" library="mlcc" deviceset="C_0603" />
        <part device="" value="ERJ-PA3J152V" name="R722" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R723" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-100k" name="R724" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J332V" name="R748" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J332V" name="R749" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R750" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R751" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R752" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-6.8K" name="R753" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="PTV112-4420A-A103" name="VR23" library="General_Will" deviceset="PTV112-4420A-A103" />
        <part device="" value="PTV112-4420A-A103" name="VR24" library="General_Will" deviceset="PTV112-4420A-A103" />
        <part device="2012-METRIC" value="TACR106K016RTA" name="C707" library="ecad" deviceset="TANT_CAP" />
        <part device="1N4148" value="1N4148,113" name="D71" library="ecad" deviceset="1N4148,113" />
        <part device="" value="LTST-C191KRKT" name="D72" library="ecad" deviceset="LED" />
        <part device="" value="LTST-C191KGKT" name="D73" library="ecad" deviceset="LED" />
        <part device="" value="LTST-C191KRKT" name="D74" library="ecad" deviceset="LED" />
        <part device="" value="LTST-C191KRKT" name="D75" library="ecad" deviceset="LED" />
        <part device="" value="LTST-C191KGKT" name="D76" library="ecad" deviceset="LED" />
        <part device="" value="KSC1008YBU" name="Q71" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q72" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q73" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q74" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="KSC1008YBU" name="Q75" library="General_Will" deviceset="KSC1008YBU" />
        <part device="" value="2SB605" name="Q77" library="General_Will" deviceset="2SB605" />
        <part device="" value="MF-RES-0603-10K" name="R701" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J393V" name="R702" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J223V" name="R703" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R704" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R705" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-220K" name="R706" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R707" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J563V" name="R708" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R709" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R710" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R711" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-1K" name="R712" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="ERJ-PA3J393V" name="R714" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MF-RES-0603-10K" name="R715" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="RHC2512FT330R" name="R725" library="resistor" deviceset="RESISTOR_2512" />
        <part device="" value="1727832" name="JFront" library="General_Will" deviceset="1727832" />
        <part device="" value="PTV111-3420A-A103" name="VR10" library="General_Will" deviceset="PTV111-3420A-A103" />
        <part device="" value="PTV111-3420A-A103" name="VR11" library="General_Will" deviceset="PTV111-3420A-A103" />
      </parts>
      <sheets>
        <sheet>
          <description />
          <plain />
          <instances>
            <instance y="55.88" part="R726" gate="G$1" x="337.82" />
            <instance y="45.72" part="R727" gate="G$1" x="337.82" />
            <instance y="53.34" part="VR12" gate="G$1" x="320.04" />
            <instance y="154.94" part="R744" gate="G$1" x="579.12" />
            <instance y="144.78" part="R745" gate="G$1" x="579.12" />
            <instance y="152.40" part="VR21" gate="G$1" x="561.34" />
            <instance y="177.80" part="R746" gate="G$1" x="579.12" />
            <instance y="167.64" part="R747" gate="G$1" x="579.12" />
            <instance y="175.26" part="VR22" gate="G$1" x="561.34" />
            <instance y="63.50" part="R728" gate="G$1" x="386.08" />
            <instance y="53.34" part="R729" gate="G$1" x="386.08" />
            <instance y="60.96" part="VR13" gate="G$1" x="368.30" />
            <instance y="86.36" part="R730" gate="G$1" x="386.08" />
            <instance y="76.20" part="R731" gate="G$1" x="386.08" />
            <instance y="83.82" part="VR14" gate="G$1" x="368.30" />
            <instance y="83.82" part="R732" gate="G$1" x="434.34" />
            <instance y="76.20" part="R733" gate="G$1" x="434.34" />
            <instance y="83.82" part="VR15" gate="G$1" x="416.56" />
            <instance y="109.22" part="R734" gate="G$1" x="434.34" />
            <instance y="99.06" part="R735" gate="G$1" x="434.34" />
            <instance y="106.68" part="VR16" gate="G$1" x="416.56" />
            <instance y="109.22" part="R736" gate="G$1" x="482.60" />
            <instance y="99.06" part="R737" gate="G$1" x="482.60" />
            <instance y="106.68" part="VR17" gate="G$1" x="464.82" />
            <instance y="132.08" part="R738" gate="G$1" x="482.60" />
            <instance y="121.92" part="R739" gate="G$1" x="482.60" />
            <instance y="129.54" part="VR18" gate="G$1" x="464.82" />
            <instance y="132.08" part="R740" gate="G$1" x="530.86" />
            <instance y="121.92" part="R741" gate="G$1" x="530.86" />
            <instance y="129.54" part="VR19" gate="G$1" x="513.08" />
            <instance y="154.94" part="R742" gate="G$1" x="530.86" />
            <instance y="144.78" part="R743" gate="G$1" x="530.86" />
            <instance y="152.40" part="VR20" gate="G$1" x="513.08" />
            <instance y="226.06" part="C708" gate="G$1" x="680.72" />
            <instance y="261.62" part="C709" gate="G$1" x="698.50" />
            <instance y="200.66" part="C710" gate="G$1" x="645.16" />
            <instance y="238.76" part="R722" gate="G$1" x="685.80" />
            <instance y="213.36" part="R723" gate="G$1" x="650.24" />
            <instance y="256.54" part="R724" gate="G$1" x="711.20" />
            <instance y="187.96" part="R748" gate="G$1" x="632.46" />
            <instance y="187.96" part="R749" gate="G$1" x="614.68" />
            <instance y="220.98" part="R750" gate="G$1" x="668.02" />
            <instance y="213.36" part="R751" gate="G$1" x="668.02" />
            <instance y="170.18" part="R752" gate="G$1" x="596.90" />
            <instance y="162.56" part="R753" gate="G$1" x="596.90" />
            <instance y="175.26" part="VR23" gate="G$1" x="619.76" />
            <instance y="243.84" part="VR24" gate="G$1" x="708.66" />
            <instance y="289.56" part="C707" gate="G$1" x="246.38" />
            <instance y="213.36" part="D71" gate="A" x="132.08" />
            <instance y="228.60" part="D72" gate="G$1" x="134.62" />
            <instance y="274.32" part="D73" gate="G$1" x="228.60" />
            <instance y="236.22" part="D74" gate="G$1" x="152.40" />
            <instance y="266.70" part="D75" gate="G$1" x="193.04" />
            <instance y="172.72" part="D76" gate="G$1" x="76.20" />
            <instance y="198.12" part="Q71" gate="G$1" x="119.38" />
            <instance y="292.10" part="Q72" gate="G$1" x="231.14" />
            <instance y="236.22" part="Q73" gate="G$1" x="172.72" />
            <instance y="190.50" part="Q74" gate="G$1" x="78.74" />
            <instance y="259.08" part="Q75" gate="G$1" x="172.72" />
            <instance y="314.96" part="Q77" gate="G$1" x="271.78" />
            <instance y="83.82" part="R701" gate="G$1" x="33.02" />
            <instance y="76.20" part="R702" gate="G$1" x="50.80" />
            <instance y="302.26" part="R703" gate="G$1" x="251.46" />
            <instance y="213.36" part="R704" gate="G$1" x="116.84" />
            <instance y="266.70" part="R705" gate="G$1" x="210.82" />
            <instance y="259.08" part="R706" gate="G$1" x="210.82" />
            <instance y="220.98" part="R707" gate="G$1" x="152.40" />
            <instance y="157.48" part="R708" gate="G$1" x="40.64" />
            <instance y="157.48" part="R709" gate="G$1" x="58.42" />
            <instance y="251.46" part="R710" gate="G$1" x="193.04" />
            <instance y="149.86" part="R711" gate="G$1" x="33.02" />
            <instance y="165.10" part="R712" gate="G$1" x="58.42" />
            <instance y="182.88" part="R714" gate="G$1" x="99.06" />
            <instance y="190.50" part="R715" gate="G$1" x="99.06" />
            <instance y="302.26" part="R725" gate="G$1" x="269.24" />
            <instance y="35.56" part="JFront" gate="G$1" x="25.40" />
            <instance y="276.86" part="VR10" gate="G$1" x="307.34" />
            <instance y="276.86" part="VR11" gate="G$1" x="289.56" />
          </instances>
          <busses />
          <nets>
            <net name="N$0">
              <segment>
                <wire x1="332.74" y1="55.88" x2="330.20" y2="55.88" width="0.3" layer="91" />
                <label x="330.20" y="55.88" size="1.27" layer="95" />
                <pinref part="R726" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="325.12" y1="53.34" x2="327.66" y2="53.34" width="0.3" layer="91" />
                <label x="327.66" y="53.34" size="1.27" layer="95" />
                <pinref part="VR12" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$1">
              <segment>
                <wire x1="342.90" y1="55.88" x2="345.44" y2="55.88" width="0.3" layer="91" />
                <label x="345.44" y="55.88" size="1.27" layer="95" />
                <pinref part="R726" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="584.20" y1="177.80" x2="586.74" y2="177.80" width="0.3" layer="91" />
                <label x="586.74" y="177.80" size="1.27" layer="95" />
                <pinref part="R746" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="584.20" y1="154.94" x2="586.74" y2="154.94" width="0.3" layer="91" />
                <label x="586.74" y="154.94" size="1.27" layer="95" />
                <pinref part="R744" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="535.94" y1="154.94" x2="538.48" y2="154.94" width="0.3" layer="91" />
                <label x="538.48" y="154.94" size="1.27" layer="95" />
                <pinref part="R742" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="535.94" y1="132.08" x2="538.48" y2="132.08" width="0.3" layer="91" />
                <label x="538.48" y="132.08" size="1.27" layer="95" />
                <pinref part="R740" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="132.08" x2="490.22" y2="132.08" width="0.3" layer="91" />
                <label x="490.22" y="132.08" size="1.27" layer="95" />
                <pinref part="R738" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="109.22" x2="490.22" y2="109.22" width="0.3" layer="91" />
                <label x="490.22" y="109.22" size="1.27" layer="95" />
                <pinref part="R736" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="109.22" x2="441.96" y2="109.22" width="0.3" layer="91" />
                <label x="441.96" y="109.22" size="1.27" layer="95" />
                <pinref part="R734" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="83.82" x2="441.96" y2="83.82" width="0.3" layer="91" />
                <label x="441.96" y="83.82" size="1.27" layer="95" />
                <pinref part="R732" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="391.16" y1="86.36" x2="393.70" y2="86.36" width="0.3" layer="91" />
                <label x="393.70" y="86.36" size="1.27" layer="95" />
                <pinref part="R730" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="391.16" y1="63.50" x2="393.70" y2="63.50" width="0.3" layer="91" />
                <label x="393.70" y="63.50" size="1.27" layer="95" />
                <pinref part="R728" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="645.16" y1="200.66" x2="645.16" y2="203.20" width="0.3" layer="91" />
                <label x="645.16" y="203.20" size="1.27" layer="95" />
                <pinref part="C710" gate="G$1" pin="P$1" />
              </segment>
            </net>
            <net name="N$2">
              <segment>
                <wire x1="342.90" y1="45.72" x2="345.44" y2="45.72" width="0.3" layer="91" />
                <label x="345.44" y="45.72" size="1.27" layer="95" />
                <pinref part="R727" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="584.20" y1="167.64" x2="586.74" y2="167.64" width="0.3" layer="91" />
                <label x="586.74" y="167.64" size="1.27" layer="95" />
                <pinref part="R747" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="584.20" y1="144.78" x2="586.74" y2="144.78" width="0.3" layer="91" />
                <label x="586.74" y="144.78" size="1.27" layer="95" />
                <pinref part="R745" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="535.94" y1="144.78" x2="538.48" y2="144.78" width="0.3" layer="91" />
                <label x="538.48" y="144.78" size="1.27" layer="95" />
                <pinref part="R743" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="535.94" y1="121.92" x2="538.48" y2="121.92" width="0.3" layer="91" />
                <label x="538.48" y="121.92" size="1.27" layer="95" />
                <pinref part="R741" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="121.92" x2="490.22" y2="121.92" width="0.3" layer="91" />
                <label x="490.22" y="121.92" size="1.27" layer="95" />
                <pinref part="R739" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="487.68" y1="99.06" x2="490.22" y2="99.06" width="0.3" layer="91" />
                <label x="490.22" y="99.06" size="1.27" layer="95" />
                <pinref part="R737" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="99.06" x2="441.96" y2="99.06" width="0.3" layer="91" />
                <label x="441.96" y="99.06" size="1.27" layer="95" />
                <pinref part="R735" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="439.42" y1="76.20" x2="441.96" y2="76.20" width="0.3" layer="91" />
                <label x="441.96" y="76.20" size="1.27" layer="95" />
                <pinref part="R733" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="391.16" y1="76.20" x2="393.70" y2="76.20" width="0.3" layer="91" />
                <label x="393.70" y="76.20" size="1.27" layer="95" />
                <pinref part="R731" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="391.16" y1="53.34" x2="393.70" y2="53.34" width="0.3" layer="91" />
                <label x="393.70" y="53.34" size="1.27" layer="95" />
                <pinref part="R729" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="25.40" y1="17.78" x2="22.86" y2="17.78" width="0.3" layer="91" />
                <label x="22.86" y="17.78" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-12" />
              </segment>
              <segment>
                <wire x1="38.10" y1="25.40" x2="35.56" y2="25.40" width="0.3" layer="91" />
                <label x="35.56" y="25.40" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-9" />
              </segment>
              <segment>
                <wire x1="38.10" y1="27.94" x2="35.56" y2="27.94" width="0.3" layer="91" />
                <label x="35.56" y="27.94" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-8" />
              </segment>
              <segment>
                <wire x1="38.10" y1="40.64" x2="35.56" y2="40.64" width="0.3" layer="91" />
                <label x="35.56" y="40.64" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-3" />
              </segment>
              <segment>
                <wire x1="12.70" y1="20.32" x2="10.16" y2="20.32" width="0.3" layer="91" />
                <label x="10.16" y="20.32" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-11" />
              </segment>
              <segment>
                <wire x1="12.70" y1="38.10" x2="10.16" y2="38.10" width="0.3" layer="91" />
                <label x="10.16" y="38.10" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-4" />
              </segment>
              <segment>
                <wire x1="261.62" y1="317.50" x2="259.08" y2="317.50" width="0.3" layer="91" />
                <label x="259.08" y="317.50" size="1.27" layer="95" />
                <pinref part="Q77" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="680.72" y1="218.44" x2="680.72" y2="215.90" width="0.3" layer="91" />
                <label x="680.72" y="215.90" size="1.27" layer="95" />
                <pinref part="C708" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="703.58" y1="248.92" x2="703.58" y2="251.46" width="0.3" layer="91" />
                <label x="703.58" y="251.46" size="1.27" layer="95" />
                <pinref part="VR24" gate="G$1" pin="X3" />
              </segment>
              <segment>
                <wire x1="716.28" y1="248.92" x2="716.28" y2="251.46" width="0.3" layer="91" />
                <label x="716.28" y="251.46" size="1.27" layer="95" />
                <pinref part="VR24" gate="G$1" pin="Y3" />
              </segment>
              <segment>
                <wire x1="645.16" y1="213.36" x2="642.62" y2="213.36" width="0.3" layer="91" />
                <label x="642.62" y="213.36" size="1.27" layer="95" />
                <pinref part="R723" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="619.76" y1="187.96" x2="622.30" y2="187.96" width="0.3" layer="91" />
                <label x="622.30" y="187.96" size="1.27" layer="95" />
                <pinref part="R749" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="637.54" y1="187.96" x2="640.08" y2="187.96" width="0.3" layer="91" />
                <label x="640.08" y="187.96" size="1.27" layer="95" />
                <pinref part="R748" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$3">
              <segment>
                <wire x1="332.74" y1="45.72" x2="330.20" y2="45.72" width="0.3" layer="91" />
                <label x="330.20" y="45.72" size="1.27" layer="95" />
                <pinref part="R727" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="320.04" y1="58.42" x2="320.04" y2="60.96" width="0.3" layer="91" />
                <label x="320.04" y="60.96" size="1.27" layer="95" />
                <pinref part="VR12" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$4">
              <segment>
                <wire x1="320.04" y1="48.26" x2="320.04" y2="45.72" width="0.3" layer="91" />
                <label x="320.04" y="45.72" size="1.27" layer="95" />
                <pinref part="VR12" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="20.32" x2="22.86" y2="20.32" width="0.3" layer="91" />
                <label x="22.86" y="20.32" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-11" />
              </segment>
            </net>
            <net name="N$5">
              <segment>
                <wire x1="574.04" y1="154.94" x2="571.50" y2="154.94" width="0.3" layer="91" />
                <label x="571.50" y="154.94" size="1.27" layer="95" />
                <pinref part="R744" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="566.42" y1="152.40" x2="568.96" y2="152.40" width="0.3" layer="91" />
                <label x="568.96" y="152.40" size="1.27" layer="95" />
                <pinref part="VR21" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$6">
              <segment>
                <wire x1="574.04" y1="144.78" x2="571.50" y2="144.78" width="0.3" layer="91" />
                <label x="571.50" y="144.78" size="1.27" layer="95" />
                <pinref part="R745" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="561.34" y1="157.48" x2="561.34" y2="160.02" width="0.3" layer="91" />
                <label x="561.34" y="160.02" size="1.27" layer="95" />
                <pinref part="VR21" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$7">
              <segment>
                <wire x1="561.34" y1="147.32" x2="561.34" y2="144.78" width="0.3" layer="91" />
                <label x="561.34" y="144.78" size="1.27" layer="95" />
                <pinref part="VR21" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="43.18" x2="22.86" y2="43.18" width="0.3" layer="91" />
                <label x="22.86" y="43.18" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-2" />
              </segment>
            </net>
            <net name="N$8">
              <segment>
                <wire x1="574.04" y1="177.80" x2="571.50" y2="177.80" width="0.3" layer="91" />
                <label x="571.50" y="177.80" size="1.27" layer="95" />
                <pinref part="R746" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="566.42" y1="175.26" x2="568.96" y2="175.26" width="0.3" layer="91" />
                <label x="568.96" y="175.26" size="1.27" layer="95" />
                <pinref part="VR22" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$9">
              <segment>
                <wire x1="574.04" y1="167.64" x2="571.50" y2="167.64" width="0.3" layer="91" />
                <label x="571.50" y="167.64" size="1.27" layer="95" />
                <pinref part="R747" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="561.34" y1="180.34" x2="561.34" y2="182.88" width="0.3" layer="91" />
                <label x="561.34" y="182.88" size="1.27" layer="95" />
                <pinref part="VR22" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$10">
              <segment>
                <wire x1="561.34" y1="170.18" x2="561.34" y2="167.64" width="0.3" layer="91" />
                <label x="561.34" y="167.64" size="1.27" layer="95" />
                <pinref part="VR22" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="45.72" x2="22.86" y2="45.72" width="0.3" layer="91" />
                <label x="22.86" y="45.72" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-1" />
              </segment>
            </net>
            <net name="N$11">
              <segment>
                <wire x1="381.00" y1="63.50" x2="378.46" y2="63.50" width="0.3" layer="91" />
                <label x="378.46" y="63.50" size="1.27" layer="95" />
                <pinref part="R728" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="373.38" y1="60.96" x2="375.92" y2="60.96" width="0.3" layer="91" />
                <label x="375.92" y="60.96" size="1.27" layer="95" />
                <pinref part="VR13" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$12">
              <segment>
                <wire x1="381.00" y1="53.34" x2="378.46" y2="53.34" width="0.3" layer="91" />
                <label x="378.46" y="53.34" size="1.27" layer="95" />
                <pinref part="R729" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="368.30" y1="66.04" x2="368.30" y2="68.58" width="0.3" layer="91" />
                <label x="368.30" y="68.58" size="1.27" layer="95" />
                <pinref part="VR13" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$13">
              <segment>
                <wire x1="368.30" y1="55.88" x2="368.30" y2="53.34" width="0.3" layer="91" />
                <label x="368.30" y="53.34" size="1.27" layer="95" />
                <pinref part="VR13" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="22.86" x2="22.86" y2="22.86" width="0.3" layer="91" />
                <label x="22.86" y="22.86" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-10" />
              </segment>
            </net>
            <net name="N$14">
              <segment>
                <wire x1="381.00" y1="86.36" x2="378.46" y2="86.36" width="0.3" layer="91" />
                <label x="378.46" y="86.36" size="1.27" layer="95" />
                <pinref part="R730" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="373.38" y1="83.82" x2="375.92" y2="83.82" width="0.3" layer="91" />
                <label x="375.92" y="83.82" size="1.27" layer="95" />
                <pinref part="VR14" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$15">
              <segment>
                <wire x1="381.00" y1="76.20" x2="378.46" y2="76.20" width="0.3" layer="91" />
                <label x="378.46" y="76.20" size="1.27" layer="95" />
                <pinref part="R731" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="368.30" y1="88.90" x2="368.30" y2="91.44" width="0.3" layer="91" />
                <label x="368.30" y="91.44" size="1.27" layer="95" />
                <pinref part="VR14" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$16">
              <segment>
                <wire x1="368.30" y1="78.74" x2="368.30" y2="76.20" width="0.3" layer="91" />
                <label x="368.30" y="76.20" size="1.27" layer="95" />
                <pinref part="VR14" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="25.40" x2="22.86" y2="25.40" width="0.3" layer="91" />
                <label x="22.86" y="25.40" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-9" />
              </segment>
            </net>
            <net name="N$17">
              <segment>
                <wire x1="429.26" y1="83.82" x2="426.72" y2="83.82" width="0.3" layer="91" />
                <label x="426.72" y="83.82" size="1.27" layer="95" />
                <pinref part="R732" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="421.64" y1="83.82" x2="424.18" y2="83.82" width="0.3" layer="91" />
                <label x="424.18" y="83.82" size="1.27" layer="95" />
                <pinref part="VR15" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$18">
              <segment>
                <wire x1="429.26" y1="76.20" x2="426.72" y2="76.20" width="0.3" layer="91" />
                <label x="426.72" y="76.20" size="1.27" layer="95" />
                <pinref part="R733" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="416.56" y1="88.90" x2="416.56" y2="91.44" width="0.3" layer="91" />
                <label x="416.56" y="91.44" size="1.27" layer="95" />
                <pinref part="VR15" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$19">
              <segment>
                <wire x1="416.56" y1="78.74" x2="416.56" y2="76.20" width="0.3" layer="91" />
                <label x="416.56" y="76.20" size="1.27" layer="95" />
                <pinref part="VR15" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="27.94" x2="22.86" y2="27.94" width="0.3" layer="91" />
                <label x="22.86" y="27.94" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-8" />
              </segment>
            </net>
            <net name="N$20">
              <segment>
                <wire x1="429.26" y1="109.22" x2="426.72" y2="109.22" width="0.3" layer="91" />
                <label x="426.72" y="109.22" size="1.27" layer="95" />
                <pinref part="R734" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="421.64" y1="106.68" x2="424.18" y2="106.68" width="0.3" layer="91" />
                <label x="424.18" y="106.68" size="1.27" layer="95" />
                <pinref part="VR16" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$21">
              <segment>
                <wire x1="429.26" y1="99.06" x2="426.72" y2="99.06" width="0.3" layer="91" />
                <label x="426.72" y="99.06" size="1.27" layer="95" />
                <pinref part="R735" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="416.56" y1="111.76" x2="416.56" y2="114.30" width="0.3" layer="91" />
                <label x="416.56" y="114.30" size="1.27" layer="95" />
                <pinref part="VR16" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$22">
              <segment>
                <wire x1="416.56" y1="101.60" x2="416.56" y2="99.06" width="0.3" layer="91" />
                <label x="416.56" y="99.06" size="1.27" layer="95" />
                <pinref part="VR16" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="30.48" x2="22.86" y2="30.48" width="0.3" layer="91" />
                <label x="22.86" y="30.48" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-7" />
              </segment>
            </net>
            <net name="N$23">
              <segment>
                <wire x1="477.52" y1="109.22" x2="474.98" y2="109.22" width="0.3" layer="91" />
                <label x="474.98" y="109.22" size="1.27" layer="95" />
                <pinref part="R736" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="469.90" y1="106.68" x2="472.44" y2="106.68" width="0.3" layer="91" />
                <label x="472.44" y="106.68" size="1.27" layer="95" />
                <pinref part="VR17" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$24">
              <segment>
                <wire x1="477.52" y1="99.06" x2="474.98" y2="99.06" width="0.3" layer="91" />
                <label x="474.98" y="99.06" size="1.27" layer="95" />
                <pinref part="R737" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="464.82" y1="111.76" x2="464.82" y2="114.30" width="0.3" layer="91" />
                <label x="464.82" y="114.30" size="1.27" layer="95" />
                <pinref part="VR17" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$25">
              <segment>
                <wire x1="464.82" y1="101.60" x2="464.82" y2="99.06" width="0.3" layer="91" />
                <label x="464.82" y="99.06" size="1.27" layer="95" />
                <pinref part="VR17" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="33.02" x2="22.86" y2="33.02" width="0.3" layer="91" />
                <label x="22.86" y="33.02" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-6" />
              </segment>
            </net>
            <net name="N$26">
              <segment>
                <wire x1="477.52" y1="132.08" x2="474.98" y2="132.08" width="0.3" layer="91" />
                <label x="474.98" y="132.08" size="1.27" layer="95" />
                <pinref part="R738" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="469.90" y1="129.54" x2="472.44" y2="129.54" width="0.3" layer="91" />
                <label x="472.44" y="129.54" size="1.27" layer="95" />
                <pinref part="VR18" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$27">
              <segment>
                <wire x1="477.52" y1="121.92" x2="474.98" y2="121.92" width="0.3" layer="91" />
                <label x="474.98" y="121.92" size="1.27" layer="95" />
                <pinref part="R739" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="464.82" y1="134.62" x2="464.82" y2="137.16" width="0.3" layer="91" />
                <label x="464.82" y="137.16" size="1.27" layer="95" />
                <pinref part="VR18" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$28">
              <segment>
                <wire x1="464.82" y1="124.46" x2="464.82" y2="121.92" width="0.3" layer="91" />
                <label x="464.82" y="121.92" size="1.27" layer="95" />
                <pinref part="VR18" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="35.56" x2="22.86" y2="35.56" width="0.3" layer="91" />
                <label x="22.86" y="35.56" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-5" />
              </segment>
            </net>
            <net name="N$29">
              <segment>
                <wire x1="525.78" y1="132.08" x2="523.24" y2="132.08" width="0.3" layer="91" />
                <label x="523.24" y="132.08" size="1.27" layer="95" />
                <pinref part="R740" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="518.16" y1="129.54" x2="520.70" y2="129.54" width="0.3" layer="91" />
                <label x="520.70" y="129.54" size="1.27" layer="95" />
                <pinref part="VR19" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$30">
              <segment>
                <wire x1="525.78" y1="121.92" x2="523.24" y2="121.92" width="0.3" layer="91" />
                <label x="523.24" y="121.92" size="1.27" layer="95" />
                <pinref part="R741" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="513.08" y1="134.62" x2="513.08" y2="137.16" width="0.3" layer="91" />
                <label x="513.08" y="137.16" size="1.27" layer="95" />
                <pinref part="VR19" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$31">
              <segment>
                <wire x1="513.08" y1="124.46" x2="513.08" y2="121.92" width="0.3" layer="91" />
                <label x="513.08" y="121.92" size="1.27" layer="95" />
                <pinref part="VR19" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="38.10" x2="22.86" y2="38.10" width="0.3" layer="91" />
                <label x="22.86" y="38.10" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-4" />
              </segment>
            </net>
            <net name="N$32">
              <segment>
                <wire x1="525.78" y1="154.94" x2="523.24" y2="154.94" width="0.3" layer="91" />
                <label x="523.24" y="154.94" size="1.27" layer="95" />
                <pinref part="R742" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="518.16" y1="152.40" x2="520.70" y2="152.40" width="0.3" layer="91" />
                <label x="520.70" y="152.40" size="1.27" layer="95" />
                <pinref part="VR20" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$33">
              <segment>
                <wire x1="525.78" y1="144.78" x2="523.24" y2="144.78" width="0.3" layer="91" />
                <label x="523.24" y="144.78" size="1.27" layer="95" />
                <pinref part="R743" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="513.08" y1="157.48" x2="513.08" y2="160.02" width="0.3" layer="91" />
                <label x="513.08" y="160.02" size="1.27" layer="95" />
                <pinref part="VR20" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$34">
              <segment>
                <wire x1="513.08" y1="147.32" x2="513.08" y2="144.78" width="0.3" layer="91" />
                <label x="513.08" y="144.78" size="1.27" layer="95" />
                <pinref part="VR20" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="25.40" y1="40.64" x2="22.86" y2="40.64" width="0.3" layer="91" />
                <label x="22.86" y="40.64" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="2-3" />
              </segment>
            </net>
            <net name="N$35">
              <segment>
                <wire x1="680.72" y1="226.06" x2="680.72" y2="228.60" width="0.3" layer="91" />
                <label x="680.72" y="228.60" size="1.27" layer="95" />
                <pinref part="C708" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="680.72" y1="238.76" x2="678.18" y2="238.76" width="0.3" layer="91" />
                <label x="678.18" y="238.76" size="1.27" layer="95" />
                <pinref part="R722" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$36">
              <segment>
                <wire x1="698.50" y1="261.62" x2="698.50" y2="264.16" width="0.3" layer="91" />
                <label x="698.50" y="264.16" size="1.27" layer="95" />
                <pinref part="C709" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="38.10" y1="35.56" x2="35.56" y2="35.56" width="0.3" layer="91" />
                <label x="35.56" y="35.56" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-5" />
              </segment>
              <segment>
                <wire x1="690.88" y1="238.76" x2="693.42" y2="238.76" width="0.3" layer="91" />
                <label x="693.42" y="238.76" size="1.27" layer="95" />
                <pinref part="R722" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="706.12" y1="256.54" x2="703.58" y2="256.54" width="0.3" layer="91" />
                <label x="703.58" y="256.54" size="1.27" layer="95" />
                <pinref part="R724" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$37">
              <segment>
                <wire x1="698.50" y1="254.00" x2="698.50" y2="251.46" width="0.3" layer="91" />
                <label x="698.50" y="251.46" size="1.27" layer="95" />
                <pinref part="C709" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="716.28" y1="256.54" x2="718.82" y2="256.54" width="0.3" layer="91" />
                <label x="718.82" y="256.54" size="1.27" layer="95" />
                <pinref part="R724" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="38.10" y1="38.10" x2="35.56" y2="38.10" width="0.3" layer="91" />
                <label x="35.56" y="38.10" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-4" />
              </segment>
            </net>
            <net name="N$38">
              <segment>
                <wire x1="645.16" y1="193.04" x2="645.16" y2="190.50" width="0.3" layer="91" />
                <label x="645.16" y="190.50" size="1.27" layer="95" />
                <pinref part="C710" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="38.10" y1="33.02" x2="35.56" y2="33.02" width="0.3" layer="91" />
                <label x="35.56" y="33.02" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-6" />
              </segment>
              <segment>
                <wire x1="655.32" y1="213.36" x2="657.86" y2="213.36" width="0.3" layer="91" />
                <label x="657.86" y="213.36" size="1.27" layer="95" />
                <pinref part="R723" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$39">
              <segment>
                <wire x1="627.38" y1="187.96" x2="624.84" y2="187.96" width="0.3" layer="91" />
                <label x="624.84" y="187.96" size="1.27" layer="95" />
                <pinref part="R748" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="601.98" y1="170.18" x2="604.52" y2="170.18" width="0.3" layer="91" />
                <label x="604.52" y="170.18" size="1.27" layer="95" />
                <pinref part="R752" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="627.38" y1="180.34" x2="627.38" y2="182.88" width="0.3" layer="91" />
                <label x="627.38" y="182.88" size="1.27" layer="95" />
                <pinref part="VR23" gate="G$1" pin="Y3" />
              </segment>
            </net>
            <net name="N$40">
              <segment>
                <wire x1="609.60" y1="187.96" x2="607.06" y2="187.96" width="0.3" layer="91" />
                <label x="607.06" y="187.96" size="1.27" layer="95" />
                <pinref part="R749" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="614.68" y1="180.34" x2="614.68" y2="182.88" width="0.3" layer="91" />
                <label x="614.68" y="182.88" size="1.27" layer="95" />
                <pinref part="VR23" gate="G$1" pin="X3" />
              </segment>
              <segment>
                <wire x1="601.98" y1="162.56" x2="604.52" y2="162.56" width="0.3" layer="91" />
                <label x="604.52" y="162.56" size="1.27" layer="95" />
                <pinref part="R753" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$41">
              <segment>
                <wire x1="673.10" y1="220.98" x2="675.64" y2="220.98" width="0.3" layer="91" />
                <label x="675.64" y="220.98" size="1.27" layer="95" />
                <pinref part="R750" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="703.58" y1="238.76" x2="703.58" y2="236.22" width="0.3" layer="91" />
                <label x="703.58" y="236.22" size="1.27" layer="95" />
                <pinref part="VR24" gate="G$1" pin="X1" />
              </segment>
            </net>
            <net name="N$42">
              <segment>
                <wire x1="662.94" y1="220.98" x2="660.40" y2="220.98" width="0.3" layer="91" />
                <label x="660.40" y="220.98" size="1.27" layer="95" />
                <pinref part="R750" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="619.76" y1="175.26" x2="622.30" y2="175.26" width="0.3" layer="91" />
                <label x="622.30" y="175.26" size="1.27" layer="95" />
                <pinref part="VR23" gate="G$1" pin="X2" />
              </segment>
            </net>
            <net name="N$43">
              <segment>
                <wire x1="662.94" y1="213.36" x2="660.40" y2="213.36" width="0.3" layer="91" />
                <label x="660.40" y="213.36" size="1.27" layer="95" />
                <pinref part="R751" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="632.46" y1="175.26" x2="635.00" y2="175.26" width="0.3" layer="91" />
                <label x="635.00" y="175.26" size="1.27" layer="95" />
                <pinref part="VR23" gate="G$1" pin="Y2" />
              </segment>
            </net>
            <net name="N$44">
              <segment>
                <wire x1="673.10" y1="213.36" x2="675.64" y2="213.36" width="0.3" layer="91" />
                <label x="675.64" y="213.36" size="1.27" layer="95" />
                <pinref part="R751" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="716.28" y1="238.76" x2="716.28" y2="236.22" width="0.3" layer="91" />
                <label x="716.28" y="236.22" size="1.27" layer="95" />
                <pinref part="VR24" gate="G$1" pin="Y1" />
              </segment>
            </net>
            <net name="N$45">
              <segment>
                <wire x1="591.82" y1="170.18" x2="589.28" y2="170.18" width="0.3" layer="91" />
                <label x="589.28" y="170.18" size="1.27" layer="95" />
                <pinref part="R752" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="591.82" y1="162.56" x2="589.28" y2="162.56" width="0.3" layer="91" />
                <label x="589.28" y="162.56" size="1.27" layer="95" />
                <pinref part="R753" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="12.70" y1="40.64" x2="10.16" y2="40.64" width="0.3" layer="91" />
                <label x="10.16" y="40.64" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-3" />
              </segment>
            </net>
            <net name="N$46">
              <segment>
                <wire x1="627.38" y1="170.18" x2="627.38" y2="167.64" width="0.3" layer="91" />
                <label x="627.38" y="167.64" size="1.27" layer="95" />
                <pinref part="VR23" gate="G$1" pin="Y1" />
              </segment>
              <segment>
                <wire x1="38.10" y1="45.72" x2="35.56" y2="45.72" width="0.3" layer="91" />
                <label x="35.56" y="45.72" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-1" />
              </segment>
            </net>
            <net name="N$47">
              <segment>
                <wire x1="614.68" y1="170.18" x2="614.68" y2="167.64" width="0.3" layer="91" />
                <label x="614.68" y="167.64" size="1.27" layer="95" />
                <pinref part="VR23" gate="G$1" pin="X1" />
              </segment>
              <segment>
                <wire x1="38.10" y1="43.18" x2="35.56" y2="43.18" width="0.3" layer="91" />
                <label x="35.56" y="43.18" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-2" />
              </segment>
            </net>
            <net name="N$48">
              <segment>
                <wire x1="721.36" y1="243.84" x2="723.90" y2="243.84" width="0.3" layer="91" />
                <label x="723.90" y="243.84" size="1.27" layer="95" />
                <pinref part="VR24" gate="G$1" pin="Y2" />
              </segment>
              <segment>
                <wire x1="12.70" y1="45.72" x2="10.16" y2="45.72" width="0.3" layer="91" />
                <label x="10.16" y="45.72" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-1" />
              </segment>
            </net>
            <net name="N$49">
              <segment>
                <wire x1="708.66" y1="243.84" x2="711.20" y2="243.84" width="0.3" layer="91" />
                <label x="711.20" y="243.84" size="1.27" layer="95" />
                <pinref part="VR24" gate="G$1" pin="X2" />
              </segment>
              <segment>
                <wire x1="12.70" y1="43.18" x2="10.16" y2="43.18" width="0.3" layer="91" />
                <label x="10.16" y="43.18" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-2" />
              </segment>
            </net>
            <net name="N$50">
              <segment>
                <wire x1="246.38" y1="284.48" x2="246.38" y2="281.94" width="0.3" layer="91" />
                <label x="246.38" y="281.94" size="1.27" layer="95" />
                <pinref part="C707" gate="G$1" pin="-" />
              </segment>
              <segment>
                <wire x1="12.70" y1="35.56" x2="10.16" y2="35.56" width="0.3" layer="91" />
                <label x="10.16" y="35.56" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-5" />
              </segment>
              <segment>
                <wire x1="45.72" y1="76.20" x2="43.18" y2="76.20" width="0.3" layer="91" />
                <label x="43.18" y="76.20" size="1.27" layer="95" />
                <pinref part="R702" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="264.16" y1="302.26" x2="261.62" y2="302.26" width="0.3" layer="91" />
                <label x="261.62" y="302.26" size="1.27" layer="95" />
                <pinref part="R725" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="93.98" y1="182.88" x2="91.44" y2="182.88" width="0.3" layer="91" />
                <label x="91.44" y="182.88" size="1.27" layer="95" />
                <pinref part="R714" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="35.56" y1="157.48" x2="33.02" y2="157.48" width="0.3" layer="91" />
                <label x="33.02" y="157.48" size="1.27" layer="95" />
                <pinref part="R708" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$51">
              <segment>
                <wire x1="246.38" y1="292.10" x2="246.38" y2="294.64" width="0.3" layer="91" />
                <label x="246.38" y="294.64" size="1.27" layer="95" />
                <pinref part="C707" gate="G$1" pin="+" />
              </segment>
              <segment>
                <wire x1="256.54" y1="302.26" x2="259.08" y2="302.26" width="0.3" layer="91" />
                <label x="259.08" y="302.26" size="1.27" layer="95" />
                <pinref part="R703" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="276.86" y1="309.88" x2="279.40" y2="309.88" width="0.3" layer="91" />
                <label x="279.40" y="309.88" size="1.27" layer="95" />
                <pinref part="Q77" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="124.46" y1="190.50" x2="127.00" y2="190.50" width="0.3" layer="91" />
                <label x="127.00" y="190.50" size="1.27" layer="95" />
                <pinref part="Q71" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="236.22" y1="284.48" x2="238.76" y2="284.48" width="0.3" layer="91" />
                <label x="238.76" y="284.48" size="1.27" layer="95" />
                <pinref part="Q72" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="177.80" y1="228.60" x2="180.34" y2="228.60" width="0.3" layer="91" />
                <label x="180.34" y="228.60" size="1.27" layer="95" />
                <pinref part="Q73" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="177.80" y1="251.46" x2="180.34" y2="251.46" width="0.3" layer="91" />
                <label x="180.34" y="251.46" size="1.27" layer="95" />
                <pinref part="Q75" gate="G$1" pin="E" />
              </segment>
              <segment>
                <wire x1="83.82" y1="182.88" x2="86.36" y2="182.88" width="0.3" layer="91" />
                <label x="86.36" y="182.88" size="1.27" layer="95" />
                <pinref part="Q74" gate="G$1" pin="E" />
              </segment>
            </net>
            <net name="N$52">
              <segment>
                <wire x1="134.62" y1="213.36" x2="137.16" y2="213.36" width="0.3" layer="91" />
                <label x="137.16" y="213.36" size="1.27" layer="95" />
                <pinref part="D71" gate="A" pin="-" />
              </segment>
              <segment>
                <wire x1="228.60" y1="271.78" x2="226.06" y2="271.78" width="0.3" layer="91" />
                <label x="226.06" y="271.78" size="1.27" layer="95" />
                <pinref part="D73" gate="G$1" pin="ANODE" />
              </segment>
            </net>
            <net name="N$53">
              <segment>
                <wire x1="129.54" y1="213.36" x2="127.00" y2="213.36" width="0.3" layer="91" />
                <label x="127.00" y="213.36" size="1.27" layer="95" />
                <pinref part="D71" gate="A" pin="+" />
              </segment>
              <segment>
                <wire x1="111.76" y1="213.36" x2="109.22" y2="213.36" width="0.3" layer="91" />
                <label x="109.22" y="213.36" size="1.27" layer="95" />
                <pinref part="R704" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="134.62" y1="226.06" x2="132.08" y2="226.06" width="0.3" layer="91" />
                <label x="132.08" y="226.06" size="1.27" layer="95" />
                <pinref part="D72" gate="G$1" pin="ANODE" />
              </segment>
            </net>
            <net name="N$54">
              <segment>
                <wire x1="134.62" y1="236.22" x2="132.08" y2="236.22" width="0.3" layer="91" />
                <label x="132.08" y="236.22" size="1.27" layer="95" />
                <pinref part="D72" gate="G$1" pin="CATHODE" />
              </segment>
              <segment>
                <wire x1="124.46" y1="205.74" x2="127.00" y2="205.74" width="0.3" layer="91" />
                <label x="127.00" y="205.74" size="1.27" layer="95" />
                <pinref part="Q71" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$55">
              <segment>
                <wire x1="228.60" y1="281.94" x2="226.06" y2="281.94" width="0.3" layer="91" />
                <label x="226.06" y="281.94" size="1.27" layer="95" />
                <pinref part="D73" gate="G$1" pin="CATHODE" />
              </segment>
              <segment>
                <wire x1="236.22" y1="299.72" x2="238.76" y2="299.72" width="0.3" layer="91" />
                <label x="238.76" y="299.72" size="1.27" layer="95" />
                <pinref part="Q72" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$56">
              <segment>
                <wire x1="152.40" y1="233.68" x2="149.86" y2="233.68" width="0.3" layer="91" />
                <label x="149.86" y="233.68" size="1.27" layer="95" />
                <pinref part="D74" gate="G$1" pin="ANODE" />
              </segment>
              <segment>
                <wire x1="147.32" y1="220.98" x2="144.78" y2="220.98" width="0.3" layer="91" />
                <label x="144.78" y="220.98" size="1.27" layer="95" />
                <pinref part="R707" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$57">
              <segment>
                <wire x1="152.40" y1="243.84" x2="149.86" y2="243.84" width="0.3" layer="91" />
                <label x="149.86" y="243.84" size="1.27" layer="95" />
                <pinref part="D74" gate="G$1" pin="CATHODE" />
              </segment>
              <segment>
                <wire x1="177.80" y1="243.84" x2="180.34" y2="243.84" width="0.3" layer="91" />
                <label x="180.34" y="243.84" size="1.27" layer="95" />
                <pinref part="Q73" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$58">
              <segment>
                <wire x1="193.04" y1="274.32" x2="190.50" y2="274.32" width="0.3" layer="91" />
                <label x="190.50" y="274.32" size="1.27" layer="95" />
                <pinref part="D75" gate="G$1" pin="CATHODE" />
              </segment>
              <segment>
                <wire x1="177.80" y1="266.70" x2="180.34" y2="266.70" width="0.3" layer="91" />
                <label x="180.34" y="266.70" size="1.27" layer="95" />
                <pinref part="Q75" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$59">
              <segment>
                <wire x1="193.04" y1="264.16" x2="190.50" y2="264.16" width="0.3" layer="91" />
                <label x="190.50" y="264.16" size="1.27" layer="95" />
                <pinref part="D75" gate="G$1" pin="ANODE" />
              </segment>
              <segment>
                <wire x1="187.96" y1="251.46" x2="185.42" y2="251.46" width="0.3" layer="91" />
                <label x="185.42" y="251.46" size="1.27" layer="95" />
                <pinref part="R710" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$60">
              <segment>
                <wire x1="76.20" y1="180.34" x2="73.66" y2="180.34" width="0.3" layer="91" />
                <label x="73.66" y="180.34" size="1.27" layer="95" />
                <pinref part="D76" gate="G$1" pin="CATHODE" />
              </segment>
              <segment>
                <wire x1="83.82" y1="198.12" x2="86.36" y2="198.12" width="0.3" layer="91" />
                <label x="86.36" y="198.12" size="1.27" layer="95" />
                <pinref part="Q74" gate="G$1" pin="C" />
              </segment>
            </net>
            <net name="N$61">
              <segment>
                <wire x1="76.20" y1="170.18" x2="73.66" y2="170.18" width="0.3" layer="91" />
                <label x="73.66" y="170.18" size="1.27" layer="95" />
                <pinref part="D76" gate="G$1" pin="ANODE" />
              </segment>
              <segment>
                <wire x1="63.50" y1="165.10" x2="66.04" y2="165.10" width="0.3" layer="91" />
                <label x="66.04" y="165.10" size="1.27" layer="95" />
                <pinref part="R712" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$62">
              <segment>
                <wire x1="109.22" y1="198.12" x2="106.68" y2="198.12" width="0.3" layer="91" />
                <label x="106.68" y="198.12" size="1.27" layer="95" />
                <pinref part="Q71" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="55.88" y1="76.20" x2="58.42" y2="76.20" width="0.3" layer="91" />
                <label x="58.42" y="76.20" size="1.27" layer="95" />
                <pinref part="R702" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="38.10" y1="83.82" x2="40.64" y2="83.82" width="0.3" layer="91" />
                <label x="40.64" y="83.82" size="1.27" layer="95" />
                <pinref part="R701" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$63">
              <segment>
                <wire x1="220.98" y1="292.10" x2="218.44" y2="292.10" width="0.3" layer="91" />
                <label x="218.44" y="292.10" size="1.27" layer="95" />
                <pinref part="Q72" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="215.90" y1="266.70" x2="218.44" y2="266.70" width="0.3" layer="91" />
                <label x="218.44" y="266.70" size="1.27" layer="95" />
                <pinref part="R705" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="215.90" y1="259.08" x2="218.44" y2="259.08" width="0.3" layer="91" />
                <label x="218.44" y="259.08" size="1.27" layer="95" />
                <pinref part="R706" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$64">
              <segment>
                <wire x1="162.56" y1="236.22" x2="160.02" y2="236.22" width="0.3" layer="91" />
                <label x="160.02" y="236.22" size="1.27" layer="95" />
                <pinref part="Q73" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="45.72" y1="157.48" x2="48.26" y2="157.48" width="0.3" layer="91" />
                <label x="48.26" y="157.48" size="1.27" layer="95" />
                <pinref part="R708" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="63.50" y1="157.48" x2="66.04" y2="157.48" width="0.3" layer="91" />
                <label x="66.04" y="157.48" size="1.27" layer="95" />
                <pinref part="R709" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$65">
              <segment>
                <wire x1="68.58" y1="190.50" x2="66.04" y2="190.50" width="0.3" layer="91" />
                <label x="66.04" y="190.50" size="1.27" layer="95" />
                <pinref part="Q74" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="38.10" y1="149.86" x2="40.64" y2="149.86" width="0.3" layer="91" />
                <label x="40.64" y="149.86" size="1.27" layer="95" />
                <pinref part="R711" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$66">
              <segment>
                <wire x1="162.56" y1="259.08" x2="160.02" y2="259.08" width="0.3" layer="91" />
                <label x="160.02" y="259.08" size="1.27" layer="95" />
                <pinref part="Q75" gate="G$1" pin="B" />
              </segment>
              <segment>
                <wire x1="104.14" y1="182.88" x2="106.68" y2="182.88" width="0.3" layer="91" />
                <label x="106.68" y="182.88" size="1.27" layer="95" />
                <pinref part="R714" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="104.14" y1="190.50" x2="106.68" y2="190.50" width="0.3" layer="91" />
                <label x="106.68" y="190.50" size="1.27" layer="95" />
                <pinref part="R715" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$67">
              <segment>
                <wire x1="276.86" y1="325.12" x2="279.40" y2="325.12" width="0.3" layer="91" />
                <label x="279.40" y="325.12" size="1.27" layer="95" />
                <pinref part="Q77" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="274.32" y1="302.26" x2="276.86" y2="302.26" width="0.3" layer="91" />
                <label x="276.86" y="302.26" size="1.27" layer="95" />
                <pinref part="R725" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$68">
              <segment>
                <wire x1="27.94" y1="83.82" x2="25.40" y2="83.82" width="0.3" layer="91" />
                <label x="25.40" y="83.82" size="1.27" layer="95" />
                <pinref part="R701" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="12.70" y1="25.40" x2="10.16" y2="25.40" width="0.3" layer="91" />
                <label x="10.16" y="25.40" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-9" />
              </segment>
              <segment>
                <wire x1="205.74" y1="266.70" x2="203.20" y2="266.70" width="0.3" layer="91" />
                <label x="203.20" y="266.70" size="1.27" layer="95" />
                <pinref part="R705" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$69">
              <segment>
                <wire x1="246.38" y1="302.26" x2="243.84" y2="302.26" width="0.3" layer="91" />
                <label x="243.84" y="302.26" size="1.27" layer="95" />
                <pinref part="R703" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="12.70" y1="17.78" x2="10.16" y2="17.78" width="0.3" layer="91" />
                <label x="10.16" y="17.78" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-12" />
              </segment>
              <segment>
                <wire x1="157.48" y1="220.98" x2="160.02" y2="220.98" width="0.3" layer="91" />
                <label x="160.02" y="220.98" size="1.27" layer="95" />
                <pinref part="R707" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="53.34" y1="165.10" x2="50.80" y2="165.10" width="0.3" layer="91" />
                <label x="50.80" y="165.10" size="1.27" layer="95" />
                <pinref part="R712" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="198.12" y1="251.46" x2="200.66" y2="251.46" width="0.3" layer="91" />
                <label x="200.66" y="251.46" size="1.27" layer="95" />
                <pinref part="R710" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="121.92" y1="213.36" x2="124.46" y2="213.36" width="0.3" layer="91" />
                <label x="124.46" y="213.36" size="1.27" layer="95" />
                <pinref part="R704" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="205.74" y1="259.08" x2="203.20" y2="259.08" width="0.3" layer="91" />
                <label x="203.20" y="259.08" size="1.27" layer="95" />
                <pinref part="R706" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$70">
              <segment>
                <wire x1="53.34" y1="157.48" x2="50.80" y2="157.48" width="0.3" layer="91" />
                <label x="50.80" y="157.48" size="1.27" layer="95" />
                <pinref part="R709" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="12.70" y1="30.48" x2="10.16" y2="30.48" width="0.3" layer="91" />
                <label x="10.16" y="30.48" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-7" />
              </segment>
            </net>
            <net name="N$71">
              <segment>
                <wire x1="27.94" y1="149.86" x2="25.40" y2="149.86" width="0.3" layer="91" />
                <label x="25.40" y="149.86" size="1.27" layer="95" />
                <pinref part="R711" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="38.10" y1="30.48" x2="35.56" y2="30.48" width="0.3" layer="91" />
                <label x="35.56" y="30.48" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-7" />
              </segment>
            </net>
            <net name="N$72">
              <segment>
                <wire x1="93.98" y1="190.50" x2="91.44" y2="190.50" width="0.3" layer="91" />
                <label x="91.44" y="190.50" size="1.27" layer="95" />
                <pinref part="R715" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="12.70" y1="22.86" x2="10.16" y2="22.86" width="0.3" layer="91" />
                <label x="10.16" y="22.86" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-10" />
              </segment>
            </net>
            <net name="N$73">
              <segment>
                <wire x1="38.10" y1="22.86" x2="35.56" y2="22.86" width="0.3" layer="91" />
                <label x="35.56" y="22.86" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-10" />
              </segment>
              <segment>
                <wire x1="289.56" y1="281.94" x2="289.56" y2="284.48" width="0.3" layer="91" />
                <label x="289.56" y="284.48" size="1.27" layer="95" />
                <pinref part="VR11" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$74">
              <segment>
                <wire x1="12.70" y1="27.94" x2="10.16" y2="27.94" width="0.3" layer="91" />
                <label x="10.16" y="27.94" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-8" />
              </segment>
              <segment>
                <wire x1="312.42" y1="276.86" x2="314.96" y2="276.86" width="0.3" layer="91" />
                <label x="314.96" y="276.86" size="1.27" layer="95" />
                <pinref part="VR10" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="307.34" y1="281.94" x2="307.34" y2="284.48" width="0.3" layer="91" />
                <label x="307.34" y="284.48" size="1.27" layer="95" />
                <pinref part="VR10" gate="G$1" pin="3" />
              </segment>
            </net>
            <net name="N$75">
              <segment>
                <wire x1="38.10" y1="17.78" x2="35.56" y2="17.78" width="0.3" layer="91" />
                <label x="35.56" y="17.78" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-12" />
              </segment>
              <segment>
                <wire x1="289.56" y1="271.78" x2="289.56" y2="269.24" width="0.3" layer="91" />
                <label x="289.56" y="269.24" size="1.27" layer="95" />
                <pinref part="VR11" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$76">
              <segment>
                <wire x1="38.10" y1="20.32" x2="35.56" y2="20.32" width="0.3" layer="91" />
                <label x="35.56" y="20.32" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="3-11" />
              </segment>
              <segment>
                <wire x1="294.64" y1="276.86" x2="297.18" y2="276.86" width="0.3" layer="91" />
                <label x="297.18" y="276.86" size="1.27" layer="95" />
                <pinref part="VR11" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$77">
              <segment>
                <wire x1="12.70" y1="33.02" x2="10.16" y2="33.02" width="0.3" layer="91" />
                <label x="10.16" y="33.02" size="1.27" layer="95" />
                <pinref part="JFront" gate="G$1" pin="1-6" />
              </segment>
              <segment>
                <wire x1="307.34" y1="271.78" x2="307.34" y2="269.24" width="0.3" layer="91" />
                <label x="307.34" y="269.24" size="1.27" layer="95" />
                <pinref part="VR10" gate="G$1" pin="1" />
              </segment>
            </net>
          </nets>
        </sheet>
      </sheets>
      <errors />
    </schematic>
  </drawing>
  <compatibility />
</eagle>
