# SMD LED
## LTST-C191KGKT
### Optoelecctronics > Light Sources and Emitters > LEDs > LEDs (Discrete)
***

### Summary
LED GREEN CLEAR THIN 0603 SMD

#### General Description
- Meet ROHS, Green Product.
- Super Thin (0.55H mm) Chip LED.
- Ultra Bright AlInGaP Chip LED.
- Package In 8mm Tape On 7" Diameter Reels.
- EIA STD package.
- I.C. compatible.
- Compatible With Automatic Placement Equipment.
- Compatible With Infrared And Vapor Phase Reflow Solder Process.

### Connectors 
- ***Anode* [StdPin]:** Anode for LED 
- ***Cathode* [StdPin]:** Cathode for LED


