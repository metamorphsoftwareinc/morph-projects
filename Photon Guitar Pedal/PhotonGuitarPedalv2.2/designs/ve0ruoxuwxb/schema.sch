<?xml version="1.0"?>
<eagle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="6.5.0" xmlns="eagle">
  <compatibility />
  <drawing>
    <settings>
      <setting alwaysvectorfont="no" />
      <setting />
    </settings>
    <grid distance="0.01" unitdist="inch" unit="inch" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch" />
    <layers>
      <layer number="1" name="Top" color="4" fill="1" visible="no" active="no" />
      <layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no" />
      <layer number="17" name="Pads" color="2" fill="1" visible="no" active="no" />
      <layer number="18" name="Vias" color="2" fill="1" visible="no" active="no" />
      <layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no" />
      <layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no" />
      <layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no" />
      <layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no" />
      <layer number="25" name="tNames" color="7" fill="1" visible="no" active="no" />
      <layer number="26" name="bNames" color="7" fill="1" visible="no" active="no" />
      <layer number="27" name="tValues" color="7" fill="1" visible="no" active="no" />
      <layer number="28" name="bValues" color="7" fill="1" visible="no" active="no" />
      <layer number="29" name="tStop" color="7" fill="3" visible="no" active="no" />
      <layer number="30" name="bStop" color="7" fill="6" visible="no" active="no" />
      <layer number="31" name="tCream" color="7" fill="4" visible="no" active="no" />
      <layer number="32" name="bCream" color="7" fill="5" visible="no" active="no" />
      <layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no" />
      <layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no" />
      <layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no" />
      <layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no" />
      <layer number="37" name="tTest" color="7" fill="1" visible="no" active="no" />
      <layer number="38" name="bTest" color="7" fill="1" visible="no" active="no" />
      <layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no" />
      <layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no" />
      <layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no" />
      <layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no" />
      <layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no" />
      <layer number="44" name="Drills" color="7" fill="1" visible="no" active="no" />
      <layer number="45" name="Holes" color="7" fill="1" visible="no" active="no" />
      <layer number="46" name="Milling" color="3" fill="1" visible="no" active="no" />
      <layer number="47" name="Measures" color="7" fill="1" visible="no" active="no" />
      <layer number="48" name="Document" color="7" fill="1" visible="no" active="no" />
      <layer number="49" name="Reference" color="7" fill="1" visible="no" active="no" />
      <layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no" />
      <layer number="91" name="Nets" color="2" fill="1" />
      <layer number="92" name="Busses" color="1" fill="1" />
      <layer number="93" name="Pins" color="2" fill="1" visible="no" />
      <layer number="94" name="Symbols" color="4" fill="1" />
      <layer number="95" name="Names" color="7" fill="1" />
      <layer number="96" name="Values" color="7" fill="1" />
      <layer number="97" name="Info" color="7" fill="1" />
      <layer number="98" name="Guide" color="6" fill="1" />
    </layers>
    <schematic xrefpart="/%S.%C%R" xreflabel="%F%N/%S.%C%R">
      <description />
      <libraries>
        <library name="mlcc">
          <description />
          <packages>
            <package name="C_1206">
              <description>&lt;B&gt;1206&lt;/B&gt; (3216 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.0762" layer="51" />
              <wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.0762" layer="51" />
              <wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.0762" layer="51" />
              <wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.0762" layer="51" />
              <wire x1="-0.635" y1="0.9144" x2="0.635" y2="0.9144" width="0.1524" layer="21" />
              <wire x1="0.635" y1="-0.9144" x2="-0.635" y2="-0.9144" width="0.1524" layer="21" />
              <smd name="1" x="-1.6" y="0" dx="1.35" dy="1.9" layer="1" />
              <smd name="2" x="1.6" y="0" dx="1.35" dy="1.9" layer="1" />
              <text x="-2.2" y="1.2" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0603">
              <description>&lt;B&gt; 0603&lt;/B&gt; (1608 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="-0.1016" y1="-0.5334" x2="0.1016" y2="-0.5334" width="0.1524" layer="21" />
              <wire x1="-0.1016" y1="0.5334" x2="0.1016" y2="0.5334" width="0.1524" layer="21" />
              <smd name="1" x="-0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <smd name="2" x="0.9" y="0" dx="1.15" dy="1.1" layer="1" />
              <text x="-1.6" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="C_0402">
              <description>&lt;B&gt; 0402&lt;/B&gt; (1005 Metric) MLCC Capacitor &lt;P&gt;</description>
              <wire x1="-0.5" y1="0.25" x2="0.5" y2="0.25" width="0.0762" layer="51" />
              <wire x1="0.5" y1="0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="0.5" y2="-0.25" width="0.0762" layer="51" />
              <wire x1="-0.5" y1="-0.25" x2="-0.5" y2="0.25" width="0.0762" layer="51" />
              <smd name="1" x="-0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <smd name="2" x="0.5" y="0" dx="0.72" dy="0.72" layer="1" />
              <text x="-1" y="0.6" size="0.508" layer="51" font="vector" ratio="15">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="CAP_NP">
              <description>&lt;B&gt;Capacitor&lt;/B&gt; -- non-polarized</description>
              <wire x1="-1.905" y1="-3.175" x2="0" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="0" y1="-3.175" x2="1.905" y2="-3.175" width="0.6096" layer="94" />
              <wire x1="-1.905" y1="-4.445" x2="0" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-4.445" x2="1.905" y2="-4.445" width="0.6096" layer="94" />
              <wire x1="0" y1="-2.54" x2="0" y2="-3.175" width="0.254" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-4.445" width="0.254" layer="94" />
              <pin name="P$1" x="0" y="0" visible="off" length="short" direction="pas" rot="R270" />
              <pin name="P$2" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90" />
              <text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
              <text x="-5.08" y="-7.62" size="1.778" layer="95" rot="R90">&gt;NAME</text>
              <text x="0.508" y="-2.286" size="1.778" layer="95">1</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="C" name="C_1206">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_1206">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0603">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset prefix="C" name="C_0402">
              <description />
              <gates>
                <gate name="G$1" symbol="CAP_NP" x="0" y="0" />
              </gates>
              <devices>
                <device package="C_0402">
                  <connects>
                    <connect gate="G$1" pin="P$1" pad="1" />
                    <connect gate="G$1" pin="P$2" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="General_Will">
          <description />
          <packages>
            <package name="SOD123">
              <description />
              <smd name="P$1" x="0" y="1.635" dx="0.88" dy="1.02" layer="1" rot="R90" />
              <smd name="P$2" x="0" y="-1.635" dx="0.88" dy="1.02" layer="1" rot="R90" />
              <wire x1="-0.9" y1="0.9" x2="-0.9" y2="-0.9" width="0.15239999999999998" layer="21" />
              <wire x1="0.9" y1="0.9" x2="0.9" y2="-0.9" width="0.15239999999999998" layer="21" />
              <circle x="-0.88" y="1.655" radius="0.15239999999999998" width="0" layer="21" />
              <text x="-1.21" y="-2.14" size="1.016" layer="25" font="vector" ratio="15" rot="R90" distance="40">&gt;NAME</text>
            </package>
            <package name="TO-18">
              <description />
              <pad name="P$1" x="-1.27" y="0" drill="0.65" />
              <pad name="P$2" x="0" y="1.27" drill="0.65" />
              <pad name="P$3" x="1.27" y="0" drill="0.65" />
              <circle x="0" y="0" radius="2.92" width="0.127" layer="21" />
              <circle x="-3.175" y="-1.27" radius="0.1524" width="0" layer="21" />
              <text x="-3.175" y="3.175" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
            </package>
            <package name="SWITCHCRAFT_RN">
              <description />
              <pad name="RING" x="7.62" y="-6.35" drill="0.41" diameter="1.27" />
              <pad name="SLEEVE" x="7.62" y="0" drill="0.41" diameter="1.27" />
              <pad name="RING_SHUNT" x="-5.08" y="-3.81" drill="0.41" diameter="1.27" />
              <pad name="TIP_SHUNT" x="0" y="3.81" drill="0.41" diameter="1.27" />
              <pad name="TIP" x="-5.08" y="6.35" drill="0.41" diameter="1.27" />
              <hole x="5.08" y="3.81" drill="2.41" />
              <hole x="5.08" y="-3.81" drill="2.41" />
              <hole x="-7.62" y="0" drill="2.41" />
              <wire x1="10.57" y1="7.91" x2="10.57" y2="-7.91" width="0.127" layer="21" />
              <wire x1="10.57" y1="-7.91" x2="-10.57" y2="-7.91" width="0.127" layer="21" />
              <wire x1="-10.57" y1="-7.91" x2="-10.57" y2="7.91" width="0.127" layer="21" />
              <wire x1="-10.57" y1="7.91" x2="10.57" y2="7.91" width="0.127" layer="21" />
              <text x="-3.9" y="8.55" size="1.27" layer="25">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="DIODE">
              <description />
              <wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94" />
              <text x="-5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
              <text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
              <polygon layer="94" width="0.254">
                <vertex x="0" y="0" />
                <vertex x="-1.27" y="2.54" />
                <vertex x="1.27" y="2.54" />
              </polygon>
              <pin name="A" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270" />
              <pin name="C" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90" />
            </symbol>
            <symbol name="NFET">
              <description />
              <wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" />
              <wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.254" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="-2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94" />
              <wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94" />
              <wire x1="2.54" y1="-2.54" x2="2.54" y2="-7.62" width="0.254" layer="94" />
              <wire x1="2.54" y1="2.54" x2="2.54" y2="7.62" width="0.254" layer="94" />
              <pin name="G" x="-7.62" y="0" length="middle" direction="pas" />
              <pin name="D" x="2.54" y="12.7" length="middle" direction="pas" rot="R270" />
              <pin name="S" x="2.54" y="-12.7" length="middle" direction="pas" rot="R90" />
              <text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
              <text x="-7.62" y="7.62" size="1.778" layer="95">&gt;VALUE</text>
              <wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.254" layer="94" />
              <wire x1="-3.81" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94" />
            </symbol>
            <symbol name="TRS_1/4&quot;">
              <description />
              <wire x1="-3.81" y1="-5.08" x2="-2.54" y2="-6.35" width="0.1524" layer="94" />
              <wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-5.08" width="0.1524" layer="94" />
              <wire x1="-1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94" />
              <wire x1="2.54" y1="5.08" x2="-5.08" y2="5.08" width="0.1524" layer="94" />
              <text x="-5.08" y="5.588" size="1.778" layer="95">&gt;NAME</text>
              <text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
              <rectangle x1="-6.35" y1="-7.62" x2="-5.08" y2="5.08" layer="94" />
              <pin name="RING" x="5.08" y="-5.08" visible="off" length="short" rot="R180" />
              <pin name="SLEEVE" x="5.08" y="5.08" visible="off" length="short" rot="R180" />
              <pin name="TIP" x="5.08" y="0" visible="off" length="short" rot="R180" />
              <wire x1="-1.27" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" />
              <wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.1524" layer="94" />
              <wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.1524" layer="94" />
              <wire x1="2.54" y1="0" x2="3.048" y2="-1.27" width="0.1524" layer="94" />
              <wire x1="3.048" y1="-1.27" x2="2.032" y2="-1.27" width="0.1524" layer="94" />
              <wire x1="2.032" y1="-1.27" x2="2.54" y2="0" width="0.1524" layer="94" />
              <wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94" />
              <wire x1="2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94" />
              <wire x1="2.54" y1="-5.08" x2="2.54" y2="-7.62" width="0.1524" layer="94" />
              <wire x1="2.54" y1="-7.62" x2="5.08" y2="-7.62" width="0.1524" layer="94" />
              <wire x1="2.54" y1="-5.08" x2="3.048" y2="-6.35" width="0.1524" layer="94" />
              <wire x1="3.048" y1="-6.35" x2="2.032" y2="-6.35" width="0.1524" layer="94" />
              <wire x1="2.032" y1="-6.35" x2="2.54" y2="-5.08" width="0.1524" layer="94" />
              <pin name="TIP-SHUNT" x="5.08" y="-2.54" visible="off" length="short" rot="R180" />
              <pin name="RING-SHUNT" x="5.08" y="-7.62" visible="off" length="short" rot="R180" />
              <text x="0" y="0" size="1.27" layer="94" align="bottom-center">T</text>
              <text x="-2.54" y="-5.08" size="1.27" layer="94" align="bottom-center">R</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset name="MMSD4148">
              <description />
              <gates>
                <gate name="G$1" symbol="DIODE" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOD123">
                  <connects>
                    <connect gate="G$1" pin="A" pad="P$2" />
                    <connect gate="G$1" pin="C" pad="P$1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="2N4338">
              <description />
              <gates>
                <gate name="G$1" symbol="NFET" x="0" y="0" />
              </gates>
              <devices>
                <device package="TO-18">
                  <connects>
                    <connect gate="G$1" pin="D" pad="P$2" />
                    <connect gate="G$1" pin="G" pad="P$3" />
                    <connect gate="G$1" pin="S" pad="P$1" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
            <deviceset name="RN112APC">
              <description />
              <gates>
                <gate name="G$1" symbol="TRS_1/4&quot;" x="0" y="0" />
              </gates>
              <devices>
                <device package="SWITCHCRAFT_RN">
                  <connects>
                    <connect gate="G$1" pin="RING" pad="RING" />
                    <connect gate="G$1" pin="RING-SHUNT" pad="RING_SHUNT" />
                    <connect gate="G$1" pin="SLEEVE" pad="SLEEVE" />
                    <connect gate="G$1" pin="TIP" pad="TIP" />
                    <connect gate="G$1" pin="TIP-SHUNT" pad="TIP_SHUNT" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="resistor">
          <description />
          <packages>
            <package name="R_0603">
              <description>&lt;B&gt;
0603
&lt;/B&gt; SMT inch-code chip resistor package&lt;P&gt;
Derived from dimensions and tolerances
in the &lt;B&gt; &lt;A HREF="http://www.samsungsem.com/global/support/library/product-catalog/__icsFiles/afieldfile/2015/01/12/CHIP_RESISTOR_150112_1.pdf"&gt;Samsung Thick Film Chip Resistor Catalog&lt;/A&gt;&lt;/B&gt;
dated December 2014,
for 
general-purpose chip resistor
reflow soldering.</description>
              <wire x1="-0.1" y1="0.3" x2="0.1" y2="0.3" width="0.1524" layer="21" />
              <wire x1="-0.1" y1="-0.3" x2="0.1" y2="-0.3" width="0.1524" layer="21" />
              <smd name="P$1" x="-0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <smd name="P$2" x="0.8" y="0" dx="0.8" dy="0.8" layer="1" />
              <text x="-1.3" y="0.8" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <wire x1="-0.8" y1="0.4" x2="0.8" y2="0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="0.8" y1="-0.4" x2="-0.8" y2="-0.4" width="0.0762" layer="51" />
              <wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.0762" layer="51" />
            </package>
          </packages>
          <symbols>
            <symbol name="R">
              <description>&lt;B&gt;Resistor&lt;/B&gt;</description>
              <wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94" />
              <wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94" />
              <wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94" />
              <wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94" />
              <wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94" />
              <pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" />
              <pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180" />
              <text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
              <text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="R" name="RESISTOR_0603">
              <description />
              <gates>
                <gate name="G$1" symbol="R" x="0" y="0" />
              </gates>
              <devices>
                <device package="R_0603">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="ecad(1)">
          <description />
          <packages>
            <package name="SOT23-5">
              <description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
              <wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.15239999999999998" layer="51" />
              <wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.15239999999999998" layer="51" />
              <wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.15239999999999998" layer="51" />
              <wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.15239999999999998" layer="51" />
              <smd name="1" x="-0.95" y="-1.35" dx="0.6" dy="1.05" layer="1" stop="no" />
              <smd name="2" x="0" y="-1.35" dx="0.6" dy="1.05" layer="1" stop="no" />
              <smd name="3" x="0.95" y="-1.35" dx="0.6" dy="1.05" layer="1" stop="no" />
              <smd name="4" x="0.95" y="1.35" dx="0.6" dy="1.05" layer="1" stop="no" />
              <smd name="5" x="-0.95" y="1.35" dx="0.6" dy="1.05" layer="1" stop="no" />
              <text x="-2.159" y="2.159" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
              <rectangle x1="-1.32" y1="0.755" x2="-0.58" y2="1.945" layer="29" />
              <rectangle x1="0.58" y1="0.755" x2="1.32" y2="1.945" layer="29" />
              <rectangle x1="-1.32" y1="-1.945" x2="-0.58" y2="-0.755" layer="29" />
              <rectangle x1="0.58" y1="-1.945" x2="1.32" y2="-0.755" layer="29" />
              <rectangle x1="-0.37" y1="-1.945" x2="0.37" y2="-0.755" layer="29" />
              <circle x="-1.8" y="-1.8" radius="0.15239999999999998" width="0" layer="21" />
            </package>
          </packages>
          <symbols>
            <symbol name="OP-AMP+-">
              <description />
              <wire x1="-1.27" y1="-3.175" x2="-1.27" y2="-1.905" width="0.1524" layer="94" />
              <wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="94" />
              <wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="94" />
              <wire x1="0" y1="-5.08" x2="0" y2="-3.8862" width="0.1524" layer="94" />
              <wire x1="0" y1="3.9116" x2="0" y2="5.08" width="0.1524" layer="94" />
              <wire x1="7.62" y1="0" x2="-2.54" y2="-5.08" width="0.4064" layer="94" />
              <wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.4064" layer="94" />
              <wire x1="-2.54" y1="5.08" x2="7.62" y2="0" width="0.4064" layer="94" />
              <pin name="+IN" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" />
              <pin name="-IN" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" />
              <pin name="OUT" x="10.16" y="0" visible="pad" length="short" direction="pas" rot="R180" />
              <pin name="V+" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270" />
              <pin name="V-" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90" />
              <text x="5.08" y="-5.715" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
              <text x="5.08" y="5.08" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
              <text x="1.27" y="5.715" size="1.778" layer="93" rot="MR270">V+</text>
              <text x="1.27" y="-4.445" size="1.778" layer="93" rot="MR270">V-</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="U" name="OPA344">
              <description />
              <gates>
                <gate name="G$1" symbol="OP-AMP+-" x="0" y="0" />
              </gates>
              <devices>
                <device package="SOT23-5">
                  <connects>
                    <connect gate="G$1" pin="+IN" pad="3" />
                    <connect gate="G$1" pin="-IN" pad="4" />
                    <connect gate="G$1" pin="OUT" pad="1" />
                    <connect gate="G$1" pin="V+" pad="5" />
                    <connect gate="G$1" pin="V-" pad="2" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
        <library name="photon_v1.0.0">
          <description />
          <packages>
            <package name="0.1INCH-1X12HEADER">
              <description />
              <pad name="P$1" x="0" y="13.97" drill="1" shape="octagon" />
              <pad name="P$2" x="0" y="11.43" drill="1" />
              <pad name="P$3" x="0" y="8.89" drill="1" />
              <pad name="P$4" x="0" y="6.35" drill="1" />
              <pad name="P$5" x="0" y="3.81" drill="1" />
              <pad name="P$6" x="0" y="1.27" drill="1" />
              <pad name="P$7" x="0" y="-1.27" drill="1" />
              <pad name="P$8" x="0" y="-3.81" drill="1" />
              <pad name="P$9" x="0" y="-6.35" drill="1" />
              <pad name="P$10" x="0" y="-8.89" drill="1" />
              <pad name="P$11" x="0" y="-11.43" drill="1" />
              <pad name="P$12" x="0" y="-13.97" drill="1" />
              <wire x1="-1.27" y1="15.24" x2="-1.27" y2="-15.24" width="0.127" layer="21" />
              <wire x1="-1.27" y1="-15.24" x2="1.27" y2="-15.24" width="0.127" layer="21" />
              <wire x1="1.27" y1="-15.24" x2="1.27" y2="15.24" width="0.127" layer="21" />
              <wire x1="1.27" y1="15.24" x2="-1.27" y2="15.24" width="0.127" layer="21" />
              <text x="1.74" y="13.895" size="1.27" layer="25">&gt;NAME</text>
            </package>
          </packages>
          <symbols>
            <symbol name="SPARK_M12">
              <description />
              <wire x1="6.35" y1="-17.78" x2="0" y2="-17.78" width="0.4064" layer="94" />
              <wire x1="3.81" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94" />
              <wire x1="3.81" y1="-12.7" x2="5.08" y2="-12.7" width="0.6096" layer="94" />
              <wire x1="3.81" y1="-15.24" x2="5.08" y2="-15.24" width="0.6096" layer="94" />
              <wire x1="0" y1="15.24" x2="0" y2="-17.78" width="0.4064" layer="94" />
              <wire x1="6.35" y1="-17.78" x2="6.35" y2="15.24" width="0.4064" layer="94" />
              <wire x1="0" y1="15.24" x2="6.35" y2="15.24" width="0.4064" layer="94" />
              <wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94" />
              <wire x1="3.81" y1="-7.62" x2="5.08" y2="-7.62" width="0.6096" layer="94" />
              <wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.6096" layer="94" />
              <wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94" />
              <wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94" />
              <wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94" />
              <wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94" />
              <wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94" />
              <wire x1="3.81" y1="12.7" x2="5.08" y2="12.7" width="0.6096" layer="94" />
              <pin name="1" x="10.16" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="2" x="10.16" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="3" x="10.16" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="4" x="10.16" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="5" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="6" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="7" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="8" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="9" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="10" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="11" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <pin name="12" x="10.16" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180" />
              <text x="0" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
              <text x="0" y="16.002" size="1.778" layer="95">&gt;NAME</text>
            </symbol>
          </symbols>
          <devicesets>
            <deviceset prefix="JP" name="1-535541-0">
              <description />
              <gates>
                <gate name="G$1" symbol="SPARK_M12" x="0" y="0" />
              </gates>
              <devices>
                <device package="0.1INCH-1X12HEADER" name="PTH-CST-L">
                  <connects>
                    <connect gate="G$1" pin="1" pad="P$1" />
                    <connect gate="G$1" pin="10" pad="P$10" />
                    <connect gate="G$1" pin="11" pad="P$11" />
                    <connect gate="G$1" pin="12" pad="P$12" />
                    <connect gate="G$1" pin="2" pad="P$2" />
                    <connect gate="G$1" pin="3" pad="P$3" />
                    <connect gate="G$1" pin="4" pad="P$4" />
                    <connect gate="G$1" pin="5" pad="P$5" />
                    <connect gate="G$1" pin="6" pad="P$6" />
                    <connect gate="G$1" pin="7" pad="P$7" />
                    <connect gate="G$1" pin="8" pad="P$8" />
                    <connect gate="G$1" pin="9" pad="P$9" />
                  </connects>
                  <technologies>
                    <technology name="" />
                  </technologies>
                </device>
              </devices>
            </deviceset>
          </devicesets>
        </library>
      </libraries>
      <attributes />
      <variantdefs />
      <classes>
        <class number="0" name="default" />
      </classes>
      <parts>
        <part device="" value="GRM31CD80G107ME39L" name="C1" library="mlcc" deviceset="C_1206" />
        <part device="" value="06036D105KAT2A" name="C2" library="mlcc" deviceset="C_0603" />
        <part device="" value="GRM31CD80G107ME39L" name="C2$1" library="mlcc" deviceset="C_1206" />
        <part device="" value="UT02ZD223MAT2D" name="C3" library="mlcc" deviceset="C_0402" />
        <part device="" value="06036D105KAT2A" name="C4" library="mlcc" deviceset="C_0603" />
        <part device="" value="MMSD4148" name="D1" library="General_Will" deviceset="MMSD4148" />
        <part device="" value="2N4338-E3" name="J1" library="General_Will" deviceset="2N4338" />
        <part device="" value="MCT0603MD4702BP100" name="R1" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MCT0603MD4702BP100" name="R2" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="RCG06031M00FKEA" name="R3" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="CRCW06033K00FKEA" name="R4" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="CRCW0603270KFKEA" name="R5" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MCT0603MD4702BP100" name="R6" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MCT0603MD4702BP100" name="R7" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MCT0603MD4702BP100" name="R8" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="MCT0603MD4702BP100" name="R9" library="resistor" deviceset="RESISTOR_0603" />
        <part device="" value="OPA344UA" name="U1" library="ecad(1)" deviceset="OPA344" />
        <part device="PTH-CST-L" value="1-535541-0" name="JP1" library="photon_v1.0.0" deviceset="1-535541-0" />
        <part device="PTH-CST-L" value="1-535541-0" name="JP2" library="photon_v1.0.0" deviceset="1-535541-0" />
        <part device="" value="RN112APC" name="RN112APC" library="General_Will" deviceset="RN112APC" />
      </parts>
      <sheets>
        <sheet>
          <description />
          <plain />
          <instances>
            <instance y="60.96" part="C1" gate="G$1" x="170.18" />
            <instance y="142.24" part="C2" gate="G$1" x="254.00" />
            <instance y="60.96" part="C2$1" gate="G$1" x="162.56" />
            <instance y="86.36" part="C3" gate="G$1" x="187.96" />
            <instance y="149.86" part="C4" gate="G$1" x="261.62" />
            <instance y="149.86" part="D1" gate="G$1" x="269.24" />
            <instance y="93.98" part="J1" gate="G$1" x="200.66" />
            <instance y="48.26" part="R1" gate="G$1" x="167.64" />
            <instance y="73.66" part="R2" gate="G$1" x="175.26" />
            <instance y="73.66" part="R3" gate="G$1" x="193.04" />
            <instance y="121.92" part="R4" gate="G$1" x="223.52" />
            <instance y="129.54" part="R5" gate="G$1" x="259.08" />
            <instance y="114.30" part="R6" gate="G$1" x="223.52" />
            <instance y="137.16" part="R7" gate="G$1" x="266.70" />
            <instance y="121.92" part="R8" gate="G$1" x="241.30" />
            <instance y="129.54" part="R9" gate="G$1" x="241.30" />
            <instance y="121.92" part="U1" gate="G$1" x="203.20" />
            <instance y="91.44" part="JP1" gate="G$1" x="91.44" />
            <instance y="91.44" part="JP2" gate="G$1" x="109.22" />
            <instance y="22.86" part="RN112APC" gate="G$1" x="127.00" />
          </instances>
          <busses />
          <nets>
            <net name="N$0">
              <segment>
                <wire x1="170.18" y1="53.34" x2="170.18" y2="50.80" width="0.3" layer="91" />
                <label x="170.18" y="50.80" size="1.27" layer="95" />
                <pinref part="C1" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="119.38" y1="96.52" x2="121.92" y2="96.52" width="0.3" layer="91" />
                <label x="121.92" y="96.52" size="1.27" layer="95" />
                <pinref part="JP2" gate="G$1" pin="9" />
              </segment>
              <segment>
                <wire x1="101.60" y1="78.74" x2="104.14" y2="78.74" width="0.3" layer="91" />
                <label x="104.14" y="78.74" size="1.27" layer="95" />
                <pinref part="JP1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="132.08" y1="27.94" x2="134.62" y2="27.94" width="0.3" layer="91" />
                <label x="134.62" y="27.94" size="1.27" layer="95" />
                <pinref part="RN112APC" gate="G$1" pin="SLEEVE" />
              </segment>
              <segment>
                <wire x1="162.56" y1="60.96" x2="162.56" y2="63.50" width="0.3" layer="91" />
                <label x="162.56" y="63.50" size="1.27" layer="95" />
                <pinref part="C2$1" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="162.56" y1="48.26" x2="160.02" y2="48.26" width="0.3" layer="91" />
                <label x="160.02" y="48.26" size="1.27" layer="95" />
                <pinref part="R1" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="203.20" y1="114.30" x2="203.20" y2="111.76" width="0.3" layer="91" />
                <label x="203.20" y="111.76" size="1.27" layer="95" />
                <pinref part="U1" gate="G$1" pin="V-" />
              </segment>
            </net>
            <net name="N$1">
              <segment>
                <wire x1="170.18" y1="60.96" x2="170.18" y2="63.50" width="0.3" layer="91" />
                <label x="170.18" y="63.50" size="1.27" layer="95" />
                <pinref part="C1" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="180.34" y1="73.66" x2="182.88" y2="73.66" width="0.3" layer="91" />
                <label x="182.88" y="73.66" size="1.27" layer="95" />
                <pinref part="R2" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="203.20" y1="129.54" x2="203.20" y2="132.08" width="0.3" layer="91" />
                <label x="203.20" y="132.08" size="1.27" layer="95" />
                <pinref part="U1" gate="G$1" pin="V+" />
              </segment>
              <segment>
                <wire x1="119.38" y1="104.14" x2="121.92" y2="104.14" width="0.3" layer="91" />
                <label x="121.92" y="104.14" size="1.27" layer="95" />
                <pinref part="JP2" gate="G$1" pin="12" />
              </segment>
            </net>
            <net name="N$2">
              <segment>
                <wire x1="254.00" y1="134.62" x2="254.00" y2="132.08" width="0.3" layer="91" />
                <label x="254.00" y="132.08" size="1.27" layer="95" />
                <pinref part="C2" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="271.78" y1="137.16" x2="274.32" y2="137.16" width="0.3" layer="91" />
                <label x="274.32" y="137.16" size="1.27" layer="95" />
                <pinref part="R7" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$3">
              <segment>
                <wire x1="254.00" y1="142.24" x2="254.00" y2="144.78" width="0.3" layer="91" />
                <label x="254.00" y="144.78" size="1.27" layer="95" />
                <pinref part="C2" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="187.96" y1="73.66" x2="185.42" y2="73.66" width="0.3" layer="91" />
                <label x="185.42" y="73.66" size="1.27" layer="95" />
                <pinref part="R3" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="162.56" y1="53.34" x2="162.56" y2="50.80" width="0.3" layer="91" />
                <label x="162.56" y="50.80" size="1.27" layer="95" />
                <pinref part="C2$1" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="172.72" y1="48.26" x2="175.26" y2="48.26" width="0.3" layer="91" />
                <label x="175.26" y="48.26" size="1.27" layer="95" />
                <pinref part="R1" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="170.18" y1="73.66" x2="167.64" y2="73.66" width="0.3" layer="91" />
                <label x="167.64" y="73.66" size="1.27" layer="95" />
                <pinref part="R2" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="203.20" y1="106.68" x2="203.20" y2="109.22" width="0.3" layer="91" />
                <label x="203.20" y="109.22" size="1.27" layer="95" />
                <pinref part="J1" gate="G$1" pin="D" />
              </segment>
              <segment>
                <wire x1="218.44" y1="114.30" x2="215.90" y2="114.30" width="0.3" layer="91" />
                <label x="215.90" y="114.30" size="1.27" layer="95" />
                <pinref part="R6" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$4">
              <segment>
                <wire x1="187.96" y1="86.36" x2="187.96" y2="88.90" width="0.3" layer="91" />
                <label x="187.96" y="88.90" size="1.27" layer="95" />
                <pinref part="C3" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="132.08" y1="17.78" x2="134.62" y2="17.78" width="0.3" layer="91" />
                <label x="134.62" y="17.78" size="1.27" layer="95" />
                <pinref part="RN112APC" gate="G$1" pin="RING" />
              </segment>
              <segment>
                <wire x1="132.08" y1="15.24" x2="134.62" y2="15.24" width="0.3" layer="91" />
                <label x="134.62" y="15.24" size="1.27" layer="95" />
                <pinref part="RN112APC" gate="G$1" pin="RING-SHUNT" />
              </segment>
              <segment>
                <wire x1="132.08" y1="20.32" x2="134.62" y2="20.32" width="0.3" layer="91" />
                <label x="134.62" y="20.32" size="1.27" layer="95" />
                <pinref part="RN112APC" gate="G$1" pin="TIP-SHUNT" />
              </segment>
              <segment>
                <wire x1="132.08" y1="22.86" x2="134.62" y2="22.86" width="0.3" layer="91" />
                <label x="134.62" y="22.86" size="1.27" layer="95" />
                <pinref part="RN112APC" gate="G$1" pin="TIP" />
              </segment>
            </net>
            <net name="N$5">
              <segment>
                <wire x1="187.96" y1="78.74" x2="187.96" y2="76.20" width="0.3" layer="91" />
                <label x="187.96" y="76.20" size="1.27" layer="95" />
                <pinref part="C3" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="198.12" y1="73.66" x2="200.66" y2="73.66" width="0.3" layer="91" />
                <label x="200.66" y="73.66" size="1.27" layer="95" />
                <pinref part="R3" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="198.12" y1="119.38" x2="195.58" y2="119.38" width="0.3" layer="91" />
                <label x="195.58" y="119.38" size="1.27" layer="95" />
                <pinref part="U1" gate="G$1" pin="+IN" />
              </segment>
            </net>
            <net name="N$6">
              <segment>
                <wire x1="261.62" y1="142.24" x2="261.62" y2="139.70" width="0.3" layer="91" />
                <label x="261.62" y="139.70" size="1.27" layer="95" />
                <pinref part="C4" gate="G$1" pin="P$2" />
              </segment>
              <segment>
                <wire x1="246.38" y1="121.92" x2="248.92" y2="121.92" width="0.3" layer="91" />
                <label x="248.92" y="121.92" size="1.27" layer="95" />
                <pinref part="R8" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="269.24" y1="154.94" x2="269.24" y2="157.48" width="0.3" layer="91" />
                <label x="269.24" y="157.48" size="1.27" layer="95" />
                <pinref part="D1" gate="G$1" pin="A" />
              </segment>
            </net>
            <net name="N$7">
              <segment>
                <wire x1="261.62" y1="149.86" x2="261.62" y2="152.40" width="0.3" layer="91" />
                <label x="261.62" y="152.40" size="1.27" layer="95" />
                <pinref part="C4" gate="G$1" pin="P$1" />
              </segment>
              <segment>
                <wire x1="246.38" y1="129.54" x2="248.92" y2="129.54" width="0.3" layer="91" />
                <label x="248.92" y="129.54" size="1.27" layer="95" />
                <pinref part="R9" gate="G$1" pin="2" />
              </segment>
            </net>
            <net name="N$8">
              <segment>
                <wire x1="269.24" y1="147.32" x2="269.24" y2="144.78" width="0.3" layer="91" />
                <label x="269.24" y="144.78" size="1.27" layer="95" />
                <pinref part="D1" gate="G$1" pin="C" />
              </segment>
              <segment>
                <wire x1="101.60" y1="104.14" x2="104.14" y2="104.14" width="0.3" layer="91" />
                <label x="104.14" y="104.14" size="1.27" layer="95" />
                <pinref part="JP1" gate="G$1" pin="12" />
              </segment>
              <segment>
                <wire x1="264.16" y1="129.54" x2="266.70" y2="129.54" width="0.3" layer="91" />
                <label x="266.70" y="129.54" size="1.27" layer="95" />
                <pinref part="R5" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="213.36" y1="121.92" x2="215.90" y2="121.92" width="0.3" layer="91" />
                <label x="215.90" y="121.92" size="1.27" layer="95" />
                <pinref part="U1" gate="G$1" pin="OUT" />
              </segment>
            </net>
            <net name="N$9">
              <segment>
                <wire x1="203.20" y1="81.28" x2="203.20" y2="78.74" width="0.3" layer="91" />
                <label x="203.20" y="78.74" size="1.27" layer="95" />
                <pinref part="J1" gate="G$1" pin="S" />
              </segment>
              <segment>
                <wire x1="218.44" y1="121.92" x2="215.90" y2="121.92" width="0.3" layer="91" />
                <label x="215.90" y="121.92" size="1.27" layer="95" />
                <pinref part="R4" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$10">
              <segment>
                <wire x1="193.04" y1="93.98" x2="190.50" y2="93.98" width="0.3" layer="91" />
                <label x="190.50" y="93.98" size="1.27" layer="95" />
                <pinref part="J1" gate="G$1" pin="G" />
              </segment>
              <segment>
                <wire x1="236.22" y1="121.92" x2="233.68" y2="121.92" width="0.3" layer="91" />
                <label x="233.68" y="121.92" size="1.27" layer="95" />
                <pinref part="R8" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="236.22" y1="129.54" x2="233.68" y2="129.54" width="0.3" layer="91" />
                <label x="233.68" y="129.54" size="1.27" layer="95" />
                <pinref part="R9" gate="G$1" pin="1" />
              </segment>
            </net>
            <net name="N$11">
              <segment>
                <wire x1="228.60" y1="121.92" x2="231.14" y2="121.92" width="0.3" layer="91" />
                <label x="231.14" y="121.92" size="1.27" layer="95" />
                <pinref part="R4" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="254.00" y1="129.54" x2="251.46" y2="129.54" width="0.3" layer="91" />
                <label x="251.46" y="129.54" size="1.27" layer="95" />
                <pinref part="R5" gate="G$1" pin="1" />
              </segment>
              <segment>
                <wire x1="198.12" y1="124.46" x2="195.58" y2="124.46" width="0.3" layer="91" />
                <label x="195.58" y="124.46" size="1.27" layer="95" />
                <pinref part="U1" gate="G$1" pin="-IN" />
              </segment>
            </net>
            <net name="N$12">
              <segment>
                <wire x1="228.60" y1="114.30" x2="231.14" y2="114.30" width="0.3" layer="91" />
                <label x="231.14" y="114.30" size="1.27" layer="95" />
                <pinref part="R6" gate="G$1" pin="2" />
              </segment>
              <segment>
                <wire x1="261.62" y1="137.16" x2="259.08" y2="137.16" width="0.3" layer="91" />
                <label x="259.08" y="137.16" size="1.27" layer="95" />
                <pinref part="R7" gate="G$1" pin="1" />
              </segment>
            </net>
          </nets>
        </sheet>
      </sheets>
      <errors />
    </schematic>
  </drawing>
  <compatibility />
</eagle>
